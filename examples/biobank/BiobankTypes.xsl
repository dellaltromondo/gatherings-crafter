<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="dataClass[ @name = 'KitPackage' ]">
    <xsl:param name="parent" />
    <xsl:param name="valueLabel" />
    <xsl:attribute name="class">
      <xsl:value-of select="@name" />
    </xsl:attribute>
    <span>
      <xsl:attribute name="data-ng-init">myStatus = {}; myStatus.isCollapsed = (thisObject.deliveryDate &amp;&amp; thisObject.deliveryDate != '')</xsl:attribute>
    </span>
    <div class="expanded" data-ng-if="!myStatus.isCollapsed">
      <button type="button" data-ng-click="myStatus.isCollapsed = true">-</button>
      <xsl:apply-templates select="./variable">
        <xsl:with-param name="parent" select="$parent" />
        <xsl:with-param name="valueLabel" select="$valueLabel" />
      </xsl:apply-templates>
    </div>
    <div class="collapsed" data-ng-if="myStatus.isCollapsed">
      <button type="button" data-ng-click="myStatus.isCollapsed = false">+</button>
      <xsl:apply-templates select="./variable[ @name = 'deliveryDate' ]">
        <xsl:with-param name="parent" select="$parent" />
        <xsl:with-param name="valueLabel" select="$valueLabel" />
      </xsl:apply-templates>
    </div>
  </xsl:template>
  
</xsl:stylesheet>