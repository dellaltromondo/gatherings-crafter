﻿<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:import href="../../dist/xslt/Utils.xsl"/>
  <xsl:import href="../../dist/xslt/CollectionVisibility.xsl"/>
  <xsl:import href="../../dist/xslt/CollectionBaseTypes.xsl"/>
  <xsl:import href="../../dist/xslt/CollectionDataClassTypes.xsl"/>
  <xsl:import href="BiobankTypes.xsl"/>

  <xsl:output method="html" indent="yes"/>
  
</xsl:stylesheet>
