/// <reference path='../../src/definitions/angular/angular.d.ts' />

/// <reference path='../../src/controllers/AbstractController.ts' />
/// <reference path='../../src/managers/CRFVersionsManager.ts' />
/// <reference path='../../src/services/JSONService.ts' />
/// <reference path='../../src/utils/GlobalUtils.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class BioBankController extends AbstractController {
        public $inject = ['$q', '$http', '$scope', '$timeout', 'jsonService', 'crfServiceURL', 'dataType'];

        private versionManager: CRFVersionsManager;

        constructor(
            protected $q: ng.IQService,
            protected $http: ng.IHttpService,
            protected $scope: IStructuralDataModel,
            protected $timeout: ng.ITimeoutService,
            protected jsonService: IJSONService<any>,
            crfServiceURL: string,
            dataType: string
        ) {

            super($q, $scope, $timeout, jsonService);
            this.versionManager = new CRFVersionsManager(crfServiceURL, dataType, null, $http);
            this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);

            this.loadJsonData();
        }

        //TODO: to be removed, must find a way to use the one defined in DataClassDirective Logic which is in a separate scope
        addElementToSet(currentSet: any[]): void {
            currentSet.push({ "guid": GuidUtils.createGUID() });
        }

        onKeyPress($event, currentSet: any[]): void {
            if ($event.which === 13) {
                var lastKit = currentSet.length;
                var packages = this.$scope.crfs.kitsPackages;
                for (var j = 0; j < packages.length; j++) {
                    if (angular.equals(currentSet, packages[j].kits)) {
                        this.addElementToSet(currentSet);
                        var packageIndex = j;
                        this.$timeout(
                            () => {
                                document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.kits" + lastKit + "\\.code")[0].focus();
                            }, 200, true);
                    } else if (angular.equals(currentSet, packages[j].liquorKits)) {
                        this.addElementToSet(currentSet);
                        var packageIndex = j;
                        this.$timeout(
                            () => {
                                document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.liquorKits" + lastKit + "\\.code")[0].focus();
                            }, 200, true);
                    } else if (angular.equals(currentSet, packages[j].microdialysisKits)) {
                        this.addElementToSet(currentSet);
                        var packageIndex = j;
                        this.$timeout(
                            () => {
                                document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.microdialysisKits" + lastKit + "\\.code")[0].focus();
                            }, 200, true);
                    } else if (angular.equals(currentSet, packages[j].plasmaKits)) {
                        this.addElementToSet(currentSet);
                        var packageIndex = j;
                        this.$timeout(
                            () => {
                                document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.plasmaKits" + lastKit + "\\.code")[0].focus();
                            }, 200, true);
                    }
                }
                $event.stopPropagation();
                $event.preventDefault();
            }
        }

    }
}