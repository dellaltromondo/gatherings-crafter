/// <reference path='../../src/definitions/angular/angular.d.ts' />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path='../../src/controllers/AbstractController.ts' />
/// <reference path='../../src/managers/CRFVersionsManager.ts' />
/// <reference path='../../src/services/JSONService.ts' />
/// <reference path='../../src/utils/GlobalUtils.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var BioBankController = (function (_super) {
                __extends(BioBankController, _super);
                function BioBankController($q, $http, $scope, $timeout, jsonService, crfServiceURL, dataType) {
                    _super.call(this, $q, $scope, $timeout, jsonService);
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.jsonService = jsonService;
                    this.$inject = ['$q', '$http', '$scope', '$timeout', 'jsonService', 'crfServiceURL', 'dataType'];
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, null, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    this.loadJsonData();
                }
                //TODO: to be removed, must find a way to use the one defined in DataClassDirective Logic which is in a separate scope
                BioBankController.prototype.addElementToSet = function (currentSet) {
                    currentSet.push({ "guid": givitiweb.GuidUtils.createGUID() });
                };
                BioBankController.prototype.onKeyPress = function ($event, currentSet) {
                    if ($event.which === 13) {
                        var lastKit = currentSet.length;
                        var packages = this.$scope.crfs.kitsPackages;
                        for (var j = 0; j < packages.length; j++) {
                            if (angular.equals(currentSet, packages[j].kits)) {
                                this.addElementToSet(currentSet);
                                var packageIndex = j;
                                this.$timeout(function () {
                                    document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.kits" + lastKit + "\\.code")[0].focus();
                                }, 200, true);
                            }
                            else if (angular.equals(currentSet, packages[j].liquorKits)) {
                                this.addElementToSet(currentSet);
                                var packageIndex = j;
                                this.$timeout(function () {
                                    document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.liquorKits" + lastKit + "\\.code")[0].focus();
                                }, 200, true);
                            }
                            else if (angular.equals(currentSet, packages[j].microdialysisKits)) {
                                this.addElementToSet(currentSet);
                                var packageIndex = j;
                                this.$timeout(function () {
                                    document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.microdialysisKits" + lastKit + "\\.code")[0].focus();
                                }, 200, true);
                            }
                            else if (angular.equals(currentSet, packages[j].plasmaKits)) {
                                this.addElementToSet(currentSet);
                                var packageIndex = j;
                                this.$timeout(function () {
                                    document.querySelector("#crfs\\.kitsPackages" + packageIndex + "\\.plasmaKits" + lastKit + "\\.code")[0].focus();
                                }, 200, true);
                            }
                        }
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                };
                return BioBankController;
            })(givitiweb.AbstractController);
            givitiweb.BioBankController = BioBankController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
