/// <reference path='../../src/definitions/angular/angular.d.ts' />
/// <reference path='../../src/definitions/angular/angular-translate.d.ts' />

/// <reference path='../../src/applications/Application.ts' />
/// <reference path='../../src/services/ProfileService.ts' />
/// <reference path="BioBankController.ts" />

module it.marionegri.givitiweb {
    'use strict';

    export class BioBankApplication extends CollectionBaseApplication {

        constructor() {
            super();
                
            this.angularApp.factory('profileService', ($q, $http) => ProfileService.factory($q, $http, eval("profileURL")));
            this.angularApp.controller('BioBankController', BioBankController);
        }

        static create() {
            return new BioBankApplication();
        }
    }
}