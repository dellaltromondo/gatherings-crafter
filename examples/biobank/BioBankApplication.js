/// <reference path='../../src/definitions/angular/angular.d.ts' />
/// <reference path='../../src/definitions/angular/angular-translate.d.ts' />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path='../../src/applications/Application.ts' />
/// <reference path='../../src/services/ProfileService.ts' />
/// <reference path="BioBankController.ts" />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var BioBankApplication = (function (_super) {
                __extends(BioBankApplication, _super);
                function BioBankApplication() {
                    _super.call(this);
                    this.angularApp.factory('profileService', function ($q, $http) { return givitiweb.ProfileService.factory($q, $http, eval("profileURL")); });
                    this.angularApp.controller('BioBankController', givitiweb.BioBankController);
                }
                BioBankApplication.create = function () {
                    return new BioBankApplication();
                };
                return BioBankApplication;
            })(givitiweb.CollectionBaseApplication);
            givitiweb.BioBankApplication = BioBankApplication;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
