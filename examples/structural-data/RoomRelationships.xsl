﻿<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable[ @dataType = 'relationships' ]">
    <xsl:param name="parent" />

    <xsl:variable name="relationsElements" select="@relationsElements"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <xsl:variable name="apos">'</xsl:variable>
    <fieldset class="roomsRelationships">
      <!-- 
          <xsl:value-of select="concat('{{ ', $relationsElements, ' }}')" />
        -->
      <xsl:attribute name="data-ng-controller">
        <xsl:value-of select="'RoomsRelationsController'" />
      </xsl:attribute>
      <xsl:attribute name="data-ng-if">
        <xsl:value-of select="concat($relationsElements, '.length &gt; 1')" />
      </xsl:attribute>
      <xsl:apply-templates select="@visible" />
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
      <xsl:call-template name="addDescription">
        <xsl:with-param name="element" select="'legend'" />
      </xsl:call-template>
      <div>
        <ul class="checkboxlist">
          <xsl:attribute name="data-ng-init">
            <xsl:value-of select="concat($fullName, ' = ', $fullName, ' || {}; ', 'model = ', $fullName)" />
          </xsl:attribute>
          <li>
            <xsl:attribute name="data-ng-repeat">
              <xsl:value-of select="concat('childElement in ', $relationsElements, ' track by childElement.guid')"/>
            </xsl:attribute>
            <xsl:attribute name="data-ng-show">
              <xsl:value-of select="concat('childElement.name &amp;&amp; childElement.name != ', $apos, $apos)" />
            </xsl:attribute>
            <xsl:variable name="parentName" select="'{{ childElement.name }}'" />
            <span class="radioDescription">
              <xsl:value-of select="$parentName"/>
            </span>
            <xsl:call-template name="relationsChild">
              <xsl:with-param name="relationsElementsChilds" select="$relationsElements"/>
              <xsl:with-param name="fullName" select="$fullName"/>
            </xsl:call-template>
          </li>
        </ul>
      </div>
    </fieldset>
  </xsl:template>

  <xsl:template name="relationsChild">
    <xsl:param name="relationsElementsChilds" />
    <xsl:param name="fullName" />
    <xsl:variable name="apos">'</xsl:variable>
    <ul>
      <li>
        <xsl:attribute name="data-ng-repeat">
          <xsl:value-of select="concat('secondChildElement in ', $relationsElementsChilds, ' track by secondChildElement.guid')"/>
        </xsl:attribute>
        <xsl:attribute name="data-ng-if" >
          <xsl:value-of select="concat('(secondChildElement.guid != childElement.guid) &amp;&amp; secondChildElement.name &amp;&amp; (secondChildElement.name != ', $apos, $apos, ')')"/>
        </xsl:attribute>
        <input>
          <xsl:attribute name="data-ng-click">
            <xsl:value-of select="concat('vm.modifyRelationship(', $fullName, ', childElement.guid, secondChildElement.guid)')" />
          </xsl:attribute>
          <xsl:attribute name="data-ng-checked">
            <xsl:value-of select="concat('vm.verifyRelationship(', $fullName, ', childElement.guid, secondChildElement.guid)')" />
          </xsl:attribute>
          <xsl:attribute name="type">
            <xsl:value-of select="'checkbox'"/>
          </xsl:attribute>
        </input>
        <label>
          <xsl:value-of select="'{{ secondChildElement.name }}'"/>
        </label>
      </li>
    </ul>
  </xsl:template>
  
</xsl:stylesheet>
