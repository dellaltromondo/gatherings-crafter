/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.StructuralDataCleaner', ['ngSanitize']);
            //var dataStorageURL: string = "https://givitiweb.marionegri.it/Services/DataStorageWebService.svc/";
            var dataStorageURL = "http://episerver/givitiweb/services/dataStorageWebService.svc/";
            // Storage
            var guidStorage = new givitiweb.NaiveGUIDStorage();
            var listGUIDStorage = new givitiweb.GUIDStorage("centreStructuralData", null, "centre", dataStorageURL);
            // Non angular inject
            angularApp.value('guidStorage', guidStorage);
            angularApp.value('listGUIDStorage', listGUIDStorage);
            // Services
            angularApp.factory('jsonService', function ($q) { return givitiweb.JSONService.factory($q, guidStorage, "centreStructuralData", dataStorageURL, null); });
            //angularApp.factory('listJSONService', ($q: ng.IQService) => { return JSONService.factory($q, listGUIDStorage, eval("dataType"), eval("dataStorageURL")) });
            // Controllers
            angularApp.controller('StructuralDataCleanerController', givitiweb.StructuralDataCleanerController);
            //Filters
            angularApp.filter('formatDate', function () { return function (date) { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
