/// <reference path='../../src/definitions/angular/angular.d.ts' />
/// <reference path='../../src/definitions/angular/angular-translate.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    var angularApp: ng.IModule = angular.module('it.marionegri.givitiweb.StructuralData', ['ngSanitize', 'gantt', 'pascalprecht.translate']);

    // Storage
    var translationStorage: TranslationStorage<any> = new TranslationStorage(eval("translationURL"));
    
    // Non angular inject
    angularApp.value('crfServiceURL', eval("crfServiceURL"));
    angularApp.value('dataType', eval("dataType"));
    angularApp.value('dataName', eval("dataName")); //centrecode
    angularApp.value('dataKey', eval("dataKey")); //CY001
    angularApp.value('readOnly', eval("readOnly"));

    // Services
    angularApp.factory('mongoService', ($q: ng.IQService) => { return MongoService.factory($q, eval("dataType"), eval("mongoStorageURL")) });
    angularApp.factory('profileService', ($q: ng.IQService) => { return ProfileService.factory($q, eval("profileURL")) });
    angularApp.factory('translationLoader', ($q: ng.IQService) => {
        return (options) => {
            var deferred: ng.IDeferred<any> = $q.defer();
            translationStorage.getTranslationsValues(options.application, options.block, options.key)
                .done((data) => deferred.resolve(data))
                .fail(() => deferred.reject(options.key));
            return deferred.promise;
        };
    });
    // Controllers
    angularApp.controller('StructuralDataController', StructuralDataController);
    angularApp.controller('MyObjectController', ObjectController);
    angularApp.controller('RoomsRelationsController', RelationsController);

    //Directives
    angularApp.directive('datePicker', DatePickerDirective.factory);
    angularApp.directive('confirmationNeeded', AlertDirective.factory);
    angularApp.directive('ic3DataCollection', DataCollectionDirective.factory);
    angularApp.directive('ic3DataClass', DataClassDirective.factory);

    //Filters
    angularApp.filter('formatDate', () => { return (date: string) => { return moment(date, GlobalConfig.jsonDateFormat).format(GlobalConfig.viewDateFormat) } });

    // Configs
    angularApp.config(['$translateProvider', ($translateProvider: ngt.ITranslateProvider) => {
        var options: any = { application: eval("applicationName"), block: eval("blockName") };
        $translateProvider.useLoader('translationLoader', options)
            .preferredLanguage(eval("culture"))
            .useSanitizeValueStrategy('escape');
    }]);
}