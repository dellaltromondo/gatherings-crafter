/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export interface IStructuralDataCleanerModel extends IGenericModel {
        guidList: string[];
        cleanedGuidList: string[];
        currentGuid: string;
        structuralData: IDictionaryOf<any>;
    }

    export class StructuralDataCleanerController extends AbstractController implements IGenericController {
        public $inject = ['$q', '$scope', '$timeout', 'guidStorage', 'listGUIDStorage', 'jsonService'/*, 'listJSONService',, 'dataType', 'dataPath'*/];

        constructor(
            protected $q: ng.IQService,
            protected $scope: IStructuralDataCleanerModel,
            protected $timeout: ng.ITimeoutService,
            protected guidStorage: IGUIDStorage<string>,
            protected listGUIDStorage: GUIDStorage<string>,
            protected jsonService: IJSONService<any>
            //protected listJSONService: IJSONService<any>,
            //protected dataType: string,
            //protected dataPath: string
        ) {

            super($q, $scope, $timeout, jsonService);
            this.$scope.guidList = [];
            this.$scope.cleanedGuidList = [];
            this.$scope.currentGuid = "";
            this.$scope.structuralData = {};
            this.$scope.vm = this;

            this.loadJsonData().then(() => { var gg = "needed to evaluate loaded data" });
        }

        cleanAllCRF(): void {
            angular.forEach(this.$scope.guidList,
                ((value, key) => {
                    this.$scope.currentGuid = value;
                    //this.$scope.cleanedGuidList.push(value);
                    this.loadSingleJson(value)
                        .then((data: any) => {
                            var cleanedCrf = this.cleanCRF(data.data);
                            //var updateCleanedGuidList = () => {
                            //    console.debug("timed OUT");
                            //};
                            //this.$timeout(updateCleanedGuidList, 1000);
                            this.saveSingleJson(data.guid, cleanedCrf)
                                .then((guid: string) => {
                                    this.$scope.cleanedGuidList.push(guid);
                                });
                        });
                }),
                this);
        }

        cleanCRF(crfData: any): any {
            var newCrf = angular.copy(crfData);
            //this.removeEmptyObjectsFromSets(newCrf);
            this.clearData(crfData);
            this.removeDeadObjects(crfData, newCrf);
            this.removeEmptyObjectsFromSets(crfData);
            this.fillData(crfData, newCrf);
            return crfData
        }

        loadJsonData(): ng.IPromise<any> {
            return this.loadGUIDList();
        }

        loadGUIDList(): ng.IPromise<string[]> {
            var deferred: ng.IDeferred<string[]> = this.$q.defer();
            this.listGUIDStorage.loadGuidList().then(
                (data: string[]) => {
                    this.$scope.guidList = data;
                    deferred.resolve(data)
                },
                error => deferred.reject()
            );
            return deferred.promise;
        }

        loadSingleJson(guid: string): ng.IPromise<any> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            this.guidStorage.update(guid);
            this.jsonService.$get()
                .then((data: any) => {
                    //this.$scope.structuralData[guid] = data;
                    deferred.resolve({ "guid": guid, "data": data });
                });
            return deferred.promise;
        }

        saveSingleJson(guid: string, data: any): ng.IPromise<string> {
            var deferred: ng.IDeferred<string> = this.$q.defer();
            this.guidStorage.update(guid);
            this.jsonService.save(data)
                .then((resultGuid: string) => {
                    //this.$scope.structuralData[guid] = data;
                    deferred.resolve(resultGuid);
                });
            return deferred.promise;
        }
    }
}