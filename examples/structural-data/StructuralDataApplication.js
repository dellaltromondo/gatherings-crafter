/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.StructuralData', ['ngSanitize', 'gantt', 'pascalprecht.translate']);
            // Storage
            var translationStorage = new givitiweb.TranslationStorage(eval("translationURL"));
            // Non angular inject
            angularApp.value('crfServiceURL', eval("crfServiceURL"));
            angularApp.value('dataType', eval("dataType"));
            angularApp.value('dataName', eval("dataName")); //centrecode
            angularApp.value('dataKey', eval("dataKey")); //CY001
            angularApp.value('readOnly', eval("readOnly"));
            // Services
            angularApp.factory('mongoService', function ($q) { return givitiweb.MongoService.factory($q, eval("dataType"), eval("mongoStorageURL")); });
            angularApp.factory('profileService', function ($q) { return givitiweb.ProfileService.factory($q, eval("profileURL")); });
            angularApp.factory('translationLoader', function ($q) {
                return function (options) {
                    var deferred = $q.defer();
                    translationStorage.getTranslationsValues(options.application, options.block, options.key)
                        .done(function (data) { return deferred.resolve(data); })
                        .fail(function () { return deferred.reject(options.key); });
                    return deferred.promise;
                };
            });
            // Controllers
            angularApp.controller('StructuralDataController', givitiweb.StructuralDataController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            angularApp.controller('RoomsRelationsController', givitiweb.RelationsController);
            //Directives
            angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
            angularApp.directive('confirmationNeeded', givitiweb.AlertDirective.factory);
            angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
            angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
            //Filters
            angularApp.filter('formatDate', function () { return function (date) { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
            // Configs
            angularApp.config(['$translateProvider', function ($translateProvider) {
                    var options = { application: eval("applicationName"), block: eval("blockName") };
                    $translateProvider.useLoader('translationLoader', options)
                        .preferredLanguage(eval("culture"))
                        .useSanitizeValueStrategy('escape');
                }]);
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
