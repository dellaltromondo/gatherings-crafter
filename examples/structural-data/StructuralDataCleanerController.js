/// <reference path='../definitions/angular/angular.d.ts' />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var StructuralDataCleanerController = (function (_super) {
                __extends(StructuralDataCleanerController, _super);
                function StructuralDataCleanerController($q, $scope, $timeout, guidStorage, listGUIDStorage, jsonService) {
                    _super.call(this, $q, $scope, $timeout, jsonService);
                    this.$q = $q;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.guidStorage = guidStorage;
                    this.listGUIDStorage = listGUIDStorage;
                    this.jsonService = jsonService;
                    this.$inject = ['$q', '$scope', '$timeout', 'guidStorage', 'listGUIDStorage', 'jsonService' /*, 'listJSONService',, 'dataType', 'dataPath'*/];
                    this.$scope.guidList = [];
                    this.$scope.cleanedGuidList = [];
                    this.$scope.currentGuid = "";
                    this.$scope.structuralData = {};
                    this.$scope.vm = this;
                    this.loadJsonData().then(function () { var gg = "needed to evaluate loaded data"; });
                }
                StructuralDataCleanerController.prototype.cleanAllCRF = function () {
                    var _this = this;
                    angular.forEach(this.$scope.guidList, (function (value, key) {
                        _this.$scope.currentGuid = value;
                        //this.$scope.cleanedGuidList.push(value);
                        _this.loadSingleJson(value)
                            .then(function (data) {
                            var cleanedCrf = _this.cleanCRF(data.data);
                            //var updateCleanedGuidList = () => {
                            //    console.debug("timed OUT");
                            //};
                            //this.$timeout(updateCleanedGuidList, 1000);
                            _this.saveSingleJson(data.guid, cleanedCrf)
                                .then(function (guid) {
                                _this.$scope.cleanedGuidList.push(guid);
                            });
                        });
                    }), this);
                };
                StructuralDataCleanerController.prototype.cleanCRF = function (crfData) {
                    var newCrf = angular.copy(crfData);
                    //this.removeEmptyObjectsFromSets(newCrf);
                    this.clearData(crfData);
                    this.removeDeadObjects(crfData, newCrf);
                    this.removeEmptyObjectsFromSets(crfData);
                    this.fillData(crfData, newCrf);
                    return crfData;
                };
                StructuralDataCleanerController.prototype.loadJsonData = function () {
                    return this.loadGUIDList();
                };
                StructuralDataCleanerController.prototype.loadGUIDList = function () {
                    var _this = this;
                    var deferred = this.$q.defer();
                    this.listGUIDStorage.loadGuidList().then(function (data) {
                        _this.$scope.guidList = data;
                        deferred.resolve(data);
                    }, function (error) { return deferred.reject(); });
                    return deferred.promise;
                };
                StructuralDataCleanerController.prototype.loadSingleJson = function (guid) {
                    var deferred = this.$q.defer();
                    this.guidStorage.update(guid);
                    this.jsonService.$get()
                        .then(function (data) {
                        //this.$scope.structuralData[guid] = data;
                        deferred.resolve({ "guid": guid, "data": data });
                    });
                    return deferred.promise;
                };
                StructuralDataCleanerController.prototype.saveSingleJson = function (guid, data) {
                    var deferred = this.$q.defer();
                    this.guidStorage.update(guid);
                    this.jsonService.save(data)
                        .then(function (resultGuid) {
                        //this.$scope.structuralData[guid] = data;
                        deferred.resolve(resultGuid);
                    });
                    return deferred.promise;
                };
                return StructuralDataCleanerController;
            })(givitiweb.AbstractController);
            givitiweb.StructuralDataCleanerController = StructuralDataCleanerController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
