/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    var angularApp: ng.IModule = angular.module('it.marionegri.givitiweb.StructuralDataCleaner', ['ngSanitize']);

    //var dataStorageURL: string = "https://givitiweb.marionegri.it/Services/DataStorageWebService.svc/";
    var dataStorageURL: string = "http://episerver/givitiweb/services/dataStorageWebService.svc/";

    // Storage
    var guidStorage: IGUIDStorage<string> = new NaiveGUIDStorage();
    var listGUIDStorage: GUIDStorage<string> = new GUIDStorage("centreStructuralData", null, "centre", dataStorageURL);

    // Non angular inject
    angularApp.value('guidStorage', guidStorage);
    angularApp.value('listGUIDStorage', listGUIDStorage);

    // Services
    angularApp.factory('jsonService', ($q) => { return JSONService.factory($q, guidStorage, "centreStructuralData", dataStorageURL, null) });
    //angularApp.factory('listJSONService', ($q: ng.IQService) => { return JSONService.factory($q, listGUIDStorage, eval("dataType"), eval("dataStorageURL")) });

    // Controllers
    angularApp.controller('StructuralDataCleanerController', StructuralDataCleanerController);

    //Filters
    angularApp.filter('formatDate', () => { return (date: string) => { return moment(date, GlobalConfig.jsonDateFormat).format(GlobalConfig.viewDateFormat) } });
}