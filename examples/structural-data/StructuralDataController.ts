module it.marionegri.givitiweb {
    "use strict";

    export enum StructuralDataPageStatus {
        LOADING,
        NO_DATA,
        SHOWING_PERIODS,
        ADDING_PERIOD,
        MODIFYING_PERIOD,
        SAVING_DATA,
        DATA_LOADING_FAILED
    }

    export interface IStructuralDataStatus extends IAbstractStatus {
        pageStatus: StructuralDataPageStatus;
        periodIndex: number;
        currentColor: string;
        currentStartDate: string;
        currentEndDate: string;
        crfStartDate: string;
        crfEndDate: string;
        projectsStartDate: IDictionaryOf<string>;
        periodEndDate: string;
        centreCode: string;
        januaryPeriodFound: boolean;
        missingPeriodsError: boolean;
        dateDuplicatesFree: boolean;
        nonCompletedFormErrorList: string[];
        fieldsValidation: any;

    }

    export interface IStructuralDataModel extends IGenericModel {
        status: IStructuralDataStatus;
        selectorIndexes: string[];
        PageStatusEnum: any;
        angularForm: any;
        // TODO: Substitute ganttRow with function and cache to prevent useless recalculations
        ganttRow: any;
    }

    export class StructuralDataController extends AbstractController implements IGenericController {
        public $inject = ['$q', '$scope', '$timeout', 'mongoService', 'profileService', 'crfServiceURL', 'readOnly', 'dataType', 'dataName', 'dataKey'];

        static colorList: string[] = ['#dc77ff', '#42cccc', '#FFDD5F', '#66cc4e', '#aa6688', '#66bbee', '#fbb000', '#224477', '#33cc99', '#ff7733', '#AA66AA', '#aadd5f'];
        private centreStructuralDataList: any;
        private versionManager: CRFVersionsManager;
        constructor(
            protected $q: ng.IQService,
            protected $http: ng.IHttpService,
            public $scope: IStructuralDataModel,
            protected $timeout: ng.ITimeoutService,
            protected mongoService: IMongoService<any>,
            protected profileService: ProfileService,
            crfServiceURL: string,
            readOnly: boolean,
            private dataType: string,
            private dataName: string,
            private dataKey: string
        ) {

            super($q, $scope, $timeout, null); // SHOULD FIX
            this.$scope.status = {
                pageStatus: StructuralDataPageStatus.LOADING,
                dataReadOnly: readOnly,
                periodIndex: 0,
                currentColor: StructuralDataController.colorList[0],
                currentStartDate: null,
                currentEndDate: null,
                crfStartDate: "0001-01-01",
                crfEndDate: "2099-12-31",
                projectsStartDate: { "giviti": "0001-01-01", "start": "0001-01-01" },
                periodEndDate: null,
                centreCode: dataKey,
                januaryPeriodFound: false,
                missingPeriodsError: false,
                fieldsValidation: [],
                nonCompletedFormErrorList: [],
                dateDuplicatesFree: true,
                dataSaved: false
            };
            this.versionManager = new CRFVersionsManager(crfServiceURL, dataType, null, $http);
            this.centreStructuralDataList = {};
            this.$scope.currentCrfPath = "";
            this.$scope.selectorIndexes = [];
            this.$scope.PageStatusEnum = StructuralDataPageStatus;
            this.$scope.angularForm = null;
            this.$scope.ganttRow = [];

            this.$scope.$watch('crfs.dataValidity.startingDate',
                (newValue: string, oldValue: string) => {
                    if (newValue < this.$scope.status.projectsStartDate["giviti"]) {
                        this.$scope.crfs.dataValidity.startingDate = this.$scope.status.projectsStartDate["giviti"];
                    }
                    this.onDateChange(newValue, oldValue);
                });

            this.loadJsonData();
        }

        checkMissingStartingDate(): void {
            if (!this.$scope.crfs.dataValidity.startingDate)
                this.$scope.status.dateDuplicatesFree = false;
        }

        checkDuplicateDates(): void {
            for (var i in this.centreStructuralDataList.centreStructuralData) {
                if (i == this.$scope.status.periodIndex.toString())
                    continue
                if (this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate == this.$scope.crfs.dataValidity.startingDate) {
                    this.$scope.status.dateDuplicatesFree = false;
                    break;
                }
            }
        }

        checkNonCompletedForms(): void {
            this.$scope.status.nonCompletedFormErrorList = [];
            for (var i in this.centreStructuralDataList.centreStructuralData) {
                if (i == this.$scope.status.periodIndex.toString())
                    continue
                if (this.centreStructuralDataList.centreStructuralData[i].isFormValid == false
                    && this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate >= this.$scope.status.projectsStartDate["giviti"]) {
                    //this.$scope.status.nonCompletedFormError = true;
                    this.$scope.status.nonCompletedFormErrorList.push(this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate);
                }
            }
        }

        checkMissingJanuary(): void {
            for (var i in this.centreStructuralDataList.centreStructuralData) {
                if (this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate <= '2014-01-01'
                    || this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate <= this.$scope.status.projectsStartDate["giviti"]) {
                    this.$scope.status.januaryPeriodFound = true;
                    break;
                }
            }
        }

        checkMissingPeriods(): void {
            var sortedPeriods: string[] = angular.copy(this.$scope.selectorIndexes);
            sortedPeriods.sort();

            for (var i = 0; i < sortedPeriods.length; i++) {
                if (sortedPeriods[i] >= this.$scope.status.projectsStartDate["giviti"]) {
                    var endingDate: Moment = moment(this.calculateCurrentEndDate(sortedPeriods[i]), GlobalConfig.jsonDateFormat);

                    if (i == sortedPeriods.length - 1) {
                        if (moment().diff(endingDate, 'days') > 1) {
                            this.$scope.status.missingPeriodsError = true;
                            break;
                        }
                    } else {
                        if (moment(sortedPeriods[i + 1], GlobalConfig.jsonDateFormat).diff(endingDate, 'days') > 1) {
                            this.$scope.status.missingPeriodsError = true;
                            break;
                        }
                    }
                }
            }
        }

        onTaskClick(event: any) {
            var taskID: string = event.task.id;
            var taskDate = event.task.data.date
            var taskIndex = taskID.split('-')[1];
            this.$scope.status.periodIndex = parseInt(taskIndex);
            this.fillCRFS();
            this.checkNonCompletedForms();
        }

        onErrorClick(errorDate: string) {
            var periodIndex: number = this.$scope.selectorIndexes.length - 1;

            for (var i = 0; i < this.$scope.selectorIndexes.length; i++) {
                if (this.$scope.selectorIndexes[i] == errorDate) {
                    periodIndex = i;
                }
            }
            this.$scope.status.periodIndex = periodIndex;
            this.fillCRFS();
            this.checkNonCompletedForms();
        }

        onDateChange(newValue: string, oldValue: string) {
            if (newValue && (newValue != oldValue || !this.$scope.currentCrfPath)) {
                this.updatePeriodStatus();
            }
        }

        onBtnSaveClick(postBackButtonID: string) {
            this.saveJsonData();
        }

        onBtnRemoveClick(postBackButtonID: string) {
            this.removeCurrentPeriod();
        }

        getGanttRowInList(event: any) {
            return this.$scope.ganttRow;
        }

        generateGanttRow(): void {
            var periods: string[] = this.$scope.selectorIndexes;
            var newProject = { "id": "structuralData", "description": "Periodi", "order": 0, "tasks": [], "data": { "fullDescription": "Dati strutturali", "parentIds": [] } };
            var rowList = [];
            if (periods) {
                var idCounter = 0;
                for (var p in periods) {
                    var color = StructuralDataController.colorList[idCounter % StructuralDataController.colorList.length];
                    if (periods[p] >= this.$scope.status.projectsStartDate["giviti"]) {
                        var newTask = { "id": "structuralData", "subject": '', "color": color, "from": null, "to": null, "data": { "date": periods[p] } };
                        newTask["from"] = periods[p];
                        newTask["to"] = this.calculateCurrentEndDate(periods[p]);
                        newTask["id"] = "structuralData" + '-' + idCounter.toString();
                        newProject["tasks"].push(newTask);
                        this.checkMissingPeriods();
                    }
                    idCounter += 1;
                }
            }
            rowList.push(newProject)
            this.$scope.ganttRow = rowList;
        }

        isToday(date: string): boolean {
            if (date == moment().format(GlobalConfig.jsonDateFormat))
                return true;
            return false;
        }

        isValidDate(date: string): boolean {
            if (moment(date, GlobalConfig.jsonDateFormat).isValid())
                return true;
            return false;
        }

        getMaxIndex(array: any[]): number {
            if (array.length == 0) {
                return -1;
            } else {
                var max = array[0];
                var maxIndex = 0;
                for (var k = 0; k < array.length; k++) {
                    if (array[k] > max) {
                        maxIndex = k;
                        max = array[k];
                    }
                }
                return maxIndex;
            }
        }

        getElementIndex(array: any[], element: any): number {
            if (array.length == 0) {
                return -1;
            } else {
                var index = 0;
                for (var k = 0; k < array.length; k++) {
                    if (array[k] == element) {
                        index = k;
                        break;
                    }
                }
                return index;
            }
        }

        addNewPeriod(): void {
            this.$scope.status.pageStatus = StructuralDataPageStatus.ADDING_PERIOD;
            this.$scope.status.periodIndex = this.centreStructuralDataList.centreStructuralData.length;
            this.$scope.crfs.dataValidity.startingDate = null;
        }

        rollbackNewPeriod(): void {
            this.$scope.status.periodIndex = this.getMaxIndex(this.$scope.selectorIndexes);
            this.fillCRFS();
            this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
        }

        rollbackModifiedPeriod(): void {
            this.fillCRFS();
            this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
        }

        removeCurrentPeriod(): void {
            this.centreStructuralDataList.centreStructuralData.splice(this.$scope.status.periodIndex, 1);
            this.$scope.status.periodIndex = -1;
            this.saveJsonData();
        }

        calculateMaxValidity(currentDate: string): Duration {
            var maxValidity: Duration = moment.duration(6, 'months')

            if (this.$scope.status.projectsStartDate["start"] != "0001-01-01") {
                if (currentDate >= this.$scope.status.projectsStartDate["start"]) {
                    maxValidity = moment.duration(1, 'months')
                }
                else {
                    maxValidity = moment.duration(6, 'months')
                }
            }
            return maxValidity;
        }

        calculateCurrentEndDate(startingDate: string): string {
            var newPeriodEndDate = this.calculatePeriodEndDate(startingDate);
            var endingDate: string;

            // CurrentEndDate could not be over today
            if (moment().format(GlobalConfig.jsonDateFormat) <= newPeriodEndDate) {
                endingDate = moment().format(GlobalConfig.jsonDateFormat);
            } else {
                endingDate = newPeriodEndDate;
            }
            return endingDate;
        }

        calculatePeriodEndDate(startingDate: string): string {
            var sortedPeriods = angular.copy(this.$scope.selectorIndexes);
            sortedPeriods.sort();
            var sortedIndex: number = sortedPeriods.indexOf(startingDate) + 1;
            var crfEndDate: string;
            var validityEndDate: string;
            var endingDate: string;

            // Need To use the parameter date to calculate the correct crfEndDate
            var validity: IPeriod = this.versionManager.getCrfValidity(startingDate)
            if (validity) {
                crfEndDate = validity.endDate;
            }
            //NOTE: for year 2014 only one period is required for data validity
            if (startingDate.indexOf('2014') > -1) {
                validityEndDate = moment("2014-12-31", GlobalConfig.jsonDateFormat)
                    .format(GlobalConfig.jsonDateFormat);
            } else if (startingDate.indexOf('2015') > -1) {
                //NOTE: for year 2015 only two periods are required for data validity
                validityEndDate = moment(startingDate, GlobalConfig.jsonDateFormat)
                    .add(moment.duration(6, 'months'))
                    .format(GlobalConfig.jsonDateFormat);
            } else {
                //NOTE: if start has been activated, shorted periods are only used after that date
                validityEndDate = moment(startingDate, GlobalConfig.jsonDateFormat)
                    .add(this.calculateMaxValidity(startingDate))
                    .format(GlobalConfig.jsonDateFormat);
            }
            // for other periods the enddate is the min between next periods or maxValidity
            if (sortedPeriods.indexOf(startingDate) != -1 && sortedPeriods[sortedIndex] <= validityEndDate) {
                endingDate = moment(sortedPeriods[sortedIndex], GlobalConfig.jsonDateFormat)
                    .add(moment.duration(-1, 'days'))
                    .format(GlobalConfig.jsonDateFormat);
            }
            else {
                endingDate = validityEndDate;
            }
            if (endingDate >= crfEndDate) {
                endingDate = crfEndDate;
            }
            return endingDate;
        }

        updatePeriodStatus(): void {
            this.$scope.status.currentColor = StructuralDataController.colorList[this.$scope.status.periodIndex % StructuralDataController.colorList.length];
            // Update status relative to currentDate
            if (this.$scope.crfs.dataValidity && this.$scope.crfs.dataValidity.startingDate) {
                if (this.$scope.status.projectsStartDate["giviti"] <= this.$scope.crfs.dataValidity.startingDate) {
                    this.$scope.status.currentStartDate = this.$scope.crfs.dataValidity.startingDate;
                }
                else {
                    this.$scope.status.currentStartDate = this.$scope.status.projectsStartDate["giviti"];
                }
            }
            else {
                this.$scope.status.currentStartDate = DateUtils.getJsonDateFromMoment(moment());
            }
            this.$scope.status.currentEndDate = this.calculateCurrentEndDate(this.$scope.status.currentStartDate);
            this.$scope.status.periodEndDate = this.calculatePeriodEndDate(this.$scope.status.currentStartDate);
            // TODO: needed to clean the form required fields from old elements. To be moved inside dataCollectionDirective and binded to directive's data loaded
            this.$scope.status.dataLoaded = false;
            this.$timeout((() => { this.$scope.status.dataLoaded = true; }), 100);
            // Update status relative to currentCRF
            var version = this.versionManager.getCrfVersion(this.$scope.status.currentStartDate);
            if (version) {
                this.$scope.currentCrfPath = this.versionManager.getCrfPath(version);
                var validity: IPeriod = this.versionManager.getCrfValidity(this.$scope.status.currentStartDate)
                if (validity) {
                    this.$scope.status.crfStartDate = validity.startDate;
                    this.$scope.status.crfEndDate = validity.endDate;
                }
            }
        }

        /** Fill the centre data inside $scope and set all supporting variables */
        fillCentreStructuralData(centreData: any): void {
            this.centreStructuralDataList = {};
            this.$scope.status.pageStatus = StructuralDataPageStatus.NO_DATA;
            this.$scope.status.periodIndex = -1;
            this.$scope.selectorIndexes = [];
            var badCrfs = [];

            if (centreData.centreStructuralData && centreData.centreStructuralData.length > 0) {
                //Find all the periods with invalid startingDate
                for (var i = 0; i < centreData.centreStructuralData.length; i++) {
                    if (!centreData.centreStructuralData[i]
                        || !this.isValidDate(centreData.centreStructuralData[i].dataValidity.startingDate)) {
                        badCrfs.push(i);
                    } else {
                        this.$scope.selectorIndexes.push(centreData.centreStructuralData[i].dataValidity.startingDate);
                    }
                }
                //Remove all invalid periods from centreData
                for (var i = badCrfs.length - 1; i >= 0; i--) {
                    centreData.centreStructuralData.splice(badCrfs[i], 1);
                }
                //Bind remaining periods to $scope
                if (this.$scope.selectorIndexes.length > 0) {
                    this.centreStructuralDataList = centreData;
                }
                //Sort periods and index
                this.$scope.selectorIndexes.sort();
                this.centreStructuralDataList.centreStructuralData
                    .sort((a, b) => {
                        if (a.dataValidity.startingDate < b.dataValidity.startingDate)
                            return -1
                        if (a.dataValidity.startingDate > b.dataValidity.startingDate)
                            return 1
                        return 0
                    });
            }
        }

        /** Fill the current periodIndex data in $scope.crfs cleaning up all dead object */
        fillCRFS(): void {
            super.fillCRFS(this.centreStructuralDataList.centreStructuralData[this.$scope.status.periodIndex]);
        }

        private getCondition() {
            var result = {};
            result[this.dataName] = this.dataKey;
            return result;
        }

        loadJsonData(): ng.IPromise<any> {
            return this.$q.all([
                this.loadVersions(),
                this.loadAfference(this.$scope.status.centreCode, "giviti"),
                this.loadAfference(this.$scope.status.centreCode, "start")
            ])
                .then(() => {
                    this.loadStructuralData();
                    return;
                });
        }

        loadVersions(): ng.IPromise<any> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            this.versionManager.loadVersions().then(() => { deferred.resolve(); });
            return deferred.promise;
        }

        loadAfference(centreCode: string, projectName: string): ng.IPromise<any> {
            return this.profileService.getAfferenceStartDate(centreCode, projectName)
                .then((startDate: string) => {
                    if (startDate != "") {
                        this.$scope.status.projectsStartDate[projectName] = moment(startDate).format(GlobalConfig.jsonDateFormat);
                    }
                    else {
                        this.$scope.status.projectsStartDate[projectName] = "0001-01-01";
                    }
                },
                    (error: any) => {
                        this.$scope.status.projectsStartDate[projectName] = "0001-01-01";
                    });
        }

        loadStructuralData(): ng.IPromise<any> {
            var condition = this.getCondition();

            return this.mongoService.getDocument(condition)
                .then((centreData: any) => {
                    if (!angular.equals(centreData, {})) {
                        this.fillCentreStructuralData(centreData);
                        this.$scope.status.periodIndex = this.getMaxIndex(this.$scope.selectorIndexes);
                        this.fillCRFS();
                        this.generateGanttRow();
                        this.$scope.status.dataLoaded = true;
                        this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
                    }
                    else {
                        this.$scope.status.pageStatus = StructuralDataPageStatus.NO_DATA;
                    }
                    this.updatePeriodStatus();
                    this.checkMissingJanuary();
                    this.checkNonCompletedForms();
                },
                    (error: any) => {
                        this.$scope.status.dataLoaded = false;
                        this.$scope.status.pageStatus = StructuralDataPageStatus.DATA_LOADING_FAILED;
                    });
        }

        saveJsonData(): ng.IPromise<any> {
            var promise: ng.IPromise<any>;
            this.checkMissingStartingDate();
            this.checkDuplicateDates();

            if (this.$scope.status.dateDuplicatesFree) {
                promise = this.saveCentreStructuralData()
                    .then(() => {
                        this.$scope.status.dataModified = false;
                        this.$scope.status.dataSaved = true;
                        var changeVariableStatus = () => {
                            this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
                        };
                        this.$timeout(changeVariableStatus, 4000);
                    });
            }
            return promise;
        }

        saveCentreStructuralData(): ng.IPromise<any> {
            this.$scope.crfs.isFormValid = this.$scope.angularForm.$valid;
            this.$scope.crfs.dataValidity.endingDate = this.$scope.status.periodEndDate;
            var condition = this.getCondition();

            if (!this.centreStructuralDataList.centreStructuralData) {
                this.centreStructuralDataList.centreStructuralData = [];
            }
            if (!this.centreStructuralDataList[this.dataName]) {
                this.centreStructuralDataList[this.dataName] = this.dataKey;
            }

            if (this.$scope.status.periodIndex == this.centreStructuralDataList.centreStructuralData.length) {
                this.centreStructuralDataList.centreStructuralData.push(angular.copy(this.$scope.crfs));
            } else if (this.$scope.status.periodIndex != -1) {
                this.centreStructuralDataList.centreStructuralData[this.$scope.status.periodIndex] = angular.copy(this.$scope.crfs);
            }
            this.$scope.status.pageStatus = StructuralDataPageStatus.SAVING_DATA;

            return this.mongoService.setDocument(condition, this.centreStructuralDataList)
                .then(() => {
                    return this.loadJsonData();
                });
        }

        //OLD: code using jsonService
        /*loadStructuralData(): ng.IPromise<any> {
            return this.jsonService.$get()
                .then((centreData: any) => {
                    if (!angular.equals(centreData, {})
                        && !angular.equals(centreData, { centreStructuralData: [] })) {

                        this.fillCentreStructuralData(centreData);
                        this.$scope.status.periodIndex = this.getMaxIndex(this.$scope.selectorIndexes);
                        this.fillCRFS();
                        this.generateGanttRow();
                        this.$scope.status.dataLoaded = true;
                        this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
                    }
                    else {
                        this.$scope.status.pageStatus = StructuralDataPageStatus.NO_DATA;
                    }
                    this.updatePeriodStatus();
                    this.checkMissingJanuary();
                    this.checkNonCompletedForms();
                },
                (error: any) => {
                    this.$scope.status.dataLoaded = false;
                    this.$scope.status.pageStatus = StructuralDataPageStatus.DATA_LOADING_FAILED;
                });
        }

        saveCentreStructuralData(): ng.IPromise<any> {
            this.$scope.crfs.isFormValid = this.$scope.angularForm.$valid;

            if (this.centreStructuralDataList.centreStructuralData === undefined) {
                this.centreStructuralDataList = { centreStructuralData: [] };
            }
            if (this.$scope.status.periodIndex == this.centreStructuralDataList.centreStructuralData.length) {
                this.centreStructuralDataList.centreStructuralData.push(angular.copy(this.$scope.crfs));
            } else if (this.$scope.status.periodIndex != -1) {
                this.centreStructuralDataList.centreStructuralData[this.$scope.status.periodIndex] = angular.copy(this.$scope.crfs);
            }
            this.$scope.status.pageStatus = StructuralDataPageStatus.SAVING_DATA;

            return this.jsonService.save(this.centreStructuralDataList)
                .then(() => {
                    return this.loadJsonData();
                });
        }*/
    }
}