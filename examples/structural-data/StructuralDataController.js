var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            "use strict";
            (function (StructuralDataPageStatus) {
                StructuralDataPageStatus[StructuralDataPageStatus["LOADING"] = 0] = "LOADING";
                StructuralDataPageStatus[StructuralDataPageStatus["NO_DATA"] = 1] = "NO_DATA";
                StructuralDataPageStatus[StructuralDataPageStatus["SHOWING_PERIODS"] = 2] = "SHOWING_PERIODS";
                StructuralDataPageStatus[StructuralDataPageStatus["ADDING_PERIOD"] = 3] = "ADDING_PERIOD";
                StructuralDataPageStatus[StructuralDataPageStatus["MODIFYING_PERIOD"] = 4] = "MODIFYING_PERIOD";
                StructuralDataPageStatus[StructuralDataPageStatus["SAVING_DATA"] = 5] = "SAVING_DATA";
                StructuralDataPageStatus[StructuralDataPageStatus["DATA_LOADING_FAILED"] = 6] = "DATA_LOADING_FAILED";
            })(givitiweb.StructuralDataPageStatus || (givitiweb.StructuralDataPageStatus = {}));
            var StructuralDataPageStatus = givitiweb.StructuralDataPageStatus;
            var StructuralDataController = (function (_super) {
                __extends(StructuralDataController, _super);
                function StructuralDataController($q, $http, $scope, $timeout, mongoService, profileService, crfServiceURL, readOnly, dataType, dataName, dataKey) {
                    var _this = this;
                    _super.call(this, $q, $scope, $timeout, null); // SHOULD FIX
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.mongoService = mongoService;
                    this.profileService = profileService;
                    this.dataType = dataType;
                    this.dataName = dataName;
                    this.dataKey = dataKey;
                    this.$inject = ['$q', '$scope', '$timeout', 'mongoService', 'profileService', 'crfServiceURL', 'readOnly', 'dataType', 'dataName', 'dataKey'];
                    this.$scope.status = {
                        pageStatus: StructuralDataPageStatus.LOADING,
                        dataReadOnly: readOnly,
                        periodIndex: 0,
                        currentColor: StructuralDataController.colorList[0],
                        currentStartDate: null,
                        currentEndDate: null,
                        crfStartDate: "0001-01-01",
                        crfEndDate: "2099-12-31",
                        projectsStartDate: { "giviti": "0001-01-01", "start": "0001-01-01" },
                        periodEndDate: null,
                        centreCode: dataKey,
                        januaryPeriodFound: false,
                        missingPeriodsError: false,
                        fieldsValidation: [],
                        nonCompletedFormErrorList: [],
                        dateDuplicatesFree: true,
                        dataSaved: false
                    };
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, null, $http);
                    this.centreStructuralDataList = {};
                    this.$scope.currentCrfPath = "";
                    this.$scope.selectorIndexes = [];
                    this.$scope.PageStatusEnum = StructuralDataPageStatus;
                    this.$scope.angularForm = null;
                    this.$scope.ganttRow = [];
                    this.$scope.$watch('crfs.dataValidity.startingDate', function (newValue, oldValue) {
                        if (newValue < _this.$scope.status.projectsStartDate["giviti"]) {
                            _this.$scope.crfs.dataValidity.startingDate = _this.$scope.status.projectsStartDate["giviti"];
                        }
                        _this.onDateChange(newValue, oldValue);
                    });
                    this.loadJsonData();
                }
                StructuralDataController.prototype.checkMissingStartingDate = function () {
                    if (!this.$scope.crfs.dataValidity.startingDate)
                        this.$scope.status.dateDuplicatesFree = false;
                };
                StructuralDataController.prototype.checkDuplicateDates = function () {
                    for (var i in this.centreStructuralDataList.centreStructuralData) {
                        if (i == this.$scope.status.periodIndex.toString())
                            continue;
                        if (this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate == this.$scope.crfs.dataValidity.startingDate) {
                            this.$scope.status.dateDuplicatesFree = false;
                            break;
                        }
                    }
                };
                StructuralDataController.prototype.checkNonCompletedForms = function () {
                    this.$scope.status.nonCompletedFormErrorList = [];
                    for (var i in this.centreStructuralDataList.centreStructuralData) {
                        if (i == this.$scope.status.periodIndex.toString())
                            continue;
                        if (this.centreStructuralDataList.centreStructuralData[i].isFormValid == false
                            && this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate >= this.$scope.status.projectsStartDate["giviti"]) {
                            //this.$scope.status.nonCompletedFormError = true;
                            this.$scope.status.nonCompletedFormErrorList.push(this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate);
                        }
                    }
                };
                StructuralDataController.prototype.checkMissingJanuary = function () {
                    for (var i in this.centreStructuralDataList.centreStructuralData) {
                        if (this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate <= '2014-01-01'
                            || this.centreStructuralDataList.centreStructuralData[i].dataValidity.startingDate <= this.$scope.status.projectsStartDate["giviti"]) {
                            this.$scope.status.januaryPeriodFound = true;
                            break;
                        }
                    }
                };
                StructuralDataController.prototype.checkMissingPeriods = function () {
                    var sortedPeriods = angular.copy(this.$scope.selectorIndexes);
                    sortedPeriods.sort();
                    for (var i = 0; i < sortedPeriods.length; i++) {
                        if (sortedPeriods[i] >= this.$scope.status.projectsStartDate["giviti"]) {
                            var endingDate = moment(this.calculateCurrentEndDate(sortedPeriods[i]), givitiweb.GlobalConfig.jsonDateFormat);
                            if (i == sortedPeriods.length - 1) {
                                if (moment().diff(endingDate, 'days') > 1) {
                                    this.$scope.status.missingPeriodsError = true;
                                    break;
                                }
                            }
                            else {
                                if (moment(sortedPeriods[i + 1], givitiweb.GlobalConfig.jsonDateFormat).diff(endingDate, 'days') > 1) {
                                    this.$scope.status.missingPeriodsError = true;
                                    break;
                                }
                            }
                        }
                    }
                };
                StructuralDataController.prototype.onTaskClick = function (event) {
                    var taskID = event.task.id;
                    var taskDate = event.task.data.date;
                    var taskIndex = taskID.split('-')[1];
                    this.$scope.status.periodIndex = parseInt(taskIndex);
                    this.fillCRFS();
                    this.checkNonCompletedForms();
                };
                StructuralDataController.prototype.onErrorClick = function (errorDate) {
                    var periodIndex = this.$scope.selectorIndexes.length - 1;
                    for (var i = 0; i < this.$scope.selectorIndexes.length; i++) {
                        if (this.$scope.selectorIndexes[i] == errorDate) {
                            periodIndex = i;
                        }
                    }
                    this.$scope.status.periodIndex = periodIndex;
                    this.fillCRFS();
                    this.checkNonCompletedForms();
                };
                StructuralDataController.prototype.onDateChange = function (newValue, oldValue) {
                    if (newValue && (newValue != oldValue || !this.$scope.currentCrfPath)) {
                        this.updatePeriodStatus();
                    }
                };
                StructuralDataController.prototype.onBtnSaveClick = function (postBackButtonID) {
                    this.saveJsonData();
                };
                StructuralDataController.prototype.onBtnRemoveClick = function (postBackButtonID) {
                    this.removeCurrentPeriod();
                };
                StructuralDataController.prototype.getGanttRowInList = function (event) {
                    return this.$scope.ganttRow;
                };
                StructuralDataController.prototype.generateGanttRow = function () {
                    var periods = this.$scope.selectorIndexes;
                    var newProject = { "id": "structuralData", "description": "Periodi", "order": 0, "tasks": [], "data": { "fullDescription": "Dati strutturali", "parentIds": [] } };
                    var rowList = [];
                    if (periods) {
                        var idCounter = 0;
                        for (var p in periods) {
                            var color = StructuralDataController.colorList[idCounter % StructuralDataController.colorList.length];
                            if (periods[p] >= this.$scope.status.projectsStartDate["giviti"]) {
                                var newTask = { "id": "structuralData", "subject": '', "color": color, "from": null, "to": null, "data": { "date": periods[p] } };
                                newTask["from"] = periods[p];
                                newTask["to"] = this.calculateCurrentEndDate(periods[p]);
                                newTask["id"] = "structuralData" + '-' + idCounter.toString();
                                newProject["tasks"].push(newTask);
                                this.checkMissingPeriods();
                            }
                            idCounter += 1;
                        }
                    }
                    rowList.push(newProject);
                    this.$scope.ganttRow = rowList;
                };
                StructuralDataController.prototype.isToday = function (date) {
                    if (date == moment().format(givitiweb.GlobalConfig.jsonDateFormat))
                        return true;
                    return false;
                };
                StructuralDataController.prototype.isValidDate = function (date) {
                    if (moment(date, givitiweb.GlobalConfig.jsonDateFormat).isValid())
                        return true;
                    return false;
                };
                StructuralDataController.prototype.getMaxIndex = function (array) {
                    if (array.length == 0) {
                        return -1;
                    }
                    else {
                        var max = array[0];
                        var maxIndex = 0;
                        for (var k = 0; k < array.length; k++) {
                            if (array[k] > max) {
                                maxIndex = k;
                                max = array[k];
                            }
                        }
                        return maxIndex;
                    }
                };
                StructuralDataController.prototype.getElementIndex = function (array, element) {
                    if (array.length == 0) {
                        return -1;
                    }
                    else {
                        var index = 0;
                        for (var k = 0; k < array.length; k++) {
                            if (array[k] == element) {
                                index = k;
                                break;
                            }
                        }
                        return index;
                    }
                };
                StructuralDataController.prototype.addNewPeriod = function () {
                    this.$scope.status.pageStatus = StructuralDataPageStatus.ADDING_PERIOD;
                    this.$scope.status.periodIndex = this.centreStructuralDataList.centreStructuralData.length;
                    this.$scope.crfs.dataValidity.startingDate = null;
                };
                StructuralDataController.prototype.rollbackNewPeriod = function () {
                    this.$scope.status.periodIndex = this.getMaxIndex(this.$scope.selectorIndexes);
                    this.fillCRFS();
                    this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
                };
                StructuralDataController.prototype.rollbackModifiedPeriod = function () {
                    this.fillCRFS();
                    this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
                };
                StructuralDataController.prototype.removeCurrentPeriod = function () {
                    this.centreStructuralDataList.centreStructuralData.splice(this.$scope.status.periodIndex, 1);
                    this.$scope.status.periodIndex = -1;
                    this.saveJsonData();
                };
                StructuralDataController.prototype.calculateMaxValidity = function (currentDate) {
                    var maxValidity = moment.duration(6, 'months');
                    if (this.$scope.status.projectsStartDate["start"] != "0001-01-01") {
                        if (currentDate >= this.$scope.status.projectsStartDate["start"]) {
                            maxValidity = moment.duration(1, 'months');
                        }
                        else {
                            maxValidity = moment.duration(6, 'months');
                        }
                    }
                    return maxValidity;
                };
                StructuralDataController.prototype.calculateCurrentEndDate = function (startingDate) {
                    var newPeriodEndDate = this.calculatePeriodEndDate(startingDate);
                    var endingDate;
                    // CurrentEndDate could not be over today
                    if (moment().format(givitiweb.GlobalConfig.jsonDateFormat) <= newPeriodEndDate) {
                        endingDate = moment().format(givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    else {
                        endingDate = newPeriodEndDate;
                    }
                    return endingDate;
                };
                StructuralDataController.prototype.calculatePeriodEndDate = function (startingDate) {
                    var sortedPeriods = angular.copy(this.$scope.selectorIndexes);
                    sortedPeriods.sort();
                    var sortedIndex = sortedPeriods.indexOf(startingDate) + 1;
                    var crfEndDate;
                    var validityEndDate;
                    var endingDate;
                    // Need To use the parameter date to calculate the correct crfEndDate
                    var validity = this.versionManager.getCrfValidity(startingDate);
                    if (validity) {
                        crfEndDate = validity.endDate;
                    }
                    //NOTE: for year 2014 only one period is required for data validity
                    if (startingDate.indexOf('2014') > -1) {
                        validityEndDate = moment("2014-12-31", givitiweb.GlobalConfig.jsonDateFormat)
                            .format(givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    else if (startingDate.indexOf('2015') > -1) {
                        //NOTE: for year 2015 only two periods are required for data validity
                        validityEndDate = moment(startingDate, givitiweb.GlobalConfig.jsonDateFormat)
                            .add(moment.duration(6, 'months'))
                            .format(givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    else {
                        //NOTE: if start has been activated, shorted periods are only used after that date
                        validityEndDate = moment(startingDate, givitiweb.GlobalConfig.jsonDateFormat)
                            .add(this.calculateMaxValidity(startingDate))
                            .format(givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    // for other periods the enddate is the min between next periods or maxValidity
                    if (sortedPeriods.indexOf(startingDate) != -1 && sortedPeriods[sortedIndex] <= validityEndDate) {
                        endingDate = moment(sortedPeriods[sortedIndex], givitiweb.GlobalConfig.jsonDateFormat)
                            .add(moment.duration(-1, 'days'))
                            .format(givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    else {
                        endingDate = validityEndDate;
                    }
                    if (endingDate >= crfEndDate) {
                        endingDate = crfEndDate;
                    }
                    return endingDate;
                };
                StructuralDataController.prototype.updatePeriodStatus = function () {
                    var _this = this;
                    this.$scope.status.currentColor = StructuralDataController.colorList[this.$scope.status.periodIndex % StructuralDataController.colorList.length];
                    // Update status relative to currentDate
                    if (this.$scope.crfs.dataValidity && this.$scope.crfs.dataValidity.startingDate) {
                        if (this.$scope.status.projectsStartDate["giviti"] <= this.$scope.crfs.dataValidity.startingDate) {
                            this.$scope.status.currentStartDate = this.$scope.crfs.dataValidity.startingDate;
                        }
                        else {
                            this.$scope.status.currentStartDate = this.$scope.status.projectsStartDate["giviti"];
                        }
                    }
                    else {
                        this.$scope.status.currentStartDate = givitiweb.DateUtils.getJsonDateFromMoment(moment());
                    }
                    this.$scope.status.currentEndDate = this.calculateCurrentEndDate(this.$scope.status.currentStartDate);
                    this.$scope.status.periodEndDate = this.calculatePeriodEndDate(this.$scope.status.currentStartDate);
                    // TODO: needed to clean the form required fields from old elements. To be moved inside dataCollectionDirective and binded to directive's data loaded
                    this.$scope.status.dataLoaded = false;
                    this.$timeout((function () { _this.$scope.status.dataLoaded = true; }), 100);
                    // Update status relative to currentCRF
                    var version = this.versionManager.getCrfVersion(this.$scope.status.currentStartDate);
                    if (version) {
                        this.$scope.currentCrfPath = this.versionManager.getCrfPath(version);
                        var validity = this.versionManager.getCrfValidity(this.$scope.status.currentStartDate);
                        if (validity) {
                            this.$scope.status.crfStartDate = validity.startDate;
                            this.$scope.status.crfEndDate = validity.endDate;
                        }
                    }
                };
                /** Fill the centre data inside $scope and set all supporting variables */
                StructuralDataController.prototype.fillCentreStructuralData = function (centreData) {
                    this.centreStructuralDataList = {};
                    this.$scope.status.pageStatus = StructuralDataPageStatus.NO_DATA;
                    this.$scope.status.periodIndex = -1;
                    this.$scope.selectorIndexes = [];
                    var badCrfs = [];
                    if (centreData.centreStructuralData && centreData.centreStructuralData.length > 0) {
                        //Find all the periods with invalid startingDate
                        for (var i = 0; i < centreData.centreStructuralData.length; i++) {
                            if (!centreData.centreStructuralData[i]
                                || !this.isValidDate(centreData.centreStructuralData[i].dataValidity.startingDate)) {
                                badCrfs.push(i);
                            }
                            else {
                                this.$scope.selectorIndexes.push(centreData.centreStructuralData[i].dataValidity.startingDate);
                            }
                        }
                        //Remove all invalid periods from centreData
                        for (var i = badCrfs.length - 1; i >= 0; i--) {
                            centreData.centreStructuralData.splice(badCrfs[i], 1);
                        }
                        //Bind remaining periods to $scope
                        if (this.$scope.selectorIndexes.length > 0) {
                            this.centreStructuralDataList = centreData;
                        }
                        //Sort periods and index
                        this.$scope.selectorIndexes.sort();
                        this.centreStructuralDataList.centreStructuralData
                            .sort(function (a, b) {
                            if (a.dataValidity.startingDate < b.dataValidity.startingDate)
                                return -1;
                            if (a.dataValidity.startingDate > b.dataValidity.startingDate)
                                return 1;
                            return 0;
                        });
                    }
                };
                /** Fill the current periodIndex data in $scope.crfs cleaning up all dead object */
                StructuralDataController.prototype.fillCRFS = function () {
                    _super.prototype.fillCRFS.call(this, this.centreStructuralDataList.centreStructuralData[this.$scope.status.periodIndex]);
                };
                StructuralDataController.prototype.getCondition = function () {
                    var result = {};
                    result[this.dataName] = this.dataKey;
                    return result;
                };
                StructuralDataController.prototype.loadJsonData = function () {
                    var _this = this;
                    return this.$q.all([
                        this.loadVersions(),
                        this.loadAfference(this.$scope.status.centreCode, "giviti"),
                        this.loadAfference(this.$scope.status.centreCode, "start")
                    ])
                        .then(function () {
                        _this.loadStructuralData();
                        return;
                    });
                };
                StructuralDataController.prototype.loadVersions = function () {
                    var deferred = this.$q.defer();
                    this.versionManager.loadVersions().then(function () { deferred.resolve(); });
                    return deferred.promise;
                };
                StructuralDataController.prototype.loadAfference = function (centreCode, projectName) {
                    var _this = this;
                    return this.profileService.getAfferenceStartDate(centreCode, projectName)
                        .then(function (startDate) {
                        if (startDate != "") {
                            _this.$scope.status.projectsStartDate[projectName] = moment(startDate).format(givitiweb.GlobalConfig.jsonDateFormat);
                        }
                        else {
                            _this.$scope.status.projectsStartDate[projectName] = "0001-01-01";
                        }
                    }, function (error) {
                        _this.$scope.status.projectsStartDate[projectName] = "0001-01-01";
                    });
                };
                StructuralDataController.prototype.loadStructuralData = function () {
                    var _this = this;
                    var condition = this.getCondition();
                    return this.mongoService.getDocument(condition)
                        .then(function (centreData) {
                        if (!angular.equals(centreData, {})) {
                            _this.fillCentreStructuralData(centreData);
                            _this.$scope.status.periodIndex = _this.getMaxIndex(_this.$scope.selectorIndexes);
                            _this.fillCRFS();
                            _this.generateGanttRow();
                            _this.$scope.status.dataLoaded = true;
                            _this.$scope.status.pageStatus = StructuralDataPageStatus.SHOWING_PERIODS;
                        }
                        else {
                            _this.$scope.status.pageStatus = StructuralDataPageStatus.NO_DATA;
                        }
                        _this.updatePeriodStatus();
                        _this.checkMissingJanuary();
                        _this.checkNonCompletedForms();
                    }, function (error) {
                        _this.$scope.status.dataLoaded = false;
                        _this.$scope.status.pageStatus = StructuralDataPageStatus.DATA_LOADING_FAILED;
                    });
                };
                StructuralDataController.prototype.saveJsonData = function () {
                    var _this = this;
                    var promise;
                    this.checkMissingStartingDate();
                    this.checkDuplicateDates();
                    if (this.$scope.status.dateDuplicatesFree) {
                        promise = this.saveCentreStructuralData()
                            .then(function () {
                            _this.$scope.status.dataModified = false;
                            _this.$scope.status.dataSaved = true;
                            var changeVariableStatus = function () {
                                _this.$scope.status.dataSaved = !_this.$scope.status.dataSaved;
                            };
                            _this.$timeout(changeVariableStatus, 4000);
                        });
                    }
                    return promise;
                };
                StructuralDataController.prototype.saveCentreStructuralData = function () {
                    var _this = this;
                    this.$scope.crfs.isFormValid = this.$scope.angularForm.$valid;
                    this.$scope.crfs.dataValidity.endingDate = this.$scope.status.periodEndDate;
                    var condition = this.getCondition();
                    if (!this.centreStructuralDataList.centreStructuralData) {
                        this.centreStructuralDataList.centreStructuralData = [];
                    }
                    if (!this.centreStructuralDataList[this.dataName]) {
                        this.centreStructuralDataList[this.dataName] = this.dataKey;
                    }
                    if (this.$scope.status.periodIndex == this.centreStructuralDataList.centreStructuralData.length) {
                        this.centreStructuralDataList.centreStructuralData.push(angular.copy(this.$scope.crfs));
                    }
                    else if (this.$scope.status.periodIndex != -1) {
                        this.centreStructuralDataList.centreStructuralData[this.$scope.status.periodIndex] = angular.copy(this.$scope.crfs);
                    }
                    this.$scope.status.pageStatus = StructuralDataPageStatus.SAVING_DATA;
                    return this.mongoService.setDocument(condition, this.centreStructuralDataList)
                        .then(function () {
                        return _this.loadJsonData();
                    });
                };
                StructuralDataController.colorList = ['#dc77ff', '#42cccc', '#FFDD5F', '#66cc4e', '#aa6688', '#66bbee', '#fbb000', '#224477', '#33cc99', '#ff7733', '#AA66AA', '#aadd5f'];
                return StructuralDataController;
            })(givitiweb.AbstractController);
            givitiweb.StructuralDataController = StructuralDataController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
