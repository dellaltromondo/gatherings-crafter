/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {

    export interface IPeriod {
        startDate?: string;
        endDate?: string;
    }

    export class CRFVersionsManager {
        private crfVersionsXML: Document;

        constructor(
            private baseURL: string,
            private crfName: string,
            private crfPath: string,
            private $http: ng.IHttpService
        ) {
            this.crfVersionsXML = null;
        }

        /** Given date in jsonFormat return the containing CRF version */
        getCrfVersion(date: string): string {
            if (this.crfVersionsXML != null) {
                var versions: any = this.crfVersionsXML.getElementsByTagName("version");
                var version: string = "";

                for (var i = 0; i < versions.length; i++) {
                    var startDate: string = versions[i].getElementsByTagName("startDate").length > 0 ? versions[i].getElementsByTagName("startDate")[0].textContent : undefined;
                    var endDate: string = versions[i].getElementsByTagName("endDate").length > 0 ? versions[i].getElementsByTagName("endDate")[0].textContent : undefined;

                    if (startDate <= date) {
                        if (endDate) {
                            if (endDate >= date) {
                                version = versions[i].getAttribute("number");
                                break;
                            }
                        } else {
                            version = versions[i].getAttribute("number");
                        }
                    }
                }
                return version;
            }
            return "";
        }

        /** Given date in jsonFormat return the containing startDate and endDate */
        getCrfValidity(date: string): IPeriod {
            if (this.crfVersionsXML != null) {
                var versions: any = this.crfVersionsXML.getElementsByTagName("version");

                var validity: IPeriod = {};

                for (var i = 0; i < versions.length; i++) {
                    var startDate: string = versions[i].getElementsByTagName("startDate").length > 0 ? versions[i].getElementsByTagName("startDate")[0].textContent : undefined;
                    var endDate: string = versions[i].getElementsByTagName("endDate").length > 0 ? versions[i].getElementsByTagName("endDate")[0].textContent : undefined;

                    if (startDate <= date) {
                        if (endDate) {
                            if (endDate >= date) {
                                validity.startDate = startDate;
                                validity.endDate = endDate;
                                break;
                            }
                        } else {
                            validity.startDate = startDate;
                            if (versions[i + 1]) {
                                validity.endDate = moment(versions[i + 1].getElementsByTagName("startDate")[0].textContent).subtract(1, "days").format(GlobalConfig.jsonDateFormat);
                            }
                            else {
                                validity.endDate = "2099-12-31";
                            }
                        }
                    }
                }
                return validity;
            }
            return null;
        }

        /** Return the URI path to get the CRF XML for specified version */
        getCrfPath(version: string): string {
            return this.baseURL + "GetCRF?path=" + this.crfPath + "&name=" + this.crfName + "&version=" + version;
        }

        /** Return the URI path to get the CRF VERSIONS */
        getVersionsPath(): string {
            return this.baseURL + "GetCRFVersions?path=" + this.crfPath + "&name=" + this.crfName;
        }

        /** Load a specified XML versions from services */
        loadVersions(): ng.IPromise<Document> {
            var versionsPath: string = this.getVersionsPath();
            return PromiseUtils.getAsyncXML(this.$http, versionsPath).then(
                (versionsData: Document) => {
                    this.crfVersionsXML = versionsData;
                    return versionsData;
                },
                error => ""
            );
        }
    }
}