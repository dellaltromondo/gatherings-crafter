/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {

    export class QueryBuilderManager {
        private exportList = [];
        private joinsList = [];

        buildQuery(json: any, startingTable: string, startingId: string, shouldAddExportConditions: boolean): string {

            var whereClause = this.buildQueryIterator(json, startingTable, "");
            var exportClause: string = this.getExportClause(startingTable, startingId);
            var joinClause: string = this.getJoinClause();

            return this.getQuery(startingTable, exportClause, joinClause, whereClause);
        }

        private getExportClause(startingTable: string, startingId: string): string {
            if (this.exportList) {
                var idName = this.getColumnFullName(startingTable, startingId);
                this.exportList.unshift(idName);
            }
            var exportClause: string = this.exportList.join(", ");
            if (!exportClause)
                exportClause = startingTable + ".*";
            return exportClause;
        }

        private getJoinClause(): string {
            var joinClause: string = "";
            for (var clauseIndex in this.joinsList) {
                var joinObject = this.joinsList[clauseIndex];
                joinClause = joinClause + " INNER JOIN " + this.quoteString(joinObject.dataType)
                    + " AS " + this.quoteString(joinObject.currentNamespace)
                    + " ON " + this.quoteString(joinObject.startingTable) + "." + this.quoteString(joinObject.key)
                    + " = " + this.quoteString(joinObject.currentNamespace) + "." + this.quoteString(joinObject.key);
            }
            return joinClause;
        }

        private getQuery(startingTable: string, exportClause: string, subQuery: string, whereClause: string): string {
            var query = "SELECT DISTINCT " + exportClause + " FROM \"" + startingTable + "\"" + subQuery;
            if (whereClause) {
                query = query + " WHERE " + whereClause;
            }
            return query;
        }

        private buildQueryIterator(json: any, startingTable: string, currentNamespace: string): string {
            var usedNamespace: string = currentNamespace;
            var whereCondition: string = "";
            if (!currentNamespace)
                usedNamespace = startingTable;

            var andObject = { booleanOperator: "and", booleans: [] };
            for (var key in json) {
                var variable: any = json[key];
                var field = this.getColumnFullName(usedNamespace, key);
                if (this.isExportType(variable)) {
                    if (this.exportList.indexOf(field) == -1) {
                        this.exportList.push(field);
                    }
                }
                if (this.isFilterType(variable)) {
                    if ((variable.filter == true) || (variable.filter == false)) {
                        var newCondition: string = this.getBooleanCondition(variable, field);
                        if (!whereCondition)
                            whereCondition = newCondition;
                        else
                            whereCondition = whereCondition + " AND " + newCondition;
                    }
                    else if (variable.filter != undefined) {
                        if (variable.operator != "" && variable.operator != undefined && variable.filter.length != 0) {
                            if (variable.operator != "between") {
                                var newCondition: string = this.getAutocompleteCondition(variable, field);
                                if (!whereCondition)
                                    whereCondition = newCondition;
                                else
                                    whereCondition = whereCondition + " AND " + newCondition;
                            }
                            else {
                                var lowerCondition: string = this.getBetweenCondition(variable, field, "lowerBound");
                                var upperCondition: string = this.getBetweenCondition(variable, field, "upperBound");
                                if (!whereCondition)
                                    whereCondition = lowerCondition + " AND " + upperCondition;
                                else
                                    whereCondition = whereCondition + " AND " + lowerCondition + " AND " + upperCondition;
                            }
                        }
                    }
                }
                if (this.isInstanceOfType(variable)) {
                    var orCondition: string = "";
                    for (var index in variable) {
                        var newNamespace = this.getNewNamespace(currentNamespace, variable[index].dataType) + "_" + index;
                        var newJoinVariable = { startingTable: usedNamespace, dataType: variable[index].dataType, key: variable[index].onKey, currentNamespace: newNamespace };
                        this.joinsList.push(newJoinVariable);
                        var newOrCondition: string = this.buildQueryIterator(variable[index].variables, variable[index].dataType, newNamespace);
                        if (newOrCondition)
                            if (!orCondition)
                                orCondition = newOrCondition;
                            else
                                orCondition = orCondition + " OR " + newOrCondition;
                    }
                    if (orCondition) {
                        if (!whereCondition)
                            whereCondition = orCondition;
                        else
                            whereCondition = whereCondition + " AND (" + orCondition + ") ";
                    }
                }
            }
            return whereCondition;
        }

        private getNewNamespace(currentNamespace: string, keyNamespace: string): string {
            var newNamespaceToken: string = "instanceOf." + keyNamespace;
            if (currentNamespace === "")
                return newNamespaceToken;
            return currentNamespace + "-" + newNamespaceToken;
        }

        private isExportType(variable: any): boolean {
            if (variable.export)
                return true;
            return false;
        }

        private isFilterType(variable: any): boolean {
            if (variable.filter && variable.operator)
                return true;
            return false;
        }

        private isInstanceOfType(variable: any): boolean {
            if (angular.isArray(variable))
                return true;
            return false;
        }

        private getBooleanCondition(variable: any, field: string): string {
            return field + " " + "=" + " " +this.quoteAndEscapeValue(variable.filter);
        }

        private getBetweenCondition(variable: any, field: string, bound: string): string {
            var selectedOperator: string = '<';
            if (bound == 'lowerBound')
                selectedOperator = '>';
            return field + " " + selectedOperator + " " + this.quoteAndEscapeValue(variable.filter[bound]);
        }

        private getAutocompleteCondition(variable: any, field: string): string {
            var selectedValue;
            var selectedOperator = variable.operator;
            if (variable.filter.length == 1) {
                selectedValue = this.quoteAndEscapeValue(variable.filter[0].key);
            }
            else {
                if (variable.operator == '=') {
                    selectedOperator = " IN ";
                }
                else {
                    selectedOperator = " NOT IN ";
                }
                var selectedValue = variable.filter.map(function(elem) {
                    
                    //TODO: should unify with quoteAndEscapeValue.
                    //Currently splitted because this.quoteAndEscapeValue doesn't work inside map
                    var value = elem.text;
                    var newValue:string = "";
                    if (value && typeof value == "string")
                        newValue = value.replace(/'/g, "''");
                    else
                        newValue = ''
                    return "'" + newValue + "'";    
                }).join(",");
                if (selectedValue) {
                    selectedValue = "(" + selectedValue + ")";
                }
            }
            return field + " " + selectedOperator + " " + selectedValue;
        }

        private quoteAndEscapeValue(value: string): string {
            var newValue:string = "";
            if (value && typeof value == "string")
                newValue = value.replace(/'/g, "''");
            else
                newValue = ''
            return "'" + newValue + "'";                    
        }
        

        private quoteString(nonQuotedString: string): string {
            return "\"" + nonQuotedString + "\"";
        }d

        private getColumnFullName(tableName: string, columnName: string): string {
            return this.quoteString(tableName) + "." + this.quoteString(columnName);
        }

    }
}
