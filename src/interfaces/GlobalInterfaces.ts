module it.marionegri.givitiweb {
    'use strict';

    export interface IDictionaryOf<T> {
        [index : string] : T;
    }
}