module it.marionegri.givitiweb {

    export class GlobalConfig {
        // dateFormat for moment.js
        public static jsonDateFormat:string = "YYYY-MM-DD";
        public static viewDateFormat:string = "DD/MM/YYYY";

        constructor() {
                throw new Error("Cannot create a new instance of GlobalConfig class");
            }
        }
    }