/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {

    export class Cache<T> {

        objectsCache = [];

        getFromCache(parameters: T): any {
            var result: any = null;
            for (var cachedObject in this.objectsCache) {
                if (angular.equals(this.objectsCache[cachedObject].parameters, parameters))
                    return this.objectsCache[cachedObject].cachedObject;
            }
            return result;
        }

        putInCache(parameters: T, newResult: any) {
            var cachedObjectFound = false;
            for (var cachedObject in this.objectsCache) {
                if (angular.equals(this.objectsCache[cachedObject].parameters, parameters)) {
                    this.objectsCache[cachedObject].cachedObject = newResult;
                    cachedObjectFound = true;
                    break;
                }
            }
            if (!cachedObjectFound) {
                var newObject = { parameters: angular.copy(parameters), cachedObject: angular.copy(newResult) }
                this.objectsCache.push(newObject);
            }
        }

        constructor() {
        }
    }
}
