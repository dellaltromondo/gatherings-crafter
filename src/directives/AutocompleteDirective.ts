/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    interface IAutocompleteModel extends ng.IScope {
        tableName: string;
        columnName: string;
        limit: string;
        page: string;
        parentModel: any;
        tags?: any;
        remoteUrlRequestFn?: () => any;
        remoteUrlResponseFormatterFn?: () => any;
    }

    export class AutocompleteDirective implements ng.IDirective {
        public $inject = ['$http', '$q', 'culture'];

        public restrict = "E";
        public require = "ngModel";
        public link;
        public scope = {
            tableName: '@tableName',
            columnName: '@columnName',
            limit: '@limit',
            page: '@page',
            parentModel: '@dataNgModel'
        };

        constructor(
            private $q: ng.IQService,
            private $http: ng.IHttpService,
            public templateUrl: string,
            public suggestionURL: string,
            private culture: string
        ) {

            this.link = this.realLink.bind(this);
        }

        realLink(scope: any, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            // Create class to handle logic for DatePicker
            new AutocompleteDirectiveLogic(this.$q, scope, this.$http, ngModelCtrl, this.suggestionURL, this.culture);
        }

        static factory($q: ng.IQService, $http: ng.IHttpService, templateUrl: string, suggestionURL: string, culture: string) {
            return new AutocompleteDirective($q, $http, templateUrl, suggestionURL, culture);
        }
    }

    export class AutocompleteDirectiveLogic {

        constructor(
            private $q: ng.IQService,
            private myScope: IAutocompleteModel,
            private $http: ng.IHttpService,
            private myModelCtrl: ng.INgModelController,
            private serviceURL: string,
            private culture: string
        ) {

            this.myModelCtrl.$render = () => { this.myRender(); };
            this.myScope.remoteUrlRequestFn = this.getValues.bind(this);
            this.myScope.remoteUrlResponseFormatterFn = this.formatResponse.bind(this);

            this.myScope.$watch('tags',
                (newValue: any, oldValue: any) =>
                { this.myWatcher(newValue, oldValue); }
            );
        }

        myRender() {
            this.myScope.tags = this.myModelCtrl.$viewValue;
        }

        myWatcher(newValue: any, oldValue: any) {
            if (newValue != oldValue) {
                this.myModelCtrl.$setViewValue(newValue);
                this.myScope.parentModel = newValue;
            }
        }

        formatResponse(data: any): any {
            var response = JSON.parse(data.data.d);
            var newResponse = [];
            for (var index in response) {
                var newObject = { text: response[index].text, key: response[index].key };
                if (newResponse.indexOf(newObject) == -1) {
                    newResponse.push(newObject);
                }
            }
            return newResponse;
        }

        getValues(searchText: string): ng.IPromise<string> {
            var method: string = "GetAutoCompleteValues";
            var deferred: ng.IDeferred<any> = this.$q.defer();
            var query = '?tableName=' + this.myScope.tableName + '&columnName=' + this.myScope.columnName + '&searchText=' + searchText + '&limit=10&culture=' + this.culture;
            this.$http.get(this.serviceURL + method + query)
                .then((data: any) => {
                    var newData = this.formatResponse(data);
                    deferred.resolve(newData);
                });
            return deferred.promise;
        }
    }
}