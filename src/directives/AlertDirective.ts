/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class AlertDirective implements ng.IDirective {

        //public restrict;
        public priority;
        public $inject = ['$parse'];
        public $parse;
        public link;

        static factory($parse): ng.IDirective {
            return new AlertDirective($parse)
        }

        constructor($parse) {
            // restrict to: A attribute, E element
            this.priority = 1;
            this.$parse = $parse;
            this.link = (scope, element, attr) => { this.myLinker(scope, element, attr); };
            // element must have ng-model attribute.
            //this.require = 'ngModel';
        }

        myLinker (scope, element, attr) {
            var msg = this.$parse(attr.confirmationNeeded);
            var clickAction = this.$parse(attr.ic3Click);
            element.click('click', function(event) {
                var callBack = function() {
                    clickAction(scope, { $event: event });
                };
                if (window.confirm(msg(scope))) {
                    scope.$apply(callBack)
                }
            });
        }

    }
}
