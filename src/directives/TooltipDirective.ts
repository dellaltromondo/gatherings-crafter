/*  TOOLTIP DIRECTIVE
*  Custom tooltip that allows text to be interacted with
*  HTML can be used inside, which allows for further
*  formatting options and links[...].
*/


module it.marionegri.givitiweb {
    'use strict';

    export class TooltipDirective implements ng.IDirective {

        public restrict = "E";
        public scope = {
            tooltip: "=text"
        }

        public template = `
            <span class="tooltip"
                data-ng-click="showPermanently = !showPermanently"
                data-ng-mouseenter="show = true"
                data-ng-mouseleave="show = false"
            >
                <label class="help">?</label>
                <span data-ng-show="show || showPermanently" translate="{{tooltip}}"></span>
            </span>
        `;
        
        link($scope) {
            $scope.show = false;
            $scope.showPermanently = false;
        }

        static factory() {
            return new TooltipDirective();
        }
    }
}