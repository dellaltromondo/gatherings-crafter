/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../utils/ColorHelper.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class DataClassDirective implements ng.IDirective {

        public $inject = ['$compile', '$timeout'];
        public restrict = "E";
        public link = this.linker.bind(this);
        public scope = {
            ic3DataClass: '=ic3DataClass',
            loaded: '=',
            processor: '=processor',
            xml: '=xml',
            crfs: '=parent'
        };

        constructor(
            private $compile: ng.ICompileService,
            private $timeout: ng.ITimeoutService,
            private $rootScope : ng.IRootScopeService,
            private patientStatus: PatientStatusService
        ) {}

        static factory($compile: ng.ICompileService, $timeout: ng.ITimeoutService, patientStatus: PatientStatusService, $rootScope: ng.IRootScopeService) {
            return new DataClassDirective($compile, $timeout, $rootScope, patientStatus);
        }

        linker(scope, element, attrs) {
            return new DataClassDirectiveLogic(scope, element, attrs, this.$compile, this.patientStatus, this.$rootScope)
        }
    }

    export class DataClassDirectiveLogic {

        private myScope: any;
        private selectedPage : string;

        constructor(scope: any,
                private element: any,
                private attrs: any,
                private $compile: ng.ICompileService,
                private patientStatus: PatientStatusService,
                private $rootScope : ng.IRootScopeService
        ) {
            this.myScope = scope;
            this.myScope.ColorHelper = ColorHelper;
            this.myScope.completenessController = this.patientStatus;
            this.$rootScope.$on('PageChanged', ( event, newSelectedPage ) => {
                this.selectedPage = newSelectedPage;
            });
            this.myScope.$watch('loaded',
                (newValue: any, oldValue: any) => { this.myWatcher(newValue, oldValue); }
            );
            if (this.myScope.loaded) {
                this.myWatcher(true, false);
            }
        }

        myWatcher(newValue: any, oldValue: any) {
            if (newValue && ((newValue != oldValue) || !this.myScope.crfs)) {
                this.myScope.vm = this;
                this.patientStatus.setRootVariable(this.myScope.crfs);
                this.patientStatus.setVM(this);
                let transformed = this.transform(this.myScope);
                if (transformed) {
                    this.element.append(transformed);
                } else {
                    this.element.html("<div class='error'>The XSL transform returned an empty value!</div>");
                }
                this.$compile(this.element.contents())(this.myScope);
            }
        }

        toggleCheckBoxesSelection(variable: any[], value: any, exclusive: any[]): void {
            var idx: number = variable.indexOf(value);

            if (idx > -1) {
                variable.splice(idx, 1);
            }
            else {
                variable.push(value);
            }
            for (var i = 0; i < exclusive.length; i++) {
                var exclusiveItemIndex = variable.indexOf(exclusive[i]);
                if (exclusiveItemIndex > -1) {
                    variable.splice(exclusiveItemIndex, 1);
                }
            }
        }

        checkCompleteness(field : any, valueInField: string): boolean {
            for (var checkVisibilityObject in field) {
                if (field[checkVisibilityObject] && field[checkVisibilityObject][valueInField] == true) {
                    return true;
                }
            }
            return false;
        }

        addElementToSet(currentSet: any[]): string {
            const guid = GuidUtils.createGUID()
            currentSet.push({ "guid": guid });
            return guid;
        }

        removeElementFromSet(currentSet: any[], guid: string): void {
            var elementFound: boolean = false;
            var index: number = -1;
            for (var element in currentSet) {
                if (currentSet[element].guid == guid) {
                    index = parseInt(element);
                    elementFound = true;
                }
            }
            if (elementFound) {
                currentSet.splice(index, 1);
            }
            else {
                throw "Element not found in variable of dataType Set! ";
            }
        }

        isNaN(valueToBeChecked: any): boolean {
            return isNaN(valueToBeChecked);
        }

        maxInArrayByProperty(array : any[], property : string) {
            return array.reduce((a, b) => a[property] > b[property] ? a : b, { [property] : -Number.MAX_VALUE });
        }

        setSelectedPage(selectedPage : string) {
            this.selectedPage = selectedPage;
            this.$rootScope.$broadcast("PageChanged", selectedPage);
            // this.$rootScope.$emit("PageChanged", selectedPage); 
        }

        getSelectedPage() : string {
            return this.selectedPage;
        }

        //TODO: only for subti
        yearDiff (aDate, suffix=true) {
            if (!aDate) {
                return "--";
            }

            aDate = new Date(aDate).getFullYear();
            let today = new Date().getFullYear();
            let difference = today - aDate;
            
            if (suffix) {
                let yearSuffix = difference == 1 ? "anno" : "anni";
                return `${difference} ${yearSuffix}`;
            }
            
            return difference;
        }

        //TODO: only for subti
        calculateGroup(aGroup) {

            let result = 0;
            if (aGroup === undefined) {
                return;
            }

            // TODO: browser support for entries?
            let entries = Object.entries(aGroup);

            if (entries.length == 0) {
                return;
            }

            // iterate all entries and parse their value
            entries.forEach(variable => {
                const [key, value] = variable;
                result += this.parseNumber(value);
            })

            return result;
        }

        //TODO: only for subti
        private parseNumber(stringToParse) {
            if ((stringToParse == "" || stringToParse === undefined) && stringToParse != 0) {
                return;
            }

            // if it's a number just take it as it is
            if (!isNaN(stringToParse)) {
                return stringToParse;
            }

            // adding 1 so that it wont consider the point
            let lastPointIndex = stringToParse.lastIndexOf(".") + 1;
            let lastUnderscoreIndex = stringToParse.lastIndexOf("_") + 1;
            // this allows underscores to be used as a way of giving unique ids to same values
            let relevantPosition = lastPointIndex > lastUnderscoreIndex ? lastPointIndex : lastUnderscoreIndex;

            let numberInString = parseInt(stringToParse.slice(relevantPosition, stringToParse.length));

            // if parsing failed for some reason
            if (isNaN(numberInString)) {
                return;
            }

            return numberInString;
        }

        transform(scope: any): DocumentFragment {
            var dataClasses = scope.xml.documentElement.getElementsByTagName("dataClass");
            var xmlFragment;
            for (var element in dataClasses) {
                if (dataClasses[element].getAttribute && dataClasses[element].getAttribute("name") == scope.ic3DataClass) {
                    xmlFragment = dataClasses[element];
                    break;
                }
            }
            if (!xmlFragment) {
                console.log("ERROR: Dataclass '" + scope.ic3DataClass + "' has not been found!");
            }

            const transformed : DocumentFragment = scope.processor.transformToFragment(xmlFragment, document);
            return transformed;
        }
        
        registerVariable(variableName: string, visibility: string) {
            this.patientStatus.registerVariable(variableName, visibility);
        }

        noChildIsChecked(parentVariable: any, childBooleanVariableName: string) {
            return Object.keys(parentVariable).every(
                el => !parentVariable[el][childBooleanVariableName]
            )
        }

        atLeastOneChildIsChecked(parentVariable: any, childBooleanVariableName: string) {
            return Object.keys(parentVariable).some(
                el => parentVariable[el][childBooleanVariableName]
            )
        }
    }
}