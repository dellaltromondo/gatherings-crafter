/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    interface IAutocompleteJSONModel extends ng.IScope {
        className: string;
        indexName: string;
        limit: string;
        page: string;
        parentModel: any;
        tags?: any;
        remoteUrlRequestFn?: () => any;
        remoteUrlResponseFormatterFn?: () => any;
    }

    export class AutocompleteJSONDirective implements ng.IDirective {
        public $inject = ['$q', '$http'];

        public restrict = "E";
        public require = "ngModel";
        public link;
        public scope = {
            className: '@className',
            indexName: '@indexName',
            limit: '@limit',
            page: '@page',
            parentModel: '@dataNgModel'
        };

        constructor(
            private $q: ng.IQService,
            private $http: ng.IHttpService,
            public templateUrl: string,
            public suggestionURL: string
        ) {

            this.link = this.realLink.bind(this);
        }

        realLink(scope: any, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            // Create class to handle logic inside directive
            new AutocompleteJSONDirectiveLogic(this.$q, scope, this.$http, ngModelCtrl, this.suggestionURL);
        }

        static factory($q: ng.IQService, $http: ng.IHttpService, templateUrl: string, suggestionURL: string) {
            return new AutocompleteJSONDirective($q, $http, templateUrl, suggestionURL);
        }
    }

    export class AutocompleteJSONDirectiveLogic {

        constructor(
            private $q: ng.IQService,
            private myScope: IAutocompleteJSONModel,
            private $http: ng.IHttpService,
            private myModelCtrl: ng.INgModelController,
            private serviceURL: string
        ) {

            this.myModelCtrl.$render = () => { this.myRender(); };
            this.myScope.remoteUrlRequestFn = this.getValues.bind(this);
            this.myScope.remoteUrlResponseFormatterFn = this.formatResponse.bind(this);

            this.myScope.$watch('tags',
                (newValue: any, oldValue: any) =>
                { this.myWatcher(newValue, oldValue); }
            );
        }

        myRender() {
            this.myScope.tags = this.myModelCtrl.$viewValue;
        }

        myWatcher(newValue: any, oldValue: any) {
            if (newValue != oldValue) {
                this.myModelCtrl.$setViewValue(newValue);
                this.myScope.parentModel = newValue;
            }
        }

        formatResponse(data: any): any {
            var response = JSON.parse(data.data.d);
            var newResponse = [];
            for (var index in response) {
                if (response[index]) {
                    var searchStr = response[index];
                    var newObject = { text: searchStr };
                    if (newResponse.indexOf(newObject) == -1) {
                        newResponse.push(newObject);
                    }
                }
            }
            return newResponse;
        }

        getValues(searchText: string): ng.IPromise<string> {
            var method: string = "GetAutoCompleteIndexValues";
            var deferred: ng.IDeferred<any> = this.$q.defer();

            if (searchText.length > 2) {
                var query = '?className=' + this.myScope.className + '&indexName=' + this.myScope.indexName + '&searchText=' + searchText + '&limit=10';
                this.$http.get(this.serviceURL + method + query)
                    .then((data: any) => {
                        var newData = this.formatResponse(data);
                        deferred.resolve(newData);
                    });
            }
            return deferred.promise;
        }
    }
}