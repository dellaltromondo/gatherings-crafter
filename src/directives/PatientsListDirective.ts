/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class PatientsListDirective implements ng.IDirective {

        public restrict = "E";
        public $compile;
        public link = this.linker.bind(this);
        public scope = {
            patientsListColumns: "=columns"
        };

        public template: string = `
        <span class="loading" data-ng-if="!dataLoaded">Sto caricando...</span>

        <button data-ng-if="dataLoaded" type="button" data-ng-click="vm.createPatient()">Nuovo ricovero</button>
        <div class="dialog" data-ng-if="deleting.patient != null">
            Vuoi davvero rimuovere il paziente {{ deleting.patient.substr(0, 6) }}?
            <button data-ng-click="vm.deletePatient(deleting.patient)">Rimuovi</button>
            <button data-ng-click="vm.permanentlyDeletePatient(deleting.patient)">Rimozione TOTALE</button>
            <button data-ng-click="deleting.patient = null">Annulla</button>
        </div>
        <div class="success" data-ng-if="deleting.success">
            Paziente cancellato correttamente!
        </div>
        <div class="error" data-ng-if="deleting.failed">
            Errore nella cancellazione del paziente!
        </div>
        <table data-ng-if="dataLoaded &amp;&amp; patientsList.length &gt; 0">
            <thead>
                <tr>
                    <th>Id</th>
                    <th data-ng-repeat="(column, label) in patientsListColumns">{{ label }}</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr data-ng-repeat="row in patientsList track by row.guid">
                    <td class="patient-id">{{ row.guid.substr(0, 6) }}</td>
                    <td data-ng-repeat="(column, label) in patientsListColumns">{{ row[ column ] }}</td>
                    <td class="status-{{ row['__status__'] }}">{{ row['__status__'] }}</td>
                    <td>
                        <button type="button" data-ng-click="vm.openPatient(row.guid)">Apri</button>
                        <button type="button" data-ng-click="deleting.patient = row.guid">Rimuovi</button>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <section id="deleted-patients">
            <button data-ng-show="!showDeleted" data-ng-click="showDeleted = true" data-ng-init="showDeleted = false">Mostra pazienti cancellati</button>
            <button data-ng-show="showDeleted" data-ng-click="showDeleted = false">Nascondi pazienti cancellati</button>
            <p data-ng-if="showDeleted &amp;&amp; dataLoaded &amp;&amp; deletedPatientsList.length == 0">Nessun paziente cancellato</p>
            <table data-ng-if="showDeleted &amp;&amp; dataLoaded &amp;&amp; deletedPatientsList.length &gt; 0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th data-ng-repeat="(column, label) in patientsListColumns">{{ label }}</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="row in deletedPatientsList track by row.guid">
                        <td class="patient-id">{{ row.guid.substr(0, 6) }}</td>
                        <td data-ng-repeat="(column, label) in patientsListColumns">{{ row[ column ] }}</td>
                        <td class="status-{{ row['__status__'] }}">{{ row['__status__'] }}</td>
                        <td>
                            <button type="button" data-ng-click="vm.restorePatient(row.guid)">Ripristina</button>
                            <button data-ng-click="vm.permanentlyDeletePatient(row.guid)">Rimozione TOTALE</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
        `;

        constructor (
            private $http : ng.IHttpService,
            private $rootScope : ng.IRootScopeService,
            private listService : IJSONService<any>,
            private deletedListService : IJSONService<any>,
            private guidStorage : IGUIDStorage<string>,
            private jsonService : IJSONService<any>,
            private ccDataStore: MongoService<any>,
            private patientStatus : PatientStatusService, 
            private $interval : ng.IIntervalService
        ) {};

        static factory($q : ng.IQService, $http : ng.IHttpService, $rootScope : ng.IRootScopeService, listService : IJSONService<any>, deletedListService : IJSONService<any>, guidStorage : IGUIDStorage<string>, jsonService : IJSONService<any>, ccDataStore: MongoService<any>, patientStatus : PatientStatusService, $interval : ng.IIntervalService) {
            return new PatientsListDirective($http, $rootScope, listService, deletedListService, guidStorage, jsonService, ccDataStore, patientStatus, $interval);
        }

        linker(scope: any, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            return new PatientsListDirectiveLogic(scope, ngModelCtrl, this.$http, this.$rootScope, this.listService, this.deletedListService, this.guidStorage, this.jsonService, this.ccDataStore, this.patientStatus, this.$interval);
        }
    }

    export class PatientsListDirectiveLogic {

        constructor(
            private myScope : any,
            private ngModelCtrl : ng.INgModelController,
            private $http : ng.IHttpService,
            private $rootScope : ng.IRootScopeService,
            private listService : IJSONService<any>,
            private deletedListService : IJSONService<any>,
            private patientGUIDStorage : IGUIDStorage<string>,
            private jsonService : IJSONService<any>,
            private ccDataStore: MongoService<any>,
            private patientStatus : PatientStatusService,
            private $interval : ng.IIntervalService
        ) {

            if (!this.myScope.patientsListColumns) {
                console.log("ERROR: no columns provided in Patient List directive!");
            }
            /*
            this.myScope.patientsListColumns = {
                "crfs.NumberVariable": "Number",
                "crfs.SingleChoiceVariable": "Single Choice"
            } 
            */  

            this.myScope.dataLoaded = false;
            this.myScope.patientsList = [];
            this.myScope.deletedPatientsList = [];
            this.myScope.deleting = { patient : null };

            this.reloadData();
            this.$interval(_ => this.reloadData(), 60 * 1000);

            this.$rootScope.$on("PatientSaved", _ => {
                this.patientGUIDStorage.$get().then( guid => {
                    let foundElement = null;
                    for(let patient of this.myScope.patientsList) {
                        if (patient.guid == guid) {
                            foundElement = patient;
                        }
                    }
    
                    if (!foundElement) {
                        foundElement = { guid };
                        this.myScope.patientsList.push(foundElement);
                    }

                    const mainScope = this.myScope.$parent;
                    for (let variable in this.myScope.patientsListColumns) {
                        foundElement[variable] = eval("mainScope." + variable )
                    }
                    foundElement["__status__"] = this.patientStatus.getStatus();
                    foundElement["__lastSaved__"] = new Date();
                });
                
                this.reloadData().then(
                    _ => this.listService.save(this.myScope.patientsList, "", "")
                );
            });

            this.myScope.vm = this;
        }

        createPatient() {
            const newGuid = GuidUtils.createGUID();
            this.patientGUIDStorage.update(newGuid);
            this.$rootScope.$broadcast("PatientCreated");
        }

        openPatient(guid: string) {
            this.patientGUIDStorage.update(guid);
            this.$rootScope.$broadcast("PatientSwitched");
        }

        deletePatient(guid: string) {
            this.reloadData().then(_ => {
            
                this.movePatientFromAListToAnother(
                    guid,
                    this.myScope.patientsList,
                    this.myScope.deletedPatientsList
                )

                return Promise.all([
                    this.listService.save(this.myScope.patientsList, "", ""),
                    this.deletedListService.save(this.myScope.deletedPatientsList, "", "")
                ]).then(
                    success => this.showDeletionCompleted(guid),
                    error => this.showDeletingError()
                );
            });
        }

        restorePatient(guid: string) {
            this.reloadData().then(_ => {

                this.movePatientFromAListToAnother(
                    guid,
                    this.myScope.deletedPatientsList,
                    this.myScope.patientsList
                );

                return Promise.all([
                    this.listService.save(this.myScope.patientsList, "", ""),
                    this.deletedListService.save(this.myScope.deletedPatientsList, "", "")
                ]);
            });
        }

        permanentlyDeletePatient(guid: string) {
            this.patientGUIDStorage.update(guid);
            this.jsonService.remove().then(
                success => {
                    this.removePatientFromList(this.myScope.patientsList, guid);
                    this.listService.save(this.myScope.patientsList, "", "").then(
                        success => this.showDeletionCompleted(guid),
                        error => this.showDeletingError()
                    );
                    this.ccDataStore.removeDocument({ '__guid__': guid });
                },
                error => this.showDeletingError()
            )
        }

        private reloadData() {

            return Promise.all([
                this.deletedListService.$get(),
                this.listService.$get()
            ]).then( ([deleted, nonDeleted]) => {
                this.mergeLists(this.myScope.deletedPatientsList, deleted ?? []);
                this.mergeLists(this.myScope.patientsList, nonDeleted ?? []);
                this.myScope.dataLoaded = true;
                this.myScope.$apply();
            });
        }

        private mergeLists(current, incoming) {
            for(let newPatient of incoming) {
                let found = false;
                for(let patient of current) {
                    if (patient.guid == newPatient.guid) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    current.push(newPatient);
                }
            }
        }

        private movePatientFromAListToAnother(guid: string, listToBeRemovedFrom, listToBeAddedTo) {
            const patient = this.removePatientFromList( listToBeRemovedFrom, guid );
            if (patient) {
                listToBeAddedTo.push( angular.copy( patient ) );
            }
        }

        private removePatientFromList(list, guid: string) {
            for(let i in list) {
                if (list[i].guid == guid) {
                    return list.splice(i, 1)[0] ?? null;
                }
            }

            return null;
        }

        private showDeletionCompleted(guid: string) {
            this.myScope.deleting.success = true;
            this.myScope.deleting.error = false;
            this.myScope.deleting.patient = null;
            setTimeout(_ => this.myScope.deleting.success = false, 5000)
        }

        private showDeletingError() {
            this.myScope.deleting.error = true;
            this.myScope.deleting.success = false;
            this.myScope.deleting.patient = null;
            setTimeout(_ => this.myScope.deleting.error = false, 5000)
        }
    }
}