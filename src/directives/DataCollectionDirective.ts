/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    declare var XSLT2Processor;
    export class DataCollectionDirective implements ng.IDirective {

        public $inject = ['$q', '$http'];
        public restrict = "E";
        public $compile;
        public link = this.linker.bind(this);
        public scope = {
            ic3DataClass: '=ic3DataClass',
            xslPath: '=xslPath',
            crfPath: '=crfPath',
            crfs: '=ngModel'
        };

        public template: string = '<ic3-data-class parent="crfs" processor="processor" xml="xml" ic3-data-class="ic3DataClass" loaded="loaded"></ic3-data-class>';

        constructor (private $q : ng.IQService, private $http : ng.IHttpService) {};

        static factory($q : ng.IQService, $http : ng.IHttpService) {
            return new DataCollectionDirective($q, $http);
        }

        linker(scope: any, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            return new DataCollectionDirectiveLogic(scope, ngModelCtrl, this.$q, this.$http);
        }
    }

    export class DataCollectionDirectiveLogic {

        constructor(
            private myScope : any,
            private ngModelCtrl : ng.INgModelController,
            private $q : ng.IQService,
            private $http : ng.IHttpService
        ) {

            this.myScope.loaded = false;
            this.myScope.$watch('crfPath',
                (newValue: any, oldValue: any) =>
                { this.myWatcher(newValue, oldValue); }
            );
        }

        myWatcher(newValue: any, oldValue: any) {
            if (newValue && ((newValue != oldValue) || !this.myScope.xml)) {
                this.myScope.loaded = false;
                this.myScope.processor = new XSLT2Processor();
                this.importCrf(this.myScope.crfPath)
                    .then(() => this.importStylesheet(this.myScope.xslPath));
            }
        }

        importStylesheet(xslPath: string): ng.IPromise<boolean> {
            var deferred: ng.IDeferred<boolean> = this.$q.defer();
            this.myScope.processor.importStylesheetURI(xslPath)
                .done(() => {
                    this.myScope.$apply(() => {
                        this.myScope.loaded = true;
                        deferred.resolve(true);
                    });
                });
            return deferred.promise;
        }

        importCrf(xmlPath: string): ng.IPromise<Document> {
            return PromiseUtils
                .getAsyncXML(this.$http, xmlPath)
                .then(dom => this.myScope.xml = dom);
        }

    }
}