/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class ColorPickerDirective implements ng.IDirective {

        public restrict;
        public require;
        public link;

        static factory(): ng.IDirective {
            return new ColorPickerDirective()
        }

        constructor() {
            // restrict to: A attribute, E element
            this.restrict = "A";

            // element must have ng-model attribute.
            this.require = 'ngModel';

            this.link = function(scope: ng.IScope, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
                element.tinycolorpicker({
                });
                element.bind("change", function(evt, newColor) {
                    ngModelCtrl.$setViewValue(newColor);
                    scope.$apply();
                });
                scope.$watch('data.color', function(newValue){
                    var colorPicker = element.data("plugin_tinycolorpicker");
                    colorPicker.setColor(newValue);
                });
            }
        }
    }
}
