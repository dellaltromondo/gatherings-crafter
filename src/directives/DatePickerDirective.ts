/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    interface IDatePickerModel extends ng.IScope {
        viewDate: string;
        maxDate: string;
        minDate: string;
    }

    export class DatePickerDirective implements ng.IDirective {

        public restrict = "A";
        public require = "ngModel";
        public scope = {
            minDate: "@minDate",
            maxDate: "@maxDate"
        };
        public template = "<input data-ng-model='viewDate' data-ng-readonly='true' class='datebutton'></input>";

        static factory() {
            return new DatePickerDirective();
        }

        link(scope: IDatePickerModel, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            function updateWidget(element, scope) {
                var options: any = {
                    format: 'd/m/Y',
                    formatDate: 'Y-m-d',
                    lang: 'it',
                    dayOfWeekStart: 1,
                    mask: true,
                    closeOnDateSelect: true,
                    allowBlank: true,
                    defaultSelect: false,
                    timepicker: false,
                    validateOnBlur: true,
                    scrollInput: false
                };
                var inputChild: any = angular.element(element).find("input");
                if (scope.maxDate)
                    options.maxDate = scope.maxDate;
                if (scope.minDate)
                    options.minDate = scope.minDate;
                inputChild.datetimepicker(options);
            };

            var inputChild: any = angular.element(element).find("input");
            updateWidget(element, scope);
            scope.$watch('maxDate', (newValue: string, oldValue: string) => {
                if (newValue != oldValue) {
                    scope.maxDate = newValue;
                    updateWidget(element, scope);
                }
            });
            scope.$watch('minDate', (newValue: string, oldValue: string) => {
                if (newValue != oldValue) {
                    scope.minDate = newValue;
                    updateWidget(element, scope);
                }
            });

            // Create class to handle logic for DatePicker
            new DatePickerDirectiveLogic(scope, ngModelCtrl);
        }
    }

    export class DatePickerDirectiveLogic {

        private myScope: IDatePickerModel;
        private myModelCtrl: ng.INgModelController;

        constructor(scope: IDatePickerModel, ngModelCtrl: ng.INgModelController) {

            this.myScope = scope;
            this.myModelCtrl = ngModelCtrl;

            this.myModelCtrl.$formatters.push(this.myFormatter);
            this.myModelCtrl.$render = () => { this.myRender(); };

            this.myScope.$watch('viewDate',
                (newValue: string, oldValue: string) =>
                { this.myWatcher(newValue, oldValue); }
            );
        }

        myFormatter(modelValue: string) {
            var momentDate = moment(modelValue, GlobalConfig.jsonDateFormat);
            var value;
            if (momentDate.isValid()) {
                value = momentDate.format(GlobalConfig.viewDateFormat)
            } else {
                value = modelValue;
            }
            return value;
        }

        myRender() {
            this.myScope.viewDate = this.myModelCtrl.$viewValue;
        }

        //TODO: may be moved to DateUtils
        crfValidityDateToMomentDate(date: string) {
            var newDate;
            if (date === 'today') {
                newDate = moment();
            }
            else {
                newDate = moment(date, GlobalConfig.jsonDateFormat)
            }
            return newDate;
        }

        myWatcher(newValue: string, oldValue: string) {
            if (newValue != oldValue) {
                var momentDate = moment(newValue, GlobalConfig.viewDateFormat);
                var isBefore = false;
                var isAfter = false;
                if (this.myScope.minDate)
                    isBefore = momentDate.isBefore(this.crfValidityDateToMomentDate(this.myScope.minDate));
                if (this.myScope.maxDate)
                    isAfter = momentDate.isAfter(this.crfValidityDateToMomentDate(this.myScope.maxDate));


                var value;
                if (momentDate.isValid() && !isBefore && !isAfter) {
                    value = momentDate.format(GlobalConfig.jsonDateFormat)
                    this.myModelCtrl.$setViewValue(value);
                } else {
                    this.myScope.viewDate = null;
                    this.myModelCtrl.$setViewValue(null);
                }
            }
        }
    }
}