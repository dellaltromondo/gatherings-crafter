/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class SetFocusDirective implements ng.IDirective {

        //public restrict;
        public restrict = "A";
        public $timeout;
        public link;

        static factory($timeout): ng.IDirective {
            return new SetFocusDirective($timeout)
        }

        constructor($timeout) {
            this.$timeout = $timeout;
            this.link = (scope, element, attr) => { this.myLinker(scope, element, attr); };
        }

        myLinker(scope, elements, attrs) {
            var timeout = this.$timeout;
            scope.$watch(attrs.setFocus,
                function(newValue) {
                    timeout(function() {
                        newValue && elements[0].focus();
                        //FIXME: assuming elements is a text input
                        elements[0].selectionStart = elements[0].selectionEnd = elements[0].value.length;
                    });
                }, true);
        }
    }
}
