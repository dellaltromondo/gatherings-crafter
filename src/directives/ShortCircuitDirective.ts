/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {

    export class ShortCircuitDirective implements ng.IDirective {

        public restrict = "A";
        public require = "?ngModel";
        public scope = {
            shortCircuitFormula: "=",
            shortCircuitModel: "="
        };
        public priority = 1000;

        static factory() {
            return new ShortCircuitDirective();
        }

        link(scope: ng.IScope, element: any, attributes: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
            new ShortCircuitDirectiveLogic(scope, ngModelCtrl);
        }
    }

    class ShortCircuitDirectiveLogic {

        private myScope: ng.IScope;
        private myModelCtrl: ng.INgModelController;

        constructor(scope: ng.IScope, ngModelCtrl: ng.INgModelController) {

            this.myScope = scope;
            this.myModelCtrl = ngModelCtrl;
            this.myScope.$watch('shortCircuitFormula',
                (newValue: string, oldValue: string) => this.myWatcher(newValue, oldValue)
            );
        }

        myWatcher(newValue : any, oldValue: any) {

            let areDifferent : boolean = newValue != "NOOP" && (
                !angular.equals(newValue, oldValue)
                || (this.myModelCtrl && !angular.equals(newValue, this.myModelCtrl.$viewValue))
                || (this.myScope.shortCircuitModel && !angular.equals(newValue, this.myScope.shortCircuitModel))
            );

            if (areDifferent) {
                if (this.myModelCtrl) {
                    this.myModelCtrl.$setViewValue(newValue);
                    this.myModelCtrl.$render(); 
                } else if (this.myScope.shortCircuitModel) {
                    if (newValue == 'EMPTY_OBJECT') {
                        newValue = {};
                    }
                    if (newValue == 'EMPTY_ARRAY') {
                        newValue = [];
                    }
                    this.myScope.shortCircuitModel = newValue;
                    // this.myScope.$apply();
                }
            }
        }
    }
}