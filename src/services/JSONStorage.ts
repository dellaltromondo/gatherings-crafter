module it.marionegri.givitiweb {

    export interface IDataStorage<T> {
        read(guid : string): ng.IPromise<T>;
        create(guid : string, jsonData: T): ng.IPromise<any>;
        update(guid : string, jsonData: T): ng.IPromise<any>;
        delete(guid : string): ng.IPromise<any>;
    }

    export interface IDataStorageConstructor<T> {
        new (dataType : string, dataStorageURL : string, $q, $http): IDataStorage<T>;
    }

    export function DataStorageCreator<T>(ctor : IDataStorageConstructor<T>, dataType : string, dataStorageURL : string, $q, $http) {
        return new ctor(dataType, dataStorageURL, $q, $http);
    }
    
    export class GivitiWebJSONStorage<T> implements IDataStorage<T>  {

        constructor(
            private guidStorage: IGUIDStorage<string>,
            private STORAGE_ID: string,
            private SERVICE_URL: string,
            private $q : ng.IQService,
            private $http : ng.IHttpService
        ) {}

        public read(guid : string): ng.IPromise<T> {
            const deferred = this.$q.defer<T>();
            this.$http.get(
                this.SERVICE_URL + "GetDataStorageValue" + "?tableName=" + this.STORAGE_ID + "&guid=" + guid
            )
            .then(
                response => {
                    ("d" in <any> response.data)
                        ? deferred.resolve(JSON.parse(response.data["d"]))
                        : deferred.reject("JSONStorage received a wrong response! " + JSON.stringify(response.data))
                },
                error => deferred.reject(error)
            );
            return deferred.promise;
        }

        public create(guid : string, jsonData: T): ng.IPromise<any> {
            return this.createOrUpdate(guid, jsonData, "CreateDataStorageValue");
        }

        public update(guid : string, jsonData: T): ng.IPromise<any> {
            return this.createOrUpdate(guid, jsonData, "UpdateDataStorageValue");
        }

        private createOrUpdate(guid : string, jsonData: T, method : string) {
            return PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.post(
                    this.SERVICE_URL + method,
                    JSON.stringify({
                        tableName: this.STORAGE_ID,
                        guid: guid,
                        value: JSON.stringify(jsonData)
                    })
                )
            );
        }

        public delete(guid : string): ng.IPromise<string> {
            return PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.post(
                    this.SERVICE_URL + "RemoveDataStorageValue",
                    JSON.stringify({
                        tableName: this.STORAGE_ID,
                        guid: guid
                    })
                )
            );
        }
    }
}