module it.marionegri.givitiweb {
    
    export class RESTStorage<T> implements IDataStorage<T>  {

        constructor(
            private STORAGE_ID: string,
            private SERVICE_URL: string,
            private $q : ng.IQService,
            private $http : ng.IHttpService
        ) {}

        public read(guid : string): ng.IPromise<T> {
            return PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.get(this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid)
            );
        }

        public create(guid : string, jsonData: T): ng.IPromise<any> {
            //TODO should use PUT instead
            return this.update(guid, jsonData);
        }

        public update(guid : string, jsonData: T): ng.IPromise<any> {
            return PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.post(
                    this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid,
                    jsonData
                )
            );
        }

        public delete(guid : string): ng.IPromise<any> {
            return PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.delete(
                    this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid
                )
            );
        }

    }
}