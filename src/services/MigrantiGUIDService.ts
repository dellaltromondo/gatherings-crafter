/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {

    export class MigrantiGUIDService implements ng.IServiceProvider {

        public $inject = ['$q'];
        private currentGuid: string;

        constructor(private $q: ng.IQService) {
            this.currentGuid = GuidUtils.getEmptyGUID();
        }

        $get(): ng.IPromise<any> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            deferred.resolve(this.currentGuid);
            return deferred.promise;
        }

        clear(){
            this.currentGuid = GuidUtils.getEmptyGUID();
        }

        save(valueToBeSaved) {
            this.currentGuid = valueToBeSaved;
        }

        static factory($q: ng.IQService): MigrantiGUIDService {
            return new MigrantiGUIDService($q);
        }

    }
}
