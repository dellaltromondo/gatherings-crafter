module it.marionegri.givitiweb {

    export class ExternalStorage {
        private EXTERNAL_APPLICATION: string = "Creactive Ambulatory FollowUp";     // "givitiweb"
        private dictionary: { [culture: string] : { Key: string; Value: string }[] };
        private currentCulture : string;

        constructor(
            private $q : ng.IQService,
            private $http: ng.IHttpService,
            private SERVICE_URL_TRANSLATIONS: string,
            private SERVICE_URL_DATASTORAGE: string
        ) {}

        /** Load all translations contained inside a translationsManager block for a given culture */
        public loadTranslations(blockName: string, culture: string): ng.IPromise<any> {
            this.currentCulture = culture;
            var method = "GetLastKeysFromBlock";
            var promise = this.$http.get<any>(this.SERVICE_URL_TRANSLATIONS + method + "?applicationName=" + this.EXTERNAL_APPLICATION + "&blockName=" + blockName + "&culture=" + culture);
            var englishPromise = this.$http.get<any>(this.SERVICE_URL_TRANSLATIONS + method + "?applicationName=" + this.EXTERNAL_APPLICATION + "&blockName=" + blockName + "&culture=en-US");
            const bothPromises = this.$q.all([
                PromiseUtils.unwrapHttpPromise(this.$q, promise),
                PromiseUtils.unwrapHttpPromise(this.$q, englishPromise)
            ]).then(
                ([data, englishData ]) => {
                    this.dictionary = {};
                    this.dictionary[culture] = data[0]["d"];
                    this.dictionary["en-US"] = englishData[0]["d"];
                }
            );
            return bothPromises;
        }

        public getTranslation(key): string {
            if (this.dictionary == null || !(this.currentCulture in this.dictionary)) {
                return "";
            }
            for (var element in this.dictionary[this.currentCulture]) {
                if (this.dictionary[this.currentCulture][element]['Key'] == key) {
                    return this.dictionary[this.currentCulture][element]['Value'];
                }
            }
            for (var element in this.dictionary["en-US"]) {
                if (this.dictionary["en-US"][element]['Key'] == key) {
                    return this.dictionary["en-US"][element]['Value'];
                }
            }
            return key;
        }

        /** Save JsonData to db inside a jsonName table with a dynamically generated GUID as key */
        public saveData(jsonName: string, jsonData: any): ng.IPromise<string> {
            const method: string = "CreateDataStorageValue";
            const myGuid: string = ExternalStorage.createGUID();
            const deferred = this.$q.defer<string>();

            const listResult = this.$http.post<string>(
                this.SERVICE_URL_DATASTORAGE + method,
                JSON.stringify({ tableName: jsonName + 'List', guid: myGuid, value: '"' + PageUtilsExt.getAdmissionKey() + '"' })
            );

            var result = this.$http.post<string>(
                this.SERVICE_URL_DATASTORAGE + method,
                JSON.stringify({ tableName: jsonName, guid: myGuid, value: JSON.stringify(jsonData) })
            );
            result.then(
                data => deferred.resolve(myGuid),
                error => deferred.reject(error)
            );
            return deferred.promise;
        }

        /** Create a new GUID used to save data inside DB */
        public static createGUID(): string {
            var guid = ExternalStorage.s4() + ExternalStorage.s4() + '-' + ExternalStorage.s4() + '-' + ExternalStorage.s4() + '-' + ExternalStorage.s4() + '-' + ExternalStorage.s4() + ExternalStorage.s4() + ExternalStorage.s4();
            return guid;
        }

        /** Helper function to return an emptyGUID */
        public static getEmptyGUID(): string {
            return "00000000-0000-0000-0000-000000000000";
        }

        /** Helper function to generate the random part of a GUID */
        private static s4(): string {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
    }

    //NOTE!!!: This is a copy of PageUtils and UriUtils classes, renamed to PageUtilsExt to prevent compiler error
    export class PageUtilsExt {
        /** Searches variable in query string and returns its value */
        static getQueryVariable(variable: string): string {
            var query: string = window.location.search.substring(1); // needed to avoid question mark
            var vars: string[] = query.split('&');
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split('=');
                if (decodeURIComponent(pair[0]) == variable) {
                    return decodeURIComponent(pair[1]);
                }
            }
        }

        static getPageData(index: number) {
            var pageData = [];
            try {
                var encodedData = PageUtilsExt.getQueryVariable("data");
                var decodedData = window.atob(encodedData);
                pageData = JSON.parse(decodedData);
            } catch (e) {
                console.log("An error occurred while reading data page: the query part of the URL is malformed? Error:" + e.message);
            }
            if (index < pageData.length) {
                 return pageData[index];
            } else {
                console.log("Index " + index + " not found in URL query data");
                return null;
            }
        }

        static getAdmissionKey() {
            return PageUtilsExt.getPageData(0);
        }

        static getAge() {
            return PageUtilsExt.getPageData(1);
        }

        static getListOfLanguages() {
            var admissionKey : string = PageUtilsExt.getPageData(0);
            var nationCode : string = admissionKey.substr(admissionKey.indexOf("-") + 1, 2);
            switch (nationCode) {
                case "IT":
                    return ["it-IT", "en-US"];
                case "IL":
                    return ["he-IL", "ar-IL", "en-US"];
                case "PL":
                    return ["pl-PL", "en-US"];
                case "SI":
                    return ["sl-SL", "en-US"];
                default:
                    return ["en-US"];
            }
        }
    }
}