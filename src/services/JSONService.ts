/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/libsodium/libsodium-wrapper.d.ts' />

module it.marionegri.givitiweb {
    "use strict";

    export interface IJSONService<T> extends ng.IServiceProvider {
        $get(): Promise<T>;
        switchTo(data: string): ng.IPromise<string>;
        save(data: T, user : string, centreCode : string, crfInfo? : CrfInfo): Promise<string>;
        remove(): ng.IPromise<any>;
    }

    interface CrfInfo {
        dataClassName : string;
        dataClassVersion : string;
    }

    interface Metadata {
        user : string;
        centreCode : string;
        timestamp : Date;
        crfInfo : CrfInfo;
        crfStatus : string;
    }

    interface DataWithMetadata<T> {
        data : T;
        metadata: Metadata;
    }

    interface Key {
        id? : string,
        timestamp? : string,
        centreCode : string,
        jsonId : string,
        value : string
    }

    export class JSONService<T> implements IJSONService<T> {

        constructor(
            private $q: ng.IQService,
            private guidStorage: IGUIDStorage<string>,
            private storage: IDataStorage<DataWithMetadata<string>>,
            private keysStorage: RESTStorage<Key>,
            private patientStatus: PatientStatusService)
        {
            ;
        }

        async $get() {
            const myGuid = await this.guidStorage.$get();
            const data = this.storage.read(myGuid);
            const key = await this.keysStorage.read(myGuid);
            return this.unwrapData(await data, key && key.length > 0 ? key[0].value : null);
        }

        switchTo(newKey: string): ng.IPromise<string> {
            return this.guidStorage.update(newKey);
        }

        async save(data: T, user : string, centreCode : string, crfInfo : CrfInfo = {}) {
            let myGuid = await this.guidStorage.$get();
            if (myGuid == GuidUtils.getEmptyGUID()) {
                myGuid = GuidUtils.createGUID();
                const newKey = this.generateKey(64);
                this.keysStorage.create(myGuid, {
                    jsonId: myGuid,
                    value: newKey,
                    centreCode
                });
                const dataWithMetadata = this.wrapWithMetadata(data, newKey, user, centreCode, crfInfo);
                await this.storage.create(myGuid, await dataWithMetadata);
                await this.guidStorage.save(myGuid);
            } else {
                let myKey = await this.keysStorage.read(myGuid);
                if ( ! myKey || myKey?.length < 1 || ! myKey[0] || !myKey[0]?.value ) {
                    const newKey = this.generateKey(64);
                    myKey = [{
                        jsonId: myGuid,
                        value: newKey,
                        centreCode
                    }];
                    this.keysStorage.create(myGuid, myKey[0]);
                }
                const dataWithMetadata = this.wrapWithMetadata(data, myKey[0].value, user, centreCode, crfInfo);
                await this.storage.update(myGuid, await dataWithMetadata);
            }

            return myGuid;
        }

        remove(): ng.IPromise<string> {
            var deferred = this.$q.defer<string>();

            this.guidStorage.$get().then(
                (myGuid: string) => {
                    const result = this.storage.delete(myGuid);
                    result.then(
                        data => {
                          this.keysStorage.delete(myGuid);
                          deferred.resolve(); // If key is really removed is not relevant to response
                        },
                        error => deferred.reject(error)
                    );
                },
                error => deferred.reject(error)
            );
            return deferred.promise;
        }

        private async wrapWithMetadata(data : T, key : string, user : string, centreCode: string, crfInfo : CrfInfo) {
            return {
                data: await this.encrypt(data, key),
                metadata: {
                    user,
                    timestamp: new Date(),
                    centreCode,
                    crfInfo,
                    crfStatus: this.patientStatus.getStatus()
                }
            }
        }

        private unwrapData(dataWithMetadata : DataWithMetadata<string>, key: string) {
            const encryptedData = dataWithMetadata?.data;
            if (!encryptedData) {
                return null;
            }

            return this.decrypt(encryptedData, key);
        }

        private async encrypt(data, myKey) {
            await sodium.ready;
            let key = sodium.from_hex(myKey);
            let nonce = sodium.randombytes_buf(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
            var encrypted = sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(JSON.stringify(data), null, null, nonce, key);
            var nonce_and_ciphertext = this.concatTypedArray(Uint8Array, nonce, encrypted); //https://github.com/jedisct1/libsodium.js/issues/130#issuecomment-361399594     
            return this.u_btoa(nonce_and_ciphertext);
        }
        
        private async decrypt(data, myKey) {
            await sodium.ready;
            let key = sodium.from_hex(myKey);
            var nonce_and_ciphertext = this.u_atob(data); //converts ascii string of garbled text into binary
            let nonce = nonce_and_ciphertext.slice(0, sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/slice      
            let ciphertext = nonce_and_ciphertext.slice(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
            var result = sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(nonce, ciphertext, null, nonce, key, "text");
            return JSON.parse(result);
        }

        private u_btoa(buffer) {       //https://stackoverflow.com/a/43271130/
            var binary = [];
            var bytes = new Uint8Array(buffer);
            for (var i = 0, il = bytes.byteLength; i < il; i++) {
                binary.push(String.fromCharCode(bytes[i]));
            }
            return btoa(binary.join(""));
        }
        
        private u_atob(ascii) {        //https://stackoverflow.com/a/43271130/
            return Uint8Array.from(atob(ascii), c => c.charCodeAt(0));
        }

        private generateKey(length) {
            let output = '';
            for (let i = 0; i < length; ++i) {
                output += (Math.floor(Math.random() * 16)).toString(16);
            }
            return output;
        }

        private concatTypedArray (ResultConstructor) {
            var totalLength = 0;
          
            for (var _len = arguments.length, arrays = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
              arrays[_key - 1] = arguments[_key];
            }
          
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;
          
            try {
              for (var _iterator = arrays[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var arr = _step.value;
          
                totalLength += arr.length;
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }
          
            var result = new ResultConstructor(totalLength);
            var offset = 0;
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;
          
            try {
              for (var _iterator2 = arrays[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var _arr = _step2.value;
          
                result.set(_arr, offset);
                offset += _arr.length;
              }
            } catch (err) {
              _didIteratorError2 = true;
              _iteratorError2 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return();
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2;
                }
              }
            }
          
            return result;
          };

        static factory<T>($q: ng.IQService, guidStorage : GivitiWebGUIDStorage<string>, storage: IDataStorage<DataWithMetadata<T>>, keysStorage : RESTStorage<Key>, patientStatus : PatientStatusService): JSONService<T> {
            return new JSONService($q, guidStorage, storage, keysStorage, patientStatus);
        }
    }
}