module it.marionegri.givitiweb {

    export class PatientStatusService {
        
        private listOfVariablesByStatus : any[][];
        private listOfVariablesByName;
        private crfs;
        private vm;
        private myIndex : number;
        private thisObject;

        static listOfServices : PatientStatusService[] = [];
        static registerSubService = (s) => PatientStatusService.listOfServices.push(s);
        static unregisterSubService = (s) => {
            const index = PatientStatusService.listOfServices.indexOf(s);
            PatientStatusService.listOfServices.splice(index, 1);
        }

        constructor(isMainService: boolean) {
            this.listOfVariablesByStatus = [
                [], [], [], []
            ];
            this.listOfVariablesByName = {};
        }

        setRootVariable(crfs: any) {
            this.crfs = crfs;
        }

        setVM(vm: any) {
            this.vm = vm;
        }

        setIndex(index: number) {
            this.myIndex = index;
        }

        setThisObject(thisObject: any) {
            this.thisObject = thisObject;
        }

        registerVariable(variableName: string, visibility: string, statusLevel: number = 3) {
            this.listOfVariablesByStatus[statusLevel - 1].push([ variableName, visibility ]);
            this.listOfVariablesByName[variableName] = visibility;
        }

        isVariableMissing(variable: string, visibility: string): boolean {

            const owner = this.listOfVariablesByName.hasOwnProperty(variable)
                ? this
                : PatientStatusService.listOfServices.find( s => s.listOfVariablesByName.hasOwnProperty(variable) );
            
            if (!owner) {
                return false;
            }
            if (visibility == null) {
                visibility = owner.getVisibilityFormulaForVariable(variable);
            }
            if (!owner.evaluateVisibility(visibility, variable)) {
                return false;
            }
            const value = owner.evaluateVariable(variable);
            if (!value) {
                return true;
            }
            if (typeof(value.length) != "undefined" && value.length == 0) {
                return true;
            }
            return false;
        }

        getMissingForStatus(statusLevel : number) : any[] {
            return this.listOfVariablesByStatus[statusLevel - 1]
                .filter( ([el, vis]) => this.isVariableMissing(el, vis) )
                .map( ([el, vis]) => el );
        }

        getAllMissingForStatus(statusLevel : number) {
            let result = [];
            for (let s of PatientStatusService.listOfServices) {
                result = result.concat(s.getMissingForStatus(statusLevel));
            }
            return result;
        }

        getStatus() : number {
            for (let k = 1; k <= 4; k++) {
                if (this.getMissingForStatus(k).length > 0) {
                    return ( k - 1 );
                }
                for (let s of PatientStatusService.listOfServices) {
                    if (s.getMissingForStatus(k).length > 0) {
                        return ( k - 1 );
                    }
                }
            }

            return 4;
        }

        private evaluateVariable(variable: string) {
            return eval('this.' + variable
                .split("rootOf[saver.getGUID()]").join("crfs")
                .split("myIndex").join("this.myIndex")
                .split("thisObject").join("this.thisObject")
                .split(".").join("?.")
            );
        }

        private evaluateVisibility(visibility: string, variable: string) {
            // TODO: this could be slow, evaluate alternatives
            const parent = variable.split(".").slice(0, -1).join(".");
            if (parent && this.listOfVariablesByName.hasOwnProperty(parent)) {
                const parentsVisibility = this.evaluateVisibility(this.listOfVariablesByName[parent], parent);
                if (! parentsVisibility) {
                    return false;
                }
            }

            if ( visibility == "" ) {
                return true;
            }

            try {
                return eval(visibility
                    .split("rootOf[saver.getGUID()]").join("crfs")
                    .split('crfs.').join('this.crfs.')
                    .split('vm.').join('this.vm.')
                    .split("myIndex").join("this.myIndex")
                    .split("thisObject").join("this.thisObject")
                )
            } catch {
                return false;
            }
        }

        private getVisibilityFormulaForVariable(variable: string): string {
            if (this.listOfVariablesByName.hasOwnProperty(variable)) {
                return this.listOfVariablesByName[variable];
            }

            const owner = PatientStatusService.listOfServices.find(
                s => s.listOfVariablesByName.hasOwnProperty(variable)
            );

            if (!owner) {
                return "false";
            }

            return owner.listOfVariablesByName[variable];
        }

    }
}