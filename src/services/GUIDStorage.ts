module it.marionegri.givitiweb {

    export interface IGUIDStorage<T> {
        $get(): ng.IPromise<T>;
        save(myGuid: T): ng.IPromise<string>;
        update(dataKey: string): ng.IPromise<T>;
        remove(key: T): ng.IPromise<string>;
    }

    export interface IGUIDStorageConstructor<T> {
        new (dataKey : string, $q : ng.IQService, $http : ng.IHttpService) : IGUIDStorage<T>;
    }

    export function GuidStorageCreator<T>(ctor : IGUIDStorageConstructor<T>, dataKey : string, $q : ng.IQService, $http : ng.IHttpService) {
        return new ctor(dataKey, $q, $http);
    }


    export class NaiveGUIDStorage implements IGUIDStorage<string> {
        private currentGUID: string = GuidUtils.createGUID();

        constructor(dataKey : string, private $q : ng.IQService, private $http : ng.IHttpService) {
            if (dataKey != null) {
                this.currentGUID = dataKey;
            }
        }

        $get(): ng.IPromise<string> {
            var deferred = this.$q.defer<string>();
            deferred.resolve(this.currentGUID);
            return deferred.promise;
        }

        save(valueToBeSaved: string): ng.IPromise<string> {
            return this.update(valueToBeSaved);
        }

        update(dataKey: string): ng.IPromise<string> {
            var deferred = this.$q.defer<string>();
            this.currentGUID = dataKey;
            deferred.resolve(this.currentGUID);
            return deferred.promise;
        }

        //TODO: set some kind of deleted property and stop returning guid if that is set
        //TODO: where this is used?
        remove(dataKey: string): ng.IPromise<string> {
            var deferred = this.$q.defer<string>();
            this.currentGUID = null;
            deferred.resolve(null);
            return deferred.promise;
        }
    }

    export class GivitiWebGUIDStorage<T> implements IGUIDStorage<T> {
        private STORAGE_ID: string;
        private DATA_KEY: string;
        private DATA_SOURCE: string;
        private SERVICE_URL: string; //"http://episerver/Givitiweb/Services/DataStorageWebService.svc/"

        constructor(dataType: string, dataSource: string, serviceURL: string, dataKey: string, private $q : ng.IQService, private $http : ng.IHttpService) {
            this.STORAGE_ID = dataType;
            this.DATA_KEY = dataKey;
            this.DATA_SOURCE = dataSource;
            this.SERVICE_URL = serviceURL;
        }

        public $get(): ng.IPromise<T> {
            const result = this.$http.get<T>(this.SERVICE_URL + "GetCustomization" + "?source=" + this.DATA_SOURCE + "&type=" + this.STORAGE_ID + "&key=" + this.DATA_KEY);
            return PromiseUtils.unwrapHttpPromise<T>(this.$q, result);
        }

        public loadGuidList(): ng.IPromise<T[]> {
            const result = this.$http.get<T[]>(this.SERVICE_URL + "GetCustomizationsList" + "?source=" + this.DATA_SOURCE + "&type=" + this.STORAGE_ID + "&key=" + this.DATA_KEY);
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }

        public save(valueToBeSaved: T): ng.IPromise<string> {
            const result = this.$http.post<string>(
                this.SERVICE_URL + "SetCustomization",
                JSON.stringify({
                    source: this.DATA_SOURCE,
                    type: this.STORAGE_ID,
                    key: this.DATA_KEY,
                    text: "Dati personalizzati " + this.DATA_KEY,
                    value: valueToBeSaved
                })
            );
            return PromiseUtils.unwrapHttpPromise<string>(this.$q, result);
        }

        public create(valueToBeSaved: T): ng.IPromise<string> {
            const result = this.$http.post<string>(
                this.SERVICE_URL + "CreateCustomization",
                JSON.stringify({
                    source: this.DATA_SOURCE,
                    type: this.STORAGE_ID,
                    key: this.DATA_KEY,
                    text: "Dati personalizzati " + this.DATA_KEY,
                    value: valueToBeSaved
                })
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }

        public update(dataKey: string): ng.IPromise<T> {
            this.DATA_KEY = dataKey;
            return this.$get();
        }

        //TODO: set some kind of deleted property and stop returning guid if that is set
        public remove(valueToBeRemoved: T): ng.IPromise<string> {
            const result = this.$http.post<string>(
                this.SERVICE_URL + "RemoveCustomization",
                JSON.stringify({
                    source: this.DATA_SOURCE,
                    key: this.DATA_KEY,
                    value: valueToBeRemoved
                })
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }
    }
}