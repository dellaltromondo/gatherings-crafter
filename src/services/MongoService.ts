/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    "use strict";

    export interface IMongoService<T> extends ng.IServiceProvider {
        getDocument(condition: any, projection?: any, sorting?: any): ng.IPromise<T>;
        setDocument(condition: any, data: T): ng.IPromise<string>;
        removeDocument(condition: any, data: any): ng.IPromise<string>;
        findDocuments(condition: any, projection?: any, sorting?: any): ng.IPromise<T>;
    }

    export class MongoService<T> implements IMongoService<T> {

        public $inject = ['$q', '$http'];

        constructor(
            private $q: ng.IQService,
            private $http : ng.IHttpService,
            private collectionName: string,
            private serviceURL: string
        ) {}

        $get(): any { };


        findDocuments(condition: any, projection?: any, sorting?: any): ng.IPromise<T> {
            var deferred: ng.IDeferred<T> = this.$q.defer();
            this.loadDocuments(condition).then(
                data => deferred.resolve(data),
                error => deferred.reject(error)
            );
            return deferred.promise;
        }

        private loadDocuments(condition: any): ng.IPromise<T> {
            var method: string = "FindDocuments";
            var result = this.$http.get<T>(
                this.serviceURL + method
                + "?collectionName=" + this.collectionName
                + "&condition=" + JSON.stringify(condition),
                {
                    withCredentials: false
                }
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }

        getDocument(condition: any, projection?: any, sorting?: any): ng.IPromise<T> {
            return this.load(condition);
        }

        private load(condition: any): ng.IPromise<T> {
            var method: string = "GetDocument";
            var result = this.$http.get<T>(
                this.serviceURL + method
                + "?collectionName=" + this.collectionName
                + "&condition=" + JSON.stringify(condition),
                {
                    withCredentials: false
                }
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }

        setDocument(condition: any, data: any): ng.IPromise<string> {
            return this.save(condition, data);
        }

        private save(condition: any, document: any): ng.IPromise<string> {
            var method: string = "SetDocument";
            var result = this.$http.post<string>(
                this.serviceURL + method,
                angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition), document: JSON.stringify(document)  }),
                {
                    withCredentials: false
                }
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }

        //TODO: check the key with the internal one
        removeDocument(condition: any): ng.IPromise<string> {
           return this.remove(condition);
        }

        public remove(condition: any): ng.IPromise<string> {
            var method: string = "RemoveDocument";
            var result = this.$http.post<string>(
                this.serviceURL + method,
                angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition) }),
                {
                    withCredentials: false
                }
            );
            return PromiseUtils.unwrapHttpPromise(this.$q, result);
        }


        static factory<T>($q: ng.IQService, $http: ng.IHttpService, collectionName: string, serviceURL: string): IMongoService<T> {
            return new MongoService($q, $http, collectionName, serviceURL);
        }
    }
}