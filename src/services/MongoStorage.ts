import gvt = it.marionegri.givitiweb;

module it.marionegri.givitiweb.subti {
    
    export class MongoStorage<T> implements gvt.IDataStorage<T>  {
        
        constructor(
            private collectionName: string,
            private serviceURL: string,
            private $q: ng.IQService,
            private $http : ng.IHttpService
        ) {}

        read(guid: string): ng.IPromise<T> {
            const condition = { internalGUID : guid };
            return gvt.PromiseUtils.unwrapHttpPromise(
                this.$q, 
                this.$http.get<T>(
                    this.serviceURL + "/GetDocument"
                    + "?collectionName=" + this.collectionName
                    + "&condition=" + JSON.stringify(condition)
                )
            );
        }        
        
        create(guid: string, jsonData: T): ng.IPromise<any> {
            return this.update(guid, jsonData);
        }
        
        update(guid: string, jsonData: T): ng.IPromise<any> {
            const condition = { internalGUID : guid };
            const document = angular.copy(jsonData);
            document['internalGUID'] = guid;
            return gvt.PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.post<string>(
                    this.serviceURL + "/SetDocument",
                    angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition), document: JSON.stringify(document)  })
                )
            );
        }
        
        delete(guid: string): ng.IPromise<any> {
            const condition = { internalGUID : guid };
            return gvt.PromiseUtils.unwrapHttpPromise(
                this.$q,
                this.$http.post<string>(
                    this.serviceURL + "/RemoveDocument",
                    angular.toJson({ collectionName: this.collectionName, condition: condition })
                )    
            );
        }

    }
}
