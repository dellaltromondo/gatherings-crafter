module it.marionegri.givitiweb {

    export class TranslationStorage<T> {

        constructor(
            private SERVICE_URL: string,
            private $q : ng.IQService,
            private $http : ng.IHttpService
        ) {}

        public getTranslationsValues(application: string, block: string, culture: string): ng.IPromise<T> {
            var method = "GetTranslationsValues";
            var result = this.$http.get<T>(
                this.SERVICE_URL + method + "?application=" + application + "&block=" + block + "&culture=" + culture,
                {
                    withCredentials: false
                }
            );
            return PromiseUtils.unwrapHttpPromise<T>(this.$q, result);
        }

    }
}