/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    "use strict";

    export class MultipleJSONService<T> implements IJSONService<IDictionaryOf<T>> {
        private jsonStorage: GivitiWebJSONStorage<T>;
        public $inject = ['$q', '$http', 'guidStorage'];

        constructor(
            private $q: ng.IQService,
            $http: ng.IHttpService,
            private guidStorage: IGUIDStorage<string>,
            storageId: string,
            serviceURL: string,
            private listOfKeys: string[]
        ) {
            this.jsonStorage = new GivitiWebJSONStorage(guidStorage, storageId, serviceURL, $q, $http);
        }

        $get(): ng.IPromise<IDictionaryOf<T>> {
            var deferred: ng.IDeferred<IDictionaryOf<T>> = this.$q.defer();
            var arrayOfPromises: ng.IPromise<T>[] = [];
            var results: IDictionaryOf<T> = {};

            if (this.listOfKeys) {
                for (var keyIdx in this.listOfKeys) {
                    var key: string = this.listOfKeys[keyIdx];
                    this.guidStorage.update(key);
                    var promise = this.jsonStorage.read(this.guidStorage.$get());
                    var callback = (key: string, data: T) => { results[key] = data; };
                    promise.then(data => callback.bind(this, key));
                    arrayOfPromises.push(promise);
                }
            }
            this.$q.all(arrayOfPromises).then(
                () => deferred.resolve(results),
                error => angular.equals(results, {}) ? deferred.reject(error) : deferred.resolve(results)
            );
            return deferred.promise;
        }

        switchTo(newListOfKeys: string[]): ng.IPromise<IDictionaryOf<T>> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            this.listOfKeys = newListOfKeys;
            deferred.resolve();
            return deferred.promise;
        }

        save(data: IDictionaryOf<T>): ng.IPromise<string[]> {
            var deferred: ng.IDeferred<string[]> = this.$q.defer();
            var arrayOfPromises: ng.IPromise<string>[] = [];
            var results: string[] = [];

            for (var keyIdx in this.listOfKeys) {
                let key: string = this.listOfKeys[keyIdx];
                this.guidStorage.update(key);
                const promise = this.jsonStorage.save(data[key]).then(
                    newKey => {
                        results.push(newKey);
                        return newKey
                    }
                );
                arrayOfPromises.push(promise);
            }
            this.$q.all(arrayOfPromises).then(
                () => deferred.resolve(results),
                (error) => { 
                    angular.equals(results, [])
                        ? deferred.reject(error)
                        : deferred.resolve(results)
                }
            );
            return deferred.promise;
        }

        remove(keysList: string[]): ng.IPromise<any> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            var arrayOfPromises: ng.IPromise<any>[] = [];
            keysList = (angular.equals(keysList, [])) ? this.listOfKeys : keysList;

            for (var keyIdx in keysList) {
                var key: string = keysList[keyIdx];
                this.guidStorage.update(key);
                var promise = this.jsonStorage.remove().then(
                    () => this.guidStorage.remove(key)
                );
                arrayOfPromises.push(promise);
            }
            this.$q.all(arrayOfPromises).then(
                () => deferred.resolve(),
                error => deferred.reject(error)
            );
            return deferred.promise;
        }

        static factory<T>($q: ng.IQService, $http : ng.IHttpService, guidStorage: IGUIDStorage<string>, storageId: string, dataStorageURL: string, listOfKeys: string[]): MultipleJSONService<T> {
            return new MultipleJSONService<T>($q, $http, guidStorage, storageId, dataStorageURL, listOfKeys);
        }
    }
}
