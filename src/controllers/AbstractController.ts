/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export interface IAbstractStatus {
        dataLoading?: boolean;
        dataLoaded?: boolean;
        dataReadOnly?: boolean;
        dataModified?: boolean;
        dataSaved: boolean;
        statusBarMessage?: string;
    }

    export interface IGenericModel extends ng.IScope {
        currentCrfPath: string;
        crfs: any;
        oldCRFS: any;
        list: any;
        status: IAbstractStatus;
        vm: IGenericController;
    }

    export interface IGenericController {
        loadJsonData(): Promise<any>;
        saveJsonData(): Promise<any>;
        clearJsonData(): void;
        fillData(crfData: any, data: any): void;
        clearData(crfData: any): void;
    }

    export abstract class AbstractController implements IGenericController {
        public $inject = ['$q', '$scope', '$timeout', 'jsonService'];

        constructor(
            protected $q: ng.IQService,
            protected $scope: IGenericModel,
            protected $timeout: ng.ITimeoutService,
            protected jsonService: IJSONService<any>,
            protected userId: string,
            protected centreCode: string
        ) {
            this.$scope.crfs = {};
            this.$scope.status = { dataLoaded: false, dataReadOnly: false, dataModified: false, dataSaved: false, statusBarMessage: '' };
            this.$scope.saver = this;
        }

        /** Load data from jsonService, bind to $scope.crfs and return the loaded data */
        async loadJsonData() {
            this.$scope.status.dataLoaded = false;
            this.$scope.status.dataLoading = true;
            const data = await this.jsonService.$get()
            this.fillData(this.$scope.crfs, data);
            this.$scope.oldCRFS = angular.copy(data);
            this.$scope.status.dataLoaded = true;
            this.$scope.status.dataLoading = false;
            this.$scope.$apply();
            return data;
        }

        /** Save crf in jsonStorage and set status.dataSaved afterwards */
        async saveJsonData() {
            await this.jsonService.save(this.$scope.crfs, this.userId, this.centreCode)
            this.$scope.status.dataModified = false;
            this.$scope.status.dataSaved = true;
            this.$scope.$apply();
            var changeVariableStatus = () => {
                this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
            };
            this.$timeout(changeVariableStatus, 4000);
        }

        /** Clean all the loaded data in $scope.crfs */
        clearJsonData(): void {
            //TODO: probably this is detaching all two-way bindings!
            this.$scope.crfs = {};
        }

        /** Get current guid from listData using $scope informations to iterate through it */
        getGuidFromList(listData: any[]): string {
            throw new Error('This method is abstract');
        }

        /** Add the specified guid to listData using $scope informations to populate it */
        addGuidToList(listData: any[], guid: string): void {
            throw new Error('This method is abstract');
        }

        /** Fill the specified data inside $scope.crfs cleaning up all dead object */
        fillCRFS(crfData: any): void {
            var newCrfs = angular.copy(crfData);
            this.clearData(this.$scope.crfs);
            this.removeDeadObjects(this.$scope.crfs, newCrfs);
            this.removeEmptyObjectsFromSets(this.$scope.crfs);
            this.fillData(this.$scope.crfs, newCrfs);
        }

        /** Fill the originally loaded data inside $scope.crfs cleaning up all modified values */
        rollbackCRFS(): void {
            this.fillCRFS(this.$scope.oldCRFS);
        }

        /** Fill crfData values using data */
        fillData(crfData: any, data: any): void {
            for (var obj in data) {
                if (typeof (data[obj]) == "object") {
                    if (obj in crfData) {
                        this.fillData(crfData[obj], data[obj]);
                    } else {
                        crfData[obj] = data[obj];
                    }
                } else {
                    crfData[obj] = data[obj];
                }
            }
        }

        /** Empties all crfData values keeping its structure */
        clearData(crfData: any): void {
            for (var obj in crfData) {
                if (typeof (crfData[obj]) == "object") {
                    if (!angular.equals(crfData[obj], {})) {
                        this.clearData(crfData[obj]);
                    }
                    else if (Object.prototype.toString.call(crfData) === '[object Array]') {
                        crfData.splice(obj, 1);
                    }
                }
                else {
                    crfData[obj] = undefined;
                }
            }
        }

        /** Empties crfData values that are not present in data */
        removeDeadObjects(crfData: any, data: any) {
            for (var obj in crfData) {
                if (data) {
                    if (typeof (crfData[obj]) == "object") {
                        if (obj in data) {
                            this.removeDeadObjects(crfData[obj], data[obj]);
                        } else if (Object.prototype.toString.call(crfData[obj]) === '[object Array]') {
                            crfData[obj] = [];
                        } else {
                            crfData[obj] = {};
                        }
                    } else {
                        if (!(obj in data)) {
                            crfData[obj] = undefined;
                        }
                    }
                }
            }
        }

        /** Empties crfData sets that contains empty values */
        removeEmptyObjectsFromSets(crfData: any) {
            var shouldRemove = [];
            for (var obj in crfData) {
                if ((crfData !== null && Object.prototype.toString.call(crfData) === '[object Array]')
                    && (crfData[obj] === null || crfData[obj] === undefined)) {
                    shouldRemove.push(obj);
                } else if ((typeof (crfData[obj]) == "object")
                    && crfData[obj] !== null
                    && crfData[obj] !== undefined) {
                    if (Object.getOwnPropertyNames(crfData[obj]).length > 0) {
                        this.removeEmptyObjectsFromSets(crfData[obj]);
                    } else {
                        if (Object.prototype.toString.call(crfData) === '[object Array]') {
                            shouldRemove.push(obj);
                        }
                    }
                }
            }
            shouldRemove.sort((a, b) => {
                if (parseInt(a) > parseInt(b))
                    return 1;
                else if (parseInt(a) < parseInt(b))
                    return -1;
                else
                    return 0;
            });
            shouldRemove.reverse();
            for (var i in shouldRemove) {
                crfData.splice(shouldRemove[i], 1);
            }
        }

    }
}