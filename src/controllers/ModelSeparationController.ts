/// <reference path='../definitions/angular/angular.d.ts' />

/// <reference path='../../src/utils/GlobalUtils.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class ModelSeparationController extends AbstractController {
        public $inject = ['$scope'];

        //TODO: use DataClass directive instead?
        private guid : string = GuidUtils.createGUID();

        constructor($q, $scope, $timeout, $element, storage, keysStorage, userId, centreCode, patientStatus) {
            super($q, $scope, $timeout, new JSONService(
                $q, new NaiveGUIDStorage(null, $q, null), storage, keysStorage, patientStatus
            ), userId, centreCode);
            Controller.registerModelSeparationController(this);

            // Adding a watch to load data when this is visible
            this.loadIfVisible($timeout, $element);
            $scope.$on("PageChanged", _ => this.loadIfVisible($timeout, $element));
        }

        loadIfVisible($timeout, $element) {
            $timeout(_ => {
                if (!this.$scope.status.dataLoaded && $element[0].offsetWidth > 0 && $element[0].offsetHeight > 0) {
                    this.loadJsonData();
                }
            }, 1);
        }

        getGUID() {
            return this.guid;
        }

        setGUID(guid : string) {
            if (guid != null) {
                this.guid = guid;
                this.jsonService.switchTo(guid);
            }
        }
    }
}