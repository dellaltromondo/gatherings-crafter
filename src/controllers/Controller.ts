/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    enum PageMode {
        LIST,
        PATIENT
    }

    export class Controller extends AbstractController implements IGenericController {
        public $inject = ['$q', '$http', '$scope', '$timeout', 'jsonService', 'crfServiceURL', 'dataType', 'dataPath'];

        private versionManager: CRFVersionsManager;
        
        static listOfModelSeparationControllers = [];
        static registerModelSeparationController(c) { this.listOfModelSeparationControllers.push(c); }

        constructor(
            protected $q: ng.IQService,
            protected $http: ng.IHttpService,
            protected $scope: IGenericModel,
            protected $rootScope: ng.IRootScopeService,
            protected $timeout: ng.ITimeoutService,
            protected jsonService: IJSONService<any>,
            protected patientStatus: PatientStatusService,
            protected userId: string,
            protected centreCode: string,
            crfServiceURL: string,
            dataType: string,
            dataPath: string,
            dataSynchronizer : DataSynchronizer /* Necessary to start DataSynch */
        ) {

            super($q, $scope, $timeout, jsonService, userId, centreCode);
            //this.versionManager = new CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
            //this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);

            this.loadJsonData();

            this.$scope.patientStatus = patientStatus;
            this.$scope.PatientStatusService = PatientStatusService;

            this.$scope.pageMode = PageMode.LIST;
            this.$scope.PageMode = PageMode;

            this.$rootScope.$on("PatientCreated", _ => this.$scope.pageMode = PageMode.PATIENT);
            this.$rootScope.$on("PatientSwitched", _ => {
                this.$scope.pageMode = PageMode.PATIENT;
                this.loadJsonData();
            });

        }

        public saveJsonData() {
            for (let c of Controller.listOfModelSeparationControllers) {
                c.saveJsonData();
            }
            const result = super.saveJsonData();

            // this.$rootScope.$emit("PatientSaved");
            this.$rootScope.$broadcast("PatientSaved");

            return result;
        }

        public closePatient() {
            this.$scope.pageMode = PageMode.LIST;
            this.$scope.crfs = {};
        }
    }
}