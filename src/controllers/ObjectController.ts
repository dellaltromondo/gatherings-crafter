/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    interface ObjectScope extends ng.IScope {
        completenessController : PatientStatusService;
        thisObject : any;
    }

    export class ObjectController {

        constructor(
            private $scope : ObjectScope
        ) {
            const myCompletenessController = new PatientStatusService();
            this.$scope.completenessController = myCompletenessController;
            PatientStatusService.registerSubService(myCompletenessController);
            this.$scope.$on('$destroy', () => {
                PatientStatusService.unregisterSubService(myCompletenessController);
            });
        }

        $postLink() {
            this.$scope.completenessController.setRootVariable(this.$scope.crfs);
            this.$scope.completenessController.setIndex(this.$scope.myIndex);
            this.$scope.completenessController.setThisObject(this.$scope.thisObject);
        }

    }
}