/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export class RelationsController {
        public $inject = ['$scope'];

        constructor($scope: ng.IScope) {
            $scope["vm"] = this;
        }

        modifyRelationship(elements, parentGuid, guid) {
            if (!(parentGuid in elements)) {
                elements[parentGuid] = [];
            }
            if (!(guid in elements)) {
                elements[guid] = [];
            }
            if (elements[parentGuid].indexOf(guid) > -1) {
                var index = elements[parentGuid].indexOf(guid);
                elements[parentGuid].splice(index, 1);
            }
            else {
                elements[parentGuid].push(guid);
            }
            if (elements[guid].indexOf(parentGuid) > -1) {
                var index = elements[guid].indexOf(parentGuid);
                elements[guid].splice(index, 1);
            }
            else {
                elements[guid].push(parentGuid);
            }
        }

        verifyRelationship(elements, parentGuid, guid) {
            var checked: boolean = false;
            if (parentGuid in elements) {
                if (elements[parentGuid].indexOf(guid) > -1) {
                    checked = true;
                }
            }
            return checked;
        }

    }
}