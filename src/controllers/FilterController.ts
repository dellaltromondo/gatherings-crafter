/// <reference path='../definitions/angular/angular.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    interface IQuery {
        title: string;
        json: any;
    }

    export interface IFilterRootModel extends ng.IRootScopeService {
        visibilityConfig: IDictionaryOf<any>;
    }

    export interface IFilterModel extends IGenericModel {
        selectedItem: string;
        addingQuery: boolean;
        updatingQuery: boolean;
        duplicatedAlert: boolean;
        guidList: string[];
        hiddenFieldValue: string;
        jsonQueries: IDictionaryOf<IQuery>;
    }

    export class FilterController extends AbstractController implements IGenericController {
        public $inject = ['$q', '$scope', '$rootScope', '$http', '$window', 'queriesListGUIDStorage', 'multipleQueriesJSONService', 'defaultVisibility'];

        constructor(
            protected $q: ng.IQService,
            public $scope: IFilterModel,
            public $rootScope: IFilterRootModel,
            private $http: ng.IHttpService,
            private $window: ng.IWindowService,
            private queriesListGUIDStorage: GivitiWebGUIDStorage<string>,
            //private translationService: TranslationService,
            private multipleQueriesJSONService: MultipleJSONService<any>,
            private defaultVisibility: IDictionaryOf<any>
        ) {

            super(null, $scope, null, null);
            this.$scope.selectedItem = null;
            this.$scope.addingQuery = false;
            this.$scope.updatingQuery = false;
            this.$scope.duplicatedAlert = false;
            this.$rootScope.visibilityConfig = defaultVisibility;

            if (document.querySelector("[id$='HiddenJSON']")[0]["value"]) {
                var parsedJSON = JSON.parse(document.querySelector("[id$='HiddenJSON']")[0]["value"]);
                this.fillData(this.$scope.crfs, parsedJSON);
            }
            this.loadJsonData();
        }

        //UGLY
        onBtnFilterClick(buttonClientID: string, json: any, startingTable: string, startingId: string) {
            var qbm = new QueryBuilderManager();
            this.$scope.hiddenFieldValue = qbm.buildQuery(json, startingTable, startingId, true);
            document.querySelector("[id$='HiddenSQL']")[0]["value"] = this.$scope.hiddenFieldValue;
            document.querySelector("[id$='HiddenJSON']")[0]["value"] = JSON.stringify(this.$scope["crfs"]);
            this.$window["__doPostBack"](buttonClientID, '');
        }

        updateQuery(id: string, title: string) {
            this.$scope.duplicatedAlert = this.isTitleDuplicated(title, id);
            if (!this.$scope.duplicatedAlert) {
                this.$scope.jsonQueries[id].title = title;
                this.$scope.jsonQueries[id].json = angular.copy(this.$scope.crfs);
                this.saveMultipleQueries(this.$scope.guidList).then((newGuidList: string[]) => {
                    this.loadJsonData();
                });
            }
        }

        selectQuery(id: string) {
            this.$scope.selectedItem = id;
        }

        showQuery(id: string) {
            this.$scope.crfs = {};
            this.fillData(this.$scope.crfs, angular.copy(this.$scope.jsonQueries[id].json));
        }

        deleteQuery(id: string) {
            this.multipleQueriesJSONService.remove([id])
                .then(() => {
                    this.queriesListGUIDStorage.remove(id)
                        .then(() => {
                            delete this.$scope.guidList[id];
                            delete this.$scope.jsonQueries[id];
                            this.loadJsonData();
                        });
                });
        }

//        translationsAreLoaded(): boolean {
//            return this.translationService.translationsAreLoaded();
//        }

        private isTitleDuplicated(title: string, guid?: string): boolean {
            for (var currentGuid in this.$scope.jsonQueries) {
                if (guid && currentGuid == guid)
                    continue;
                if (this.$scope.jsonQueries[currentGuid].title.toUpperCase() == title.toUpperCase()) {
                    return true;
                }
            }
            return false;
        }

        private getQueryGUIDList(): ng.IPromise<string[]> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            this.queriesListGUIDStorage.loadGuidList()
                .then((guidList: string[]) => {
                    deferred.resolve(guidList);
                });
            return deferred.promise;
            /* var method: string = "GetCustomizationsList";
             var deferred: ng.IDeferred<any> = this.$q.defer();
             var query = '?source=account&type=' + this.queryDataType + '&key=' + this.dataKey;
             this.$http.get(this.dataStorageURL + method + query)
                 .then((data: any) => {
                     deferred.resolve(JSON.parse(data.data.d));
                 });
             return deferred.promise;*/
        }

        private createQueryCustomization(guid: string): ng.IPromise<any> {
            var deferred: ng.IDeferred<any> = this.$q.defer();
            this.queriesListGUIDStorage.create(guid)
                .then(() => {
                    deferred.resolve("");
                });
            return deferred.promise;
            /*var method: string = "CreateCustomization";
            var deferred: ng.IDeferred<any> = this.$q.defer();
            var data: string = JSON.stringify({ 'source': 'account', 'type': this.queryDataType, 'key': this.dataKey, 'text': 'userQuery', value: guid });
            this.$http.post(this.dataStorageURL + method, data)
                .then((data: any) => {
                    deferred.resolve("");
                });
            return deferred.promise;*/
        }

        private selectQueryIdForCurrentlyLoadedJson() {
            for (var key in this.$scope.jsonQueries) {
                if (angular.equals(this.$scope.jsonQueries[key].json, this.$scope.crfs)) {
                    this.selectQuery(key);
                    break;
                }
            }
        }

        loadJsonData(): ng.IPromise<IDictionaryOf<any>> {
            var deferred: ng.IDeferred<IDictionaryOf<any>> = this.$q.defer();
            this.getQueryGUIDList().then((guidList: string[]) => {
                this.multipleQueriesJSONService.switchTo(guidList)
                    .then(() => {
                        this.multipleQueriesJSONService.$get()
                            .then((data: IDictionaryOf<any>) => {
                                if (!this.$scope.jsonQueries) {
                                    this.$scope.jsonQueries = {};
                                    this.$scope.guidList = [];
                                }
                                this.$scope.jsonQueries = angular.copy(data);
                                this.selectQueryIdForCurrentlyLoadedJson();
                                this.$scope.guidList = guidList;
                                deferred.resolve(data);
                            });
                    });
            });
            return deferred.promise;
        }

        saveCurrentQuery(title: string) {
            this.$scope.duplicatedAlert = this.isTitleDuplicated(title);
            if (!this.$scope.duplicatedAlert) {
                var guid = GuidUtils.getEmptyGUID();
                this.$scope.guidList.push(guid);
                this.$scope.jsonQueries[guid] = { title: title, json: angular.copy(this.$scope.crfs) };
                this.saveMultipleQueries(this.$scope.guidList).then((newGuidList: string[]) => {
                    for (var index in newGuidList) {
                        if (this.$scope.guidList.indexOf(newGuidList[index]) == -1) {
                            guid = newGuidList[index];
                            break;
                        }
                    }
                    this.createQueryCustomization(guid).then(() => {
                        this.loadJsonData();
                    });
                });
            }
        }

        saveMultipleQueries(queriesGuidsList: string[]): ng.IPromise<string[]> {
            var deferred: ng.IDeferred<string[]> = this.$q.defer();
            this.multipleQueriesJSONService.switchTo(queriesGuidsList)
                .then(() => {
                    this.multipleQueriesJSONService.save(this.$scope.jsonQueries)
                        .then((data: string[]) => {
                            deferred.resolve(data);
                        });
                });
            return deferred.promise;
        }
    }
}