/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />

module it.marionegri.givitiweb {
    'use strict';

    export interface ApplicationConfig {

        userId : string;
        centreCode : string;

        // Storage
        dataStorageType : IDataStorageConstructor<any>;
        guidStorageType : IGUIDStorageConstructor<any>;
        dataStorageURL : string;
        keysStorageURL : string;
        dataType : string;
        dataKey : string;

        ccDataStorageURL : string;
        ccDataCollection : string;
        secretVariables : string[];
        
        // CRFs
        crfServiceURL : string;
        dataPath : string;

        // Translations config
        translationsURL : string;
        applicationName : string;
        blockName : string;
        culture : string;

        needsAuthorizations: boolean;
    }
    
    export class CollectionBaseApplication {

        protected angularApp : ng.IModule = null;

        constructor (config : ApplicationConfig, dependencies = ['ngSanitize', 'pascalprecht.translate']) {
            
            this.angularApp = angular.module('it.marionegri.givitiweb.AngularCrafter', dependencies);

            // Non angular inject
            //TODO: remove?
            this.angularApp.value('dataType', config.dataType); // Get from dataclass?
            this.angularApp.value('crfServiceURL', config.crfServiceURL);
            this.angularApp.value('dataPath', config.dataPath);
            this.angularApp.value('userId', config.userId);
            this.angularApp.value('centreCode', config.centreCode);

            // Services

            // TODO: generalize type checking?
            // Duck checking dataStorageType
            const storageClass = (
                    config.dataStorageType
                    && config.dataStorageType.prototype.create
                    && config.dataStorageType.prototype.delete
                    && config.dataStorageType.prototype.read
                    && config.dataStorageType.prototype.update
                )
                ? config.dataStorageType
                : RESTStorage
            ;

            const guidStorageClass = (
                    config.guidStorageType
                    && config.guidStorageType.prototype.$get
                    && config.guidStorageType.prototype.save
                    && config.guidStorageType.prototype.update
                    && config.guidStorageType.prototype.remove
                )
                ? config.guidStorageType
                : NaiveGUIDStorage
            ;

            this.angularApp.factory('storage', ($q, $http) => DataStorageCreator(storageClass, config.dataType, config.dataStorageURL, $q, $http));
            this.angularApp.factory('keysStorage', ($q, $http) => new RESTStorage('keys', config.keysStorageURL, $q, $http));
            this.angularApp.factory('guidStorage', ($q, $http) => GuidStorageCreator(guidStorageClass, config.dataKey, $q, $http) /* TODO new GUIDStorage(eval("dataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"), $q, $http)*/);

            this.angularApp.factory('listStorage', ($q, $http) => DataStorageCreator(storageClass, '__patientsList__', config.dataStorageURL, $q, $http));
            this.angularApp.factory('listGuidStorage', ($q, $http) => new NaiveGUIDStorage(config.centreCode, $q, $http));

            this.angularApp.factory('deletedListStorage', ($q, $http) => DataStorageCreator(storageClass, '__deletedPatientsList__', config.dataStorageURL, $q, $http));
            this.angularApp.factory('deletedListGuidStorage', ($q, $http) => new NaiveGUIDStorage(config.centreCode, $q, $http));

            this.angularApp.factory('dataSynchStorage', ($q, $http) => DataStorageCreator(storageClass, '__dataSynch__', config.dataStorageURL, $q, $http));
            this.angularApp.factory('dataSynchGuidStorage', ($q, $http) => new NaiveGUIDStorage(config.centreCode, $q, $http));

            this.angularApp.factory('translationsStorage', ($q, $http) => new TranslationStorage<string>(config.translationsURL, $q, $http));
            this.angularApp.factory('patientStatus', () => new PatientStatusService(true));
            this.angularApp.factory('jsonService', ($q, guidStorage, storage, keysStorage, patientStatus) => JSONService.factory($q, guidStorage, storage, keysStorage, patientStatus));

            this.angularApp.factory('ccDataStore', ($q, $http) => new MongoService($q, $http, config.ccDataCollection, config.ccDataStorageURL));

            this.angularApp.factory('listService', ($q, listGuidStorage, listStorage, keysStorage, patientStatus) => JSONService.factory($q, listGuidStorage, listStorage, keysStorage, patientStatus)); //TODO: remove patientStatus
            this.angularApp.factory('deletedListService', ($q, deletedListGuidStorage, deletedListStorage, keysStorage, patientStatus) => JSONService.factory($q, deletedListGuidStorage, deletedListStorage, keysStorage, patientStatus)); //TODO: remove patientStatus
            this.angularApp.factory('dataSynchService', ($q, dataSynchGuidStorage, dataSynchStorage, keysStorage, patientStatus) => JSONService.factory($q, dataSynchGuidStorage, dataSynchStorage, keysStorage, patientStatus));
            this.angularApp.factory('dataSynchronizer', (jsonService, listService, dataSynchService, ccDataStore, centreCode, $q, $interval) => new DataSynchronizer(jsonService, listService, dataSynchService, ccDataStore, centreCode, $q, $interval, config.secretVariables) );
            
            this.angularApp.factory('translationLoader', ($q: ng.IQService, translationsStorage) => {
                return (options) => {
                    var deferred: ng.IDeferred<any> = $q.defer();
                    translationsStorage.getTranslationsValues(options.application, options.block, options.key).then(
                        data => deferred.resolve(data),
                        error => deferred.reject(options.key)
                    );
                    return deferred.promise;
                };
            });

            // Controllers
            this.angularApp.controller('AngularCrafterController', Controller);
            this.angularApp.controller('MyObjectController', ObjectController);
            this.angularApp.controller('ModelSeparationController', ModelSeparationController);

            //TODO: remove
            this.angularApp.controller('RoomsRelationsController', RelationsController);

            //Directives
            this.angularApp.directive('datePicker', DatePickerDirective.factory);
            this.angularApp.directive('tooltip', TooltipDirective.factory);
                
            this.angularApp.directive('ic3DataCollection', DataCollectionDirective.factory);
            this.angularApp.directive('ic3DataClass', DataClassDirective.factory);
            this.angularApp.directive('ic3PatientsList', PatientsListDirective.factory);
            this.angularApp.directive('shortCircuitFormula', ShortCircuitDirective.factory);
            this.angularApp.directive("dateFormatter", () => {
                return {
                    require: 'ngModel',
                    link: (scope, elem, attr, modelCtrl) => {
                        modelCtrl.$parsers = [value => {
                            modelCtrl.$viewValue = moment(value).format()
                            return value;
                        }];
                        modelCtrl.$formatters.push(modelValue => modelValue != null ? new Date(modelValue) : null);
                    }
                }
            });
            // Prevent time to be saved as date
            // TODO: verify if it's better to save as string, with input template
            this.angularApp.directive("timeFormatter", () => {
                return {
                    require: 'ngModel',
                    link: (scope, elem, attr, modelCtrl) => {
                        modelCtrl.$parsers = [value => {
                            return value;
                        }];
                        modelCtrl.$formatters.push(modelValue => modelValue);
                    }
                }
            });

            //Filters
            this.angularApp.filter('formatDate', () => { return (date: string) => { return moment(date, GlobalConfig.jsonDateFormat).format(GlobalConfig.viewDateFormat) } });

            // Configs
            this.angularApp.config( ($translateProvider: ngt.ITranslateProvider) => {
                var options: any = { application: config.applicationName, block: config.blockName };
                $translateProvider.useLoader('translationLoader', options)
                    .preferredLanguage(config.culture)
                    .useSanitizeValueStrategy('escape');
            });

            if (config?.needsAuthorizations) {
                this.angularApp.config( ( $httpProvider : ng.IHttpProvider ) => {
                    $httpProvider.defaults.withCredentials = true;
                });
            }
        }

        static create(config, dependencies = ['ngSanitize', 'pascalprecht.translate']) {
            return new CollectionBaseApplication(config, dependencies);
        }
    }
}