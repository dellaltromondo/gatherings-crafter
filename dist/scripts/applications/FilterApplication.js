/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.FilterApplication', ['ngTagsInput', 'pascalprecht.translate']);
            // Storage
            var queriesListGUIDStorage = new givitiweb.GivitiWebGUIDStorage(eval("queryDataType"), eval("dataKey"), "account", eval("dataStorageURL"));
            var queriesGUIDStorage = new givitiweb.NaiveGUIDStorage();
            // Non angular inject
            angularApp.value('queriesListGUIDStorage', queriesListGUIDStorage);
            angularApp.value('suggestionURL', eval("suggestionURL"));
            angularApp.value('templateUrl', eval("templateURL"));
            angularApp.value('defaultVisibility', eval("defaultVisibility"));
            angularApp.value('culture', eval("culture"));
            // Services
            angularApp.factory('multipleQueriesJSONService', ($q) => {
                return givitiweb.MultipleJSONService.factory($q, queriesGUIDStorage, eval("queryDataType"), eval("dataStorageURL"), null);
            });
            // Controllers
            angularApp.controller('FilterController', givitiweb.FilterController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            // Directives
            angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
            angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
            angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
            angularApp.directive('ic3Autocomplete', givitiweb.AutocompleteDirective.factory);
            // Filters
            angularApp.filter('formatDate', () => { return (date) => { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
            // Configs
            angularApp.config(['$translateProvider', ($translateProvider) => {
                    var filterTranslations = {
                        "from_vw_Users_to_vw_Contacts": "Contatti Utente",
                        "from_vw_Centres_to_vw_Contacts": "Contatti Centro",
                        "from_vw_Subjects_to_vw_Contacts": "Contatti Esterno",
                        "from_vw_Users_to_vw_Roles_UCP": "Ruolo nel centro per un progetto",
                        "from_vw_Centres_to_vw_Roles_UCP": "Ruolo degli utenti per un progetto",
                        "from_vw_Projects_to_vw_Roles_UCP": "Ruolo degli utenti in un centro",
                        "from_vw_Contacts_to_vw_Users": "Utenti con questo contatto",
                        "from_vw_Contacts_to_vw_Centres": "Centri con questo contatto",
                        "from_vw_Contacts_to_vw_Subjects": "Esterni con questo contatto",
                        "from_vw_Roles_UCP_to_vw_Users": "Dettagli Utente",
                        "from_vw_Roles_UCP_to_vw_Centres": "Dettagli Centro",
                        "from_vw_Roles_UCP_to_vw_Projects": "Dettagli Progetto",
                        "from_vw_Users_to_vw_Roles_UC": "Afferenza al centro",
                        "from_vw_Roles_UC_to_vw_Users": "Dettagli dell'utente ",
                        "from_vw_Roles_UC_to_vw_Centres": "Dettagli centro",
                        "from_vw_Centres_to_vw_Roles_UC": "Utenti afferenti",
                        "from_vw_Centres_to_vw_Roles_CP": "Progetti del centro",
                        "from_vw_Roles_CP_to_vw_Centres": "Dettagli centro",
                        "from_vw_Roles_CP_to_vw_Projects": "Dettagli progetto",
                        "from_vw_Projects_to_vw_Roles_CP": "Centri partecipanti"
                    };
                    $translateProvider.translations("it-IT", filterTranslations)
                        .preferredLanguage("it-IT")
                        .useSanitizeValueStrategy('escape');
                }]);
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
