/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class CollectionBaseApplication {
                constructor(config, dependencies = ['ngSanitize', 'pascalprecht.translate']) {
                    this.angularApp = null;
                    this.angularApp = angular.module('it.marionegri.givitiweb.AngularCrafter', dependencies);
                    // Non angular inject
                    //TODO: remove?
                    this.angularApp.value('dataType', config.dataType); // Get from dataclass?
                    this.angularApp.value('crfServiceURL', config.crfServiceURL);
                    this.angularApp.value('dataPath', config.dataPath);
                    this.angularApp.value('userId', config.userId);
                    this.angularApp.value('centreCode', config.centreCode);
                    // Services
                    // TODO: generalize type checking?
                    // Duck checking dataStorageType
                    const storageClass = (config.dataStorageType
                        && config.dataStorageType.prototype.create
                        && config.dataStorageType.prototype.delete
                        && config.dataStorageType.prototype.read
                        && config.dataStorageType.prototype.update)
                        ? config.dataStorageType
                        : givitiweb.RESTStorage;
                    const guidStorageClass = (config.guidStorageType
                        && config.guidStorageType.prototype.$get
                        && config.guidStorageType.prototype.save
                        && config.guidStorageType.prototype.update
                        && config.guidStorageType.prototype.remove)
                        ? config.guidStorageType
                        : givitiweb.NaiveGUIDStorage;
                    this.angularApp.factory('storage', ($q, $http) => givitiweb.DataStorageCreator(storageClass, config.dataType, config.dataStorageURL, $q, $http));
                    this.angularApp.factory('keysStorage', ($q, $http) => new givitiweb.RESTStorage('keys', config.keysStorageURL, $q, $http));
                    this.angularApp.factory('guidStorage', ($q, $http) => givitiweb.GuidStorageCreator(guidStorageClass, config.dataKey, $q, $http) /* TODO new GUIDStorage(eval("dataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"), $q, $http)*/);
                    this.angularApp.factory('listStorage', ($q, $http) => givitiweb.DataStorageCreator(storageClass, '__patientsList__', config.dataStorageURL, $q, $http));
                    this.angularApp.factory('listGuidStorage', ($q, $http) => new givitiweb.NaiveGUIDStorage(config.centreCode, $q, $http));
                    this.angularApp.factory('deletedListStorage', ($q, $http) => givitiweb.DataStorageCreator(storageClass, '__deletedPatientsList__', config.dataStorageURL, $q, $http));
                    this.angularApp.factory('deletedListGuidStorage', ($q, $http) => new givitiweb.NaiveGUIDStorage(config.centreCode, $q, $http));
                    this.angularApp.factory('dataSynchStorage', ($q, $http) => givitiweb.DataStorageCreator(storageClass, '__dataSynch__', config.dataStorageURL, $q, $http));
                    this.angularApp.factory('dataSynchGuidStorage', ($q, $http) => new givitiweb.NaiveGUIDStorage(config.centreCode, $q, $http));
                    this.angularApp.factory('translationsStorage', ($q, $http) => new givitiweb.TranslationStorage(config.translationsURL, $q, $http));
                    this.angularApp.factory('patientStatus', () => new givitiweb.PatientStatusService(true));
                    this.angularApp.factory('jsonService', ($q, guidStorage, storage, keysStorage, patientStatus) => givitiweb.JSONService.factory($q, guidStorage, storage, keysStorage, patientStatus));
                    this.angularApp.factory('ccDataStore', ($q, $http) => new givitiweb.MongoService($q, $http, config.ccDataCollection, config.ccDataStorageURL));
                    this.angularApp.factory('listService', ($q, listGuidStorage, listStorage, keysStorage, patientStatus) => givitiweb.JSONService.factory($q, listGuidStorage, listStorage, keysStorage, patientStatus)); //TODO: remove patientStatus
                    this.angularApp.factory('deletedListService', ($q, deletedListGuidStorage, deletedListStorage, keysStorage, patientStatus) => givitiweb.JSONService.factory($q, deletedListGuidStorage, deletedListStorage, keysStorage, patientStatus)); //TODO: remove patientStatus
                    this.angularApp.factory('dataSynchService', ($q, dataSynchGuidStorage, dataSynchStorage, keysStorage, patientStatus) => givitiweb.JSONService.factory($q, dataSynchGuidStorage, dataSynchStorage, keysStorage, patientStatus));
                    this.angularApp.factory('dataSynchronizer', (jsonService, listService, dataSynchService, ccDataStore, centreCode, $q, $interval) => new givitiweb.DataSynchronizer(jsonService, listService, dataSynchService, ccDataStore, centreCode, $q, $interval, config.secretVariables));
                    this.angularApp.factory('translationLoader', ($q, translationsStorage) => {
                        return (options) => {
                            var deferred = $q.defer();
                            translationsStorage.getTranslationsValues(options.application, options.block, options.key).then(data => deferred.resolve(data), error => deferred.reject(options.key));
                            return deferred.promise;
                        };
                    });
                    // Controllers
                    this.angularApp.controller('AngularCrafterController', givitiweb.Controller);
                    this.angularApp.controller('MyObjectController', givitiweb.ObjectController);
                    this.angularApp.controller('ModelSeparationController', givitiweb.ModelSeparationController);
                    //TODO: remove
                    this.angularApp.controller('RoomsRelationsController', givitiweb.RelationsController);
                    //Directives
                    this.angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
                    this.angularApp.directive('tooltip', givitiweb.TooltipDirective.factory);
                    this.angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
                    this.angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
                    this.angularApp.directive('ic3PatientsList', givitiweb.PatientsListDirective.factory);
                    this.angularApp.directive('shortCircuitFormula', givitiweb.ShortCircuitDirective.factory);
                    this.angularApp.directive("dateFormatter", () => {
                        return {
                            require: 'ngModel',
                            link: (scope, elem, attr, modelCtrl) => {
                                modelCtrl.$parsers = [value => {
                                        modelCtrl.$viewValue = moment(value).format();
                                        return value;
                                    }];
                                modelCtrl.$formatters.push(modelValue => modelValue != null ? new Date(modelValue) : null);
                            }
                        };
                    });
                    // Prevent time to be saved as date
                    // TODO: verify if it's better to save as string, with input template
                    this.angularApp.directive("timeFormatter", () => {
                        return {
                            require: 'ngModel',
                            link: (scope, elem, attr, modelCtrl) => {
                                modelCtrl.$parsers = [value => {
                                        return value;
                                    }];
                                modelCtrl.$formatters.push(modelValue => modelValue);
                            }
                        };
                    });
                    //Filters
                    this.angularApp.filter('formatDate', () => { return (date) => { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
                    // Configs
                    this.angularApp.config(($translateProvider) => {
                        var options = { application: config.applicationName, block: config.blockName };
                        $translateProvider.useLoader('translationLoader', options)
                            .preferredLanguage(config.culture)
                            .useSanitizeValueStrategy('escape');
                    });
                    if (config === null || config === void 0 ? void 0 : config.needsAuthorizations) {
                        this.angularApp.config(($httpProvider) => {
                            $httpProvider.defaults.withCredentials = true;
                        });
                    }
                }
                static create(config, dependencies = ['ngSanitize', 'pascalprecht.translate']) {
                    return new CollectionBaseApplication(config, dependencies);
                }
            }
            givitiweb.CollectionBaseApplication = CollectionBaseApplication;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
