/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.EventRegistration', ['ngSanitize', 'pascalprecht.translate']);
            // Storage
            var guidStorage = new givitiweb.GivitiWebGUIDStorage(eval("dataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"));
            var translationStorage = new givitiweb.TranslationStorage(eval("translationURL"));
            // Non angular inject
            angularApp.value('crfServiceURL', eval("crfServiceURL"));
            angularApp.value('dataSource', eval("dataSource"));
            angularApp.value('dataType', eval("dataType"));
            angularApp.value('dataPath', eval("dataPath"));
            angularApp.value('guidStorage', guidStorage);
            angularApp.value('existingData', JSON.parse(eval("existingData")));
            // Services
            angularApp.factory('jsonService', ($q) => { return givitiweb.JSONService.factory($q, guidStorage, eval("dataType"), eval("dataStorageURL"), null); });
            angularApp.factory('profileService', ($q) => { return givitiweb.ProfileService.factory($q, eval("profileURL")); });
            angularApp.factory('translationLoader', ($q) => {
                return (options) => {
                    var deferred = $q.defer();
                    translationStorage.getTranslationsValues(options.application, options.block, options.key)
                        .done((data) => deferred.resolve(data))
                        .fail(() => deferred.reject(options.key));
                    return deferred.promise;
                };
            });
            // Controllers
            angularApp.controller('EventRegistrationController', givitiweb.EventRegistrationController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            // Directives
            angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
            angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
            angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
            // Filters
            angularApp.filter('formatDate', () => { return (date) => { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
            // Configs
            angularApp.config(['$translateProvider', ($translateProvider) => {
                    var options = { application: eval("applicationName"), block: eval("blockName") };
                    $translateProvider.useLoader('translationLoader', options)
                        .preferredLanguage(eval("culture"))
                        .useSanitizeValueStrategy('escape');
                }]);
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
