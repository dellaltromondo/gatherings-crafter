/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.HighmapsInfo', ['ngSanitize', 'cgBusy', 'highcharts-ng']);
            // Controllers
            angularApp.controller('HighmapsInfoController', givitiweb.HighmapsInfoController);
            // Storage
            //var guidStorage: GUIDStorage<any> = new GUIDStorage(eval("listDataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"));
            // Services
            //angularApp.factory('listGuidService', ($q) => {return GUIDService.factory($q, eval("listDataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"))});
            /*
                angularApp.factory('guidService', ($q) => {return MigrantiGUIDService.factory($q)});
                angularApp.factory('jsonService', ($q, guidService) => {return JSONService.factory($q, guidService, eval("dataType"), eval("dataStorageURL"))});
                angularApp.factory('MigrantijsonService', ($q) => {return JSONService.factory($q, guidStorage, eval("listDataType"), eval("dataStorageURL"))});
                angularApp.factory('translationService', ($rootScope) => {return TranslationService.factory($rootScope, "givitiweb", "Commons", "it-IT", eval("translationURL"))});
            */
            // Storage
            var schedeDatiGUIDStorage = new givitiweb.NaiveGUIDStorage();
            // Services
            angularApp.factory('multipleSchedeDatiJSONService', ($q) => {
                return givitiweb.MultipleJSONService.factory($q, schedeDatiGUIDStorage, eval("dataType"), eval("dataStorageURL"), JSON.parse(eval("formAllGuid")));
            });
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
