/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/angular/angular-translate.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.AngularCrafter', ['ngSanitize', 'pascalprecht.translate']);
            // Storage
            var translationStorage = new givitiweb.TranslationStorage(eval("translationURL"));
            // Non angular inject
            angularApp.value('crfServiceURL', eval("crfServiceURL"));
            angularApp.value('dataType', eval("dataType"));
            angularApp.value('dataPath', eval("dataPath"));
            angularApp.value('dataName', eval("dataName")); //centrecode
            angularApp.value('dataKey', eval("dataKey")); //CY001
            // Services
            angularApp.factory('mongoService', ($q) => { return givitiweb.MongoService.factory($q, eval("dataType"), eval("mongoStorageURL")); });
            angularApp.factory('profileService', ($q) => { return givitiweb.ProfileService.factory($q, eval("profileURL")); });
            angularApp.factory('translationLoader', ($q) => {
                return (options) => {
                    var deferred = $q.defer();
                    translationStorage.getTranslationsValues(options.application, options.block, options.key)
                        .done((data) => deferred.resolve(data))
                        .fail(() => deferred.reject(options.key));
                    return deferred.promise;
                };
            });
            // Controllers
            angularApp.controller('AngularCrafterController', givitiweb.NascitaListController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            //Directives
            angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
            angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
            angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
            // aggiunta direttive per filtrare le informazioni geografiche
            angularApp.directive('regionPicker', givitiweb.RegionPickerDirective.factory);
            angularApp.directive('provincePicker', givitiweb.ProvincePickerDirective.factory);
            angularApp.directive('townPicker', givitiweb.TownPickerDirective.factory);
            //Filters
            angularApp.filter('formatDate', () => { return (date) => { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
            // Configs
            angularApp.config(['$translateProvider', ($translateProvider) => {
                    var options = { application: eval("applicationName"), block: eval("blockName") };
                    $translateProvider.useLoader('translationLoader', options)
                        .preferredLanguage(eval("culture"))
                        .useSanitizeValueStrategy('escape');
                }]);
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
