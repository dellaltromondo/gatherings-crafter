/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.Migranti', ['ngSanitize', 'ngAutocomplete']);
            // Controllers
            angularApp.controller('MigrantiController', givitiweb.MigrantiController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            angularApp.controller('MyController', givitiweb.Controller);
            // Storage
            var guidStorage = new givitiweb.GivitiWebGUIDStorage(eval("listDataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"));
            // Services
            //angularApp.factory('listGuidService', ($q) => {return GUIDService.factory($q, eval("listDataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"))});
            angularApp.factory('guidService', ($q) => { return givitiweb.MigrantiGUIDService.factory($q); });
            angularApp.factory('jsonService', ($q, guidService) => { return givitiweb.JSONService.factory($q, guidService, eval("dataType"), eval("dataStorageURL"), null); });
            angularApp.factory('MigrantijsonService', ($q) => { return givitiweb.JSONService.factory($q, guidStorage, eval("listDataType"), eval("dataStorageURL"), null); });
            //Directives
            angularApp.directive('datePicker', givitiweb.DatePickerDirective.factory);
            angularApp.directive('regionPicker', givitiweb.RegionPickerDirective.factory);
            angularApp.directive('provincePicker', givitiweb.ProvincePickerDirective.factory);
            angularApp.directive('townPicker', givitiweb.TownPickerDirective.factory);
            angularApp.directive('confirmationNeeded', givitiweb.AlertDirective.factory);
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
