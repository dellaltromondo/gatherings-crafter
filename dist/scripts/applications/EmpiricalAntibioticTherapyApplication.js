/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            var angularApp = angular.module('it.marionegri.givitiweb.EmpiricalAntibioticTherapy', ['ngSanitize', 'ngTagsInput']);
            // Storage
            var guidStorage = new givitiweb.GivitiWebGUIDStorage(eval("dataType"), eval("dataKey"), eval("dataSource"), eval("dataStorageURL"));
            var indexesGUIDStorage = new givitiweb.GivitiWebGUIDStorage(eval("dataType"), "", "storage", eval("dataStorageURL"));
            // Non angular inject
            angularApp.value('crfServiceURL', eval("crfServiceURL"));
            angularApp.value('dataType', eval("dataType"));
            angularApp.value('dataPath', eval("dataPath"));
            angularApp.value('suggestionURL', eval("suggestionURL"));
            angularApp.value('templateUrl', eval("templateURL"));
            // Services
            angularApp.factory('jsonService', ($q) => { return givitiweb.JSONService.factory($q, guidStorage, eval("dataType"), eval("dataStorageURL"), null); });
            angularApp.factory('multipleIndexesJSONService', ($q) => {
                return givitiweb.MultipleJSONService.factory($q, indexesGUIDStorage, eval("dataType") + "_Indexes", eval("dataStorageURL"), null);
            });
            // Controllers
            angularApp.controller('EmpiricalAntibioticTherapyController', givitiweb.EmpiricalAntibioticTherapyController);
            angularApp.controller('MyObjectController', givitiweb.ObjectController);
            //Directives
            angularApp.directive('ic3DataCollection', givitiweb.DataCollectionDirective.factory);
            angularApp.directive('ic3DataClass', givitiweb.DataClassDirective.factory);
            angularApp.directive('ic3Autocomplete', givitiweb.AutocompleteJSONDirective.factory);
            //Filters
            angularApp.filter('translate', () => { return (key) => { return key; }; });
            angularApp.filter('formatDate', () => { return (date) => { return moment(date, givitiweb.GlobalConfig.jsonDateFormat).format(givitiweb.GlobalConfig.viewDateFormat); }; });
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
