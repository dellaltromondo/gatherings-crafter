var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            let GlobalConfig = /** @class */ (() => {
                class GlobalConfig {
                    constructor() {
                        throw new Error("Cannot create a new instance of GlobalConfig class");
                    }
                }
                // dateFormat for moment.js
                GlobalConfig.jsonDateFormat = "YYYY-MM-DD";
                GlobalConfig.viewDateFormat = "DD/MM/YYYY";
                return GlobalConfig;
            })();
            givitiweb.GlobalConfig = GlobalConfig;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
