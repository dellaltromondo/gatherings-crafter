var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            let ColorHelper = /** @class */ (() => {
                class ColorHelper {
                    constructor(colorMap) {
                        this.colorMap = (colorMap) ? colorMap : ColorFromPercentage.defaultColorMap;
                    }
                    formatColor(color) {
                        return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
                    }
                    getColor(percentage) {
                        if (percentage < 0) {
                            percentage = 0;
                        }
                        if (percentage > 1) {
                            percentage = 1;
                        }
                        for (var i = 1; i < this.colorMap.length - 1; i++) {
                            if (percentage <= this.colorMap[i].percentage) {
                                break;
                            }
                        }
                        var lower = this.colorMap[i - 1];
                        var upper = this.colorMap[i];
                        var range = upper.percentage - lower.percentage;
                        var percentageRange = (percentage - lower.percentage) / range;
                        var lowerPercentage = 1 - percentageRange;
                        var upperPercentage = percentageRange;
                        var color = {
                            r: Math.floor(lower.color.r * lowerPercentage + upper.color.r * upperPercentage),
                            g: Math.floor(lower.color.g * lowerPercentage + upper.color.g * upperPercentage),
                            b: Math.floor(lower.color.b * lowerPercentage + upper.color.b * upperPercentage)
                        };
                        return color;
                    }
                    static getRandomColor() {
                        var colorList = ['#B4F7FF', '#FFB9FF', '#FEFE90', '#A2FFA2', '#FF6F6F', '#B4F7FF', '#FFCA5F', '#00FF7E', '#B9B9FF', '#B4F7FF', '#FFB9FF', '#FEFE90', '#A2FFA2', '#FF6F6F', '#B4F7FF', '#FFCA5F', '#00FF7E', '#B9B9FF'];
                        return colorList[Math.floor(Math.random() * colorList.length)];
                    }
                    static adjustBrightness(col, amt) {
                        var usePound = false;
                        if (col[0] == "#") {
                            col = col.slice(1);
                            usePound = true;
                        }
                        var R = parseInt(col.substring(0, 2), 16);
                        var G = parseInt(col.substring(2, 4), 16);
                        var B = parseInt(col.substring(4, 6), 16);
                        // to make the colour less bright than the input
                        // change the following three "+" symbols to "-"
                        R = R + amt;
                        G = G + amt;
                        B = B + amt;
                        if (R > 255)
                            R = 255;
                        else if (R < 0)
                            R = 0;
                        if (G > 255)
                            G = 255;
                        else if (G < 0)
                            G = 0;
                        if (B > 255)
                            B = 255;
                        else if (B < 0)
                            B = 0;
                        var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
                        var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
                        var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));
                        return (usePound ? "#" : "") + RR + GG + BB;
                    }
                }
                ColorHelper.defaultColorMap = [
                    // ORDER is important! 
                    { percentage: 0.0, color: { r: 0x00, g: 0xff, b: 0 } },
                    { percentage: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
                    { percentage: 1.0, color: { r: 0xff, g: 0x00, b: 0 } }
                ];
                return ColorHelper;
            })();
            givitiweb.ColorHelper = ColorHelper;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
