/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class Cache {
                constructor() {
                    this.objectsCache = [];
                }
                getFromCache(parameters) {
                    var result = null;
                    for (var cachedObject in this.objectsCache) {
                        if (angular.equals(this.objectsCache[cachedObject].parameters, parameters))
                            return this.objectsCache[cachedObject].cachedObject;
                    }
                    return result;
                }
                putInCache(parameters, newResult) {
                    var cachedObjectFound = false;
                    for (var cachedObject in this.objectsCache) {
                        if (angular.equals(this.objectsCache[cachedObject].parameters, parameters)) {
                            this.objectsCache[cachedObject].cachedObject = newResult;
                            cachedObjectFound = true;
                            break;
                        }
                    }
                    if (!cachedObjectFound) {
                        var newObject = { parameters: angular.copy(parameters), cachedObject: angular.copy(newResult) };
                        this.objectsCache.push(newObject);
                    }
                }
            }
            givitiweb.Cache = Cache;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
