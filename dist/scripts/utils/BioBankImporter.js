/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            let BioBankImporter = /** @class */ (() => {
                class BioBankImporter {
                    constructor($scope, $q, $http) {
                        this.$scope = $scope;
                        this.$q = $q;
                        this.$http = $http;
                        this.inject = ["$scope", "$q", "$http"];
                        $scope["vm"] = this;
                    }
                    run(importString, dataStorageUrl) {
                        var centresData = this.parseCentresData(importString);
                        for (var centreIndex in centresData) {
                            var myCentre = centresData[centreIndex];
                            var guidStorage = new givitiweb.GivitiWebGUIDStorage("centreBioBankData", myCentre.centreCode, "centre", dataStorageUrl, this.$q, this.$http);
                            var myJsonService = new givitiweb.JSONService(this.$q, guidStorage);
                            var saveCallback = (centre, jsonService, data) => {
                                var newData;
                                if (!angular.equals(data, {})) {
                                    newData = angular.copy(data);
                                }
                                else {
                                    newData = angular.copy(BioBankImporter.fullBaseJson);
                                }
                                var myPackage = angular.copy(BioBankImporter.packageBaseJson);
                                myPackage.guid = givitiweb.GuidUtils.createGUID();
                                myPackage.kits = angular.copy(centre.kits);
                                myPackage.liquorKits = angular.copy(centre.liquorKits);
                                var found = false;
                                for (var index in newData.kitsPackages) {
                                    if (newData.kitsPackages[index].notes == BioBankImporter.packageBaseJson.notes) {
                                        newData.kitsPackages[index] = myPackage;
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found) {
                                    newData.kitsPackages.push(myPackage);
                                }
                                this.save(jsonService, newData, centre.centreCode);
                            };
                            myJsonService.$get().then(saveCallback.bind(this, myCentre, myJsonService));
                        }
                    }
                    parseCentresData(importString) {
                        var centresData = [];
                        var rows = importString.split("#");
                        for (var rowIndex in rows) {
                            if (rows[rowIndex] != "") {
                                var fields = rows[rowIndex].split(";");
                                var centre = {
                                    centreCode: fields[0],
                                    kits: [],
                                    liquorKits: []
                                };
                                for (var k = 2; k < fields.length; k++) {
                                    if (fields[k] && fields[k][0] == "5") {
                                        centre.kits.push({ "guid": givitiweb.GuidUtils.createGUID(), "code": parseInt(fields[k]) });
                                    }
                                    else if (fields[k] && fields[k][0] == "6") {
                                        centre.liquorKits.push({ "guid": givitiweb.GuidUtils.createGUID(), "code": parseInt(fields[k]) });
                                        ;
                                    }
                                    else {
                                        this.$scope.log += "Error when importing row: " + rows[rowIndex];
                                    }
                                }
                                centresData.push(centre);
                            }
                        }
                        return centresData;
                    }
                    save(jsonService, data, centreCode) {
                        return jsonService.save(data)
                            .then(() => {
                            this.$scope.log += "Data for " + centreCode + " saved successfully" + "\n";
                        }, () => {
                            this.$scope.log += "Error when saving data for " + centreCode + "\n";
                        });
                    }
                }
                BioBankImporter.fullBaseJson = {
                    "kitsPackages": []
                };
                BioBankImporter.packageBaseJson = {
                    "guid": "b1bd94f2-1c7a-c4e0-71e1-5df7b6d2217f",
                    "kits": [],
                    "liquorKits": [],
                    "notes": "Consegnate al meeting iniziale",
                    "deliveryDate": "2014-12-17"
                };
                return BioBankImporter;
            })();
            givitiweb.BioBankImporter = BioBankImporter;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
