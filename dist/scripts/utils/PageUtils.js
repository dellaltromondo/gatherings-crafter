var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class PageUtils {
                static getPageData(index) {
                    var pageData = [];
                    try {
                        var encodedData = givitiweb.UriUtils.getQueryVariable("data");
                        var decodedData = window.atob(encodedData);
                        pageData = JSON.parse(decodedData);
                    }
                    catch (e) {
                        console.log("An error occurred while reading data page: the query part of the URL is malformed? Error:" + e.message);
                    }
                    if (index < pageData.length) {
                        return pageData[index];
                    }
                    else {
                        console.log("Index " + index + " not found in URL query data");
                        return null;
                    }
                }
                static getAdmissionKey() {
                    return PageUtils.getPageData(0);
                }
                static getAge() {
                    return PageUtils.getPageData(1);
                }
                static getListOfLanguages() {
                    var admissionKey = PageUtils.getPageData(0);
                    var nationCode = admissionKey.substr(admissionKey.indexOf("-") + 1, 2);
                    switch (nationCode) {
                        case "IT":
                            return ["it-IT", "en-US"];
                        case "IL":
                            return ["he-IL", "ar-IL", "en-US"];
                        case "PL":
                            return ["pl-PL", "en-US"];
                        case "SI":
                            return ["sl-SL", "en-US"];
                        default:
                            return ["en-US"];
                    }
                }
            }
            givitiweb.PageUtils = PageUtils;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
