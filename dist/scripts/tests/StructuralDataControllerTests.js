/// <reference path="tsUnit.ts" />
/// <reference path="../controllers/StructuralDataController.ts" />
/// <reference path="../utils/GlobalConfig.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var StructuralDataControllerTests;
(function (StructuralDataControllerTests_1) {
    var gvt = it.marionegri.givitiweb;
    var StructuralDataControllerTests = (function (_super) {
        __extends(StructuralDataControllerTests, _super);
        function StructuralDataControllerTests() {
            _super.apply(this, arguments);
            this.crfWithTwoElementsSet = {
                dataValidity: { startingDate: moment("2015-01-01").format(gvt.GlobalConfig.jsonDateFormat) },
                testSet: [{ name: "Primo", guid: "aaa" }, { name: "Secondo", guid: "bbb" }]
            };
            this.crfWithThreeElementsSet = {
                dataValidity: { startingDate: moment("2015-01-15").format(gvt.GlobalConfig.jsonDateFormat) },
                testSet: [{ name: "Primo", guid: "aaa" }, { name: "Secondo", guid: "bbb" }, { name: "Terzo", guid: "ccc" }]
            };
        }
        StructuralDataControllerTests.prototype.setUp = function () {
            window["periodMaxMonthValidity"] = "6";
            this.initializeControllerWithData({});
        };
        StructuralDataControllerTests.prototype.tearDown = function () {
            this.myController = null;
        };
        StructuralDataControllerTests.prototype.initializeControllerWithData = function (data) {
            //LOL
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.StructuralDataController(null, null, mockScope, null, null, null, null, true, null, null, null);
        };
        StructuralDataControllerTests.prototype.testFillListOfData = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.isFalse(angular.equals(this.myController.$scope.crfs, {}));
        };
        StructuralDataControllerTests.prototype.testFilledCrfIsLast = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
        };
        StructuralDataControllerTests.prototype.testFilledCrfIsLastWhenReverseOrder = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
        };
        StructuralDataControllerTests.prototype.testFillListOfDataDoesNotReferenceOriginals = function () {
            var localCopy = angular.copy(this.crfWithTwoElementsSet);
            this.myController.fillCentreStructuralData({ centreStructuralData: [localCopy] });
            localCopy.testSet[0].name = "PrimoCambiato";
            this.areIdentical(this.myController.$scope.crfs.testSet[0].name, "Primo");
        };
        StructuralDataControllerTests.prototype.testClearDataDoesNotChangeOriginalData = function () {
            var firstCrf = angular.copy(this.crfWithTwoElementsSet);
            var secondCrf = angular.copy(this.crfWithThreeElementsSet);
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            var newCrfs = angular.copy(this.crfWithTwoElementsSet);
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.myController.clearData(this.myController.$scope.crfs);
            this.isTrue(angular.equals(firstCrf, this.crfWithTwoElementsSet));
            this.isTrue(angular.equals(secondCrf, this.crfWithThreeElementsSet));
        };
        StructuralDataControllerTests.prototype.testUpdateIndexDoesNotChangeOriginalData = function () {
            var firstCrf = angular.copy(this.crfWithTwoElementsSet);
            var secondCrf = angular.copy(this.crfWithThreeElementsSet);
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.isTrue(angular.equals(firstCrf, this.crfWithTwoElementsSet));
            this.isTrue(angular.equals(secondCrf, this.crfWithThreeElementsSet));
        };
        StructuralDataControllerTests.prototype.testUpdateIndex = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 2);
        };
        StructuralDataControllerTests.prototype.testUpdateIndexWhenReverseOrder = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
        };
        StructuralDataControllerTests.prototype.testUpdateIndexDoesNotChangeSetReference = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            var oldCrfsSet = this.myController.$scope.crfs.testSet;
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            var newCrfsSet = this.myController.$scope.crfs.testSet;
            this.areIdentical(oldCrfsSet, newCrfsSet);
        };
        StructuralDataControllerTests.prototype.testUpdateIndexDoesNotCollapseSetReference = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.areNotIdentical(this.myController.$scope.crfs.testSet, this.crfWithTwoElementsSet);
        };
        StructuralDataControllerTests.prototype.testSetLatestIndexSetsPeriodIndex = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.areIdentical(this.myController.$scope.status.periodIndex, 1);
        };
        StructuralDataControllerTests.prototype.testMultipleUpdateIndex = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.myController.$scope.status.periodIndex = 1;
            this.myController.fillCRFS();
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
        };
        StructuralDataControllerTests.prototype.testMultipleUpdateIndexWhenReverseOrder = function () {
            this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.myController.$scope.status.periodIndex = 1;
            this.myController.fillCRFS();
            this.areIdentical(this.myController.$scope.crfs.testSet.length, 2);
        };
        StructuralDataControllerTests.prototype.testMultipleUpdateIndexBugForRoomsRelationships = function () {
            var json = [
                {
                    "dataValidity": { "startingDate": "2015-01-15" },
                    "roomsData": {
                        "roomsSet": [],
                        "roomsRelationships": {
                            "b2057ce3-7dbe-91a4-ba13-c1799d9fa0ac": [
                                "e02e843e-b6f1-286d-5c23-845181dcec35"
                            ]
                        }
                    }
                },
                {
                    "dataValidity": { "startingDate": "2014-11-01" },
                    "roomsData": {
                        "roomsSet": [],
                        "roomsRelationships": {}
                    }
                }
            ];
            this.myController.fillCentreStructuralData({ centreStructuralData: json });
            this.myController.$scope.status.periodIndex = 0;
            this.myController.fillCRFS();
            this.myController.$scope.status.periodIndex = 1;
            this.myController.fillCRFS();
            this.isFalse(angular.equals(this.myController.$scope.crfs.roomsData.roomsRelationships["b2057ce3-7dbe-91a4-ba13-c1799d9fa0ac"], {}));
        };
        return StructuralDataControllerTests;
    })(tsUnit.TestClass);
    StructuralDataControllerTests_1.StructuralDataControllerTests = StructuralDataControllerTests;
})(StructuralDataControllerTests || (StructuralDataControllerTests = {}));
