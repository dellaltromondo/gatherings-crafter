/// <reference path="tsUnit.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ExampleTests;
(function (ExampleTests) {
    var RealClass = (function () {
        function RealClass() {
            this.name = 'Real';
        }
        RealClass.prototype.run = function () {
            return false;
        };
        RealClass.prototype.returnValue = function () {
            return this;
        };
        return RealClass;
    })();
    ExampleTests.RealClass = RealClass;
    var SetUpTestClassStub = (function (_super) {
        __extends(SetUpTestClassStub, _super);
        function SetUpTestClassStub() {
            _super.apply(this, arguments);
            this.TestProperty = '';
        }
        SetUpTestClassStub.prototype.setUp = function () {
            this.TestProperty = 'SETUP';
        };
        SetUpTestClassStub.prototype.testAfterInitialSetUp = function () {
            this.areIdentical('SETUP', this.TestProperty);
            this.TestProperty = 'OVERWRITE';
        };
        SetUpTestClassStub.prototype.testOfSetUpWithFail = function () {
            this.areIdentical('SETUP', this.TestProperty);
            throw "Internal test error: Thrown by testOfSetUpWithFail";
        };
        SetUpTestClassStub.prototype.testNextInStub = function () {
            this.areIdentical('SETUP', this.TestProperty);
            this.TestProperty = 'OVERWRITE';
        };
        return SetUpTestClassStub;
    })(tsUnit.TestClass);
    var SetUpWithParametersTestClassStub = (function (_super) {
        __extends(SetUpWithParametersTestClassStub, _super);
        function SetUpWithParametersTestClassStub() {
            _super.call(this);
            this.TestProperty = '';
            this.parameterizeUnitTest(this.testSum, [
                [2, 1],
                [3, 2],
                [4, 3]
            ]);
        }
        SetUpWithParametersTestClassStub.prototype.setUp = function () {
            this.TestProperty = 'SETUP';
        };
        SetUpWithParametersTestClassStub.prototype.testSum = function (a, b) {
            this.areIdentical('SETUP', this.TestProperty);
            this.areIdentical(a, b + 1);
            this.TestProperty = 'OVERWRITE';
        };
        return SetUpWithParametersTestClassStub;
    })(tsUnit.TestClass);
    var TearDownTestClassStub = (function (_super) {
        __extends(TearDownTestClassStub, _super);
        function TearDownTestClassStub() {
            _super.apply(this, arguments);
            this.TestProperty = '';
        }
        TearDownTestClassStub.prototype.testAfterInitialSetUp = function () {
            this.areIdentical('', this.TestProperty);
            this.TestProperty = 'OVERWRITE';
        };
        TearDownTestClassStub.prototype.tearDown = function () {
            this.TestProperty = 'TEARDOWN';
        };
        return TearDownTestClassStub;
    })(tsUnit.TestClass);
    var TearDownWithFailingTestClassStub = (function (_super) {
        __extends(TearDownWithFailingTestClassStub, _super);
        function TearDownWithFailingTestClassStub() {
            _super.apply(this, arguments);
            this.TestProperty = '';
        }
        TearDownWithFailingTestClassStub.prototype.testWhichWillFail = function () {
            this.areIdentical('', this.TestProperty);
            this.TestProperty = 'OVERWRITE';
            throw "Internal test error: Thrown by testWhichWillFail";
        };
        TearDownWithFailingTestClassStub.prototype.tearDown = function () {
            this.TestProperty = 'TEARDOWN';
        };
        return TearDownWithFailingTestClassStub;
    })(tsUnit.TestClass);
    var TearDownTestWithParametersTestClassStub = (function (_super) {
        __extends(TearDownTestWithParametersTestClassStub, _super);
        function TearDownTestWithParametersTestClassStub() {
            _super.call(this);
            this.tearDownCounter = 0;
            this.parameterizeUnitTest(this.testForTearDown, [
                [0],
                [1],
                [2]
            ]);
        }
        TearDownTestWithParametersTestClassStub.prototype.testForTearDown = function (index) {
            this.areIdentical(index, this.tearDownCounter);
        };
        TearDownTestWithParametersTestClassStub.prototype.tearDown = function () {
            this.tearDownCounter++;
        };
        return TearDownTestWithParametersTestClassStub;
    })(tsUnit.TestClass);
    var TearDownAndSetUpTests = (function (_super) {
        __extends(TearDownAndSetUpTests, _super);
        function TearDownAndSetUpTests() {
            _super.apply(this, arguments);
        }
        TearDownAndSetUpTests.prototype.testOfSetUp = function () {
            var stub = new SetUpTestClassStub();
            var testPropertyOnBegin = stub.TestProperty;
            var test = new tsUnit.Test();
            test.addTestClass(stub, 'SetUpTestClassStub');
            var results = test.run(new tsUnit.RunAllTests());
            this.areIdentical(testPropertyOnBegin, '', "TestProperty should be empty on start");
            this.isTrue((results.passes.length == 2) && (results.errors.length == 1), "All internal tests should passed (appertly setUp didn't work)");
        };
        TearDownAndSetUpTests.prototype.testOfSetUpWithParameters = function () {
            var stub = new SetUpWithParametersTestClassStub();
            var testPropertyOnBegin = stub.TestProperty;
            var test = new tsUnit.Test();
            test.addTestClass(stub, 'SetUpWithParametersTestClassStub');
            var results = test.run(new tsUnit.RunAllTests());
            this.areIdentical(testPropertyOnBegin, '', "TestProperty should be empty on start");
            this.isTrue((results.passes.length == 3) && (results.errors.length == 0), "All internal tests should passed (appertly setUp didn't work)");
        };
        TearDownAndSetUpTests.prototype.testOfTearDown = function () {
            var stub = new TearDownTestClassStub();
            var testPropertyOnBegin = stub.TestProperty;
            var test = new tsUnit.Test();
            test.addTestClass(stub, 'TearDownTestClassStub');
            test.run(new tsUnit.RunAllTests());
            this.areIdentical(testPropertyOnBegin, '', "TestProperty should be empty on start");
            this.areIdentical(stub.TestProperty, 'TEARDOWN', "TestProperty should be overwrite by TearDown method");
        };
        TearDownAndSetUpTests.prototype.testTearDownWithFailedTest = function () {
            var stub = new TearDownWithFailingTestClassStub();
            var testPropertyOnBegin = stub.TestProperty;
            var test = new tsUnit.Test();
            test.addTestClass(stub, 'TearDownWithFailingTestClassStub');
            test.run(new tsUnit.RunAllTests());
            this.areIdentical(testPropertyOnBegin, '', "TestProperty should be empty on start");
            this.areIdentical(stub.TestProperty, 'TEARDOWN', "TestProperty should be overwrite by TearDown method");
        };
        TearDownAndSetUpTests.prototype.testTearDownWithParameters = function () {
            var stub = new TearDownTestWithParametersTestClassStub();
            var test = new tsUnit.Test();
            test.addTestClass(stub, 'TearDownTestWithParametersTestClassStub');
            var results = test.run(new tsUnit.RunAllTests());
            this.areIdentical(3, stub.tearDownCounter);
            this.isTrue((results.passes.length == 3) && (results.errors.length == 0), "All internal tests should passed (appertly setUp didn't work)");
        };
        return TearDownAndSetUpTests;
    })(tsUnit.TestClass);
    ExampleTests.TearDownAndSetUpTests = TearDownAndSetUpTests;
    var FakesTests = (function (_super) {
        __extends(FakesTests, _super);
        function FakesTests() {
            _super.apply(this, arguments);
        }
        FakesTests.prototype.callDefaultFunctionOnFake = function () {
            var fakeObject = new tsUnit.Fake(new RealClass());
            var target = fakeObject.create();
            var result = target.run();
            this.areIdentical(undefined, result);
        };
        FakesTests.prototype.callSubstituteFunctionOnFake = function () {
            var fakeObject = new tsUnit.Fake(new RealClass());
            fakeObject.addFunction('run', function () { return true; });
            var target = fakeObject.create();
            var result = target.run();
            this.isTrue(result);
        };
        FakesTests.prototype.callSubstituteFunctionToObtainSecondFake = function () {
            var innerFake = new tsUnit.Fake(new RealClass());
            innerFake.addFunction('run', function () { return true; });
            var outerFake = new tsUnit.Fake(new RealClass());
            outerFake.addFunction('returnValue', function () { return innerFake.create(); });
            var target = outerFake.create();
            var interimResult = target.returnValue();
            var result = interimResult.run();
            this.isTrue(result);
        };
        FakesTests.prototype.callDefaultPropertyOnFake = function () {
            var fakeObject = new tsUnit.Fake(new RealClass());
            var target = fakeObject.create();
            var result = target.name;
            this.areIdentical(null, result);
        };
        FakesTests.prototype.callSubstitutePropertyOnFake = function () {
            var fakeObject = new tsUnit.Fake(new RealClass());
            fakeObject.addProperty('name', 'Test');
            var target = fakeObject.create();
            var result = target.name;
            this.areIdentical('Test', result);
        };
        return FakesTests;
    })(tsUnit.TestClass);
    ExampleTests.FakesTests = FakesTests;
    var AssertAreIdenticalTests = (function (_super) {
        __extends(AssertAreIdenticalTests, _super);
        function AssertAreIdenticalTests() {
            _super.apply(this, arguments);
        }
        AssertAreIdenticalTests.prototype.withIdenticalNumbers = function () {
            this.areIdentical(5, 5);
        };
        AssertAreIdenticalTests.prototype.withDifferentNumbers = function () {
            this.throws(function () {
                this.areIdentical(5, 4);
            });
        };
        AssertAreIdenticalTests.prototype.withIdenticalStings = function () {
            this.areIdentical('Hello', 'Hello');
        };
        AssertAreIdenticalTests.prototype.withDifferentStrings = function () {
            this.throws(function () {
                this.areIdentical('Hello', 'Jello');
            });
        };
        AssertAreIdenticalTests.prototype.withSameInstance = function () {
            var x = { test: 'Object' };
            var y = x;
            this.areIdentical(x, y);
        };
        AssertAreIdenticalTests.prototype.withDifferentInstance = function () {
            this.throws(function () {
                var x = { test: 'Object' };
                var y = { test: 'Object' };
                this.areIdentical(x, y);
            });
        };
        AssertAreIdenticalTests.prototype.withDifferentTypes = function () {
            this.throws(function () {
                this.areIdentical('1', 1);
            });
        };
        AssertAreIdenticalTests.prototype.withIdenticalCollections = function () {
            var x = [1, 2, 3, 5];
            var y = [1, 2, 3, 5];
            this.areCollectionsIdentical(x, y);
        };
        AssertAreIdenticalTests.prototype.withDifferentCollections = function () {
            var x = [1, 2, 3, 5];
            var y = [1, 2, 4, 5];
            this.throws(function () {
                this.areCollectionsIdentical(x, y);
            });
        };
        return AssertAreIdenticalTests;
    })(tsUnit.TestClass);
    ExampleTests.AssertAreIdenticalTests = AssertAreIdenticalTests;
    var AssertAreNotIdenticalTests = (function (_super) {
        __extends(AssertAreNotIdenticalTests, _super);
        function AssertAreNotIdenticalTests() {
            _super.apply(this, arguments);
        }
        AssertAreNotIdenticalTests.prototype.withIdenticalNumbers = function () {
            this.throws(function () {
                this.areNotIdentical(4, 4);
            });
        };
        AssertAreNotIdenticalTests.prototype.withDifferentNumbers = function () {
            this.areNotIdentical(4, -4);
        };
        AssertAreNotIdenticalTests.prototype.withIdenticalStrings = function () {
            this.throws(function () {
                this.areNotIdentical('Hello', 'Hello');
            });
        };
        AssertAreNotIdenticalTests.prototype.withDifferentStrings = function () {
            this.areNotIdentical('Hello', 'Hella');
        };
        AssertAreNotIdenticalTests.prototype.withSameInstance = function () {
            this.throws(function () {
                var x = { test: 'Object' };
                var y = x;
                this.areNotIdentical(x, y);
            });
        };
        AssertAreNotIdenticalTests.prototype.withDifferentInstance = function () {
            var x = { test: 'Object' };
            var y = { test: 'Object' };
            this.areNotIdentical(x, y);
        };
        AssertAreNotIdenticalTests.prototype.withDifferentTypes = function () {
            this.areNotIdentical('1', 1);
        };
        AssertAreNotIdenticalTests.prototype.withIdenticalCollections = function () {
            var x = [1, 2, 3, 5];
            var y = [1, 2, 3, 5];
            this.throws(function () {
                this.areCollectionsNotIdentical(x, y);
            });
        };
        AssertAreNotIdenticalTests.prototype.withDifferentCollections = function () {
            var x = [1, 2, 3, 5];
            var y = [1, 2, 4, 5];
            this.areCollectionsNotIdentical(x, y);
        };
        return AssertAreNotIdenticalTests;
    })(tsUnit.TestClass);
    ExampleTests.AssertAreNotIdenticalTests = AssertAreNotIdenticalTests;
    var IsTrueTests = (function (_super) {
        __extends(IsTrueTests, _super);
        function IsTrueTests() {
            _super.apply(this, arguments);
        }
        IsTrueTests.prototype.withBoolTrue = function () {
            this.isTrue(true);
        };
        IsTrueTests.prototype.withBoolFalse = function () {
            this.throws(function () {
                this.isTrue(false);
            });
        };
        return IsTrueTests;
    })(tsUnit.TestClass);
    ExampleTests.IsTrueTests = IsTrueTests;
    var IsFalseTests = (function (_super) {
        __extends(IsFalseTests, _super);
        function IsFalseTests() {
            _super.apply(this, arguments);
        }
        IsFalseTests.prototype.withBoolFalse = function () {
            this.isFalse(false);
        };
        IsFalseTests.prototype.withBoolTrue = function () {
            this.throws(function () {
                this.isFalse(true);
            });
        };
        return IsFalseTests;
    })(tsUnit.TestClass);
    ExampleTests.IsFalseTests = IsFalseTests;
    var IsTruthyTests = (function (_super) {
        __extends(IsTruthyTests, _super);
        function IsTruthyTests() {
            _super.apply(this, arguments);
        }
        IsTruthyTests.prototype.withBoolTrue = function () {
            this.isTruthy(true);
        };
        IsTruthyTests.prototype.withNonEmptyString = function () {
            this.isTruthy('Hello');
        };
        IsTruthyTests.prototype.withTrueString = function () {
            this.isTruthy('True');
        };
        IsTruthyTests.prototype.with1 = function () {
            this.isTruthy(1);
        };
        IsTruthyTests.prototype.withBoolFalse = function () {
            this.throws(function () {
                this.isTruthy(false);
            });
        };
        IsTruthyTests.prototype.withEmptyString = function () {
            this.throws(function () {
                this.isTruthy('');
            });
        };
        IsTruthyTests.prototype.withZero = function () {
            this.throws(function () {
                this.isTruthy(0);
            });
        };
        IsTruthyTests.prototype.withNull = function () {
            this.throws(function () {
                this.isTruthy(null);
            });
        };
        IsTruthyTests.prototype.withUndefined = function () {
            this.throws(function () {
                this.isTruthy(undefined);
            });
        };
        return IsTruthyTests;
    })(tsUnit.TestClass);
    ExampleTests.IsTruthyTests = IsTruthyTests;
    var IsFalseyTests = (function (_super) {
        __extends(IsFalseyTests, _super);
        function IsFalseyTests() {
            _super.apply(this, arguments);
        }
        IsFalseyTests.prototype.withBoolFalse = function () {
            this.isFalsey(false);
        };
        IsFalseyTests.prototype.withEmptyString = function () {
            this.isFalsey('');
        };
        IsFalseyTests.prototype.withZero = function () {
            this.isFalsey(0);
        };
        IsFalseyTests.prototype.withNull = function () {
            this.isFalsey(null);
        };
        IsFalseyTests.prototype.withUndefined = function () {
            this.isFalsey(undefined);
        };
        IsFalseyTests.prototype.withBoolTrue = function () {
            this.throws(function () {
                this.isFalsey(true);
            });
        };
        IsFalseyTests.prototype.withNonEmptyString = function () {
            this.throws(function () {
                this.isFalsey('Hello');
            });
        };
        IsFalseyTests.prototype.withTrueString = function () {
            this.throws(function () {
                this.isFalsey('True');
            });
        };
        IsFalseyTests.prototype.with1 = function () {
            this.throws(function () {
                this.isFalsey(1);
            });
        };
        return IsFalseyTests;
    })(tsUnit.TestClass);
    ExampleTests.IsFalseyTests = IsFalseyTests;
    var FailTests = (function (_super) {
        __extends(FailTests, _super);
        function FailTests() {
            _super.apply(this, arguments);
        }
        FailTests.prototype.expectFails = function () {
            this.throws(function () {
                this.fail();
            });
        };
        return FailTests;
    })(tsUnit.TestClass);
    ExampleTests.FailTests = FailTests;
    var ExecutesWithinTests = (function (_super) {
        __extends(ExecutesWithinTests, _super);
        function ExecutesWithinTests() {
            _super.apply(this, arguments);
        }
        ExecutesWithinTests.prototype.withFastFunction = function () {
            this.executesWithin(function () {
                var start = window.performance.now();
                while ((window.performance.now() - start) < 20) { }
            }, 100);
        };
        ExecutesWithinTests.prototype.withSlowFunction = function () {
            var _this = this;
            this.throws(function () {
                _this.executesWithin(function () {
                    var start = window.performance.now();
                    while ((window.performance.now() - start) < 101) { }
                }, 100);
            });
        };
        ExecutesWithinTests.prototype.withFailingFunction = function () {
            var _this = this;
            this.throws(function () {
                _this.executesWithin(function () {
                    var start = window.performance.now();
                    throw 'Error';
                }, 100);
            });
        };
        return ExecutesWithinTests;
    })(tsUnit.TestClass);
    ExampleTests.ExecutesWithinTests = ExecutesWithinTests;
    var ParameterizedTests = (function (_super) {
        __extends(ParameterizedTests, _super);
        function ParameterizedTests() {
            _super.call(this);
            this.parameterizeUnitTest(this.sumTests, [
                [1, 1, 2],
                [2, 3, 5],
                [4, 5, 9]
            ]);
            this.parameterizeUnitTest(this.nonSumTests, [
                [1, 1, 1],
                [4, 5, 1]
            ]);
        }
        ParameterizedTests.prototype.sumTests = function (a, b, sum) {
            var c = a + b;
            this.areIdentical(c, sum);
        };
        ParameterizedTests.prototype.nonSumTests = function (a, b, notsum) {
            var c = a + b;
            this.areNotIdentical(c, notsum);
        };
        ParameterizedTests.prototype.normalTest = function () {
            this.isTrue(true);
        };
        return ParameterizedTests;
    })(tsUnit.TestClass);
    ExampleTests.ParameterizedTests = ParameterizedTests;
})(ExampleTests || (ExampleTests = {}));
