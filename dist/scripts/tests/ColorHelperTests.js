/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path="tsUnit.ts" />
/// <reference path="../utils/ColorHelper.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ColorHelperTests;
(function (ColorHelperTests) {
    var gvt = it.marionegri.givitiweb;
    var GetColorTests = (function (_super) {
        __extends(GetColorTests, _super);
        function GetColorTests() {
            _super.apply(this, arguments);
        }
        GetColorTests.prototype.setUp = function () {
            this.myColorHelper = new gvt.ColorFromPercentage();
        };
        GetColorTests.prototype.tearDown = function () {
            this.myColorHelper = null;
        };
        GetColorTests.prototype.testZeroPercentage = function () {
            var result = this.myColorHelper.getColor(0);
            var color = { r: 0x00, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        };
        GetColorTests.prototype.testHalfPercentage = function () {
            var result = this.myColorHelper.getColor(0.5);
            var color = { r: 0xff, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        };
        GetColorTests.prototype.testFullPercentage = function () {
            var result = this.myColorHelper.getColor(1);
            var color = { r: 0xff, g: 0x00, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        };
        GetColorTests.prototype.testNegativePercentage = function () {
            var result = this.myColorHelper.getColor(-1);
            var color = { r: 0x00, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        };
        GetColorTests.prototype.testMoreThanOnePercentage = function () {
            var result = this.myColorHelper.getColor(1.5);
            var color = { r: 0xff, g: 0x00, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        };
        GetColorTests.prototype.testBelowHalfPercentage = function () {
            var result = this.myColorHelper.getColor(0.3);
            var check = result.r <= 0xff && result.g == 0xff && result.b == 0;
            this.isTrue(check);
        };
        GetColorTests.prototype.testOverHalfPercentage = function () {
            var result = this.myColorHelper.getColor(0.6);
            var check = result.r == 0xff && result.g < 0xff && result.b == 0;
            this.isTrue(check);
        };
        GetColorTests.defaultColorMap = [
            // ORDER is important!
            { percentage: 0.0, color: { r: 0x00, g: 0xff, b: 0 } },
            { percentage: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
            { percentage: 1.0, color: { r: 0xff, g: 0x00, b: 0 } }
        ];
        return GetColorTests;
    })(tsUnit.TestClass);
    ColorHelperTests.GetColorTests = GetColorTests;
})(ColorHelperTests || (ColorHelperTests = {}));
