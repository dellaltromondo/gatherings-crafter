/// <reference path="tsUnit.ts" />
/// <reference path="ControllerTests.ts" />
window.onload = function () {
    // Instantiate tsUnit and pass in modules that contain tests
    var test = new tsUnit.Test(ControllerTests, StructuralDataControllerTests);
    // Show the test results
    test.showResults(document.getElementById('result'), test.run());
};
