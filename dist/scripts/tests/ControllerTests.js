/// <reference path="tsUnit.ts" />
/// <reference path="../controllers/Controller.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ControllerTests;
(function (ControllerTests) {
    var gvt = it.marionegri.givitiweb;
    var ClearDataTests = (function (_super) {
        __extends(ClearDataTests, _super);
        function ClearDataTests() {
            _super.apply(this, arguments);
        }
        ClearDataTests.prototype.setUp = function () {
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null, null, null);
        };
        ClearDataTests.prototype.tearDown = function () {
            this.myController = null;
        };
        ClearDataTests.prototype.testClearedString = function () {
            var crfData = { "campo2": "ciao" };
            var crfDataWithEmptyString = { "campo2": "" };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo2, "");
        };
        ClearDataTests.prototype.testClearedStringInObject = function () {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "ciao" } };
            var crfDataWithEmptyString = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo3.campo4, "");
        };
        ClearDataTests.prototype.testClearedStringInArray = function () {
            var crfData = { 'campo1': ["ciao", "ciaone"], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo1[0], "");
        };
        return ClearDataTests;
    })(tsUnit.TestClass);
    ControllerTests.ClearDataTests = ClearDataTests;
    var FillDataTests = (function (_super) {
        __extends(FillDataTests, _super);
        function FillDataTests() {
            _super.apply(this, arguments);
        }
        FillDataTests.prototype.setUp = function () {
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null, null, null);
        };
        FillDataTests.prototype.tearDown = function () {
            this.myController = null;
        };
        FillDataTests.prototype.testFilledString = function () {
            var crfData = { "campo1": "" };
            var crfDataWithString = { "campo1": "ciao" };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData, crfDataWithString), "Unable to fill string correctly");
        };
        FillDataTests.prototype.testFilledStringInObject = function () {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            var crfDataWithString = { 'campo1': [], "campo2": "", "campo3": { "campo4": "ciao" } };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData, crfDataWithString), "Unable to fill Object correctly");
        };
        FillDataTests.prototype.testFilledStringInArray = function () {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            var crfDataWithString = { 'campo1': ["ciao", "riciao"], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData, crfDataWithString), "Unable to fill vector correctly");
        };
        FillDataTests.prototype.testFillDataDoesNotReferenceOriginals = function () {
            var crfData = { 'campo1': [] };
            var newData = { 'campo1': ["ciao", "riciao"] };
            this.myController.fillData(crfData, newData);
            newData.campo1[0] = "ciaoCambiato";
            this.areIdentical(crfData.campo1[0], "ciao");
        };
        FillDataTests.prototype.testFillDataDoesNotReferenceOriginalsInDepth = function () {
            var crfData = { 'campo1': { 'campo2': [] } };
            var newData = { 'campo1': { 'campo2': ["ciao", "riciao"] } };
            this.myController.fillData(crfData, newData);
            newData.campo1.campo2[0] = "ciaoCambiato";
            this.areIdentical(crfData.campo1.campo2[0], "ciao");
        };
        return FillDataTests;
    })(tsUnit.TestClass);
    ControllerTests.FillDataTests = FillDataTests;
    var RemoveElementFromSetTests = (function (_super) {
        __extends(RemoveElementFromSetTests, _super);
        function RemoveElementFromSetTests() {
            _super.apply(this, arguments);
        }
        RemoveElementFromSetTests.prototype.setUp = function () {
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null, null, null);
        };
        RemoveElementFromSetTests.prototype.tearDown = function () {
            this.myController = null;
        };
        RemoveElementFromSetTests.prototype.testElementIsRemoved = function () {
            var guid = gvt.GuidUtils.createGUID();
            var currentSet = [{ guid: guid }];
            //this.myController.removeElementFromSet(currentSet, guid);
            this.areIdentical(currentSet.length, 0);
        };
        RemoveElementFromSetTests.prototype.testElementIsNotRemoved = function () {
            var guid1 = "a";
            var guid2 = "b";
            var guid3 = "c";
            var currentSet = [{ guid: guid1 }, { guid: guid3 }];
            try {
            }
            catch (e) {
            }
            this.areIdentical(currentSet.length, 2);
        };
        RemoveElementFromSetTests.prototype.testFailedWhenElementNotFound = function () {
            var guid1 = "a";
            var guid2 = "b";
            var currentSet = [{ guid: guid1 }];
            this.throws(function () {
                //this.myController.removeElementFromSet(currentSet, guid2);
            });
        };
        return RemoveElementFromSetTests;
    })(tsUnit.TestClass);
    ControllerTests.RemoveElementFromSetTests = RemoveElementFromSetTests;
    var RemoveEmptyElementFromSetTests = (function (_super) {
        __extends(RemoveEmptyElementFromSetTests, _super);
        function RemoveEmptyElementFromSetTests() {
            _super.apply(this, arguments);
        }
        RemoveEmptyElementFromSetTests.prototype.setUp = function () {
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null, null, null);
        };
        RemoveEmptyElementFromSetTests.prototype.testNotFailWhenNull = function () {
            try {
                this.myController.removeEmptyObjectsFromSets(null);
            }
            catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are passed");
            }
        };
        RemoveEmptyElementFromSetTests.prototype.testNotFailWhenObjectContainsNull = function () {
            try {
                this.myController.removeEmptyObjectsFromSets({ a: null });
            }
            catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are passed");
            }
        };
        RemoveEmptyElementFromSetTests.prototype.testNotFailWhenNullInArray = function () {
            try {
                this.myController.removeEmptyObjectsFromSets([null]);
            }
            catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are in the array");
            }
        };
        RemoveEmptyElementFromSetTests.prototype.testRemoveNullsFromArray = function () {
            var array = [null];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        };
        RemoveEmptyElementFromSetTests.prototype.testRemove = function () {
            var array = [{ a: "a" }, { b: "b" }, {}];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, [{ a: "a" }, { b: "b" }]));
        };
        RemoveEmptyElementFromSetTests.prototype.testRemoveWhenLonelyElement = function () {
            var array = [{}];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        };
        RemoveEmptyElementFromSetTests.prototype.testRemoveMultipleElements = function () {
            var array = [{}, {}];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        };
        RemoveEmptyElementFromSetTests.prototype.testRemoveWhenObjectChild = function () {
            var data = { array: [{ a: "a" }, { b: "b" }, {}] };
            this.myController.removeEmptyObjectsFromSets(data);
            this.isTrue(angular.equals(data.array, [{ a: "a" }, { b: "b" }]));
        };
        return RemoveEmptyElementFromSetTests;
    })(tsUnit.TestClass);
    ControllerTests.RemoveEmptyElementFromSetTests = RemoveEmptyElementFromSetTests;
    var AddElementToSetTests = (function (_super) {
        __extends(AddElementToSetTests, _super);
        function AddElementToSetTests() {
            _super.apply(this, arguments);
        }
        AddElementToSetTests.prototype.setUp = function () {
            var mockJSONService = new tsUnit.Fake(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", function () { return { then: function () { } }; });
            var mockScope = {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null, null, null);
        };
        AddElementToSetTests.prototype.tearDown = function () {
            this.myController = null;
        };
        AddElementToSetTests.prototype.testNewElementAddedWhenEmpty = function () {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 1);
        };
        AddElementToSetTests.prototype.testNewElementAddedWhenNotEmpty = function () {
            var mySet = [{ guid: "fake" }, { guid: "secondFake" }];
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 3);
        };
        AddElementToSetTests.prototype.testMultipleAdding = function () {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 3);
        };
        AddElementToSetTests.prototype.testFailWhenUndefinedList = function () {
            var mySet;
            this.throws(function () {
                //this.myController.addElementToSet(mySet);
            });
        };
        AddElementToSetTests.prototype.testFailWhenString = function () {
            var mySet = "";
            this.throws(function () {
                //this.myController.addElementToSet(mySet);
            });
        };
        AddElementToSetTests.prototype.testGUIDIsNotEmpty = function () {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            this.areNotIdentical(mySet[0].guid, "");
            this.areNotIdentical(mySet[0].guid, gvt.GuidUtils.getEmptyGUID());
        };
        AddElementToSetTests.prototype.testGUIDIsDifferentWhenAddingTwo = function () {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            this.areNotIdentical(mySet[0].guid, mySet[1].guid);
        };
        return AddElementToSetTests;
    })(tsUnit.TestClass);
    ControllerTests.AddElementToSetTests = AddElementToSetTests;
})(ControllerTests || (ControllerTests = {}));
