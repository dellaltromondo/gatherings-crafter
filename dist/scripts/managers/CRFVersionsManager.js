/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class CRFVersionsManager {
                constructor(baseURL, crfName, crfPath, $http) {
                    this.baseURL = baseURL;
                    this.crfName = crfName;
                    this.crfPath = crfPath;
                    this.$http = $http;
                    this.crfVersionsXML = null;
                }
                /** Given date in jsonFormat return the containing CRF version */
                getCrfVersion(date) {
                    if (this.crfVersionsXML != null) {
                        var versions = this.crfVersionsXML.getElementsByTagName("version");
                        var version = "";
                        for (var i = 0; i < versions.length; i++) {
                            var startDate = versions[i].getElementsByTagName("startDate").length > 0 ? versions[i].getElementsByTagName("startDate")[0].textContent : undefined;
                            var endDate = versions[i].getElementsByTagName("endDate").length > 0 ? versions[i].getElementsByTagName("endDate")[0].textContent : undefined;
                            if (startDate <= date) {
                                if (endDate) {
                                    if (endDate >= date) {
                                        version = versions[i].getAttribute("number");
                                        break;
                                    }
                                }
                                else {
                                    version = versions[i].getAttribute("number");
                                }
                            }
                        }
                        return version;
                    }
                    return "";
                }
                /** Given date in jsonFormat return the containing startDate and endDate */
                getCrfValidity(date) {
                    if (this.crfVersionsXML != null) {
                        var versions = this.crfVersionsXML.getElementsByTagName("version");
                        var validity = {};
                        for (var i = 0; i < versions.length; i++) {
                            var startDate = versions[i].getElementsByTagName("startDate").length > 0 ? versions[i].getElementsByTagName("startDate")[0].textContent : undefined;
                            var endDate = versions[i].getElementsByTagName("endDate").length > 0 ? versions[i].getElementsByTagName("endDate")[0].textContent : undefined;
                            if (startDate <= date) {
                                if (endDate) {
                                    if (endDate >= date) {
                                        validity.startDate = startDate;
                                        validity.endDate = endDate;
                                        break;
                                    }
                                }
                                else {
                                    validity.startDate = startDate;
                                    if (versions[i + 1]) {
                                        validity.endDate = moment(versions[i + 1].getElementsByTagName("startDate")[0].textContent).subtract(1, "days").format(givitiweb.GlobalConfig.jsonDateFormat);
                                    }
                                    else {
                                        validity.endDate = "2099-12-31";
                                    }
                                }
                            }
                        }
                        return validity;
                    }
                    return null;
                }
                /** Return the URI path to get the CRF XML for specified version */
                getCrfPath(version) {
                    return this.baseURL + "GetCRF?path=" + this.crfPath + "&name=" + this.crfName + "&version=" + version;
                }
                /** Return the URI path to get the CRF VERSIONS */
                getVersionsPath() {
                    return this.baseURL + "GetCRFVersions?path=" + this.crfPath + "&name=" + this.crfName;
                }
                /** Load a specified XML versions from services */
                loadVersions() {
                    var versionsPath = this.getVersionsPath();
                    return givitiweb.PromiseUtils.getAsyncXML(this.$http, versionsPath).then((versionsData) => {
                        this.crfVersionsXML = versionsData;
                        return versionsData;
                    }, error => "");
                }
            }
            givitiweb.CRFVersionsManager = CRFVersionsManager;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
