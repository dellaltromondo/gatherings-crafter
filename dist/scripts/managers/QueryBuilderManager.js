/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class QueryBuilderManager {
                constructor() {
                    this.exportList = [];
                    this.joinsList = [];
                }
                buildQuery(json, startingTable, startingId, shouldAddExportConditions) {
                    var whereClause = this.buildQueryIterator(json, startingTable, "");
                    var exportClause = this.getExportClause(startingTable, startingId);
                    var joinClause = this.getJoinClause();
                    return this.getQuery(startingTable, exportClause, joinClause, whereClause);
                }
                getExportClause(startingTable, startingId) {
                    if (this.exportList) {
                        var idName = this.getColumnFullName(startingTable, startingId);
                        this.exportList.unshift(idName);
                    }
                    var exportClause = this.exportList.join(", ");
                    if (!exportClause)
                        exportClause = startingTable + ".*";
                    return exportClause;
                }
                getJoinClause() {
                    var joinClause = "";
                    for (var clauseIndex in this.joinsList) {
                        var joinObject = this.joinsList[clauseIndex];
                        joinClause = joinClause + " INNER JOIN " + this.quoteString(joinObject.dataType)
                            + " AS " + this.quoteString(joinObject.currentNamespace)
                            + " ON " + this.quoteString(joinObject.startingTable) + "." + this.quoteString(joinObject.key)
                            + " = " + this.quoteString(joinObject.currentNamespace) + "." + this.quoteString(joinObject.key);
                    }
                    return joinClause;
                }
                getQuery(startingTable, exportClause, subQuery, whereClause) {
                    var query = "SELECT DISTINCT " + exportClause + " FROM \"" + startingTable + "\"" + subQuery;
                    if (whereClause) {
                        query = query + " WHERE " + whereClause;
                    }
                    return query;
                }
                buildQueryIterator(json, startingTable, currentNamespace) {
                    var usedNamespace = currentNamespace;
                    var whereCondition = "";
                    if (!currentNamespace)
                        usedNamespace = startingTable;
                    var andObject = { booleanOperator: "and", booleans: [] };
                    for (var key in json) {
                        var variable = json[key];
                        var field = this.getColumnFullName(usedNamespace, key);
                        if (this.isExportType(variable)) {
                            if (this.exportList.indexOf(field) == -1) {
                                this.exportList.push(field);
                            }
                        }
                        if (this.isFilterType(variable)) {
                            if ((variable.filter == true) || (variable.filter == false)) {
                                var newCondition = this.getBooleanCondition(variable, field);
                                if (!whereCondition)
                                    whereCondition = newCondition;
                                else
                                    whereCondition = whereCondition + " AND " + newCondition;
                            }
                            else if (variable.filter != undefined) {
                                if (variable.operator != "" && variable.operator != undefined && variable.filter.length != 0) {
                                    if (variable.operator != "between") {
                                        var newCondition = this.getAutocompleteCondition(variable, field);
                                        if (!whereCondition)
                                            whereCondition = newCondition;
                                        else
                                            whereCondition = whereCondition + " AND " + newCondition;
                                    }
                                    else {
                                        var lowerCondition = this.getBetweenCondition(variable, field, "lowerBound");
                                        var upperCondition = this.getBetweenCondition(variable, field, "upperBound");
                                        if (!whereCondition)
                                            whereCondition = lowerCondition + " AND " + upperCondition;
                                        else
                                            whereCondition = whereCondition + " AND " + lowerCondition + " AND " + upperCondition;
                                    }
                                }
                            }
                        }
                        if (this.isInstanceOfType(variable)) {
                            var orCondition = "";
                            for (var index in variable) {
                                var newNamespace = this.getNewNamespace(currentNamespace, variable[index].dataType) + "_" + index;
                                var newJoinVariable = { startingTable: usedNamespace, dataType: variable[index].dataType, key: variable[index].onKey, currentNamespace: newNamespace };
                                this.joinsList.push(newJoinVariable);
                                var newOrCondition = this.buildQueryIterator(variable[index].variables, variable[index].dataType, newNamespace);
                                if (newOrCondition)
                                    if (!orCondition)
                                        orCondition = newOrCondition;
                                    else
                                        orCondition = orCondition + " OR " + newOrCondition;
                            }
                            if (orCondition) {
                                if (!whereCondition)
                                    whereCondition = orCondition;
                                else
                                    whereCondition = whereCondition + " AND (" + orCondition + ") ";
                            }
                        }
                    }
                    return whereCondition;
                }
                getNewNamespace(currentNamespace, keyNamespace) {
                    var newNamespaceToken = "instanceOf." + keyNamespace;
                    if (currentNamespace === "")
                        return newNamespaceToken;
                    return currentNamespace + "-" + newNamespaceToken;
                }
                isExportType(variable) {
                    if (variable.export)
                        return true;
                    return false;
                }
                isFilterType(variable) {
                    if (variable.filter && variable.operator)
                        return true;
                    return false;
                }
                isInstanceOfType(variable) {
                    if (angular.isArray(variable))
                        return true;
                    return false;
                }
                getBooleanCondition(variable, field) {
                    return field + " " + "=" + " " + this.quoteAndEscapeValue(variable.filter);
                }
                getBetweenCondition(variable, field, bound) {
                    var selectedOperator = '<';
                    if (bound == 'lowerBound')
                        selectedOperator = '>';
                    return field + " " + selectedOperator + " " + this.quoteAndEscapeValue(variable.filter[bound]);
                }
                getAutocompleteCondition(variable, field) {
                    var selectedValue;
                    var selectedOperator = variable.operator;
                    if (variable.filter.length == 1) {
                        selectedValue = this.quoteAndEscapeValue(variable.filter[0].key);
                    }
                    else {
                        if (variable.operator == '=') {
                            selectedOperator = " IN ";
                        }
                        else {
                            selectedOperator = " NOT IN ";
                        }
                        var selectedValue = variable.filter.map(function (elem) {
                            //TODO: should unify with quoteAndEscapeValue.
                            //Currently splitted because this.quoteAndEscapeValue doesn't work inside map
                            var value = elem.text;
                            var newValue = "";
                            if (value && typeof value == "string")
                                newValue = value.replace(/'/g, "''");
                            else
                                newValue = '';
                            return "'" + newValue + "'";
                        }).join(",");
                        if (selectedValue) {
                            selectedValue = "(" + selectedValue + ")";
                        }
                    }
                    return field + " " + selectedOperator + " " + selectedValue;
                }
                quoteAndEscapeValue(value) {
                    var newValue = "";
                    if (value && typeof value == "string")
                        newValue = value.replace(/'/g, "''");
                    else
                        newValue = '';
                    return "'" + newValue + "'";
                }
                quoteString(nonQuotedString) {
                    return "\"" + nonQuotedString + "\"";
                }
                getColumnFullName(tableName, columnName) {
                    return this.quoteString(tableName) + "." + this.quoteString(columnName);
                }
            }
            givitiweb.QueryBuilderManager = QueryBuilderManager;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
