/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class EmpiricalAntibioticTherapyController extends givitiweb.AbstractController {
                constructor($q, $http, $scope, $rootScope, $timeout, jsonService, multipleIndexesJSONService, crfServiceURL, dataType, dataPath) {
                    super($q, $scope, $timeout, jsonService);
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$rootScope = $rootScope;
                    this.$timeout = $timeout;
                    this.jsonService = jsonService;
                    this.multipleIndexesJSONService = multipleIndexesJSONService;
                    this.$inject = ['$q', '$http', '$scope', '$rootScope', '$timeout', 'jsonService', 'multipleIndexesJSONService', 'crfServiceURL', 'dataType', 'dataPath'];
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    this.$scope.indexesData = {};
                    this.loadJsonData();
                }
                loadMultipleIndexes(indexDataKeyList) {
                    var deferred = this.$q.defer();
                    this.multipleIndexesJSONService.switchTo(indexDataKeyList)
                        .then(() => {
                        this.multipleIndexesJSONService.$get()
                            .then((data) => {
                            if (!this.$scope.indexesData)
                                this.$scope.indexesData = {};
                            for (var indexDataKey in data) {
                                if (angular.equals(data[indexDataKey], {})) {
                                    this.$scope.indexesData[indexDataKey] = [];
                                }
                                else {
                                    this.$scope.indexesData[indexDataKey] = data[indexDataKey];
                                }
                            }
                            deferred.resolve(data);
                        });
                    });
                    return deferred.promise;
                }
                saveJsonData() {
                    var indexDataKeyList = [];
                    for (var idx in this.$rootScope.indexes) {
                        var newIndexDataKey = givitiweb.JsonUtils.getIndexDataKey(this.$rootScope.indexes[idx].indexClass, this.$rootScope.indexes[idx].indexName);
                        givitiweb.JsonUtils.addValueToList(indexDataKeyList, newIndexDataKey);
                    }
                    if (indexDataKeyList.length > 0) {
                        return this.loadMultipleIndexes(indexDataKeyList)
                            .then(() => {
                            for (var idx in this.$rootScope.indexes) {
                                var newIndexDataKey = givitiweb.JsonUtils.getIndexDataKey(this.$rootScope.indexes[idx].indexClass, this.$rootScope.indexes[idx].indexName);
                                var variableValue = givitiweb.JsonUtils.getValueFromJson(this.$scope, this.$rootScope.indexes[idx].variablePath);
                                var values = givitiweb.JsonUtils.getValuesFromVariable(variableValue);
                                for (var idx2 in values) {
                                    if (!this.$scope.indexesData[newIndexDataKey])
                                        this.$scope.indexesData[newIndexDataKey] = [];
                                    givitiweb.JsonUtils.addValueToList(this.$scope.indexesData[newIndexDataKey], values[idx2]);
                                }
                            }
                            return this.$q.all([
                                this.saveMultipleIndexes(indexDataKeyList),
                                super.saveJsonData()
                            ]);
                        });
                    }
                    else {
                        return super.saveJsonData();
                    }
                }
                saveMultipleIndexes(indexDataKeyList) {
                    var deferred = this.$q.defer();
                    this.multipleIndexesJSONService.switchTo(indexDataKeyList)
                        .then(() => {
                        this.multipleIndexesJSONService.save(this.$scope.indexesData)
                            .then((data) => {
                            deferred.resolve(data);
                        });
                    });
                    return deferred.promise;
                }
            }
            givitiweb.EmpiricalAntibioticTherapyController = EmpiricalAntibioticTherapyController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
