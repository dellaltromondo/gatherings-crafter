/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../../src/utils/GlobalUtils.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class ModelSeparationController extends givitiweb.AbstractController {
                constructor($q, $scope, $timeout, $element, storage, keysStorage, userId, centreCode, patientStatus) {
                    super($q, $scope, $timeout, new givitiweb.JSONService($q, new givitiweb.NaiveGUIDStorage(null, $q, null), storage, keysStorage, patientStatus), userId, centreCode);
                    this.$inject = ['$scope'];
                    //TODO: use DataClass directive instead?
                    this.guid = givitiweb.GuidUtils.createGUID();
                    givitiweb.Controller.registerModelSeparationController(this);
                    // Adding a watch to load data when this is visible
                    this.loadIfVisible($timeout, $element);
                    $scope.$on("PageChanged", _ => this.loadIfVisible($timeout, $element));
                }
                loadIfVisible($timeout, $element) {
                    $timeout(_ => {
                        if (!this.$scope.status.dataLoaded && $element[0].offsetWidth > 0 && $element[0].offsetHeight > 0) {
                            this.loadJsonData();
                        }
                    }, 1);
                }
                getGUID() {
                    return this.guid;
                }
                setGUID(guid) {
                    if (guid != null) {
                        this.guid = guid;
                        this.jsonService.switchTo(guid);
                    }
                }
            }
            givitiweb.ModelSeparationController = ModelSeparationController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
