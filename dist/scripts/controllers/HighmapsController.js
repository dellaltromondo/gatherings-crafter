/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/highcharts/highcharts-ng.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class HighmapsController {
                /*
                constructor($scope: HighchartsModel, MigrantijsonService: JSONService) {
                    $scope.lvm = this;
                }
        */
                constructor($q, $scope, multipleSchedeDatiJSONService) {
                    this.$q = $q;
                    this.$scope = $scope;
                    this.multipleSchedeDatiJSONService = multipleSchedeDatiJSONService;
                    // public $inject = ['$scope', 'MigrantijsonService'];
                    this.$inject = ['$q', '$scope', 'multipleSchedeDatiJSONService'];
                    this.initHighMaps();
                    this.$scope.lvm = this;
                    this.$scope.loading = true;
                    this.$scope.loadJsonData = this.loadJsonData()
                        .then(() => {
                        this.iterateJSONMap();
                        this.$scope.loading = false;
                    });
                }
                // PARTE CARICAMENTO JSON
                initHighMaps() {
                    this.$scope.Highcharts = {
                        options: {
                            legend: {
                                enabled: true
                            },
                            plotOptions: {
                                map: {
                                    mapData: Highcharts.map['countries/it/it-all']
                                }
                            },
                            width: 1000,
                            height: 1000,
                            colorAxis: {
                                min: 1,
                                max: 300,
                                type: 'logarithmic'
                            },
                            mapNavigation: {
                                enabled: true,
                                enableDoubleClickZoomTo: true
                            },
                        },
                        chartType: 'map',
                        title: {
                            text: 'Mappa parti in casa'
                        },
                        series: []
                    };
                }
                loadJsonData() {
                    return this.$q.all([
                        this.loadMultipleform(),
                    ]);
                }
                loadMultipleform() {
                    return this.multipleSchedeDatiJSONService.$get()
                        .then((data) => {
                        this.$scope.dizionarioSchedaDati = data;
                    });
                }
                iterateJSONMap() {
                    var data = this.$scope.dizionarioSchedaDati;
                    var crfDatiGeo = [];
                    var i = 0;
                    //recupero dal json in db tutte le schede che hanno regioni e province
                    for (var guid in data) {
                        if (data[guid].core.primaParte != null && data[guid].core.primaParte.provinciaResidenza != null) {
                            crfDatiGeo[i] = { "regione": data[guid].core.primaParte.regioneResidenza, "provincia": data[guid].core.primaParte.provinciaResidenza };
                            i++;
                        }
                    }
                    this.$scope.listaPosizioni = crfDatiGeo;
                    //var countProvince = [];
                    var listProvince = JSON.parse(eval("jsonProvince"));
                    //            for (var i = 0, l = crfDatiGeo.length; i < l; i++) {
                    //                var prov = crfDatiGeo[i].provincia;
                    //                countProvince.push(prov);
                    //            }
                    var countListProvince = [];
                    for (var obj in listProvince) {
                        for (var r = 0; r < listProvince[obj].length; r++) {
                            for (var s = 0; s < crfDatiGeo.length; s++) {
                                if (listProvince[obj][r].key == crfDatiGeo[s].provincia) {
                                    countListProvince.push(listProvince[obj][r].label);
                                }
                            }
                        }
                    }
                    //this.$scope.listaProvinceNominali = countListProvince;
                    this.$scope.listaProvinceNominaliSbagliato = this.compressArray(countListProvince);
                    //ultima parte
                    var finale = [];
                    var listState = JSON.parse(eval("itMap"));
                    var listaProv = this.compressArray(countListProvince);
                    for (var b = 0; b < listaProv.length; b++) {
                        for (var x = 0; x < listState.features.length; x++) {
                            if (listState.features[x].properties.name == listaProv[b].provincia) {
                                var values = { "hc-key": listState.features[x].properties["hc-key"], "value": listaProv[b].totale };
                                finale.push(values);
                            }
                        }
                    }
                    this.$scope.risultato = finale;
                    this.$scope.Highcharts.series.push({ data: finale, joinBy: 'hc-key', name: 'numero parti', allAreas: true, states: { hover: { color: '#BADA55' } } });
                }
                compressArray(original) {
                    var compressed = [];
                    // make a copy of the input array
                    var copy = original.slice(0);
                    // first loop goes over every element
                    for (var i = 0; i < original.length; i++) {
                        var myCount = 0;
                        // loop over every element in the copy and see if it's the same
                        for (var w = 0; w < copy.length; w++) {
                            if (original[i] == copy[w]) {
                                // increase amount of times duplicate is found
                                myCount++;
                                // sets item to undefined
                                delete copy[w];
                            }
                        }
                        if (myCount > 0) {
                            var result = new Object();
                            result = { "provincia": original[i], "totale": myCount };
                            compressed.push(result);
                        }
                    }
                    return compressed;
                }
            }
            givitiweb.HighmapsController = HighmapsController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
