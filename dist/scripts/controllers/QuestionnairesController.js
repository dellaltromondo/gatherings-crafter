/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class QuestionnairesController extends givitiweb.AbstractController {
                constructor($q, $http, $scope, $timeout, guidStorage, jsonService, listJSONService, crfServiceURL, dataType, dataPath) {
                    super($q, $scope, $timeout, jsonService);
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.guidStorage = guidStorage;
                    this.jsonService = jsonService;
                    this.listJSONService = listJSONService;
                    this.dataType = dataType;
                    this.$inject = ['$q', '$http', '$scope', '$timeout', 'guidStorage', 'jsonService', 'listJSONService', 'crfServiceURL', 'dataType', 'dataPath'];
                    this.admissionKey = givitiweb.PageUtils.getAdmissionKey();
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    this.loadJsonData();
                }
                getGuidFromList(listData) {
                    var result = givitiweb.GuidUtils.getEmptyGUID();
                    for (var i in listData) {
                        if (listData[i].admissionKey == this.admissionKey) {
                            for (var j in listData[i].questionnaires) {
                                if (listData[i].questionnaires[j].dataType == this.dataType) {
                                    result = listData[i].questionnaires[j].guid;
                                }
                            }
                        }
                    }
                    return result;
                }
                addGuidToList(listData, guid) {
                    if (listData && guid) {
                        var admissionIdx = -1;
                        for (var listIdx in listData) {
                            if (listData[listIdx].admissionKey == this.admissionKey)
                                admissionIdx = listIdx;
                            break;
                        }
                        if (admissionIdx != -1) {
                            var questIdx = -1;
                            for (var listIdx in listData[admissionIdx].questionnaires) {
                                if (listData[admissionIdx].questionnaires[listIdx].dataType == this.dataType)
                                    questIdx = listIdx;
                                break;
                            }
                            if (questIdx != -1) {
                                listData[admissionIdx].questionnaires[questIdx].guid = guid;
                            }
                            else {
                                listData[admissionIdx].questionnaires.push({ "dataType": this.dataType, "guid": guid });
                            }
                        }
                        else {
                            listData.push({ "admissionKey": this.admissionKey, "questionnaires": [{ "dataType": this.dataType, "guid": guid }] });
                        }
                    }
                }
                loadJsonData() {
                    var deferred = this.$q.defer();
                    this.listJSONService.$get()
                        .then((listData) => {
                        if (angular.equals(listData, {})) {
                            this.$scope.list = [];
                        }
                        else {
                            this.$scope.list = listData;
                        }
                        var guid = this.getGuidFromList(listData);
                        this.guidStorage.update(guid);
                        super.loadJsonData()
                            .then((data) => {
                            deferred.resolve(data);
                        });
                    });
                    return deferred.promise;
                }
                saveJsonData() {
                    var deferred = this.$q.defer();
                    this.jsonService.save(this.$scope.crfs)
                        .then((guid) => {
                        this.addGuidToList(this.$scope.list, guid);
                        this.listJSONService.save(this.$scope.list)
                            .then(() => {
                            this.$scope.status.dataModified = false;
                            this.$scope.status.dataSaved = true;
                            var changeVariableStatus = () => {
                                this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
                            };
                            this.$timeout(changeVariableStatus, 4000);
                            deferred.resolve({});
                        });
                    });
                    return deferred.promise;
                }
            }
            givitiweb.QuestionnairesController = QuestionnairesController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
