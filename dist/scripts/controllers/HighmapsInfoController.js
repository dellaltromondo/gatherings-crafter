/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/highcharts/highcharts-ng.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class HighmapsInfoController {
                /*
                constructor($scope: HighchartsModel, MigrantijsonService: JSONService) {
                    $scope.lvm = this;
                }
        */
                constructor($q, $scope, multipleSchedeDatiJSONService) {
                    this.$q = $q;
                    this.$scope = $scope;
                    this.multipleSchedeDatiJSONService = multipleSchedeDatiJSONService;
                    // public $inject = ['$scope', 'MigrantijsonService'];
                    this.$inject = ['$q', '$scope', 'multipleSchedeDatiJSONService'];
                    this.initHighMaps();
                    this.initHighCharts();
                    this.$scope.lvm = this;
                    this.$scope.loading = true;
                    this.$scope.loadJsonData = this.loadJsonData()
                        .then(() => {
                        this.iterateJSONMap();
                        this.iterateJSONChart();
                        this.$scope.loading = false;
                    });
                }
                initHighCharts() {
                    this.$scope.highchartsNG = {
                        title: {
                            text: 'Parti in casa anno 2015'
                        },
                        xAxis: {
                            categories: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']
                        },
                        //series: [{ type: 'column', name: 'vuoto', data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] }]
                        series: []
                    };
                }
                //PARTE DI TEST
                //FINE PARTE DI TEST
                initData() {
                    var data = new Date();
                    var gg, month, aaaa;
                    var result;
                    gg = data.getDate() + "/";
                    month = data.getMonth() + 1 + "/";
                    aaaa = data.getFullYear();
                    var Hh, Mm, Ss, mm;
                    Hh = data.getHours() + ":";
                    Mm = data.getMinutes() + ":";
                    Ss = data.getSeconds();
                    result = gg + month + aaaa + " alle ore " + Hh + Mm + Ss;
                    return result;
                }
                // PARTE CARICAMENTO JSON
                initHighMaps() {
                    this.$scope.Highcharts = {
                        options: {
                            legend: {
                                enabled: true
                            },
                            plotOptions: {
                                map: {
                                    mapData: Highcharts.map['countries/it/it-all']
                                }
                            },
                            width: 1000,
                            height: 1000,
                            colorAxis: {
                                min: 1,
                                max: 300,
                                type: 'logarithmic'
                            },
                            mapNavigation: {
                                enabled: true,
                                enableDoubleClickZoomTo: true
                            },
                        },
                        chartType: 'map',
                        title: {
                            text: 'Mappa parti in casa aggiornati al ' + this.initData()
                        },
                        series: []
                    };
                }
                loadJsonData() {
                    return this.$q.all([
                        this.loadMultipleform(),
                    ]);
                }
                loadMultipleform() {
                    return this.multipleSchedeDatiJSONService.$get()
                        .then((data) => {
                        this.$scope.dizionarioSchedaDati = data;
                    });
                }
                iterateJSONMap() {
                    var data = this.$scope.dizionarioSchedaDati;
                    var crfDatiGeo = [];
                    var i = 0;
                    //recupero dal json in db tutte le schede che hanno regioni e province
                    for (var guid in data) {
                        if (data[guid].core.primaParte != null && data[guid].core.primaParte.provinciaResidenza != null) {
                            crfDatiGeo[i] = { "regione": data[guid].core.primaParte.regioneResidenza, "provincia": data[guid].core.primaParte.provinciaResidenza };
                            i++;
                        }
                    }
                    this.$scope.listaPosizioni = crfDatiGeo;
                    //var countProvince = [];
                    var listProvince = JSON.parse(eval("jsonProvince"));
                    //            for (var i = 0, l = crfDatiGeo.length; i < l; i++) {
                    //                var prov = crfDatiGeo[i].provincia;
                    //                countProvince.push(prov);
                    //            }
                    var countListProvince = [];
                    for (var obj in listProvince) {
                        for (var r = 0; r < listProvince[obj].length; r++) {
                            for (var s = 0; s < crfDatiGeo.length; s++) {
                                if (listProvince[obj][r].key == crfDatiGeo[s].provincia) {
                                    countListProvince.push(listProvince[obj][r].label);
                                }
                            }
                        }
                    }
                    //this.$scope.listaProvinceNominali = countListProvince;
                    this.$scope.listaProvinceNominaliSbagliato = this.compressArray(countListProvince);
                    //ultima parte
                    var finale = [];
                    var listState = JSON.parse(eval("itMap"));
                    var listaProv = this.compressArray(countListProvince);
                    for (var b = 0; b < listaProv.length; b++) {
                        for (var x = 0; x < listState.features.length; x++) {
                            if (listState.features[x].properties.name == listaProv[b].provincia) {
                                var values = { "hc-key": listState.features[x].properties["hc-key"], "value": listaProv[b].totale };
                                finale.push(values);
                            }
                        }
                    }
                    this.$scope.risultato = finale;
                    this.$scope.Highcharts.series.push({
                        data: finale,
                        joinBy: 'hc-key',
                        name: 'numero parti',
                        allAreas: true,
                        states: {
                            hover: { color: '#BADA55' }
                        },
                        point: {
                            events: {
                                click: 
                                //this.$scope.initHighCharts();
                                (event) => {
                                    //alert('TEST, valore:' + event.point.value + 'provincia:' + this['hc-key'])
                                    //this.$scope.$apply(() => this.$scope.highchartsNG.series.push({ data: [0, 0, 0, 0, 0, 0, 0, event.point.value, 0, 0, 0, 0] }));
                                    //this.$scope.$apply(() => this.$scope.highchartsNG.title = {text: "parti a " + event.point.name + " nel 2015"});
                                    //this.$scope.$apply(() => this.$scope.highchartsNG.series = { data: [] });
                                    this.$scope.$apply(() => this.iterateJSONParti(event.point.name));
                                    //recupero nome provincia passo al metodo che poi filtra per il mio crf
                                }
                            }
                        }
                    });
                }
                iterateJSONParti(value) {
                    var data = this.$scope.dizionarioSchedaDati;
                    var list = [];
                    var maternita = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var ospedale = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var datoMancante = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var media = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var casa = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var partiCasa = 0;
                    var partiMaternita = 0;
                    var partiOspedale = 0;
                    var partoNonDefinito = 0;
                    var id;
                    for (var guid in data) {
                        //controllo che esista la terza parte che contiene il parto e se esiste la data del parto
                        if (data[guid].core.terzaParte != null && data[guid].core.terzaParte.dataParto != null && data[guid].core.primaParte != null && data[guid].core.primaParte.provinciaResidenza != null) {
                            var id = data[guid].core.terzaParte.luogoParto;
                            var str = data[guid].core.terzaParte.dataParto;
                            var res = str.substring(0, 4);
                            var month = parseInt(str.substring(5, 7));
                            //controllo che sia del 2015 il parto
                            if (res == '2015' && data[guid].core.primaParte.provinciaResidenza == this.iterateJsonProvince(value)) {
                                //devo ciclare per il mese
                                switch (data[guid].core.terzaParte.luogoParto) {
                                    case 'luogoPartoCodification.casa':
                                        casa[month - 1] += 1;
                                        partiCasa++;
                                        break;
                                    case 'luogoPartoCodification.casaMaternita':
                                        maternita[month - 1] += 1;
                                        partiMaternita++;
                                        break;
                                    case 'luogoPartoCodification.ospedale':
                                        ospedale[month - 1] += 1;
                                        partiOspedale++;
                                        break;
                                    case '':
                                        datoMancante[month - 1] += 1;
                                        partoNonDefinito++;
                                        break;
                                    default:
                                        datoMancante[month - 1] += 1;
                                }
                            }
                            list.push(id);
                        }
                        else {
                            partoNonDefinito++;
                        }
                        for (var i = 0; i < casa.length; i++) {
                            media[i] = (casa[i] + maternita[i] + ospedale[i] + datoMancante[i]) / 4;
                        }
                    }
                    //this.$scope.listaDati = list;
                    this.$scope.highchartsNG.title = { text: "Parti a " + value + " nel 2015" };
                    this.$scope.highchartsNG.series = [];
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'Casa', data: casa });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'maternita', data: maternita });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'ospedale', data: ospedale });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'missing', data: datoMancante });
                }
                iterateJsonProvince(value) {
                    var listProvince = JSON.parse(eval("jsonProvince"));
                    var result;
                    for (var obj in listProvince) {
                        for (var r = 0; r < listProvince[obj].length; r++) {
                            if (listProvince[obj][r].label == value) {
                                return result = listProvince[obj][r].key;
                            }
                        }
                    }
                }
                iterateJSONChart() {
                    var data = this.$scope.dizionarioSchedaDati;
                    var list = [];
                    var maternita = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var ospedale = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var datoMancante = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var media = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var casa = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var partiCasa = 0;
                    var partiMaternita = 0;
                    var partiOspedale = 0;
                    var partoNonDefinito = 0;
                    var id;
                    for (var guid in data) {
                        //controllo che esista la terza parte che contiene il parto e se esiste la data del parto
                        if (data[guid].core.terzaParte != null && data[guid].core.terzaParte.dataParto != null) {
                            var id = data[guid].core.terzaParte.luogoParto;
                            var str = data[guid].core.terzaParte.dataParto;
                            var res = str.substring(0, 4);
                            var month = parseInt(str.substring(5, 7));
                            //controllo che sia del 2015 il parto
                            if (res == '2015') {
                                //devo ciclare per il mese
                                switch (data[guid].core.terzaParte.luogoParto) {
                                    case 'luogoPartoCodification.casa':
                                        casa[month - 1] += 1;
                                        partiCasa++;
                                        break;
                                    case 'luogoPartoCodification.casaMaternita':
                                        maternita[month - 1] += 1;
                                        partiMaternita++;
                                        break;
                                    case 'luogoPartoCodification.ospedale':
                                        ospedale[month - 1] += 1;
                                        partiOspedale++;
                                        break;
                                    case '':
                                        datoMancante[month - 1] += 1;
                                        partoNonDefinito++;
                                        break;
                                    default:
                                        datoMancante[month - 1] += 1;
                                }
                            }
                            list.push(id);
                        }
                        else {
                            partoNonDefinito++;
                        }
                        for (var i = 0; i < casa.length; i++) {
                            media[i] = (casa[i] + maternita[i] + ospedale[i] + datoMancante[i]) / 4;
                        }
                    }
                    this.$scope.highchartsNG.title = { text: "Parti totali nel 2015" };
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'Casa', data: casa });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'maternita', data: maternita });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'ospedale', data: ospedale });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'missing', data: datoMancante });
                }
                compressArray(original) {
                    var compressed = [];
                    // make a copy of the input array
                    var copy = original.slice(0);
                    // first loop goes over every element
                    for (var i = 0; i < original.length; i++) {
                        var myCount = 0;
                        // loop over every element in the copy and see if it's the same
                        for (var w = 0; w < copy.length; w++) {
                            if (original[i] == copy[w]) {
                                // increase amount of times duplicate is found
                                myCount++;
                                // sets item to undefined
                                delete copy[w];
                            }
                        }
                        if (myCount > 0) {
                            var result = new Object();
                            result = { "provincia": original[i], "totale": myCount };
                            compressed.push(result);
                        }
                    }
                    return compressed;
                }
            }
            givitiweb.HighmapsInfoController = HighmapsInfoController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
