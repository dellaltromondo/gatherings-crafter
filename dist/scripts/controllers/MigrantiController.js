/// <reference path='../definitions/highcharts/highcharts-ng.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class MigrantiController {
                constructor($scope, MigrantijsonService, guidService, $location, $anchorScroll) {
                    this.$inject = ['$scope', 'MigrantijsonService', 'guidService', '$location', '$anchorScroll'];
                    this.$scope = $scope;
                    this.$scope.angularForm = null;
                    this.migrantiJsonService = MigrantijsonService;
                    this.guidService = guidService;
                    this.load();
                    this.$location = $location;
                    this.$anchorScroll = $anchorScroll;
                    $scope["lvm"] = this;
                    $scope.status = { currentPage: "browser" };
                    //$scope.vm = this;
                    $scope.listMigranti = [];
                }
                clearGuid() {
                    this.guidService.clear();
                }
                setSelectedGuid(guid) {
                    this.guidService.save(guid);
                }
                saveAndUpdateList(vm, nome) {
                    vm.saveJsonData().then(() => this.updateList(nome));
                }
                updateList(nome) {
                    this.guidService.$get().then((guid) => {
                        var index = this.indexOfGuidInList(guid);
                        if (index != -1) {
                            this.$scope.listMigranti[index].nome = nome;
                        }
                        else {
                            this.$scope.listMigranti.push({ nome: nome, guid: guid });
                        }
                        this.save();
                    });
                }
                scrollTo(idOfElement) {
                    this.$location.hash(idOfElement);
                    this.$anchorScroll();
                }
                deleteMigranteInList(guid) {
                    var index = this.indexOfGuidInList(guid);
                    if (index != -1) {
                        this.$scope.listMigranti.splice(index, 1);
                    }
                    this.save();
                }
                load() {
                    this.migrantiJsonService.$get().then((data) => {
                        if (Object.getOwnPropertyNames(data).length > 0) {
                            this.$scope.listMigranti = data.list;
                        }
                    });
                }
                save() {
                    return this.migrantiJsonService.save({ list: angular.copy(this.$scope.listMigranti) })
                        .then(() => {
                        this.load();
                    });
                }
                allowAlphabet(crf) {
                    if (crf) {
                        if (!crf.match("^[a-zA-Z]+$") && crf != "") {
                            crf.value = "";
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }
                allowDiagnosis(crf) {
                    if (crf) {
                        for (var i = 0; i < crf.length; i++) {
                            var temp = crf.charAt(i);
                            if (i == 0) {
                                if (!temp.match("^[a-zA-Z]+$")) {
                                    return false;
                                }
                            }
                            else {
                                if (!temp.match("^[0-9]+$")) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                }
                indexOfGuidInList(guid) {
                    for (var i = 0; i < this.$scope.listMigranti.length; i++) {
                        if (this.$scope.listMigranti[i].guid == guid) {
                            return i;
                        }
                    }
                    return -1;
                }
            }
            givitiweb.MigrantiController = MigrantiController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
