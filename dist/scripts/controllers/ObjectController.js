/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class ObjectController {
                constructor($scope) {
                    this.$scope = $scope;
                    const myCompletenessController = new givitiweb.PatientStatusService();
                    this.$scope.completenessController = myCompletenessController;
                    givitiweb.PatientStatusService.registerSubService(myCompletenessController);
                    this.$scope.$on('$destroy', () => {
                        givitiweb.PatientStatusService.unregisterSubService(myCompletenessController);
                    });
                }
                $postLink() {
                    this.$scope.completenessController.setRootVariable(this.$scope.crfs);
                    this.$scope.completenessController.setIndex(this.$scope.myIndex);
                    this.$scope.completenessController.setThisObject(this.$scope.thisObject);
                }
            }
            givitiweb.ObjectController = ObjectController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
