/// <reference path='../definitions/angular/angular.d.ts' />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class AbstractController {
                constructor($q, $scope, $timeout, jsonService, userId, centreCode) {
                    this.$q = $q;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.jsonService = jsonService;
                    this.userId = userId;
                    this.centreCode = centreCode;
                    this.$inject = ['$q', '$scope', '$timeout', 'jsonService'];
                    this.$scope.crfs = {};
                    this.$scope.status = { dataLoaded: false, dataReadOnly: false, dataModified: false, dataSaved: false, statusBarMessage: '' };
                    this.$scope.saver = this;
                }
                /** Load data from jsonService, bind to $scope.crfs and return the loaded data */
                loadJsonData() {
                    return __awaiter(this, void 0, void 0, function* () {
                        this.$scope.status.dataLoaded = false;
                        this.$scope.status.dataLoading = true;
                        const data = yield this.jsonService.$get();
                        this.fillData(this.$scope.crfs, data);
                        this.$scope.oldCRFS = angular.copy(data);
                        this.$scope.status.dataLoaded = true;
                        this.$scope.status.dataLoading = false;
                        this.$scope.$apply();
                        return data;
                    });
                }
                /** Save crf in jsonStorage and set status.dataSaved afterwards */
                saveJsonData() {
                    return __awaiter(this, void 0, void 0, function* () {
                        yield this.jsonService.save(this.$scope.crfs, this.userId, this.centreCode);
                        this.$scope.status.dataModified = false;
                        this.$scope.status.dataSaved = true;
                        this.$scope.$apply();
                        var changeVariableStatus = () => {
                            this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
                        };
                        this.$timeout(changeVariableStatus, 4000);
                    });
                }
                /** Clean all the loaded data in $scope.crfs */
                clearJsonData() {
                    //TODO: probably this is detaching all two-way bindings!
                    this.$scope.crfs = {};
                }
                /** Get current guid from listData using $scope informations to iterate through it */
                getGuidFromList(listData) {
                    throw new Error('This method is abstract');
                }
                /** Add the specified guid to listData using $scope informations to populate it */
                addGuidToList(listData, guid) {
                    throw new Error('This method is abstract');
                }
                /** Fill the specified data inside $scope.crfs cleaning up all dead object */
                fillCRFS(crfData) {
                    var newCrfs = angular.copy(crfData);
                    this.clearData(this.$scope.crfs);
                    this.removeDeadObjects(this.$scope.crfs, newCrfs);
                    this.removeEmptyObjectsFromSets(this.$scope.crfs);
                    this.fillData(this.$scope.crfs, newCrfs);
                }
                /** Fill the originally loaded data inside $scope.crfs cleaning up all modified values */
                rollbackCRFS() {
                    this.fillCRFS(this.$scope.oldCRFS);
                }
                /** Fill crfData values using data */
                fillData(crfData, data) {
                    for (var obj in data) {
                        if (typeof (data[obj]) == "object") {
                            if (obj in crfData) {
                                this.fillData(crfData[obj], data[obj]);
                            }
                            else {
                                crfData[obj] = data[obj];
                            }
                        }
                        else {
                            crfData[obj] = data[obj];
                        }
                    }
                }
                /** Empties all crfData values keeping its structure */
                clearData(crfData) {
                    for (var obj in crfData) {
                        if (typeof (crfData[obj]) == "object") {
                            if (!angular.equals(crfData[obj], {})) {
                                this.clearData(crfData[obj]);
                            }
                            else if (Object.prototype.toString.call(crfData) === '[object Array]') {
                                crfData.splice(obj, 1);
                            }
                        }
                        else {
                            crfData[obj] = undefined;
                        }
                    }
                }
                /** Empties crfData values that are not present in data */
                removeDeadObjects(crfData, data) {
                    for (var obj in crfData) {
                        if (data) {
                            if (typeof (crfData[obj]) == "object") {
                                if (obj in data) {
                                    this.removeDeadObjects(crfData[obj], data[obj]);
                                }
                                else if (Object.prototype.toString.call(crfData[obj]) === '[object Array]') {
                                    crfData[obj] = [];
                                }
                                else {
                                    crfData[obj] = {};
                                }
                            }
                            else {
                                if (!(obj in data)) {
                                    crfData[obj] = undefined;
                                }
                            }
                        }
                    }
                }
                /** Empties crfData sets that contains empty values */
                removeEmptyObjectsFromSets(crfData) {
                    var shouldRemove = [];
                    for (var obj in crfData) {
                        if ((crfData !== null && Object.prototype.toString.call(crfData) === '[object Array]')
                            && (crfData[obj] === null || crfData[obj] === undefined)) {
                            shouldRemove.push(obj);
                        }
                        else if ((typeof (crfData[obj]) == "object")
                            && crfData[obj] !== null
                            && crfData[obj] !== undefined) {
                            if (Object.getOwnPropertyNames(crfData[obj]).length > 0) {
                                this.removeEmptyObjectsFromSets(crfData[obj]);
                            }
                            else {
                                if (Object.prototype.toString.call(crfData) === '[object Array]') {
                                    shouldRemove.push(obj);
                                }
                            }
                        }
                    }
                    shouldRemove.sort((a, b) => {
                        if (parseInt(a) > parseInt(b))
                            return 1;
                        else if (parseInt(a) < parseInt(b))
                            return -1;
                        else
                            return 0;
                    });
                    shouldRemove.reverse();
                    for (var i in shouldRemove) {
                        crfData.splice(shouldRemove[i], 1);
                    }
                }
            }
            givitiweb.AbstractController = AbstractController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
