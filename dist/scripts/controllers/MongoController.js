/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class MongoController extends givitiweb.AbstractController {
                constructor($q, $http, $scope, $timeout, mongoService, crfServiceURL, dataType, dataPath, dataName, dataKey) {
                    super($q, $scope, $timeout, null); //TODO: modify abstractController to not require jsonService
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.mongoService = mongoService;
                    this.crfServiceURL = crfServiceURL;
                    this.dataType = dataType;
                    this.dataPath = dataPath;
                    this.dataName = dataName;
                    this.dataKey = dataKey;
                    this.$inject = ['$q', '$http', '$scope', '$timeout', 'mongoService', 'crfServiceURL', 'dataType', 'dataPath'];
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    this.loadJsonData();
                }
                getCondition() {
                    var result = {};
                    result[this.dataName] = this.dataKey;
                    return result;
                }
                /** Load data from mongoService, bind to $scope.crfs and return the loaded data */
                loadJsonData() {
                    var condition = this.getCondition();
                    return this.mongoService.getDocument(condition)
                        .then((data) => {
                        this.fillData(this.$scope.crfs, data);
                        this.$scope.oldCRFS = angular.copy(data);
                        this.$scope.status.dataLoaded = true;
                        return data;
                    });
                }
                /** Save crf in mongoService and set status.dataSaved afterwards */
                saveJsonData() {
                    var condition = this.getCondition();
                    if (!this.$scope.crfs) {
                        this.$scope.crfs = {};
                    }
                    if (!this.$scope.crfs[this.dataName]) {
                        this.$scope.crfs[this.dataName] = this.dataKey;
                    }
                    return this.mongoService.setDocument(condition, this.$scope.crfs)
                        .then(() => {
                        this.$scope.status.dataModified = false;
                        this.$scope.status.dataSaved = true;
                        var changeVariableStatus = () => {
                            this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
                        };
                        this.$timeout(changeVariableStatus, 4000);
                    });
                }
            }
            givitiweb.MongoController = MongoController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
