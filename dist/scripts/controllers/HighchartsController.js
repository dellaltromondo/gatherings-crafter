/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/highcharts/highcharts-ng.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class HighchartsController {
                /*
                constructor($scope: HighchartsModel, MigrantijsonService: JSONService) {
                    $scope.lvm = this;
                }
        */
                constructor($q, $scope, multipleSchedeDatiJSONService) {
                    this.$q = $q;
                    this.$scope = $scope;
                    this.multipleSchedeDatiJSONService = multipleSchedeDatiJSONService;
                    // public $inject = ['$scope', 'MigrantijsonService'];
                    this.$inject = ['$q', '$scope', 'multipleSchedeDatiJSONService'];
                    this.initHighCharts();
                    this.$scope.lvm = this;
                    this.$scope.loading = true;
                    this.$scope.loadJsonData = this.loadJsonData()
                        .then(() => {
                        this.iterateJSON();
                        this.$scope.loading = false;
                    });
                }
                // PARTE CARICAMENTO JSON
                initHighCharts() {
                    this.$scope.highchartsNG = {
                        title: {
                            text: 'Parti in casa anno 2015'
                        },
                        xAxis: {
                            categories: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre']
                        },
                        series: []
                    };
                }
                loadJsonData() {
                    return this.$q.all([
                        this.loadMultipleform(),
                    ]);
                }
                loadMultipleform() {
                    return this.multipleSchedeDatiJSONService.$get()
                        .then((data) => {
                        this.$scope.dizionarioSchedaDati = data;
                    });
                }
                addPoints() {
                    var seriesArray = this.$scope.highchartsNG.series;
                    var rndIdx = Math.floor(Math.random() * seriesArray.length);
                    seriesArray[rndIdx].data = seriesArray[rndIdx].data.concat([1, 10, 20]);
                }
                addSeries() {
                    var rnd = [];
                    for (var i = 0; i < 10; i++) {
                        rnd.push(Math.floor(Math.random() * 20) + 1);
                    }
                    this.$scope.highchartsNG.series.push({ data: rnd });
                }
                removeRandomSeries() {
                    var seriesArray = this.$scope.highchartsNG.series;
                    var rndIdx = Math.floor(Math.random() * seriesArray.length);
                    seriesArray.splice(rndIdx, 1);
                }
                swapChartType() {
                    if (this.$scope.highchartsNG.options.chart.type === 'line') {
                        this.$scope.highchartsNG.options.chart.type = 'bar';
                    }
                    else {
                        this.$scope.highchartsNG.options.chart.type = 'line';
                    }
                }
                iterateJSON() {
                    var data = this.$scope.dizionarioSchedaDati;
                    var list = [];
                    var maternita = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var ospedale = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var datoMancante = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var media = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var casa = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    var partiCasa = 0;
                    var partiMaternita = 0;
                    var partiOspedale = 0;
                    var partoNonDefinito = 0;
                    var id;
                    for (var guid in data) {
                        //controllo che esista la terza parte che contiene il parto e se esiste la data del parto
                        if (data[guid].core.terzaParte != null && data[guid].core.terzaParte.dataParto != null) {
                            var id = data[guid].core.terzaParte.luogoParto;
                            var str = data[guid].core.terzaParte.dataParto;
                            var res = str.substring(0, 4);
                            var month = parseInt(str.substring(5, 7));
                            //controllo che sia del 2015 il parto
                            if (res == '2015') {
                                //devo ciclare per il mese
                                switch (data[guid].core.terzaParte.luogoParto) {
                                    case 'luogoPartoCodification.casa':
                                        casa[month - 1] += 1;
                                        partiCasa++;
                                        break;
                                    case 'luogoPartoCodification.casaMaternita':
                                        maternita[month - 1] += 1;
                                        partiMaternita++;
                                        break;
                                    case 'luogoPartoCodification.ospedale':
                                        ospedale[month - 1] += 1;
                                        partiOspedale++;
                                        break;
                                    case '':
                                        datoMancante[month - 1] += 1;
                                        partoNonDefinito++;
                                        break;
                                    default:
                                        datoMancante[month - 1] += 1;
                                }
                            }
                            list.push(id);
                        }
                        else {
                            partoNonDefinito++;
                        }
                        for (var i = 0; i < casa.length; i++) {
                            media[i] = (casa[i] + maternita[i] + ospedale[i] + datoMancante[i]) / 4;
                        }
                    }
                    this.$scope.listaDati = list;
                    this.$scope.highchartsNG.series.push({
                        type: 'pie', name: 'numero parti',
                        data: [{ name: 'Parti in casa', y: partiCasa, color: Highcharts.getOptions().colors[1] },
                            { name: 'Parti in casa maternita', y: partiMaternita, color: Highcharts.getOptions().colors[2] },
                            { name: 'Parti in ospedale', y: partiOspedale, color: Highcharts.getOptions().colors[3] },
                            { name: 'Missing', y: partoNonDefinito, color: Highcharts.getOptions().colors[4] }], center: [100, 40], size: 100, showInLegend: false, dataLabels: { enabled: false }
                    });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'Casa', data: casa });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'maternita', data: maternita });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'ospedale', data: ospedale });
                    this.$scope.highchartsNG.series.push({ type: 'column', name: 'missing', data: datoMancante });
                    //this.$scope.highchartsNG.series.push({ type: 'spline', name: 'Media parti', data: media, marker: { lineWidth: 2, lineColor: Highcharts.getOptions().colors[3], fillColor: 'white' } })
                }
            }
            givitiweb.HighchartsController = HighchartsController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
