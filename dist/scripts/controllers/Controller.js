/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            let PageMode;
            (function (PageMode) {
                PageMode[PageMode["LIST"] = 0] = "LIST";
                PageMode[PageMode["PATIENT"] = 1] = "PATIENT";
            })(PageMode || (PageMode = {}));
            let Controller = /** @class */ (() => {
                class Controller extends givitiweb.AbstractController {
                    constructor($q, $http, $scope, $rootScope, $timeout, jsonService, patientStatus, userId, centreCode, crfServiceURL, dataType, dataPath, dataSynchronizer /* Necessary to start DataSynch */) {
                        super($q, $scope, $timeout, jsonService, userId, centreCode);
                        this.$q = $q;
                        this.$http = $http;
                        this.$scope = $scope;
                        this.$rootScope = $rootScope;
                        this.$timeout = $timeout;
                        this.jsonService = jsonService;
                        this.patientStatus = patientStatus;
                        this.userId = userId;
                        this.centreCode = centreCode;
                        this.$inject = ['$q', '$http', '$scope', '$timeout', 'jsonService', 'crfServiceURL', 'dataType', 'dataPath'];
                        //this.versionManager = new CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                        //this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                        this.loadJsonData();
                        this.$scope.patientStatus = patientStatus;
                        this.$scope.PatientStatusService = givitiweb.PatientStatusService;
                        this.$scope.pageMode = PageMode.LIST;
                        this.$scope.PageMode = PageMode;
                        this.$rootScope.$on("PatientCreated", _ => this.$scope.pageMode = PageMode.PATIENT);
                        this.$rootScope.$on("PatientSwitched", _ => {
                            this.$scope.pageMode = PageMode.PATIENT;
                            this.loadJsonData();
                        });
                    }
                    static registerModelSeparationController(c) { this.listOfModelSeparationControllers.push(c); }
                    saveJsonData() {
                        for (let c of Controller.listOfModelSeparationControllers) {
                            c.saveJsonData();
                        }
                        const result = super.saveJsonData();
                        // this.$rootScope.$emit("PatientSaved");
                        this.$rootScope.$broadcast("PatientSaved");
                        return result;
                    }
                    closePatient() {
                        this.$scope.pageMode = PageMode.LIST;
                        this.$scope.crfs = {};
                    }
                }
                Controller.listOfModelSeparationControllers = [];
                return Controller;
            })();
            givitiweb.Controller = Controller;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
