/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class EventRegistrationController extends givitiweb.AbstractController {
                constructor($q, $http, $scope, $window, $timeout, guidStorage, jsonService, profileService, crfServiceURL, dataSource, dataType, dataPath, existingData) {
                    super($q, $scope, $timeout, jsonService);
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$window = $window;
                    this.$timeout = $timeout;
                    this.guidStorage = guidStorage;
                    this.jsonService = jsonService;
                    this.profileService = profileService;
                    this.dataSource = dataSource;
                    this.existingData = existingData;
                    this.$inject = ['$q', '$http', '$scope', '$timeout', '$window', 'guidStorage', 'jsonService', 'profileService', 'crfServiceURL', 'dataType', 'dataPath', 'existingData'];
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    this.loadJsonData();
                }
                loadJsonData() {
                    return this.jsonService.$get()
                        .then((data) => {
                        if (!angular.equals(data, {})) {
                            this.fillData(this.$scope.crfs, data);
                        }
                        else if (!angular.equals(this.existingData, {})) {
                            this.fillData(this.$scope.crfs, this.existingData);
                        }
                    });
                }
                saveJsonData(buttonClientID = "", hiddenEmailID = "") {
                    var resultPromise = this.$q.defer();
                    if (this.dataSource == 'user') {
                        this.profileService.getUser(this.$scope.crfs.participant.firstname, this.$scope.crfs.participant.lastname, this.$scope.crfs.participant.email)
                            .then((idUser) => {
                            this.guidStorage.update(idUser)
                                .then(() => {
                                super.saveJsonData()
                                    .then(() => {
                                    document.querySelector("[id$='" + hiddenEmailID + "']")[0]["value"] = this.$scope.crfs.participant.email;
                                    this.$window["__doPostBack"](buttonClientID, '');
                                    resultPromise.resolve(true);
                                });
                            });
                        });
                    }
                    else if (this.dataSource == 'account') {
                        super.saveJsonData()
                            .then(() => {
                            this.$window["__doPostBack"](buttonClientID, '');
                            resultPromise.resolve(true);
                        });
                    }
                    return resultPromise.promise;
                }
            }
            givitiweb.EventRegistrationController = EventRegistrationController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
