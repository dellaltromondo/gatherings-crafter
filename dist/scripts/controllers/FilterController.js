/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class FilterController extends givitiweb.AbstractController {
                constructor($q, $scope, $rootScope, $http, $window, queriesListGUIDStorage, 
                //private translationService: TranslationService,
                multipleQueriesJSONService, defaultVisibility) {
                    super(null, $scope, null, null);
                    this.$q = $q;
                    this.$scope = $scope;
                    this.$rootScope = $rootScope;
                    this.$http = $http;
                    this.$window = $window;
                    this.queriesListGUIDStorage = queriesListGUIDStorage;
                    this.multipleQueriesJSONService = multipleQueriesJSONService;
                    this.defaultVisibility = defaultVisibility;
                    this.$inject = ['$q', '$scope', '$rootScope', '$http', '$window', 'queriesListGUIDStorage', 'multipleQueriesJSONService', 'defaultVisibility'];
                    this.$scope.selectedItem = null;
                    this.$scope.addingQuery = false;
                    this.$scope.updatingQuery = false;
                    this.$scope.duplicatedAlert = false;
                    this.$rootScope.visibilityConfig = defaultVisibility;
                    if (document.querySelector("[id$='HiddenJSON']")[0]["value"]) {
                        var parsedJSON = JSON.parse(document.querySelector("[id$='HiddenJSON']")[0]["value"]);
                        this.fillData(this.$scope.crfs, parsedJSON);
                    }
                    this.loadJsonData();
                }
                //UGLY
                onBtnFilterClick(buttonClientID, json, startingTable, startingId) {
                    var qbm = new givitiweb.QueryBuilderManager();
                    this.$scope.hiddenFieldValue = qbm.buildQuery(json, startingTable, startingId, true);
                    document.querySelector("[id$='HiddenSQL']")[0]["value"] = this.$scope.hiddenFieldValue;
                    document.querySelector("[id$='HiddenJSON']")[0]["value"] = JSON.stringify(this.$scope["crfs"]);
                    this.$window["__doPostBack"](buttonClientID, '');
                }
                updateQuery(id, title) {
                    this.$scope.duplicatedAlert = this.isTitleDuplicated(title, id);
                    if (!this.$scope.duplicatedAlert) {
                        this.$scope.jsonQueries[id].title = title;
                        this.$scope.jsonQueries[id].json = angular.copy(this.$scope.crfs);
                        this.saveMultipleQueries(this.$scope.guidList).then((newGuidList) => {
                            this.loadJsonData();
                        });
                    }
                }
                selectQuery(id) {
                    this.$scope.selectedItem = id;
                }
                showQuery(id) {
                    this.$scope.crfs = {};
                    this.fillData(this.$scope.crfs, angular.copy(this.$scope.jsonQueries[id].json));
                }
                deleteQuery(id) {
                    this.multipleQueriesJSONService.remove([id])
                        .then(() => {
                        this.queriesListGUIDStorage.remove(id)
                            .then(() => {
                            delete this.$scope.guidList[id];
                            delete this.$scope.jsonQueries[id];
                            this.loadJsonData();
                        });
                    });
                }
                //        translationsAreLoaded(): boolean {
                //            return this.translationService.translationsAreLoaded();
                //        }
                isTitleDuplicated(title, guid) {
                    for (var currentGuid in this.$scope.jsonQueries) {
                        if (guid && currentGuid == guid)
                            continue;
                        if (this.$scope.jsonQueries[currentGuid].title.toUpperCase() == title.toUpperCase()) {
                            return true;
                        }
                    }
                    return false;
                }
                getQueryGUIDList() {
                    var deferred = this.$q.defer();
                    this.queriesListGUIDStorage.loadGuidList()
                        .then((guidList) => {
                        deferred.resolve(guidList);
                    });
                    return deferred.promise;
                    /* var method: string = "GetCustomizationsList";
                     var deferred: ng.IDeferred<any> = this.$q.defer();
                     var query = '?source=account&type=' + this.queryDataType + '&key=' + this.dataKey;
                     this.$http.get(this.dataStorageURL + method + query)
                         .then((data: any) => {
                             deferred.resolve(JSON.parse(data.data.d));
                         });
                     return deferred.promise;*/
                }
                createQueryCustomization(guid) {
                    var deferred = this.$q.defer();
                    this.queriesListGUIDStorage.create(guid)
                        .then(() => {
                        deferred.resolve("");
                    });
                    return deferred.promise;
                    /*var method: string = "CreateCustomization";
                    var deferred: ng.IDeferred<any> = this.$q.defer();
                    var data: string = JSON.stringify({ 'source': 'account', 'type': this.queryDataType, 'key': this.dataKey, 'text': 'userQuery', value: guid });
                    this.$http.post(this.dataStorageURL + method, data)
                        .then((data: any) => {
                            deferred.resolve("");
                        });
                    return deferred.promise;*/
                }
                selectQueryIdForCurrentlyLoadedJson() {
                    for (var key in this.$scope.jsonQueries) {
                        if (angular.equals(this.$scope.jsonQueries[key].json, this.$scope.crfs)) {
                            this.selectQuery(key);
                            break;
                        }
                    }
                }
                loadJsonData() {
                    var deferred = this.$q.defer();
                    this.getQueryGUIDList().then((guidList) => {
                        this.multipleQueriesJSONService.switchTo(guidList)
                            .then(() => {
                            this.multipleQueriesJSONService.$get()
                                .then((data) => {
                                if (!this.$scope.jsonQueries) {
                                    this.$scope.jsonQueries = {};
                                    this.$scope.guidList = [];
                                }
                                this.$scope.jsonQueries = angular.copy(data);
                                this.selectQueryIdForCurrentlyLoadedJson();
                                this.$scope.guidList = guidList;
                                deferred.resolve(data);
                            });
                        });
                    });
                    return deferred.promise;
                }
                saveCurrentQuery(title) {
                    this.$scope.duplicatedAlert = this.isTitleDuplicated(title);
                    if (!this.$scope.duplicatedAlert) {
                        var guid = givitiweb.GuidUtils.getEmptyGUID();
                        this.$scope.guidList.push(guid);
                        this.$scope.jsonQueries[guid] = { title: title, json: angular.copy(this.$scope.crfs) };
                        this.saveMultipleQueries(this.$scope.guidList).then((newGuidList) => {
                            for (var index in newGuidList) {
                                if (this.$scope.guidList.indexOf(newGuidList[index]) == -1) {
                                    guid = newGuidList[index];
                                    break;
                                }
                            }
                            this.createQueryCustomization(guid).then(() => {
                                this.loadJsonData();
                            });
                        });
                    }
                }
                saveMultipleQueries(queriesGuidsList) {
                    var deferred = this.$q.defer();
                    this.multipleQueriesJSONService.switchTo(queriesGuidsList)
                        .then(() => {
                        this.multipleQueriesJSONService.save(this.$scope.jsonQueries)
                            .then((data) => {
                            deferred.resolve(data);
                        });
                    });
                    return deferred.promise;
                }
            }
            givitiweb.FilterController = FilterController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
