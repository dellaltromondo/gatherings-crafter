/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class NeuroPsicoInfantileController extends givitiweb.AbstractController {
                constructor($q, $http, $scope, $timeout, mongoService, crfServiceURL, dataType, dataPath, dataName, dataKey) {
                    super($q, $scope, $timeout, null); //TODO: modify abstractController to not require jsonService
                    this.$q = $q;
                    this.$http = $http;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.mongoService = mongoService;
                    this.crfServiceURL = crfServiceURL;
                    this.dataType = dataType;
                    this.dataPath = dataPath;
                    this.dataName = dataName;
                    this.dataKey = dataKey;
                    this.$inject = ['$q', '$http', '$scope', '$timeout', 'mongoService', 'crfServiceURL', 'dataType', 'dataPath'];
                    this.versionManager = new givitiweb.CRFVersionsManager(crfServiceURL, dataType, dataPath, $http);
                    this.$scope.currentCrfPath = this.versionManager.getCrfPath(null);
                    // mettere un metodo per  controllare se esiste il guid della scheda (bambino) se esiste caricare il jsondata di quella pagina
                    //this.loadJsonData();
                    this.$scope.visibile = "lista";
                    this.$scope.crfList = {};
                    this.$scope.crfId = {};
                    this.createNewCrf();
                }
                getVisibile() {
                    this.$scope.visibile = "NonVisibile";
                }
                getVisibileSi() {
                    this.$scope.visibile = "lista";
                }
                createNewCrf() {
                    this.clearJsonData();
                    var id = givitiweb.GuidUtils.createGUID();
                    this.$scope.crfs["idScheda"] = id;
                    // in questo caso il guid mi viene passato dalla pagina
                }
                /*
                getCondition() {
                    var result = {};
                    result[this.dataName] = this.dataKey;
                    return result;
                }
        */
                getCondition(idSchedaFromView) {
                    var result = {};
                    result["idScheda"] = this.$scope.crfs["idScheda"];
                    if (idSchedaFromView) {
                        result["idScheda"] = idSchedaFromView;
                    }
                    return result;
                }
                loadAllJsonData() {
                    var condition = {};
                    condition[this.dataName] = this.dataKey;
                    return this.mongoService.findDocuments(condition)
                        .then((data) => {
                        //this.fillData(this.$scope.crfs, data);
                        this.$scope.crfList = data;
                        //this.$scope.oldCRFS = angular.copy(data);
                        this.$scope.status.dataLoaded = true;
                        return data;
                    });
                }
                /** Load data from mongoService, bind to $scope.crfs and return the loaded data */
                loadJsonData(idSchedaFromView) {
                    var condition = this.getCondition(idSchedaFromView);
                    return this.mongoService.getDocument(condition)
                        .then((data) => {
                        this.fillData(this.$scope.crfs, data);
                        this.$scope.oldCRFS = angular.copy(data);
                        this.$scope.status.dataLoaded = true;
                        return data;
                    });
                }
                /** Save crf in mongoService and set status.dataSaved afterwards */
                saveJsonData() {
                    var condition = this.getCondition();
                    if (!this.$scope.crfs) {
                        this.$scope.crfs = {};
                    }
                    if (!this.$scope.crfs[this.dataName]) {
                        this.$scope.crfs[this.dataName] = this.dataKey;
                    }
                    return this.mongoService.setDocument(condition, this.$scope.crfs)
                        .then(() => {
                        this.$scope.status.dataModified = false;
                        this.$scope.status.dataSaved = true;
                        var changeVariableStatus = () => {
                            this.$scope.status.dataSaved = !this.$scope.status.dataSaved;
                        };
                        this.$timeout(changeVariableStatus, 4000);
                    });
                }
            }
            givitiweb.NeuroPsicoInfantileController = NeuroPsicoInfantileController;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
