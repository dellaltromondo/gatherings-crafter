var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            function DataStorageCreator(ctor, dataType, dataStorageURL, $q, $http) {
                return new ctor(dataType, dataStorageURL, $q, $http);
            }
            givitiweb.DataStorageCreator = DataStorageCreator;
            class GivitiWebJSONStorage {
                constructor(guidStorage, STORAGE_ID, SERVICE_URL, $q, $http) {
                    this.guidStorage = guidStorage;
                    this.STORAGE_ID = STORAGE_ID;
                    this.SERVICE_URL = SERVICE_URL;
                    this.$q = $q;
                    this.$http = $http;
                }
                read(guid) {
                    const deferred = this.$q.defer();
                    this.$http.get(this.SERVICE_URL + "GetDataStorageValue" + "?tableName=" + this.STORAGE_ID + "&guid=" + guid)
                        .then(response => {
                        ("d" in response.data)
                            ? deferred.resolve(JSON.parse(response.data["d"]))
                            : deferred.reject("JSONStorage received a wrong response! " + JSON.stringify(response.data));
                    }, error => deferred.reject(error));
                    return deferred.promise;
                }
                create(guid, jsonData) {
                    return this.createOrUpdate(guid, jsonData, "CreateDataStorageValue");
                }
                update(guid, jsonData) {
                    return this.createOrUpdate(guid, jsonData, "UpdateDataStorageValue");
                }
                createOrUpdate(guid, jsonData, method) {
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.post(this.SERVICE_URL + method, JSON.stringify({
                        tableName: this.STORAGE_ID,
                        guid: guid,
                        value: JSON.stringify(jsonData)
                    })));
                }
                delete(guid) {
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.post(this.SERVICE_URL + "RemoveDataStorageValue", JSON.stringify({
                        tableName: this.STORAGE_ID,
                        guid: guid
                    })));
                }
            }
            givitiweb.GivitiWebJSONStorage = GivitiWebJSONStorage;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
