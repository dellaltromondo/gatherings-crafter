/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class MigrantiGUIDService {
                constructor($q) {
                    this.$q = $q;
                    this.$inject = ['$q'];
                    this.currentGuid = givitiweb.GuidUtils.getEmptyGUID();
                }
                $get() {
                    var deferred = this.$q.defer();
                    deferred.resolve(this.currentGuid);
                    return deferred.promise;
                }
                clear() {
                    this.currentGuid = givitiweb.GuidUtils.getEmptyGUID();
                }
                save(valueToBeSaved) {
                    this.currentGuid = valueToBeSaved;
                }
                static factory($q) {
                    return new MigrantiGUIDService($q);
                }
            }
            givitiweb.MigrantiGUIDService = MigrantiGUIDService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
