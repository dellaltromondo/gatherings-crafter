var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            let PatientStatusService = /** @class */ (() => {
                class PatientStatusService {
                    constructor(isMainService) {
                        this.listOfVariablesByStatus = [
                            [], [], [], []
                        ];
                        this.listOfVariablesByName = {};
                    }
                    setRootVariable(crfs) {
                        this.crfs = crfs;
                    }
                    setVM(vm) {
                        this.vm = vm;
                    }
                    setIndex(index) {
                        this.myIndex = index;
                    }
                    setThisObject(thisObject) {
                        this.thisObject = thisObject;
                    }
                    registerVariable(variableName, visibility, statusLevel = 3) {
                        this.listOfVariablesByStatus[statusLevel - 1].push([variableName, visibility]);
                        this.listOfVariablesByName[variableName] = visibility;
                    }
                    isVariableMissing(variable, visibility) {
                        const owner = this.listOfVariablesByName.hasOwnProperty(variable)
                            ? this
                            : PatientStatusService.listOfServices.find(s => s.listOfVariablesByName.hasOwnProperty(variable));
                        if (!owner) {
                            return false;
                        }
                        if (visibility == null) {
                            visibility = owner.getVisibilityFormulaForVariable(variable);
                        }
                        if (!owner.evaluateVisibility(visibility, variable)) {
                            return false;
                        }
                        const value = owner.evaluateVariable(variable);
                        if (!value) {
                            return true;
                        }
                        if (typeof (value.length) != "undefined" && value.length == 0) {
                            return true;
                        }
                        return false;
                    }
                    getMissingForStatus(statusLevel) {
                        return this.listOfVariablesByStatus[statusLevel - 1]
                            .filter(([el, vis]) => this.isVariableMissing(el, vis))
                            .map(([el, vis]) => el);
                    }
                    getAllMissingForStatus(statusLevel) {
                        let result = [];
                        for (let s of PatientStatusService.listOfServices) {
                            result = result.concat(s.getMissingForStatus(statusLevel));
                        }
                        return result;
                    }
                    getStatus() {
                        for (let k = 1; k <= 4; k++) {
                            if (this.getMissingForStatus(k).length > 0) {
                                return (k - 1);
                            }
                            for (let s of PatientStatusService.listOfServices) {
                                if (s.getMissingForStatus(k).length > 0) {
                                    return (k - 1);
                                }
                            }
                        }
                        return 4;
                    }
                    evaluateVariable(variable) {
                        return eval('this.' + variable
                            .split("rootOf[saver.getGUID()]").join("crfs")
                            .split("myIndex").join("this.myIndex")
                            .split("thisObject").join("this.thisObject")
                            .split(".").join("?."));
                    }
                    evaluateVisibility(visibility, variable) {
                        // TODO: this could be slow, evaluate alternatives
                        const parent = variable.split(".").slice(0, -1).join(".");
                        if (parent && this.listOfVariablesByName.hasOwnProperty(parent)) {
                            const parentsVisibility = this.evaluateVisibility(this.listOfVariablesByName[parent], parent);
                            if (!parentsVisibility) {
                                return false;
                            }
                        }
                        if (visibility == "") {
                            return true;
                        }
                        try {
                            return eval(visibility
                                .split("rootOf[saver.getGUID()]").join("crfs")
                                .split('crfs.').join('this.crfs.')
                                .split('vm.').join('this.vm.')
                                .split("myIndex").join("this.myIndex")
                                .split("thisObject").join("this.thisObject"));
                        }
                        catch (_a) {
                            return false;
                        }
                    }
                    getVisibilityFormulaForVariable(variable) {
                        if (this.listOfVariablesByName.hasOwnProperty(variable)) {
                            return this.listOfVariablesByName[variable];
                        }
                        const owner = PatientStatusService.listOfServices.find(s => s.listOfVariablesByName.hasOwnProperty(variable));
                        if (!owner) {
                            return "false";
                        }
                        return owner.listOfVariablesByName[variable];
                    }
                }
                PatientStatusService.listOfServices = [];
                PatientStatusService.registerSubService = (s) => PatientStatusService.listOfServices.push(s);
                PatientStatusService.unregisterSubService = (s) => {
                    const index = PatientStatusService.listOfServices.indexOf(s);
                    PatientStatusService.listOfServices.splice(index, 1);
                };
                return PatientStatusService;
            })();
            givitiweb.PatientStatusService = PatientStatusService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
