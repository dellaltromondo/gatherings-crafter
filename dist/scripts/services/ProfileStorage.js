var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class ProfileStorage {
                constructor(SERVICE_URL, $q, $http) {
                    this.SERVICE_URL = SERVICE_URL;
                    this.$q = $q;
                    this.$http = $http;
                }
                findUser(firstname, lastname, contact) {
                    var method = "FindUser";
                    const result = this.$http.get(this.SERVICE_URL + method + "?firstname=" + firstname + "&lastname=" + lastname + "&contact=" + contact);
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                getUCPR(idUser, centreCode, projectName, types, status) {
                    var method = "GetUCPR";
                    const result = this.$http.get(this.SERVICE_URL + method + "?idUser=" + idUser + "&centreCode=" + centreCode + "&projectName=" + projectName + "&type=" + types + "&status=" + status);
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                CreateUser(username, firstname, lastname, taxcode, nation, note, email) {
                    var method = "CreateUser";
                    const result = this.$http.post(this.SERVICE_URL + method, JSON.stringify({ username: username, firstname: firstname, lastname: lastname, taxcode: taxcode, nation: nation, note: note, email: email }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
            }
            givitiweb.ProfileStorage = ProfileStorage;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
