/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            "use strict";
            class MongoService {
                constructor($q, $http, collectionName, serviceURL) {
                    this.$q = $q;
                    this.$http = $http;
                    this.collectionName = collectionName;
                    this.serviceURL = serviceURL;
                    this.$inject = ['$q', '$http'];
                }
                $get() { }
                ;
                findDocuments(condition, projection, sorting) {
                    var deferred = this.$q.defer();
                    this.loadDocuments(condition).then(data => deferred.resolve(data), error => deferred.reject(error));
                    return deferred.promise;
                }
                loadDocuments(condition) {
                    var method = "FindDocuments";
                    var result = this.$http.get(this.serviceURL + method
                        + "?collectionName=" + this.collectionName
                        + "&condition=" + JSON.stringify(condition), {
                        withCredentials: false
                    });
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                getDocument(condition, projection, sorting) {
                    return this.load(condition);
                }
                load(condition) {
                    var method = "GetDocument";
                    var result = this.$http.get(this.serviceURL + method
                        + "?collectionName=" + this.collectionName
                        + "&condition=" + JSON.stringify(condition), {
                        withCredentials: false
                    });
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                setDocument(condition, data) {
                    return this.save(condition, data);
                }
                save(condition, document) {
                    var method = "SetDocument";
                    var result = this.$http.post(this.serviceURL + method, angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition), document: JSON.stringify(document) }), {
                        withCredentials: false
                    });
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                //TODO: check the key with the internal one
                removeDocument(condition) {
                    return this.remove(condition);
                }
                remove(condition) {
                    var method = "RemoveDocument";
                    var result = this.$http.post(this.serviceURL + method, angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition) }), {
                        withCredentials: false
                    });
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                static factory($q, $http, collectionName, serviceURL) {
                    return new MongoService($q, $http, collectionName, serviceURL);
                }
            }
            givitiweb.MongoService = MongoService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
