/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            "use strict";
            class MultipleJSONService {
                constructor($q, $http, guidStorage, storageId, serviceURL, listOfKeys) {
                    this.$q = $q;
                    this.guidStorage = guidStorage;
                    this.listOfKeys = listOfKeys;
                    this.$inject = ['$q', '$http', 'guidStorage'];
                    this.jsonStorage = new givitiweb.GivitiWebJSONStorage(guidStorage, storageId, serviceURL, $q, $http);
                }
                $get() {
                    var deferred = this.$q.defer();
                    var arrayOfPromises = [];
                    var results = {};
                    if (this.listOfKeys) {
                        for (var keyIdx in this.listOfKeys) {
                            var key = this.listOfKeys[keyIdx];
                            this.guidStorage.update(key);
                            var promise = this.jsonStorage.read(this.guidStorage.$get());
                            var callback = (key, data) => { results[key] = data; };
                            promise.then(data => callback.bind(this, key));
                            arrayOfPromises.push(promise);
                        }
                    }
                    this.$q.all(arrayOfPromises).then(() => deferred.resolve(results), error => angular.equals(results, {}) ? deferred.reject(error) : deferred.resolve(results));
                    return deferred.promise;
                }
                switchTo(newListOfKeys) {
                    var deferred = this.$q.defer();
                    this.listOfKeys = newListOfKeys;
                    deferred.resolve();
                    return deferred.promise;
                }
                save(data) {
                    var deferred = this.$q.defer();
                    var arrayOfPromises = [];
                    var results = [];
                    for (var keyIdx in this.listOfKeys) {
                        let key = this.listOfKeys[keyIdx];
                        this.guidStorage.update(key);
                        const promise = this.jsonStorage.save(data[key]).then(newKey => {
                            results.push(newKey);
                            return newKey;
                        });
                        arrayOfPromises.push(promise);
                    }
                    this.$q.all(arrayOfPromises).then(() => deferred.resolve(results), (error) => {
                        angular.equals(results, [])
                            ? deferred.reject(error)
                            : deferred.resolve(results);
                    });
                    return deferred.promise;
                }
                remove(keysList) {
                    var deferred = this.$q.defer();
                    var arrayOfPromises = [];
                    keysList = (angular.equals(keysList, [])) ? this.listOfKeys : keysList;
                    for (var keyIdx in keysList) {
                        var key = keysList[keyIdx];
                        this.guidStorage.update(key);
                        var promise = this.jsonStorage.remove().then(() => this.guidStorage.remove(key));
                        arrayOfPromises.push(promise);
                    }
                    this.$q.all(arrayOfPromises).then(() => deferred.resolve(), error => deferred.reject(error));
                    return deferred.promise;
                }
                static factory($q, $http, guidStorage, storageId, dataStorageURL, listOfKeys) {
                    return new MultipleJSONService($q, $http, guidStorage, storageId, dataStorageURL, listOfKeys);
                }
            }
            givitiweb.MultipleJSONService = MultipleJSONService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
