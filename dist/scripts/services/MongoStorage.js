var gvt = it.marionegri.givitiweb;
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            var subti;
            (function (subti) {
                class MongoStorage {
                    constructor(collectionName, serviceURL, $q, $http) {
                        this.collectionName = collectionName;
                        this.serviceURL = serviceURL;
                        this.$q = $q;
                        this.$http = $http;
                    }
                    read(guid) {
                        const condition = { internalGUID: guid };
                        return gvt.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.get(this.serviceURL + "/GetDocument"
                            + "?collectionName=" + this.collectionName
                            + "&condition=" + JSON.stringify(condition)));
                    }
                    create(guid, jsonData) {
                        return this.update(guid, jsonData);
                    }
                    update(guid, jsonData) {
                        const condition = { internalGUID: guid };
                        const document = angular.copy(jsonData);
                        document['internalGUID'] = guid;
                        return gvt.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.post(this.serviceURL + "/SetDocument", angular.toJson({ collectionName: this.collectionName, condition: JSON.stringify(condition), document: JSON.stringify(document) })));
                    }
                    delete(guid) {
                        const condition = { internalGUID: guid };
                        return gvt.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.post(this.serviceURL + "/RemoveDocument", angular.toJson({ collectionName: this.collectionName, condition: condition })));
                    }
                }
                subti.MongoStorage = MongoStorage;
            })(subti = givitiweb.subti || (givitiweb.subti = {}));
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
