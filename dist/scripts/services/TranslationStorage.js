var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class TranslationStorage {
                constructor(SERVICE_URL, $q, $http) {
                    this.SERVICE_URL = SERVICE_URL;
                    this.$q = $q;
                    this.$http = $http;
                }
                getTranslationsValues(application, block, culture) {
                    var method = "GetTranslationsValues";
                    var result = this.$http.get(this.SERVICE_URL + method + "?application=" + application + "&block=" + block + "&culture=" + culture, {
                        withCredentials: false
                    });
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
            }
            givitiweb.TranslationStorage = TranslationStorage;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
