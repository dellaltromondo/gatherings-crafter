/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../definitions/libsodium/libsodium-wrapper.d.ts' />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            "use strict";
            class JSONService {
                constructor($q, guidStorage, storage, keysStorage, patientStatus) {
                    this.$q = $q;
                    this.guidStorage = guidStorage;
                    this.storage = storage;
                    this.keysStorage = keysStorage;
                    this.patientStatus = patientStatus;
                    ;
                }
                $get() {
                    return __awaiter(this, void 0, void 0, function* () {
                        const myGuid = yield this.guidStorage.$get();
                        const data = this.storage.read(myGuid);
                        const key = yield this.keysStorage.read(myGuid);
                        return this.unwrapData(yield data, key && key.length > 0 ? key[0].value : null);
                    });
                }
                switchTo(newKey) {
                    return this.guidStorage.update(newKey);
                }
                save(data, user, centreCode, crfInfo = {}) {
                    var _a;
                    return __awaiter(this, void 0, void 0, function* () {
                        let myGuid = yield this.guidStorage.$get();
                        if (myGuid == givitiweb.GuidUtils.getEmptyGUID()) {
                            myGuid = givitiweb.GuidUtils.createGUID();
                            const newKey = this.generateKey(64);
                            this.keysStorage.create(myGuid, {
                                jsonId: myGuid,
                                value: newKey,
                                centreCode
                            });
                            const dataWithMetadata = this.wrapWithMetadata(data, newKey, user, centreCode, crfInfo);
                            yield this.storage.create(myGuid, yield dataWithMetadata);
                            yield this.guidStorage.save(myGuid);
                        }
                        else {
                            let myKey = yield this.keysStorage.read(myGuid);
                            if (!myKey || (myKey === null || myKey === void 0 ? void 0 : myKey.length) < 1 || !myKey[0] || !((_a = myKey[0]) === null || _a === void 0 ? void 0 : _a.value)) {
                                const newKey = this.generateKey(64);
                                myKey = [{
                                        jsonId: myGuid,
                                        value: newKey,
                                        centreCode
                                    }];
                                this.keysStorage.create(myGuid, myKey[0]);
                            }
                            const dataWithMetadata = this.wrapWithMetadata(data, myKey[0].value, user, centreCode, crfInfo);
                            yield this.storage.update(myGuid, yield dataWithMetadata);
                        }
                        return myGuid;
                    });
                }
                remove() {
                    var deferred = this.$q.defer();
                    this.guidStorage.$get().then((myGuid) => {
                        const result = this.storage.delete(myGuid);
                        result.then(data => {
                            this.keysStorage.delete(myGuid);
                            deferred.resolve(); // If key is really removed is not relevant to response
                        }, error => deferred.reject(error));
                    }, error => deferred.reject(error));
                    return deferred.promise;
                }
                wrapWithMetadata(data, key, user, centreCode, crfInfo) {
                    return __awaiter(this, void 0, void 0, function* () {
                        return {
                            data: yield this.encrypt(data, key),
                            metadata: {
                                user,
                                timestamp: new Date(),
                                centreCode,
                                crfInfo,
                                crfStatus: this.patientStatus.getStatus()
                            }
                        };
                    });
                }
                unwrapData(dataWithMetadata, key) {
                    const encryptedData = dataWithMetadata === null || dataWithMetadata === void 0 ? void 0 : dataWithMetadata.data;
                    if (!encryptedData) {
                        return null;
                    }
                    return this.decrypt(encryptedData, key);
                }
                encrypt(data, myKey) {
                    return __awaiter(this, void 0, void 0, function* () {
                        yield sodium.ready;
                        let key = sodium.from_hex(myKey);
                        let nonce = sodium.randombytes_buf(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
                        var encrypted = sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(JSON.stringify(data), null, null, nonce, key);
                        var nonce_and_ciphertext = this.concatTypedArray(Uint8Array, nonce, encrypted); //https://github.com/jedisct1/libsodium.js/issues/130#issuecomment-361399594     
                        return this.u_btoa(nonce_and_ciphertext);
                    });
                }
                decrypt(data, myKey) {
                    return __awaiter(this, void 0, void 0, function* () {
                        yield sodium.ready;
                        let key = sodium.from_hex(myKey);
                        var nonce_and_ciphertext = this.u_atob(data); //converts ascii string of garbled text into binary
                        let nonce = nonce_and_ciphertext.slice(0, sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/slice      
                        let ciphertext = nonce_and_ciphertext.slice(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
                        var result = sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(nonce, ciphertext, null, nonce, key, "text");
                        return JSON.parse(result);
                    });
                }
                u_btoa(buffer) {
                    var binary = [];
                    var bytes = new Uint8Array(buffer);
                    for (var i = 0, il = bytes.byteLength; i < il; i++) {
                        binary.push(String.fromCharCode(bytes[i]));
                    }
                    return btoa(binary.join(""));
                }
                u_atob(ascii) {
                    return Uint8Array.from(atob(ascii), c => c.charCodeAt(0));
                }
                generateKey(length) {
                    let output = '';
                    for (let i = 0; i < length; ++i) {
                        output += (Math.floor(Math.random() * 16)).toString(16);
                    }
                    return output;
                }
                concatTypedArray(ResultConstructor) {
                    var totalLength = 0;
                    for (var _len = arguments.length, arrays = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                        arrays[_key - 1] = arguments[_key];
                    }
                    var _iteratorNormalCompletion = true;
                    var _didIteratorError = false;
                    var _iteratorError = undefined;
                    try {
                        for (var _iterator = arrays[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            var arr = _step.value;
                            totalLength += arr.length;
                        }
                    }
                    catch (err) {
                        _didIteratorError = true;
                        _iteratorError = err;
                    }
                    finally {
                        try {
                            if (!_iteratorNormalCompletion && _iterator.return) {
                                _iterator.return();
                            }
                        }
                        finally {
                            if (_didIteratorError) {
                                throw _iteratorError;
                            }
                        }
                    }
                    var result = new ResultConstructor(totalLength);
                    var offset = 0;
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;
                    try {
                        for (var _iterator2 = arrays[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var _arr = _step2.value;
                            result.set(_arr, offset);
                            offset += _arr.length;
                        }
                    }
                    catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    }
                    finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        }
                        finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }
                    return result;
                }
                ;
                static factory($q, guidStorage, storage, keysStorage, patientStatus) {
                    return new JSONService($q, guidStorage, storage, keysStorage, patientStatus);
                }
            }
            givitiweb.JSONService = JSONService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
