/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class ProfileService {
                constructor($q, $http, dataStorageURL) {
                    this.$q = $q;
                    this.$http = $http;
                    this.dataStorageURL = dataStorageURL;
                    this.$inject = ['$q', '$http'];
                    this.profileStorage = new givitiweb.ProfileStorage(dataStorageURL, $q, $http);
                }
                $get() {
                    var deferred = this.$q.defer();
                    return deferred.promise;
                }
                getUser(firstname, lastname, contact) {
                    var deferred = this.$q.defer();
                    this.profileStorage.findUser(firstname, lastname, contact).then(data => {
                        if (data == "-1") {
                            this.profileStorage
                                .CreateUser(null, firstname, lastname, null, null, "Created for event registration", contact)
                                .then(newData => deferred.resolve(newData));
                        }
                        else {
                            deferred.resolve(data);
                        }
                    }, error => deferred.resolve("-1"));
                    return deferred.promise;
                }
                getAfferenceStartDate(centreCode, projectName) {
                    var deferred = this.$q.defer();
                    this.profileStorage.getUCPR(-1, centreCode, projectName, "partecipantCentreProject", "activeCentreProject").then(data => {
                        if (!angular.equals(data, [])) {
                            deferred.resolve(data[0].startDate);
                        }
                        else {
                            deferred.resolve("");
                        }
                    }, error => deferred.reject());
                    return deferred.promise;
                }
                getUCPR(idUser, centreCode, projectName, types, status) {
                    var deferred = this.$q.defer();
                    this.profileStorage.getUCPR(idUser, centreCode, projectName, types, status).then(data => deferred.resolve(data), error => deferred.reject());
                    return deferred.promise;
                }
                static factory($q, $http, dataStorageURL) {
                    return new ProfileService($q, $http, dataStorageURL);
                }
            }
            givitiweb.ProfileService = ProfileService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
