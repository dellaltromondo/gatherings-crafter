var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class RESTStorage {
                constructor(STORAGE_ID, SERVICE_URL, $q, $http) {
                    this.STORAGE_ID = STORAGE_ID;
                    this.SERVICE_URL = SERVICE_URL;
                    this.$q = $q;
                    this.$http = $http;
                }
                read(guid) {
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.get(this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid));
                }
                create(guid, jsonData) {
                    //TODO should use PUT instead
                    return this.update(guid, jsonData);
                }
                update(guid, jsonData) {
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.post(this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid, jsonData));
                }
                delete(guid) {
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, this.$http.delete(this.SERVICE_URL + "/" + this.STORAGE_ID + "/" + guid));
                }
            }
            givitiweb.RESTStorage = RESTStorage;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
