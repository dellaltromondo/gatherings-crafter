var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            function GuidStorageCreator(ctor, dataKey, $q, $http) {
                return new ctor(dataKey, $q, $http);
            }
            givitiweb.GuidStorageCreator = GuidStorageCreator;
            class NaiveGUIDStorage {
                constructor(dataKey, $q, $http) {
                    this.$q = $q;
                    this.$http = $http;
                    this.currentGUID = givitiweb.GuidUtils.createGUID();
                    if (dataKey != null) {
                        this.currentGUID = dataKey;
                    }
                }
                $get() {
                    var deferred = this.$q.defer();
                    deferred.resolve(this.currentGUID);
                    return deferred.promise;
                }
                save(valueToBeSaved) {
                    return this.update(valueToBeSaved);
                }
                update(dataKey) {
                    var deferred = this.$q.defer();
                    this.currentGUID = dataKey;
                    deferred.resolve(this.currentGUID);
                    return deferred.promise;
                }
                //TODO: set some kind of deleted property and stop returning guid if that is set
                //TODO: where this is used?
                remove(dataKey) {
                    var deferred = this.$q.defer();
                    this.currentGUID = null;
                    deferred.resolve(null);
                    return deferred.promise;
                }
            }
            givitiweb.NaiveGUIDStorage = NaiveGUIDStorage;
            class GivitiWebGUIDStorage {
                constructor(dataType, dataSource, serviceURL, dataKey, $q, $http) {
                    this.$q = $q;
                    this.$http = $http;
                    this.STORAGE_ID = dataType;
                    this.DATA_KEY = dataKey;
                    this.DATA_SOURCE = dataSource;
                    this.SERVICE_URL = serviceURL;
                }
                $get() {
                    const result = this.$http.get(this.SERVICE_URL + "GetCustomization" + "?source=" + this.DATA_SOURCE + "&type=" + this.STORAGE_ID + "&key=" + this.DATA_KEY);
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                loadGuidList() {
                    const result = this.$http.get(this.SERVICE_URL + "GetCustomizationsList" + "?source=" + this.DATA_SOURCE + "&type=" + this.STORAGE_ID + "&key=" + this.DATA_KEY);
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                save(valueToBeSaved) {
                    const result = this.$http.post(this.SERVICE_URL + "SetCustomization", JSON.stringify({
                        source: this.DATA_SOURCE,
                        type: this.STORAGE_ID,
                        key: this.DATA_KEY,
                        text: "Dati personalizzati " + this.DATA_KEY,
                        value: valueToBeSaved
                    }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                create(valueToBeSaved) {
                    const result = this.$http.post(this.SERVICE_URL + "CreateCustomization", JSON.stringify({
                        source: this.DATA_SOURCE,
                        type: this.STORAGE_ID,
                        key: this.DATA_KEY,
                        text: "Dati personalizzati " + this.DATA_KEY,
                        value: valueToBeSaved
                    }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                update(dataKey) {
                    this.DATA_KEY = dataKey;
                    return this.$get();
                }
                //TODO: set some kind of deleted property and stop returning guid if that is set
                remove(valueToBeRemoved) {
                    const result = this.$http.post(this.SERVICE_URL + "RemoveCustomization", JSON.stringify({
                        source: this.DATA_SOURCE,
                        key: this.DATA_KEY,
                        value: valueToBeRemoved
                    }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
            }
            givitiweb.GivitiWebGUIDStorage = GivitiWebGUIDStorage;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
