/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class PDFBlacksmithService {
                constructor(PDFBlacksmithUrl, templateName, $q, $http) {
                    this.PDFBlacksmithUrl = PDFBlacksmithUrl;
                    this.templateName = templateName;
                    this.$q = $q;
                    this.$http = $http;
                }
                generatePDFAndDownload(xmlData) {
                    var result = this.$http.post(this.PDFBlacksmithUrl + "GeneratePDF", angular.toJson({ templateName: this.templateName, data: xmlData }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
                generatePDFOnServer(xmlData, fileName) {
                    var result = this.$http.post(this.PDFBlacksmithUrl + "ArchivePDF", angular.toJson({ templateName: this.templateName, data: xmlData, pdfName: fileName }));
                    return givitiweb.PromiseUtils.unwrapHttpPromise(this.$q, result);
                }
            }
            givitiweb.PDFBlacksmithService = PDFBlacksmithService;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
