var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class DataSynchronizer {
                constructor(originalJsonService, listService, dataSynchService, ccDataStore, centreCode, $q, $interval, secretVariables) {
                    this.listService = listService;
                    this.dataSynchService = dataSynchService;
                    this.ccDataStore = ccDataStore;
                    this.centreCode = centreCode;
                    this.secretVariables = secretVariables;
                    this.myJsonService = angular.copy(originalJsonService);
                    $interval(_ => {
                        console.log("==> Running DataSynch!");
                        Promise.all([
                            this.listService.$get(),
                            this.dataSynchService.$get()
                        ])
                            .then(([patientsList, dataSynchList]) => {
                            dataSynchList = dataSynchList || {};
                            const patient = this.pickFirstToSend(patientsList, dataSynchList);
                            if (!patient) {
                                return;
                            }
                            this.myJsonService.switchTo(patient.guid);
                            this.myJsonService.$get().then(completeData => {
                                const filteredData = this.filterData(completeData);
                                filteredData['__guid__'] = patient.guid;
                                filteredData['__status__'] = patient.__status__;
                                filteredData['__centrecode__'] = this.centreCode;
                                console.log(JSON.stringify(filteredData));
                                this.ccDataStore.setDocument({ '__guid__': patient.guid }, filteredData);
                                dataSynchList[patient.guid] = new Date();
                                this.dataSynchService.save(dataSynchList, "", "");
                            });
                        });
                    }, 10 * 1000);
                }
                pickFirstToSend(patientsList, dataSynchList) {
                    if (!patientsList || patientsList.length == 0) {
                        return null;
                    }
                    const toBeSinchronized = patientsList.filter(p => !dataSynchList.hasOwnProperty(p.guid) || dataSynchList[p.guid] < p.__lastSaved__);
                    if (toBeSinchronized.length == 0) {
                        return null;
                    }
                    let olderDate = null;
                    let older = null;
                    for (let p of toBeSinchronized) {
                        if ((olderDate == null || dataSynchList[p.guid] < olderDate) && patientsList) {
                            olderDate = dataSynchList[p.guid];
                            older = p;
                        }
                    }
                    return older;
                }
                filterData(originalData) {
                    if (!originalData) {
                        return {};
                    }
                    this.crfs = originalData;
                    for (let variable of this.secretVariables) {
                        eval("delete this." + variable);
                    }
                    const result = this.crfs;
                    this.crfs = null;
                    return result;
                }
            }
            givitiweb.DataSynchronizer = DataSynchronizer;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
