/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            class ShortCircuitDirective {
                constructor() {
                    this.restrict = "A";
                    this.require = "?ngModel";
                    this.scope = {
                        shortCircuitFormula: "=",
                        shortCircuitModel: "="
                    };
                    this.priority = 1000;
                }
                static factory() {
                    return new ShortCircuitDirective();
                }
                link(scope, element, attributes, ngModelCtrl) {
                    new ShortCircuitDirectiveLogic(scope, ngModelCtrl);
                }
            }
            givitiweb.ShortCircuitDirective = ShortCircuitDirective;
            class ShortCircuitDirectiveLogic {
                constructor(scope, ngModelCtrl) {
                    this.myScope = scope;
                    this.myModelCtrl = ngModelCtrl;
                    this.myScope.$watch('shortCircuitFormula', (newValue, oldValue) => this.myWatcher(newValue, oldValue));
                }
                myWatcher(newValue, oldValue) {
                    let areDifferent = newValue != "NOOP" && (!angular.equals(newValue, oldValue)
                        || (this.myModelCtrl && !angular.equals(newValue, this.myModelCtrl.$viewValue))
                        || (this.myScope.shortCircuitModel && !angular.equals(newValue, this.myScope.shortCircuitModel)));
                    if (areDifferent) {
                        if (this.myModelCtrl) {
                            this.myModelCtrl.$setViewValue(newValue);
                            this.myModelCtrl.$render();
                        }
                        else if (this.myScope.shortCircuitModel) {
                            if (newValue == 'EMPTY_OBJECT') {
                                newValue = {};
                            }
                            if (newValue == 'EMPTY_ARRAY') {
                                newValue = [];
                            }
                            this.myScope.shortCircuitModel = newValue;
                            // this.myScope.$apply();
                        }
                    }
                }
            }
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
