/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class ColorPickerDirective {
                constructor() {
                    // restrict to: A attribute, E element
                    this.restrict = "A";
                    // element must have ng-model attribute.
                    this.require = 'ngModel';
                    this.link = function (scope, element, attributes, ngModelCtrl) {
                        element.tinycolorpicker({});
                        element.bind("change", function (evt, newColor) {
                            ngModelCtrl.$setViewValue(newColor);
                            scope.$apply();
                        });
                        scope.$watch('data.color', function (newValue) {
                            var colorPicker = element.data("plugin_tinycolorpicker");
                            colorPicker.setColor(newValue);
                        });
                    };
                }
                static factory() {
                    return new ColorPickerDirective();
                }
            }
            givitiweb.ColorPickerDirective = ColorPickerDirective;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
