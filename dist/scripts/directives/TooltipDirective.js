/*  TOOLTIP DIRECTIVE
*  Custom tooltip that allows text to be interacted with
*  HTML can be used inside, which allows for further
*  formatting options and links[...].
*/
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class TooltipDirective {
                constructor() {
                    this.restrict = "E";
                    this.scope = {
                        tooltip: "=text"
                    };
                    this.template = `
            <span class="tooltip"
                data-ng-click="showPermanently = !showPermanently"
                data-ng-mouseenter="show = true"
                data-ng-mouseleave="show = false"
            >
                <label class="help">?</label>
                <span data-ng-show="show || showPermanently" translate="{{tooltip}}"></span>
            </span>
        `;
                }
                link($scope) {
                    $scope.show = false;
                    $scope.showPermanently = false;
                }
                static factory() {
                    return new TooltipDirective();
                }
            }
            givitiweb.TooltipDirective = TooltipDirective;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
