/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class PatientsListDirective {
                constructor($http, $rootScope, listService, deletedListService, guidStorage, jsonService, ccDataStore, patientStatus, $interval) {
                    this.$http = $http;
                    this.$rootScope = $rootScope;
                    this.listService = listService;
                    this.deletedListService = deletedListService;
                    this.guidStorage = guidStorage;
                    this.jsonService = jsonService;
                    this.ccDataStore = ccDataStore;
                    this.patientStatus = patientStatus;
                    this.$interval = $interval;
                    this.restrict = "E";
                    this.link = this.linker.bind(this);
                    this.scope = {
                        patientsListColumns: "=columns"
                    };
                    this.template = `
        <span class="loading" data-ng-if="!dataLoaded">Sto caricando...</span>

        <button data-ng-if="dataLoaded" type="button" data-ng-click="vm.createPatient()">Nuovo ricovero</button>
        <div class="dialog" data-ng-if="deleting.patient != null">
            Vuoi davvero rimuovere il paziente {{ deleting.patient.substr(0, 6) }}?
            <button data-ng-click="vm.deletePatient(deleting.patient)">Rimuovi</button>
            <button data-ng-click="vm.permanentlyDeletePatient(deleting.patient)">Rimozione TOTALE</button>
            <button data-ng-click="deleting.patient = null">Annulla</button>
        </div>
        <div class="success" data-ng-if="deleting.success">
            Paziente cancellato correttamente!
        </div>
        <div class="error" data-ng-if="deleting.failed">
            Errore nella cancellazione del paziente!
        </div>
        <table data-ng-if="dataLoaded &amp;&amp; patientsList.length &gt; 0">
            <thead>
                <tr>
                    <th>Id</th>
                    <th data-ng-repeat="(column, label) in patientsListColumns">{{ label }}</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr data-ng-repeat="row in patientsList track by row.guid">
                    <td class="patient-id">{{ row.guid.substr(0, 6) }}</td>
                    <td data-ng-repeat="(column, label) in patientsListColumns">{{ row[ column ] }}</td>
                    <td class="status-{{ row['__status__'] }}">{{ row['__status__'] }}</td>
                    <td>
                        <button type="button" data-ng-click="vm.openPatient(row.guid)">Apri</button>
                        <button type="button" data-ng-click="deleting.patient = row.guid">Rimuovi</button>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <section id="deleted-patients">
            <button data-ng-show="!showDeleted" data-ng-click="showDeleted = true" data-ng-init="showDeleted = false">Mostra pazienti cancellati</button>
            <button data-ng-show="showDeleted" data-ng-click="showDeleted = false">Nascondi pazienti cancellati</button>
            <p data-ng-if="showDeleted &amp;&amp; dataLoaded &amp;&amp; deletedPatientsList.length == 0">Nessun paziente cancellato</p>
            <table data-ng-if="showDeleted &amp;&amp; dataLoaded &amp;&amp; deletedPatientsList.length &gt; 0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th data-ng-repeat="(column, label) in patientsListColumns">{{ label }}</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="row in deletedPatientsList track by row.guid">
                        <td class="patient-id">{{ row.guid.substr(0, 6) }}</td>
                        <td data-ng-repeat="(column, label) in patientsListColumns">{{ row[ column ] }}</td>
                        <td class="status-{{ row['__status__'] }}">{{ row['__status__'] }}</td>
                        <td>
                            <button type="button" data-ng-click="vm.restorePatient(row.guid)">Ripristina</button>
                            <button data-ng-click="vm.permanentlyDeletePatient(row.guid)">Rimozione TOTALE</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
        `;
                }
                ;
                static factory($q, $http, $rootScope, listService, deletedListService, guidStorage, jsonService, ccDataStore, patientStatus, $interval) {
                    return new PatientsListDirective($http, $rootScope, listService, deletedListService, guidStorage, jsonService, ccDataStore, patientStatus, $interval);
                }
                linker(scope, element, attributes, ngModelCtrl) {
                    return new PatientsListDirectiveLogic(scope, ngModelCtrl, this.$http, this.$rootScope, this.listService, this.deletedListService, this.guidStorage, this.jsonService, this.ccDataStore, this.patientStatus, this.$interval);
                }
            }
            givitiweb.PatientsListDirective = PatientsListDirective;
            class PatientsListDirectiveLogic {
                constructor(myScope, ngModelCtrl, $http, $rootScope, listService, deletedListService, patientGUIDStorage, jsonService, ccDataStore, patientStatus, $interval) {
                    this.myScope = myScope;
                    this.ngModelCtrl = ngModelCtrl;
                    this.$http = $http;
                    this.$rootScope = $rootScope;
                    this.listService = listService;
                    this.deletedListService = deletedListService;
                    this.patientGUIDStorage = patientGUIDStorage;
                    this.jsonService = jsonService;
                    this.ccDataStore = ccDataStore;
                    this.patientStatus = patientStatus;
                    this.$interval = $interval;
                    if (!this.myScope.patientsListColumns) {
                        console.log("ERROR: no columns provided in Patient List directive!");
                    }
                    /*
                    this.myScope.patientsListColumns = {
                        "crfs.NumberVariable": "Number",
                        "crfs.SingleChoiceVariable": "Single Choice"
                    }
                    */
                    this.myScope.dataLoaded = false;
                    this.myScope.patientsList = [];
                    this.myScope.deletedPatientsList = [];
                    this.myScope.deleting = { patient: null };
                    this.reloadData();
                    this.$interval(_ => this.reloadData(), 60 * 1000);
                    this.$rootScope.$on("PatientSaved", _ => {
                        this.patientGUIDStorage.$get().then(guid => {
                            let foundElement = null;
                            for (let patient of this.myScope.patientsList) {
                                if (patient.guid == guid) {
                                    foundElement = patient;
                                }
                            }
                            if (!foundElement) {
                                foundElement = { guid };
                                this.myScope.patientsList.push(foundElement);
                            }
                            const mainScope = this.myScope.$parent;
                            for (let variable in this.myScope.patientsListColumns) {
                                foundElement[variable] = eval("mainScope." + variable);
                            }
                            foundElement["__status__"] = this.patientStatus.getStatus();
                            foundElement["__lastSaved__"] = new Date();
                        });
                        this.reloadData().then(_ => this.listService.save(this.myScope.patientsList, "", ""));
                    });
                    this.myScope.vm = this;
                }
                createPatient() {
                    const newGuid = givitiweb.GuidUtils.createGUID();
                    this.patientGUIDStorage.update(newGuid);
                    this.$rootScope.$broadcast("PatientCreated");
                }
                openPatient(guid) {
                    this.patientGUIDStorage.update(guid);
                    this.$rootScope.$broadcast("PatientSwitched");
                }
                deletePatient(guid) {
                    this.reloadData().then(_ => {
                        this.movePatientFromAListToAnother(guid, this.myScope.patientsList, this.myScope.deletedPatientsList);
                        return Promise.all([
                            this.listService.save(this.myScope.patientsList, "", ""),
                            this.deletedListService.save(this.myScope.deletedPatientsList, "", "")
                        ]).then(success => this.showDeletionCompleted(guid), error => this.showDeletingError());
                    });
                }
                restorePatient(guid) {
                    this.reloadData().then(_ => {
                        this.movePatientFromAListToAnother(guid, this.myScope.deletedPatientsList, this.myScope.patientsList);
                        return Promise.all([
                            this.listService.save(this.myScope.patientsList, "", ""),
                            this.deletedListService.save(this.myScope.deletedPatientsList, "", "")
                        ]);
                    });
                }
                permanentlyDeletePatient(guid) {
                    this.patientGUIDStorage.update(guid);
                    this.jsonService.remove().then(success => {
                        this.removePatientFromList(this.myScope.patientsList, guid);
                        this.listService.save(this.myScope.patientsList, "", "").then(success => this.showDeletionCompleted(guid), error => this.showDeletingError());
                        this.ccDataStore.removeDocument({ '__guid__': guid });
                    }, error => this.showDeletingError());
                }
                reloadData() {
                    return Promise.all([
                        this.deletedListService.$get(),
                        this.listService.$get()
                    ]).then(([deleted, nonDeleted]) => {
                        this.mergeLists(this.myScope.deletedPatientsList, deleted !== null && deleted !== void 0 ? deleted : []);
                        this.mergeLists(this.myScope.patientsList, nonDeleted !== null && nonDeleted !== void 0 ? nonDeleted : []);
                        this.myScope.dataLoaded = true;
                        this.myScope.$apply();
                    });
                }
                mergeLists(current, incoming) {
                    for (let newPatient of incoming) {
                        let found = false;
                        for (let patient of current) {
                            if (patient.guid == newPatient.guid) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            current.push(newPatient);
                        }
                    }
                }
                movePatientFromAListToAnother(guid, listToBeRemovedFrom, listToBeAddedTo) {
                    const patient = this.removePatientFromList(listToBeRemovedFrom, guid);
                    if (patient) {
                        listToBeAddedTo.push(angular.copy(patient));
                    }
                }
                removePatientFromList(list, guid) {
                    var _a;
                    for (let i in list) {
                        if (list[i].guid == guid) {
                            return (_a = list.splice(i, 1)[0]) !== null && _a !== void 0 ? _a : null;
                        }
                    }
                    return null;
                }
                showDeletionCompleted(guid) {
                    this.myScope.deleting.success = true;
                    this.myScope.deleting.error = false;
                    this.myScope.deleting.patient = null;
                    setTimeout(_ => this.myScope.deleting.success = false, 5000);
                }
                showDeletingError() {
                    this.myScope.deleting.error = true;
                    this.myScope.deleting.success = false;
                    this.myScope.deleting.patient = null;
                    setTimeout(_ => this.myScope.deleting.error = false, 5000);
                }
            }
            givitiweb.PatientsListDirectiveLogic = PatientsListDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
