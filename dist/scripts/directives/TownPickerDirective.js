/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class TownPickerDirective {
                constructor($parse, $http) {
                    this.$parse = $parse;
                    this.$http = $http;
                    this.restrict = "A";
                    this.require = "ngModel";
                    this.scope = {
                        filterSource: "="
                    };
                    this.template = "<select data-ng-model='internalValue' data-ng-options='town.key as town.label for town in filteredTowns'></select>";
                    this.$inject = ["$parse", "$http"];
                    this.link = this.myLinker;
                }
                static factory($parse, $http) {
                    return new TownPickerDirective($parse, $http);
                }
                myLinker(scope, element, attributes, ngModelCtrl) {
                    new TownPickerDirectiveLogic(scope, ngModelCtrl, this.$parse, this.$http);
                }
            }
            givitiweb.TownPickerDirective = TownPickerDirective;
            class TownPickerDirectiveLogic {
                constructor(scope, ngModelCtrl, $parse, $http) {
                    this.$parse = $parse;
                    this.$http = $http;
                    this.myScope = scope;
                    this.$parse = $parse;
                    this.myModelCtrl = ngModelCtrl;
                    this.myModelCtrl.$render = () => { this.myRender(); };
                    this.myScope.$watch('internalValue', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                    this.myScope.$watch('filterSource', (newValue, oldValue) => {
                        if (newValue && this.myScope.towns) {
                            this.myScope.filteredTowns = this.myScope.towns[newValue];
                        }
                    });
                    // TODO: generalize from config?
                    this.$http.get(eval("jsonPath") + "towns.json").then(response => this.myScope.towns = response.data, err => console.log(err));
                }
                myRender() {
                    this.myScope.internalValue = this.myModelCtrl.$viewValue;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        this.myModelCtrl.$setViewValue(newValue);
                    }
                }
            }
            givitiweb.TownPickerDirectiveLogic = TownPickerDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
