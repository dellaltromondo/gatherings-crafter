/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class RegionPickerDirective {
                constructor($http) {
                    this.$http = $http;
                    this.restrict = "A";
                    this.require = "ngModel";
                    this.scope = {};
                    this.template = "<select data-ng-model='internalValue' data-ng-options='region.codice as region.nome for region in regions'></select>";
                }
                static factory($http) {
                    return new RegionPickerDirective($http);
                }
                link(scope, element, attributes, ngModelCtrl) {
                    new RegionPickerDirectiveLogic(scope, ngModelCtrl, this.$http);
                }
            }
            givitiweb.RegionPickerDirective = RegionPickerDirective;
            class RegionPickerDirectiveLogic {
                constructor(scope, ngModelCtrl, $http) {
                    this.$http = $http;
                    this.myScope = scope;
                    this.myModelCtrl = ngModelCtrl;
                    this.myModelCtrl.$render = () => { this.myRender(); };
                    this.myScope.$watch('internalValue', (newValue, oldValue) => this.myWatcher(newValue, oldValue));
                    // TODO: generalize from config?
                    this.$http.get(eval("jsonPath") + "regions.json").then(response => this.myScope.regions = response.data, err => console.log(err));
                }
                myRender() {
                    this.myScope.internalValue = this.myModelCtrl.$viewValue;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        this.myModelCtrl.$setViewValue(newValue);
                    }
                }
            }
            givitiweb.RegionPickerDirectiveLogic = RegionPickerDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
