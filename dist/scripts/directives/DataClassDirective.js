/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path='../utils/ColorHelper.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class DataClassDirective {
                constructor($compile, $timeout, $rootScope, patientStatus) {
                    this.$compile = $compile;
                    this.$timeout = $timeout;
                    this.$rootScope = $rootScope;
                    this.patientStatus = patientStatus;
                    this.$inject = ['$compile', '$timeout'];
                    this.restrict = "E";
                    this.link = this.linker.bind(this);
                    this.scope = {
                        ic3DataClass: '=ic3DataClass',
                        loaded: '=',
                        processor: '=processor',
                        xml: '=xml',
                        crfs: '=parent'
                    };
                }
                static factory($compile, $timeout, patientStatus, $rootScope) {
                    return new DataClassDirective($compile, $timeout, $rootScope, patientStatus);
                }
                linker(scope, element, attrs) {
                    return new DataClassDirectiveLogic(scope, element, attrs, this.$compile, this.patientStatus, this.$rootScope);
                }
            }
            givitiweb.DataClassDirective = DataClassDirective;
            class DataClassDirectiveLogic {
                constructor(scope, element, attrs, $compile, patientStatus, $rootScope) {
                    this.element = element;
                    this.attrs = attrs;
                    this.$compile = $compile;
                    this.patientStatus = patientStatus;
                    this.$rootScope = $rootScope;
                    this.myScope = scope;
                    this.myScope.ColorHelper = givitiweb.ColorHelper;
                    this.myScope.completenessController = this.patientStatus;
                    this.$rootScope.$on('PageChanged', (event, newSelectedPage) => {
                        this.selectedPage = newSelectedPage;
                    });
                    this.myScope.$watch('loaded', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                    if (this.myScope.loaded) {
                        this.myWatcher(true, false);
                    }
                }
                myWatcher(newValue, oldValue) {
                    if (newValue && ((newValue != oldValue) || !this.myScope.crfs)) {
                        this.myScope.vm = this;
                        this.patientStatus.setRootVariable(this.myScope.crfs);
                        this.patientStatus.setVM(this);
                        let transformed = this.transform(this.myScope);
                        if (transformed) {
                            this.element.append(transformed);
                        }
                        else {
                            this.element.html("<div class='error'>The XSL transform returned an empty value!</div>");
                        }
                        this.$compile(this.element.contents())(this.myScope);
                    }
                }
                toggleCheckBoxesSelection(variable, value, exclusive) {
                    var idx = variable.indexOf(value);
                    if (idx > -1) {
                        variable.splice(idx, 1);
                    }
                    else {
                        variable.push(value);
                    }
                    for (var i = 0; i < exclusive.length; i++) {
                        var exclusiveItemIndex = variable.indexOf(exclusive[i]);
                        if (exclusiveItemIndex > -1) {
                            variable.splice(exclusiveItemIndex, 1);
                        }
                    }
                }
                checkCompleteness(field, valueInField) {
                    for (var checkVisibilityObject in field) {
                        if (field[checkVisibilityObject] && field[checkVisibilityObject][valueInField] == true) {
                            return true;
                        }
                    }
                    return false;
                }
                addElementToSet(currentSet) {
                    const guid = givitiweb.GuidUtils.createGUID();
                    currentSet.push({ "guid": guid });
                    return guid;
                }
                removeElementFromSet(currentSet, guid) {
                    var elementFound = false;
                    var index = -1;
                    for (var element in currentSet) {
                        if (currentSet[element].guid == guid) {
                            index = parseInt(element);
                            elementFound = true;
                        }
                    }
                    if (elementFound) {
                        currentSet.splice(index, 1);
                    }
                    else {
                        throw "Element not found in variable of dataType Set! ";
                    }
                }
                isNaN(valueToBeChecked) {
                    return isNaN(valueToBeChecked);
                }
                maxInArrayByProperty(array, property) {
                    return array.reduce((a, b) => a[property] > b[property] ? a : b, { [property]: -Number.MAX_VALUE });
                }
                setSelectedPage(selectedPage) {
                    this.selectedPage = selectedPage;
                    this.$rootScope.$broadcast("PageChanged", selectedPage);
                    // this.$rootScope.$emit("PageChanged", selectedPage); 
                }
                getSelectedPage() {
                    return this.selectedPage;
                }
                //TODO: only for subti
                yearDiff(aDate, suffix = true) {
                    if (!aDate) {
                        return "--";
                    }
                    aDate = new Date(aDate).getFullYear();
                    let today = new Date().getFullYear();
                    let difference = today - aDate;
                    if (suffix) {
                        let yearSuffix = difference == 1 ? "anno" : "anni";
                        return `${difference} ${yearSuffix}`;
                    }
                    return difference;
                }
                //TODO: only for subti
                calculateGroup(aGroup) {
                    let result = 0;
                    if (aGroup === undefined) {
                        return;
                    }
                    // TODO: browser support for entries?
                    let entries = Object.entries(aGroup);
                    if (entries.length == 0) {
                        return;
                    }
                    // iterate all entries and parse their value
                    entries.forEach(variable => {
                        const [key, value] = variable;
                        result += this.parseNumber(value);
                    });
                    return result;
                }
                //TODO: only for subti
                parseNumber(stringToParse) {
                    if ((stringToParse == "" || stringToParse === undefined) && stringToParse != 0) {
                        return;
                    }
                    // if it's a number just take it as it is
                    if (!isNaN(stringToParse)) {
                        return stringToParse;
                    }
                    // adding 1 so that it wont consider the point
                    let lastPointIndex = stringToParse.lastIndexOf(".") + 1;
                    let lastUnderscoreIndex = stringToParse.lastIndexOf("_") + 1;
                    // this allows underscores to be used as a way of giving unique ids to same values
                    let relevantPosition = lastPointIndex > lastUnderscoreIndex ? lastPointIndex : lastUnderscoreIndex;
                    let numberInString = parseInt(stringToParse.slice(relevantPosition, stringToParse.length));
                    // if parsing failed for some reason
                    if (isNaN(numberInString)) {
                        return;
                    }
                    return numberInString;
                }
                transform(scope) {
                    var dataClasses = scope.xml.documentElement.getElementsByTagName("dataClass");
                    var xmlFragment;
                    for (var element in dataClasses) {
                        if (dataClasses[element].getAttribute && dataClasses[element].getAttribute("name") == scope.ic3DataClass) {
                            xmlFragment = dataClasses[element];
                            break;
                        }
                    }
                    if (!xmlFragment) {
                        console.log("ERROR: Dataclass '" + scope.ic3DataClass + "' has not been found!");
                    }
                    const transformed = scope.processor.transformToFragment(xmlFragment, document);
                    return transformed;
                }
                registerVariable(variableName, visibility) {
                    this.patientStatus.registerVariable(variableName, visibility);
                }
                noChildIsChecked(parentVariable, childBooleanVariableName) {
                    return Object.keys(parentVariable).every(el => !parentVariable[el][childBooleanVariableName]);
                }
                atLeastOneChildIsChecked(parentVariable, childBooleanVariableName) {
                    return Object.keys(parentVariable).some(el => parentVariable[el][childBooleanVariableName]);
                }
            }
            givitiweb.DataClassDirectiveLogic = DataClassDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
