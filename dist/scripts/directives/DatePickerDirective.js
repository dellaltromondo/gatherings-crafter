/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class DatePickerDirective {
                constructor() {
                    this.restrict = "A";
                    this.require = "ngModel";
                    this.scope = {
                        minDate: "@minDate",
                        maxDate: "@maxDate"
                    };
                    this.template = "<input data-ng-model='viewDate' data-ng-readonly='true' class='datebutton'></input>";
                }
                static factory() {
                    return new DatePickerDirective();
                }
                link(scope, element, attributes, ngModelCtrl) {
                    function updateWidget(element, scope) {
                        var options = {
                            format: 'd/m/Y',
                            formatDate: 'Y-m-d',
                            lang: 'it',
                            dayOfWeekStart: 1,
                            mask: true,
                            closeOnDateSelect: true,
                            allowBlank: true,
                            defaultSelect: false,
                            timepicker: false,
                            validateOnBlur: true,
                            scrollInput: false
                        };
                        var inputChild = angular.element(element).find("input");
                        if (scope.maxDate)
                            options.maxDate = scope.maxDate;
                        if (scope.minDate)
                            options.minDate = scope.minDate;
                        inputChild.datetimepicker(options);
                    }
                    ;
                    var inputChild = angular.element(element).find("input");
                    updateWidget(element, scope);
                    scope.$watch('maxDate', (newValue, oldValue) => {
                        if (newValue != oldValue) {
                            scope.maxDate = newValue;
                            updateWidget(element, scope);
                        }
                    });
                    scope.$watch('minDate', (newValue, oldValue) => {
                        if (newValue != oldValue) {
                            scope.minDate = newValue;
                            updateWidget(element, scope);
                        }
                    });
                    // Create class to handle logic for DatePicker
                    new DatePickerDirectiveLogic(scope, ngModelCtrl);
                }
            }
            givitiweb.DatePickerDirective = DatePickerDirective;
            class DatePickerDirectiveLogic {
                constructor(scope, ngModelCtrl) {
                    this.myScope = scope;
                    this.myModelCtrl = ngModelCtrl;
                    this.myModelCtrl.$formatters.push(this.myFormatter);
                    this.myModelCtrl.$render = () => { this.myRender(); };
                    this.myScope.$watch('viewDate', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                }
                myFormatter(modelValue) {
                    var momentDate = moment(modelValue, givitiweb.GlobalConfig.jsonDateFormat);
                    var value;
                    if (momentDate.isValid()) {
                        value = momentDate.format(givitiweb.GlobalConfig.viewDateFormat);
                    }
                    else {
                        value = modelValue;
                    }
                    return value;
                }
                myRender() {
                    this.myScope.viewDate = this.myModelCtrl.$viewValue;
                }
                //TODO: may be moved to DateUtils
                crfValidityDateToMomentDate(date) {
                    var newDate;
                    if (date === 'today') {
                        newDate = moment();
                    }
                    else {
                        newDate = moment(date, givitiweb.GlobalConfig.jsonDateFormat);
                    }
                    return newDate;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        var momentDate = moment(newValue, givitiweb.GlobalConfig.viewDateFormat);
                        var isBefore = false;
                        var isAfter = false;
                        if (this.myScope.minDate)
                            isBefore = momentDate.isBefore(this.crfValidityDateToMomentDate(this.myScope.minDate));
                        if (this.myScope.maxDate)
                            isAfter = momentDate.isAfter(this.crfValidityDateToMomentDate(this.myScope.maxDate));
                        var value;
                        if (momentDate.isValid() && !isBefore && !isAfter) {
                            value = momentDate.format(givitiweb.GlobalConfig.jsonDateFormat);
                            this.myModelCtrl.$setViewValue(value);
                        }
                        else {
                            this.myScope.viewDate = null;
                            this.myModelCtrl.$setViewValue(null);
                        }
                    }
                }
            }
            givitiweb.DatePickerDirectiveLogic = DatePickerDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
