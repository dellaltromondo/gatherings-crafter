/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class AutocompleteJSONDirective {
                constructor($q, $http, templateUrl, suggestionURL) {
                    this.$q = $q;
                    this.$http = $http;
                    this.templateUrl = templateUrl;
                    this.suggestionURL = suggestionURL;
                    this.$inject = ['$q', '$http'];
                    this.restrict = "E";
                    this.require = "ngModel";
                    this.scope = {
                        className: '@className',
                        indexName: '@indexName',
                        limit: '@limit',
                        page: '@page',
                        parentModel: '@dataNgModel'
                    };
                    this.link = this.realLink.bind(this);
                }
                realLink(scope, element, attributes, ngModelCtrl) {
                    // Create class to handle logic inside directive
                    new AutocompleteJSONDirectiveLogic(this.$q, scope, this.$http, ngModelCtrl, this.suggestionURL);
                }
                static factory($q, $http, templateUrl, suggestionURL) {
                    return new AutocompleteJSONDirective($q, $http, templateUrl, suggestionURL);
                }
            }
            givitiweb.AutocompleteJSONDirective = AutocompleteJSONDirective;
            class AutocompleteJSONDirectiveLogic {
                constructor($q, myScope, $http, myModelCtrl, serviceURL) {
                    this.$q = $q;
                    this.myScope = myScope;
                    this.$http = $http;
                    this.myModelCtrl = myModelCtrl;
                    this.serviceURL = serviceURL;
                    this.myModelCtrl.$render = () => { this.myRender(); };
                    this.myScope.remoteUrlRequestFn = this.getValues.bind(this);
                    this.myScope.remoteUrlResponseFormatterFn = this.formatResponse.bind(this);
                    this.myScope.$watch('tags', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                }
                myRender() {
                    this.myScope.tags = this.myModelCtrl.$viewValue;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        this.myModelCtrl.$setViewValue(newValue);
                        this.myScope.parentModel = newValue;
                    }
                }
                formatResponse(data) {
                    var response = JSON.parse(data.data.d);
                    var newResponse = [];
                    for (var index in response) {
                        if (response[index]) {
                            var searchStr = response[index];
                            var newObject = { text: searchStr };
                            if (newResponse.indexOf(newObject) == -1) {
                                newResponse.push(newObject);
                            }
                        }
                    }
                    return newResponse;
                }
                getValues(searchText) {
                    var method = "GetAutoCompleteIndexValues";
                    var deferred = this.$q.defer();
                    if (searchText.length > 2) {
                        var query = '?className=' + this.myScope.className + '&indexName=' + this.myScope.indexName + '&searchText=' + searchText + '&limit=10';
                        this.$http.get(this.serviceURL + method + query)
                            .then((data) => {
                            var newData = this.formatResponse(data);
                            deferred.resolve(newData);
                        });
                    }
                    return deferred.promise;
                }
            }
            givitiweb.AutocompleteJSONDirectiveLogic = AutocompleteJSONDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
