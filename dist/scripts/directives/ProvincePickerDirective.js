/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class ProvincePickerDirective {
                constructor($parse, $http) {
                    this.$parse = $parse;
                    this.$http = $http;
                    this.restrict = "A";
                    this.require = "ngModel";
                    this.scope = {
                        filterSource: "="
                    };
                    this.template = "<select data-ng-model='internalValue' data-ng-options='province.key as province.label for province in filteredProvinces'></select>";
                    this.$inject = ["$parse"];
                    this.link = this.myLinker;
                }
                static factory($parse, $http) {
                    return new ProvincePickerDirective($parse, $http);
                }
                myLinker(scope, element, attributes, ngModelCtrl) {
                    new ProvincePickerDirectiveLogic(scope, ngModelCtrl, this.$parse, this.$http);
                }
            }
            givitiweb.ProvincePickerDirective = ProvincePickerDirective;
            class ProvincePickerDirectiveLogic {
                constructor(scope, ngModelCtrl, $parse, $http) {
                    this.myScope = scope;
                    this.$parse = $parse;
                    this.$http = $http;
                    this.myModelCtrl = ngModelCtrl;
                    this.myModelCtrl.$render = () => this.myRender();
                    this.myScope.$watch('internalValue', (newValue, oldValue) => this.myWatcher(newValue, oldValue));
                    this.myScope.$watch('filterSource', (newValue, oldValue) => {
                        if (newValue && this.myScope.provinces) {
                            this.myScope.filteredProvinces = this.myScope.provinces[newValue];
                        }
                    });
                    // TODO: generalize from config?
                    this.$http.get(eval("jsonPath") + "provinces.json").then(response => this.myScope.provinces = response.data, err => console.log(err));
                }
                myRender() {
                    this.myScope.internalValue = this.myModelCtrl.$viewValue;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        this.myModelCtrl.$setViewValue(newValue);
                    }
                }
            }
            givitiweb.ProvincePickerDirectiveLogic = ProvincePickerDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
