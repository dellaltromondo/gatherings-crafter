/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class AlertDirective {
                constructor($parse) {
                    this.$inject = ['$parse'];
                    // restrict to: A attribute, E element
                    this.priority = 1;
                    this.$parse = $parse;
                    this.link = (scope, element, attr) => { this.myLinker(scope, element, attr); };
                    // element must have ng-model attribute.
                    //this.require = 'ngModel';
                }
                static factory($parse) {
                    return new AlertDirective($parse);
                }
                myLinker(scope, element, attr) {
                    var msg = this.$parse(attr.confirmationNeeded);
                    var clickAction = this.$parse(attr.ic3Click);
                    element.click('click', function (event) {
                        var callBack = function () {
                            clickAction(scope, { $event: event });
                        };
                        if (window.confirm(msg(scope))) {
                            scope.$apply(callBack);
                        }
                    });
                }
            }
            givitiweb.AlertDirective = AlertDirective;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
