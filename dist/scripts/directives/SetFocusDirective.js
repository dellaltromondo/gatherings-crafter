/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class SetFocusDirective {
                constructor($timeout) {
                    //public restrict;
                    this.restrict = "A";
                    this.$timeout = $timeout;
                    this.link = (scope, element, attr) => { this.myLinker(scope, element, attr); };
                }
                static factory($timeout) {
                    return new SetFocusDirective($timeout);
                }
                myLinker(scope, elements, attrs) {
                    var timeout = this.$timeout;
                    scope.$watch(attrs.setFocus, function (newValue) {
                        timeout(function () {
                            newValue && elements[0].focus();
                            //FIXME: assuming elements is a text input
                            elements[0].selectionStart = elements[0].selectionEnd = elements[0].value.length;
                        });
                    }, true);
                }
            }
            givitiweb.SetFocusDirective = SetFocusDirective;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
