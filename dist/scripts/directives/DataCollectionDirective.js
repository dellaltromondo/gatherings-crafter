/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class DataCollectionDirective {
                constructor($q, $http) {
                    this.$q = $q;
                    this.$http = $http;
                    this.$inject = ['$q', '$http'];
                    this.restrict = "E";
                    this.link = this.linker.bind(this);
                    this.scope = {
                        ic3DataClass: '=ic3DataClass',
                        xslPath: '=xslPath',
                        crfPath: '=crfPath',
                        crfs: '=ngModel'
                    };
                    this.template = '<ic3-data-class parent="crfs" processor="processor" xml="xml" ic3-data-class="ic3DataClass" loaded="loaded"></ic3-data-class>';
                }
                ;
                static factory($q, $http) {
                    return new DataCollectionDirective($q, $http);
                }
                linker(scope, element, attributes, ngModelCtrl) {
                    return new DataCollectionDirectiveLogic(scope, ngModelCtrl, this.$q, this.$http);
                }
            }
            givitiweb.DataCollectionDirective = DataCollectionDirective;
            class DataCollectionDirectiveLogic {
                constructor(myScope, ngModelCtrl, $q, $http) {
                    this.myScope = myScope;
                    this.ngModelCtrl = ngModelCtrl;
                    this.$q = $q;
                    this.$http = $http;
                    this.myScope.loaded = false;
                    this.myScope.$watch('crfPath', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                }
                myWatcher(newValue, oldValue) {
                    if (newValue && ((newValue != oldValue) || !this.myScope.xml)) {
                        this.myScope.loaded = false;
                        this.myScope.processor = new XSLT2Processor();
                        this.importCrf(this.myScope.crfPath)
                            .then(() => this.importStylesheet(this.myScope.xslPath));
                    }
                }
                importStylesheet(xslPath) {
                    var deferred = this.$q.defer();
                    this.myScope.processor.importStylesheetURI(xslPath)
                        .done(() => {
                        this.myScope.$apply(() => {
                            this.myScope.loaded = true;
                            deferred.resolve(true);
                        });
                    });
                    return deferred.promise;
                }
                importCrf(xmlPath) {
                    return givitiweb.PromiseUtils
                        .getAsyncXML(this.$http, xmlPath)
                        .then(dom => this.myScope.xml = dom);
                }
            }
            givitiweb.DataCollectionDirectiveLogic = DataCollectionDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
