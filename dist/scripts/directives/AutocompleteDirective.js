/// <reference path='../definitions/angular/angular.d.ts' />
var it;
(function (it) {
    var marionegri;
    (function (marionegri) {
        var givitiweb;
        (function (givitiweb) {
            'use strict';
            class AutocompleteDirective {
                constructor($q, $http, templateUrl, suggestionURL, culture) {
                    this.$q = $q;
                    this.$http = $http;
                    this.templateUrl = templateUrl;
                    this.suggestionURL = suggestionURL;
                    this.culture = culture;
                    this.$inject = ['$http', '$q', 'culture'];
                    this.restrict = "E";
                    this.require = "ngModel";
                    this.scope = {
                        tableName: '@tableName',
                        columnName: '@columnName',
                        limit: '@limit',
                        page: '@page',
                        parentModel: '@dataNgModel'
                    };
                    this.link = this.realLink.bind(this);
                }
                realLink(scope, element, attributes, ngModelCtrl) {
                    // Create class to handle logic for DatePicker
                    new AutocompleteDirectiveLogic(this.$q, scope, this.$http, ngModelCtrl, this.suggestionURL, this.culture);
                }
                static factory($q, $http, templateUrl, suggestionURL, culture) {
                    return new AutocompleteDirective($q, $http, templateUrl, suggestionURL, culture);
                }
            }
            givitiweb.AutocompleteDirective = AutocompleteDirective;
            class AutocompleteDirectiveLogic {
                constructor($q, myScope, $http, myModelCtrl, serviceURL, culture) {
                    this.$q = $q;
                    this.myScope = myScope;
                    this.$http = $http;
                    this.myModelCtrl = myModelCtrl;
                    this.serviceURL = serviceURL;
                    this.culture = culture;
                    this.myModelCtrl.$render = () => { this.myRender(); };
                    this.myScope.remoteUrlRequestFn = this.getValues.bind(this);
                    this.myScope.remoteUrlResponseFormatterFn = this.formatResponse.bind(this);
                    this.myScope.$watch('tags', (newValue, oldValue) => { this.myWatcher(newValue, oldValue); });
                }
                myRender() {
                    this.myScope.tags = this.myModelCtrl.$viewValue;
                }
                myWatcher(newValue, oldValue) {
                    if (newValue != oldValue) {
                        this.myModelCtrl.$setViewValue(newValue);
                        this.myScope.parentModel = newValue;
                    }
                }
                formatResponse(data) {
                    var response = JSON.parse(data.data.d);
                    var newResponse = [];
                    for (var index in response) {
                        var newObject = { text: response[index].text, key: response[index].key };
                        if (newResponse.indexOf(newObject) == -1) {
                            newResponse.push(newObject);
                        }
                    }
                    return newResponse;
                }
                getValues(searchText) {
                    var method = "GetAutoCompleteValues";
                    var deferred = this.$q.defer();
                    var query = '?tableName=' + this.myScope.tableName + '&columnName=' + this.myScope.columnName + '&searchText=' + searchText + '&limit=10&culture=' + this.culture;
                    this.$http.get(this.serviceURL + method + query)
                        .then((data) => {
                        var newData = this.formatResponse(data);
                        deferred.resolve(newData);
                    });
                    return deferred.promise;
                }
            }
            givitiweb.AutocompleteDirectiveLogic = AutocompleteDirectiveLogic;
        })(givitiweb = marionegri.givitiweb || (marionegri.givitiweb = {}));
    })(marionegri = it.marionegri || (it.marionegri = {}));
})(it || (it = {}));
