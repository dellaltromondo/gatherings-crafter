<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <!-- ######## DATACLASS ######## -->
  <!-- group of variables that can be inserted in different point of the data collection -->
  
  <xsl:template match="dataClass">
    <xsl:param name="parent" />

    <xsl:choose>
      <xsl:when test="not($parent)">
        <xsl:variable name="selectedParent" select="'crfs'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="selectedParent" select="$parent"/>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$selectedParent" />
    </xsl:apply-templates>
    
    <xsl:apply-templates select="./warning">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>
  
  <!-- ######## INSTANCE OF DATACLASS ######## -->
  <!-- variable with datatype not in BaseTypes -->
  <!-- searches for a dataclass that has the same name of the datatype -->

  <xsl:template match="variable">
    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
        <xsl:with-param name="parent" select="$fullName"/>
        <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages"/>
    </xsl:apply-templates>

    <xsl:apply-templates select="./warning">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>

</xsl:stylesheet>
