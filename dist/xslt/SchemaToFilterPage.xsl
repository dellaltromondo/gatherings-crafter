﻿<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:import href="Utils.xsl"/>
  <xsl:import href="FilterBaseTypes.xsl"/>
  <xsl:import href="FilterDataClassTypes.xsl"/>

  <xsl:output method="html" indent="yes"/>

</xsl:stylesheet>
