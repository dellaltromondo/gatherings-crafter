<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="no" encoding="UTF-8" />

  <xsl:param name="dataClass" />
  <!-- select="'CentreStructuralData'" -->

  <xsl:template match="root">
    <root>
      <xsl:call-template name="dataClass">
        <xsl:with-param name="classes" select="//dataClass[ @name= $dataClass ]" />
        <xsl:with-param name="parent" select="''" />
      </xsl:call-template>
    </root>
  </xsl:template>

  <xsl:template name="dataClass">
    <xsl:param name="classes" />
    <xsl:param name="parent" />
    <xsl:apply-templates select="$classes/variable">
      <xsl:with-param name="parent" select="$parent" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="variable">
    <xsl:param name="node" />
    <xsl:param name="parent" />
    <variable>
      <xsl:attribute name="name">
        <xsl:value-of select="$node/@name" />
      </xsl:attribute>
      <xsl:attribute name="fullName">
        <xsl:value-of select="$parent" />
      </xsl:attribute>
      <xsl:if test="($node/@label) and ($node/@label) != ''">
        <xsl:attribute name="label">
          <xsl:value-of select="$node/@label" />
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="not ($node/@label)">
        <xsl:attribute name="label">
          <xsl:value-of select="$node/@name" />
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="dataType">
        <xsl:value-of select="$node/@dataType" />
      </xsl:attribute>
      <xsl:attribute name="required">
        <!--<xsl:value-of select="$node/@required" />-->
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="'crfs.'" />
          <xsl:with-param name="replacement" select="''" />
          <xsl:with-param name="string" select="$node/@required" />
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="valuesSet">
        <!--<xsl:value-of select="$node/@valuesSet" />-->
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="'crfs.'" />
          <xsl:with-param name="replacement" select="''" />
          <xsl:with-param name="string" select="$node/@valuesSet" />
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="visible">
        <!--<xsl:value-of select="$node/@visible" />-->
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="'crfs.'" />
          <xsl:with-param name="replacement" select="''" />
          <xsl:with-param name="string" select="$node/@visible" />
        </xsl:call-template>
      </xsl:attribute>
      <xsl:attribute name="computedFormula">
        <!--<xsl:value-of select="$node/@computedFormula" />-->
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="'crfs.'" />
          <xsl:with-param name="replacement" select="''" />
          <xsl:with-param name="string" select="$node/@computedFormula" />
        </xsl:call-template>
      </xsl:attribute>
    </variable>
  </xsl:template>

  <xsl:template match="variable [ @dataType= 'group' ] ">
    <xsl:param name="parent" />
    <xsl:choose>
      <xsl:when test="$parent and $parent != ''">
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
        <xsl:apply-templates select="./variable">
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="@name" />
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
        <xsl:apply-templates select="./variable">
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="variable [ @dataType != 'group' and not (@interface)]">
    <xsl:param name="parent" />
    <xsl:variable name="dataType" select="@dataType" />
    <xsl:choose>
      <xsl:when test="$parent and $parent != ''">
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
        <xsl:call-template name="dataClass">
          <xsl:with-param name="classes"
						select="//dataClass[ @name = $dataType ]" />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="@name" />
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
        <xsl:call-template name="dataClass">
          <xsl:with-param name="classes"
						select="//dataClass[ @name = $dataType ]" />
          <xsl:with-param name="parent" select="$fullName" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="variable [ @valuesSetKey ]">
    <xsl:param name="parent" />
    <xsl:variable name="dataType" select="@dataType" />
    <xsl:variable name="valuesSetKey" select="@valuesSetKey" />
    <xsl:choose>
      <xsl:when test="$parent and $parent != ''">
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
        <xsl:apply-templates select="//valuesSet[ @name=$valuesSetKey ]/value" mode="iterateForObject">
          <xsl:with-param name="parent" select="$fullName" />
          <xsl:with-param name="dataType" select="$dataType" />
          <xsl:with-param name="valuesSetName" select="$valuesSetKey" />
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="@name" />
        <xsl:apply-templates select="//valuesSet[ @name=$valuesSetKey ]/value" mode="iterateForObject">
          <xsl:with-param name="parent" select="$fullName" />
          <xsl:with-param name="dataType" select="$dataType" />
          <xsl:with-param name="valuesSetName" select="$valuesSetKey" />
        </xsl:apply-templates>      
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'set' ]">
    <xsl:param name="parent" />
    <xsl:variable name="dataType" select="@itemsDataType"/>
    <xsl:choose>
      <xsl:when test="$parent and $parent != ''">
        <xsl:variable name="setName" select="concat($parent, '.', @name)"/>
        <xsl:variable name="elementName" select="concat($setName, '[', '0-N' ,']')" />
        <!--<xsl:variable name="fullName" select="$setName" />-->
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$setName" />
        </xsl:call-template>
        <xsl:call-template name="dataClass">
          <xsl:with-param name="classes"
				    select="//dataClass[ @name = $dataType ]" />
          <xsl:with-param name="parent" select="$elementName" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="setName" select="@name"/>
        <xsl:variable name="elementName" select="concat($setName, '[', '0-N' ,']')" />
        <!--<xsl:variable name="fullName" select="$elementName" />-->
        <xsl:call-template name="variable">
          <xsl:with-param name="node" select="." />
          <xsl:with-param name="parent" select="$setName" />
        </xsl:call-template>
        <xsl:call-template name="dataClass">
          <xsl:with-param name="classes"
				    select="//dataClass[ @name = $dataType ]" />
          <xsl:with-param name="parent" select="$elementName" />
        </xsl:call-template>     
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
   
  <xsl:template match="value" mode="iterateForObject">
    <xsl:param name="parent" />
    <xsl:param name="dataType" />
    <xsl:param name="valuesSetName" />
    <xsl:variable name="fullName" select="concat($parent, '.',  $valuesSetName, '__', @name, '')" />
    <xsl:call-template name="dataClass">
      <xsl:with-param name="classes" select="//dataClass[ @name = $dataType]" />
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:param name="target" select='"&apos;"'/>
    <xsl:param name="replacement" select="'\&quot;'"/>

    <xsl:if test="$string">
      <xsl:value-of select='substring-before(concat($string,$target),$target)'/>
      <xsl:if test='contains($string, $target)'>
        <xsl:value-of select='$replacement'/>
      </xsl:if>
      <xsl:call-template name="replace">
        <xsl:with-param name="string" select='substring-after($string, $target)'/>
        <xsl:with-param name="target" select="$target"/>
        <xsl:with-param name="replacement" select="$replacement"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
