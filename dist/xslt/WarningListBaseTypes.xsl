<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable [ @dataType= 'group' ] " >
    <xsl:param name="parent"/>

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:apply-templates select="./variable" >
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

    <xsl:apply-templates select="./warning">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="warning">
    <xsl:param name="fullName" />

    <xsl:variable name="warningName" select="concat( $fullName, '.WARNING_', @name, '_CONFIRMED')" />
    <xsl:variable name="warningID">
      <xsl:call-template name="escapeFullName">
        <xsl:with-param name="fullName" select="$warningName" />
      </xsl:call-template>
    </xsl:variable>
    <div data-ng-show="{ @if }" data-ng-click="{ concat('vm.scrollToElement(''', $warningID, ''')') }">
      <xsl:attribute name="data-ng-class">
        <xsl:value-of select="concat('{''warning'': !', $warningName, ', ''solvedWarning'': ', $warningName, '}')" />
      </xsl:attribute>
      <xsl:value-of select="." />
      <input type="checkbox" data-ng-model="{ $warningName }" /><span>I confirm the inserted data</span>
    </div>
  </xsl:template>

</xsl:stylesheet>
