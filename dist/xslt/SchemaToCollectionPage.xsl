﻿<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml"
>

  <xsl:import href="Utils.xsl"/>
  <xsl:import href="CollectionBaseTypes.xsl"/>
  <xsl:import href="CollectionDataClassTypes.xsl"/>
  <xsl:import href="CollectionGeoTypes.xsl"/>

  <xsl:param name="DEBUG" select="false()" />

  <xsl:output method="html" indent="yes"/>
  
</xsl:stylesheet>
