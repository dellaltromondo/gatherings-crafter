<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <!-- ######## DATACLASS ######## -->
  <!-- group of variables that can be inserted in different point of the data collection -->
  
  <xsl:template match="dataClass">
    <xsl:param name="parent" />

    <xsl:choose>
      <xsl:when test="not($parent)">
        <xsl:variable name="selectedParent" select="'crfs'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="selectedParent" select="$parent"/>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$selectedParent" />
    </xsl:apply-templates>

    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>
  
  <!-- ######## INSTANCE OF DATACLASS ######## -->
  <!-- variable with datatype not in BaseTypes -->
  <!-- searches for a dataclass that has the same name of the datatype -->

  <xsl:template match="variable">
    <xsl:param name="parent" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
      <li class="requirement" data-ng-show="completenessController.isVariableMissing('{$fullName}')" data-ng-click="vm.scrollToElement('{$fullName}')">
        <xsl:call-template name="getTranslatedLabel">
          <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
        </xsl:call-template>
      </li>
    </xsl:if>

    <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
        <xsl:with-param name="parent" select="$fullName"/>
        <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages"/>
    </xsl:apply-templates>

    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>

</xsl:stylesheet>
