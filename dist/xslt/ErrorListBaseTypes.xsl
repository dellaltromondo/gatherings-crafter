<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable [ @dataType= 'group' ] " >
    <xsl:param name="parent"/>

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:apply-templates select="./variable" >
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

    <xsl:apply-templates select="./error">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="error">
    <xsl:param name="fullName" />

    <xsl:variable name="errorName" select="concat( $fullName, '.ERROR_', @name)" />
    <xsl:variable name="errorID">
      <xsl:call-template name="escapeFullName">
        <xsl:with-param name="fullName" select="$errorName" />
      </xsl:call-template>
    </xsl:variable>
    <div class="error" data-ng-show="{ @if }" data-ng-click="vm.scrollToElement('{ $errorID }')">
      <xsl:value-of select="." />
    </div>
  </xsl:template>

</xsl:stylesheet>
