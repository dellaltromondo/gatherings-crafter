<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <!-- ######## GROUP ######## -->
  <!-- container for other variables -->

  <xsl:template match="variable [ @dataType= 'group' ] " >
    <xsl:param name="parent"/>
    <xsl:param name="meIsAPage"/>

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()"/>
      </xsl:otherwise>  
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="@saveAsSeparated">
        <xsl:variable name="fullName" select="concat('$parent.', $parent, '.', @name)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$meIsAPage">
        <xsl:variable name="container">section</xsl:variable>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="container">fieldset</xsl:variable>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:element name="{$container}">
      <xsl:if test="@saveAsSeparated">
        <xsl:attribute name="data-ng-controller">ModelSeparationController</xsl:attribute>
      </xsl:if>
      <xsl:attribute name="data-ng-init">
        <xsl:if test="@saveAsSeparated">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || saver.getGUID(); saver.setGUID(', $fullName, '); rootOf[saver.getGUID()] = crfs')"></xsl:value-of>
        </xsl:if>
        <xsl:if test="not(@saveAsSeparated)">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || {}')"></xsl:value-of>
        </xsl:if>
      </xsl:attribute> 

      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

      <xsl:attribute name="class" select="@name" />

      <xsl:apply-templates select="@visible" />
      <xsl:call-template name="addDescription">
        <xsl:with-param name="element" select="'legend'" />
        <xsl:with-param name="cssclass" select="'groupDescription'" />
      </xsl:call-template>

      <xsl:call-template name="clearVariableWhenHidden">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="defaultValueIfHidden" select="concat($apos, 'EMPTY_OBJECT', $apos)" />
      </xsl:call-template>

      <xsl:if test="$container = 'section'">
        <xsl:variable name="quot">'</xsl:variable>
        <xsl:attribute name="data-ng-show" select="concat('vm.getSelectedPage() == ', $quot, $fullName, $quot)" />
      </xsl:if>

      <xsl:if test="$separateChildrenInPages">
        <nav>
          <xsl:apply-templates select="./variable" mode="nav">
            <xsl:with-param name="parent" select="$fullName" />
          </xsl:apply-templates>
          <xsl:variable name="quot">'</xsl:variable>
          <xsl:variable name="firstChildName" select="./variable[1]/@name" />
          <xsl:attribute name="data-ng-init" select="concat('vm.setSelectedPage(', $quot, $fullName, '.', $firstChildName, $quot, ')' )" />
        </nav>
      </xsl:if>

      <xsl:if test="$DEBUG">
        <details>
          <summary>Debug</summary>
          <span data-ng-bind="saver.getGUID()"></span>
        </details>
      </xsl:if>

      <xsl:choose>
        <xsl:when test="@saveAsSeparated">
          <button type="button" data-ng-click="saver.loadJsonData()" data-ng-if="!status.dataLoaded">Expand</button>
          <xsl:variable name="nextParent" select="'rootOf[saver.getGUID()]'"></xsl:variable>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="nextParent" select="$fullName"></xsl:variable>
        </xsl:otherwise>
      </xsl:choose>

      <div>
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()"/>
        </xsl:call-template>
        <xsl:if test="@saveAsSeparated">
          <xsl:attribute name="data-ng-if">status.dataLoaded</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="./variable" >
          <xsl:with-param name="parent" select="$nextParent" />
          <xsl:with-param name="meIsAPage" select="$separateChildrenInPages"/>
        </xsl:apply-templates>
      </div>

      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

    </xsl:element>
  </xsl:template>
  
  <!-- ######## DATACLASS ######## -->
  <!-- same as a "group" of variables, but can be inserted in different points of the data collection -->

  <xsl:template match="dataClass">
    <!-- for storing the name of the variable with @valuesSetKey -->
    <!-- (use this for $arrayOfExcludes) -->
    <xsl:param name="containerName" />
    <!-- myValue represents the valuesSetName.@name string that will allow the future variable -->
    <!-- to identify itself (use this for $arrayOfExcludes) -->
    <xsl:param name="myValue" />
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />
    <xsl:param name="separateChildrenInPages" />
    <xsl:param name="arrayOfExcludes" />

    <xsl:choose>
      <xsl:when test="not($parent)">
        <xsl:variable name="selectedParent" select="'crfs'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="selectedParent" select="$parent"/>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="class">
      <xsl:value-of select="@name" />
    </xsl:attribute>
    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$selectedParent" />
      <xsl:with-param name="myValue" select="$myValue" />
      <xsl:with-param name="containerName" select="$containerName" />
      <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
      <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
    </xsl:apply-templates>

    <xsl:apply-templates select="./error">
      <xsl:with-param name="fullName" select="$selectedParent" />
    </xsl:apply-templates>
    <xsl:apply-templates select="./warning">
      <xsl:with-param name="fullName" select="$selectedParent" />
    </xsl:apply-templates>
    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>
  
  <!-- ######## INSTANCE OF DATACLASS ######## -->
  <!-- variable with datatype not in BaseTypes -->
  <!-- searches for a dataclass that has the same name of the datatype -->

  <xsl:template match="variable">
    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()"/>
      </xsl:otherwise>  
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="@saveAsSeparated">
        <xsl:variable name="fullName" select="concat('$parent.', $parent, '.', @name)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$meIsAPage">
        <xsl:variable name="container">section</xsl:variable>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="container">fieldset</xsl:variable>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:element name="{$container}">
      <xsl:if test="@saveAsSeparated">
        <xsl:attribute name="data-ng-controller">ModelSeparationController</xsl:attribute>
      </xsl:if>
      <xsl:attribute name="data-ng-init">
        <xsl:if test="@saveAsSeparated">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || saver.getGUID(); saver.setGUID(', $fullName, '); rootOf[saver.getGUID()] = crfs')"></xsl:value-of>
        </xsl:if>
        <xsl:if test="not(@saveAsSeparated)">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || {}; thisObject = ', $fullName)"></xsl:value-of>
        </xsl:if>
      </xsl:attribute>

      <xsl:attribute name="class" select="@dataType" />

      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

      <xsl:apply-templates select="@visible" />
      <xsl:call-template name="addDescription">
        <xsl:with-param name="element" select="'legend'" />
        <xsl:with-param name="cssclass" select="'groupDescription'" />
      </xsl:call-template>
      <xsl:call-template name="clearVariableWhenHidden">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="defaultValueIfHidden" select="concat($apos, 'EMPTY_OBJECT', $apos)" />
      </xsl:call-template>
      
      <xsl:if test="$meIsAPage">
        <xsl:variable name="quot">'</xsl:variable>
        <xsl:attribute name="data-ng-show" select="concat('vm.getSelectedPage() == ', $quot, $fullName, $quot)" />
      </xsl:if>

      <xsl:if test="$separateChildrenInPages">
        <nav>
          <xsl:apply-templates select="//dataClass[ @name = $dataType ]/variable" mode="nav">
            <xsl:with-param name="parent" select="$fullName" />
          </xsl:apply-templates>
          <xsl:variable name="quot">'</xsl:variable>
          <xsl:variable name="firstChildName" select="//dataClass[ @name = $dataType ]/variable[1]/@name" />
          <xsl:attribute name="data-ng-init" select="concat('vm.setSelectedPage(', $quot, $fullName, '.', $firstChildName, $quot, ')' )" />
        </nav>
      </xsl:if>

      <xsl:if test="$DEBUG">
        <details>
          <summary>Debug</summary>
          <span data-ng-bind="saver.getGUID()"></span>
        </details>
      </xsl:if>

      <xsl:choose>
        <xsl:when test="@saveAsSeparated">
          <button type="button" data-ng-click="saver.loadJsonData()" data-ng-if="!status.dataLoaded &amp;&amp; !status.dataLoading">Load Data</button>
          <span data-ng-if="status.dataLoading">Loading data...</span>
          <xsl:variable name="nextParent" select="'rootOf[saver.getGUID()]'"></xsl:variable>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="nextParent" select="$fullName"></xsl:variable>
        </xsl:otherwise>
      </xsl:choose>

      <div>
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()"/>
        </xsl:call-template>
        <xsl:if test="@saveAsSeparated">
          <xsl:attribute name="data-ng-if">status.dataLoaded</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
          <xsl:with-param name="parent" select="$nextParent"/>
          <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages"/>
        </xsl:apply-templates>
      </div>

      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

    </xsl:element>
  </xsl:template>

  <!-- ###### NAVIGATION FOR PAGES ##### -->
  <!-- construct buttons for page navigation, for each child variable -->

  <xsl:template match="variable" mode="nav">
    <xsl:param name="parent"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <button type="button" data-ng-click="vm.setSelectedPage('{ $fullName }')">
      <xsl:call-template name="getTranslatedLabel" />
    </button>
  </xsl:template>

</xsl:stylesheet>
