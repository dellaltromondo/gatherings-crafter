<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable [ @dataType= 'label' ] " >
    <!-- Labels are not missing by default -->  
  </xsl:template>    

  <xsl:template match="variable[ @valuesSetKey ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:variable name="valuesSetKey" select="@valuesSetKey"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <xsl:apply-templates select="//valuesSet[ @name=$valuesSetKey ]/value" mode="iterateForObject">
      <xsl:with-param name="dataType" select="$dataType"/>
      <xsl:with-param name="parent" select="$fullName"/>
      <xsl:with-param name="valuesSetName" select="$valuesSetKey"/>
    </xsl:apply-templates>

    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
    
  </xsl:template>

  <xsl:template match="value" mode ="iterateForObject">
    <xsl:param name="dataType"/>
    <xsl:param name="parent" />
    <xsl:param name="valuesSetName" />

    <!-- check -->
    <xsl:variable name="myValue" select="concat($valuesSetName, '__', @name)" />

    <xsl:choose>
      <xsl:when test="@label and @label != ''">
        <xsl:variable name="valueLabel" select="@label"></xsl:variable>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="valueLabel" select="@name"></xsl:variable>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:variable name="fullName" select="concat($parent, '.',  $valuesSetName, '__',  @name, '')" />
    
    <xsl:apply-templates select="//dataClass[ @name = $dataType]">
      <xsl:with-param name="parent" select="$fullName"/>
      <xsl:with-param name="labelFromRepeater" select="$valueLabel" />
      <xsl:with-param name="myValue" select="$myValue"/>
      <xsl:with-param name="containerName" select="$parent"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="variable [ @dataType= 'group' ] " >
    <xsl:param name="parent"/>

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
      <li data-ng-show="completenessController.isVariableMissing('{ $fullName }')" data-ng-click="vm.scrollToElement('{ $fullName }')">
        <xsl:apply-templates select="@visible" />
        <xsl:call-template name="getTranslatedLabel">
          <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
        </xsl:call-template>
      </li>
    </xsl:if>

    <xsl:apply-templates select="./variable" >
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>

  <xsl:template match="requirement">
    <xsl:param name="fullName" />

    <xsl:variable name="requirementName" select="concat( $fullName, '.REQUIREMENT_', @name)" />
    <li class="requirement" data-ng-show="!{ @formula }" data-ng-click="vm.scrollToElement('{ $requirementName }')">
      <xsl:value-of select="." />
    </li>
  </xsl:template>

</xsl:stylesheet>
