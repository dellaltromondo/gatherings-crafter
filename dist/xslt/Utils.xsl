<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <!-- ######## DESCRIPTION AND TOOLTIP ######## -->

  <xsl:template name="addDescription">
    <xsl:param name="fullName" />
    <xsl:param name="separatedTooltip" select="'true'" />
    <xsl:param name="element" select="'label'" />
    <xsl:param name="cssclass" select="'variableDescription'" />
    <xsl:param name="labelFromRepeater" />

    <xsl:element name="{ $element }">
      <xsl:attribute name="class" select="$cssclass" />
      <xsl:if test="$fullName and ($fullName != '') and ($element = 'label')">
        <xsl:attribute name="for">
          <xsl:call-template name="escapeFullName">
            <xsl:with-param name="fullName" select="$fullName" />
          </xsl:call-template>
        </xsl:attribute>
      </xsl:if>
      <xsl:call-template name="getTranslatedLabel">
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <xsl:if test="$separatedTooltip and $separatedTooltip = 'false'">
        <xsl:apply-templates select="@tooltip" />
      </xsl:if>
      <!-- moved it inside of element so as to keep it on same line as label -->
      <xsl:if test="$separatedTooltip and $separatedTooltip = 'true'">
        <xsl:call-template name="addSeparatedTooltip" />
      </xsl:if>
    </xsl:element>

  </xsl:template>

  <xsl:template name="addSeparatedTooltip">
    <xsl:if test="@tooltip and @tooltip != ''">
      <xsl:variable name="apos">'</xsl:variable>
      <xsl:variable name="escapedString" >
          <xsl:apply-templates select="@tooltip" mode="escape" />
      </xsl:variable>
      <!-- Translation is taken care of inside the directive -->
      <xsl:variable name="tooltipText" select="concat($apos, $escapedString, $apos)" />
      <tooltip>
        <xsl:attribute name="text">
          <xsl:value-of select="$tooltipText" disable-output-escaping="yes" />
        </xsl:attribute>
      </tooltip>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@tooltip">
    <xsl:attribute name="title">
      <xsl:call-template name="translateString">
        <xsl:with-param name="string" select="." />
      </xsl:call-template>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template name="getTranslatedLabel">
    <xsl:param name="labelFromRepeater" />
    <xsl:choose>
      <xsl:when test="@label and @label != ''">
        <xsl:variable name="text" select="@label" />
      </xsl:when>
      <xsl:when test="@labelFromKey and $labelFromRepeater and $labelFromRepeater != ''">
        <xsl:variable name="text" select="$labelFromRepeater" />
      </xsl:when>
      <xsl:when test="@name">
        <xsl:variable name="text" select="@name" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="text" select="'-- this variable has no label or name! > --'" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:call-template name="translateString">
      <xsl:with-param name="string" select="$text" />
    </xsl:call-template>
  </xsl:template>

  <!-- ######## ID ######## -->

  <xsl:template name="addID">
    <xsl:param name="fullName" />
    
    <xsl:call-template name="addGenericID">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="addWrapperID">
    <xsl:param name="fullName" />

    <xsl:call-template name="addGenericID">
      <xsl:with-param name="fullName" select="$fullName" />
      <xsl:with-param name="suffix" select="'-wrapper'" />
    </xsl:call-template>
  </xsl:template>
    
  <xsl:template name="addGenericID">
    <xsl:param name="fullName" />
    <xsl:param name="suffix" select="''" />

    <xsl:variable name="escapedFullName">
      <xsl:call-template name="escapeFullName">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:attribute name="id" select="concat($escapedFullName, $suffix)"/>
  </xsl:template>

  <!-- ######## SIMPLE ATTRIBUTES ######## -->

  <xsl:template match="@required">
    <xsl:attribute name="data-ng-required" select="."/>
  </xsl:template>
  
  <xsl:template name="nestedRequired">
    <xsl:param name="required"/>
    <xsl:if test="$required">
      <xsl:attribute name="data-ng-required" select="$required"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@visible">
    <xsl:attribute name="data-ng-if" select="." />
  </xsl:template>
  
  <xsl:template match="@precision">
    <xsl:variable name="prec" select="."/>
    <xsl:attribute name="step">
      <xsl:if test="$prec != '' and $prec != 0">
        <xsl:value-of select="'0.'"/>
      </xsl:if>
      <xsl:for-each select="1 to . -1">
        <xsl:value-of select="0"/>
      </xsl:for-each>
      <xsl:value-of select="1"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="@computedFormula">
    <xsl:attribute name="data-ng-bind" select="."/>
    <xsl:attribute name="short-circuit-formula" select="." />
  </xsl:template>

  <xsl:template match="excludes">
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:value-of select="concat($apos, ., $apos)" />
    <xsl:if test="position() != last()">
      ,
    </xsl:if>
  </xsl:template>


  <!-- ######## VARIABLES COMPACT ATTRIBUTES ######## -->

  <xsl:template name="wrapperAttributes">
    <xsl:param name="fullName" />
    <xsl:param name="separatedTooltip" select="'true'"/>
    <xsl:param name="element" select="'label'"/>
    <xsl:param name="csslabelclass" select="'variableDescription'" />
    <xsl:param name="labelFromRepeater" />
    <xsl:param name="defaultValueIfHidden" select="'null'"/>

    <xsl:apply-templates select="@visible" />
    <xsl:call-template name="addWrapperID">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:call-template>
    <xsl:call-template name="addDescription">
      <xsl:with-param name="fullName" select="$fullName" />
      <xsl:with-param name="separatedTooltip" select="$separatedTooltip" />
      <xsl:with-param name="cssclass" select="$csslabelclass" />
      <xsl:with-param name="element" select="$element" />
      <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
    </xsl:call-template>
    <xsl:call-template name="clearVariableWhenHidden">
      <xsl:with-param name="fullName" select="$fullName" />
      <xsl:with-param name="defaultValueIfHidden" select="$defaultValueIfHidden" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="inputAttributes">
    <xsl:param name="fullName" />
    <xsl:param name="isInput" select="true()"/>

    <xsl:variable name="modelAttribute">
      <xsl:if test="$isInput">data-ng-model</xsl:if>
      <xsl:if test="not($isInput)">short-circuit-model</xsl:if>
    </xsl:variable>

    <xsl:apply-templates select="@required"/>
    <xsl:apply-templates select="@computedFormula" />
    <xsl:attribute name="{ $modelAttribute }" select="$fullName" />
    <xsl:call-template name="addID">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:call-template>
    <xsl:call-template name="applyRequiredForStatus">
      <xsl:with-param name="fullName" select="$fullName" />
      <xsl:with-param name="visible" select="@visible" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="afterVariable">
    <xsl:param name="fullName" />

    <xsl:apply-templates select="./error">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
    <xsl:apply-templates select="./warning">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
    <xsl:apply-templates select="./requirement">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="applyRequiredForStatus">
    <xsl:param name="fullName" />
    <xsl:param name="visible" />
    <xsl:param name="defaultStatus" />

    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="escapedVisibility">
      <xsl:call-template name="replace">
        <xsl:with-param name="target" select='"&apos;"' />
        <xsl:with-param name="replacement" select='"\&apos;"'/>
        <xsl:with-param name="string" select="$visible" />
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="maybeStatusLevel">
      <xsl:choose>
        <xsl:when test="@requiredForStatus">
          <xsl:value-of select="concat(', ', @requiredForStatus)" />
        </xsl:when>
        <xsl:when test="$defaultStatus">
          <xsl:value-of select="concat(', ', $defaultStatus)" />
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
      <span style="display: none;" data-ng-init="completenessController.registerVariable('{$fullName}', '{$escapedVisibility}' {$maybeStatusLevel});" />
    </xsl:if>
  </xsl:template>

  <!-- ######## ERRORS and WARNINGS ########## -->

  <xsl:template match="error">
    <xsl:param name="fullName" />

    <xsl:variable name="errorName" select="concat( $fullName, '.ERROR_', @name)" />
    <div class="error" data-ng-show="{ @if }">
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$errorName" />
      </xsl:call-template>
      <xsl:call-template name="applyRequiredForStatus">
        <xsl:with-param name="fullName" select="$errorName" />
        <xsl:with-param name="visible" select="@if" />
        <xsl:with-param name="defaultStatus" select="2" />
      </xsl:call-template>
      <xsl:value-of select="." />
    </div>
  </xsl:template>

  <xsl:template match="warning">
    <xsl:param name="fullName" />

    <xsl:variable name="warningName" select="concat( $fullName, '.WARNING_', @name, '_CONFIRMED')" />
    <div data-ng-show="{ @if }">
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$warningName" />
      </xsl:call-template>
      <xsl:call-template name="applyRequiredForStatus">
        <xsl:with-param name="fullName" select="$warningName" />
        <xsl:with-param name="visible" select="@if" />
        <xsl:with-param name="defaultStatus" select="2" />
      </xsl:call-template>
      <xsl:attribute name="data-ng-class">
        <xsl:value-of select="concat('{''warning'': !', $warningName, ', ''solvedWarning'': ', $warningName, '}')" />
      </xsl:attribute>
      <xsl:value-of select="." />
      <input type="checkbox" data-ng-model="{ $warningName }" /><span>I confirm the inserted data</span>
    </div>
  </xsl:template>

  <xsl:template match="requirement">
    <xsl:param name="fullName" />

    <xsl:variable name="requirementName" select="concat( $fullName, '.REQUIREMENT_', @name)" />
    <div class="requirement" data-ng-show="!({ @formula })">
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$requirementName" />
      </xsl:call-template>
      <xsl:call-template name="applyRequiredForStatus">
        <xsl:with-param name="fullName" select="$requirementName" />
        <xsl:with-param name="visible" select="concat('!(', @formula, ')')" />
        <xsl:with-param name="defaultStatus" select="3" />
      </xsl:call-template>
      <xsl:value-of select="." />
    </div>
  </xsl:template>

  <!-- ######## STRING MANIPULATION ######## -->
  
  <xsl:template name="translateString">
    <xsl:param name="string" />
    <xsl:variable name="escapedString" >
      <xsl:apply-templates select="$string" mode="escape" />
    </xsl:variable>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="translation" select="concat('{{ ', $apos, $escapedString, $apos, ' | ', 'translate }}')" />
    <xsl:value-of select="$translation" disable-output-escaping="yes" />
  </xsl:template>

  <xsl:template match="@*" mode="escape">
    <!-- Escape the apostrophes second -->
    <xsl:call-template name="replace">
      <xsl:with-param name="target" select='"&apos;"' />
      <xsl:with-param name="replacement" select='"\&apos;"'/>
      <xsl:with-param name="string">
        <!-- Escape the backslashes first, and then pass that result directly into the next template -->
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="'\'" />
          <xsl:with-param name="replacement" select="'\\'" />
          <xsl:with-param name="string" select="." />
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="escapeFullName">
    <xsl:param name="fullName" />
    <xsl:call-template name="replace">
      <xsl:with-param name="target" select="'['" />
      <xsl:with-param name="replacement" select="':{{'" />
      <xsl:with-param name="string">
        <xsl:call-template name="replace">
          <xsl:with-param name="target" select="']'" />
          <xsl:with-param name="replacement" select="'}}'" />
          <xsl:with-param name="string" select="$fullName" />
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="replace">
    <xsl:param name="string"/>
    <xsl:param name="target" select='"&apos;"'/>
    <xsl:param name="replacement" select="'\&quot;'"/>

    <xsl:if test="$string">
      <xsl:value-of select='substring-before(concat($string,$target),$target)'/>
      <xsl:if test='contains($string, $target)'>
        <xsl:value-of select='$replacement'/>
      </xsl:if>

      <xsl:call-template name="replace">
        <xsl:with-param name="string" select='substring-after($string, $target)'/>
        <xsl:with-param name="target" select="$target"/>
        <xsl:with-param name="replacement" select="$replacement"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

<!-- -->

  <xsl:template name="clearVariableWhenHidden">
    <xsl:param name="fullName"></xsl:param>
    <xsl:param name="defaultValueIfHidden"></xsl:param>
  
    <xsl:if test="@visible">
      <xsl:variable name="apos">'</xsl:variable>
      <xsl:attribute
        name="short-circuit-formula"
        select="concat('( ', @visible , ' ) ? ', $apos, 'NOOP', $apos, ' : ', $defaultValueIfHidden)"
      />
      <xsl:attribute
        name="short-circuit-model"
        select="$fullName"
      />
    </xsl:if>
  </xsl:template>

  <xsl:template name="addIndexToRootScope">
    <xsl:param name="indexName"></xsl:param>
    <xsl:param name="indexClass"></xsl:param>

    <xsl:if test="$indexName != '' and $indexClass != ''">
      <xsl:variable name="apos">'</xsl:variable>
      <xsl:attribute name="data-ng-init" select="concat('$root.indexes === undefined ? $root.indexes = [] : false;', '$root.indexes.push({ variablePath: ', $apos, $fullName, $apos, ', indexClass: ', $apos, $indexClass, $apos, ', indexName: ', $apos, $indexName, $apos, '} )')" />
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="autocomplete-input">
    <xsl:param name="valuesSetName" />
    <xsl:param name="fullName" />

    <ic3-autocomplete page="1" limit="10" type="{ @dataType }" parent="{ $fullName }">
      <xsl:apply-templates select="@required"/>
      <xsl:apply-templates select="@computedFormula" />
      <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]">
        <xsl:with-param name="valuesSetName" select="$valuesSetName" />
      </xsl:apply-templates>
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
      <xsl:attribute name="data-ng-model" select="concat($fullName, '.filter' )"/>
    </ic3-autocomplete>
  </xsl:template>

  <xsl:template name="ic3DataClass">
    <xsl:param name="init"/>
    <xsl:param name="visibility"/>
    <xsl:param name="parent"/>
    <xsl:param name="ic3DataType"/>
    <div data-ng-init="{ $init }" data-ng-if="{ $visibility }">
      <ic3-data-class 
        xml="xml"
        processor="processor"
        parent="{ $parent }"
        loaded="loaded"
        ic3-data-class="{ $ic3DataType }"
      >
      </ic3-data-class>
    </div>
  </xsl:template>

  <xsl:template match="valuesSet[ @sourceType = 'index' ]">
    <xsl:param name="valuesSetName" />

    <xsl:attribute name="class-name" select="//valuesSet[ @name = $valuesSetName ]/class"/>
    <xsl:attribute name="index-name" select="//valuesSet[ @name = $valuesSetName ]/name"/>
  </xsl:template>

  <xsl:template match="valuesSet[ @sourceType = 'table' ]">
    <xsl:param name="valuesSetName" />

    <xsl:attribute name="table-name" select="//valuesSet[ @name = $valuesSetName ]/table" />
    <xsl:attribute name="column-name" select="//valuesSet[ @name = $valuesSetName ]/column"/>
  </xsl:template>

</xsl:stylesheet>
