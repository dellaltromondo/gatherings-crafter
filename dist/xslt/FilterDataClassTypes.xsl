<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="dataClass">
    <xsl:param name="parent" />
    <xsl:param name="valueLabel" />

    <xsl:variable name="currentDataClassName" select="@name" />

    <!-- TODO
    <button type="button" class="top-chevron" data-ng-click="{ concat('showMore = !showMore') }">
      <xsl:attribute name="data-ng-class" select="concat('{''icon-chevron-up'': showMore, ''icon-chevron-down'': !showMore}')" />
    </button>
    -->

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="'crfs'" />
      <xsl:with-param name="parentDataClass" select="$currentDataClassName" />
      <xsl:with-param name="valueLabel" select="$valueLabel" />
    </xsl:apply-templates>

    <!-- REVERSE -->
    <xsl:for-each select="//dataClass/variable[@dataType = $currentDataClassName]">
      <xsl:variable name="childDataClassName" select="(..)/@name"/>
      <xsl:apply-templates select=".">
        <xsl:with-param name="parent" select="'crfs'" />
        <xsl:with-param name="parentDataClass" select="$currentDataClassName" />
        <xsl:with-param name="currentDataClassFromParent" select="$childDataClassName" />
      </xsl:apply-templates>
    </xsl:for-each>
    <!-- END REVERSE -->

    <!-- TODO
    <button type="button" class="show-more" data-ng-click="{ concat('showMore = !showMore') }">
      <xsl:attribute name="data-ng-class" select="concat('{''icon-chevron-up'': showMore, ''icon-dots-three-horizontal'': !showMore}')" />
    </button>
    -->

    <!-- OR -->
    <div>
      <span>
        <li>
          <xsl:call-template name="ic3DataClass">
            <xsl:with-param name="init" select="'newCrfs = {};'"/>
            <xsl:with-param name="visibility" select="'orEnabled'"/>
            <xsl:with-param name="parent" select="concat('newCrfs.', $currentDataClassName)"/>
            <xsl:with-param name="ic3DataType" select="concat('''', $currentDataClassName, '''')"/>
          </xsl:call-template>
        </li>
      </span>
    </div>

  </xsl:template>

  <xsl:template match="variable">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:param name="currentDataClassFromParent" />

    <xsl:choose>
      <xsl:when test="$currentDataClassFromParent">
        <xsl:variable name="currentDataClass" select="$currentDataClassFromParent"/>
        <xsl:variable name="fullName" select="concat($parent, '.instanceOf_', $currentDataClassFromParent)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="currentDataClass" select="@dataType"/>
        <xsl:variable name="fullName" select="concat($parent, '.instanceOf_', @dataType)" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:variable name="fromParentToClassLabel" select="concat('from_', $parentDataClass, '_to_', $currentDataClass)"/>
    <xsl:variable name="translatedFromParentToClassLabel" select="concat('{{ ''', $fromParentToClassLabel, ''' | translate }}')" />
    <span>
      <xsl:attribute name="data-ng-init">
        <xsl:value-of select="concat('(', $fullName, ' === undefined)? ', $fullName, ' = []: ', $fullName, '=', $fullName )"/>
      </xsl:attribute>
      <div>
        <xsl:attribute name="data-ng-repeat">
          <xsl:value-of select="concat('instance in ', $fullName, ' track by $index')"/>
        </xsl:attribute>
        <span class="or-label" data-ng-show="$index $gt; 0">
          <!-- LABEL BELOW SHOULD BE TRANSLATED-->
          oppure
        </span>
        <div class="expanded-dataclass">
          <xsl:attribute name="data-ng-init">
            <xsl:value-of select="concat('instance.show = $root.visibilityConfig.', $parentDataClass, '.', @name)" />
          </xsl:attribute>
          <xsl:apply-templates select="@visible" />
          <!-- LABEL BELOW SHOULD BE TRANSLATED WITH PROPER TEMPLATE-->
          <label>
            <xsl:value-of select="$translatedFromParentToClassLabel"/>
          </label>
          <!-- Add / remove buttons -->
          <button type="button" class="icon-cross">
            <xsl:attribute name="data-ng-click">
              <xsl:value-of select="concat($fullName, '.splice($index, 1)')"/>
            </xsl:attribute>
          </button>

          <xsl:variable name="dataTypeForClass" select="concat('instance.dataType = ''', $currentDataClass, '''')" />
          <span>
            <xsl:attribute name="data-ng-init">
            </xsl:attribute>
            <xsl:call-template name="ic3DataClass">
              <xsl:with-param name="init" select="concat('showMore = false; ', $dataTypeForClass, ';', 'instance.onKey = ''', @name, '''')"/>
              <xsl:with-param name="visibility" select="'instance.show = true'"/>
              <xsl:with-param name="parent" select="'instance.variables'"/>
              <xsl:with-param name="ic3DataType" select="concat('''', $currentDataClass, '''')"/>
            </xsl:call-template>
          </span>
        </div>
      </div>

      <button type="button" class="dataclass-link">
        <xsl:attribute name="data-ng-click" select="concat('newCrfs = {};', $fullName, '.push(newCrfs); instance.show = true')"/>
        <xsl:value-of select="$translatedFromParentToClassLabel"/>
      </button>
    </span>
  </xsl:template>

</xsl:stylesheet>
