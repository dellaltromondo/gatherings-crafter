<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml"
>

  <xsl:template match="variable[ @dataType = 'number' ] ">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="{ @dataType }">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>

        <xsl:apply-templates select="@precision" />
        <xsl:if test="@min and @min != ''">
          <xsl:attribute name="min" select="@min" />
        </xsl:if>
        <xsl:if test="@max and @max != ''">
          <xsl:attribute name="max" select="@max" />
        </xsl:if>

      </input>
      <!-- added this so as to add suffixes -->
      <xsl:if test="@measure">
        <div>
          <span class="measure">
            <xsl:value-of select="@measure"/>
          </span>
        </div>
      </xsl:if>
      
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'text' or @dataType = 'email' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="{ @dataType }">
        <!-- readonly -->
        <xsl:if test="@readonly">
          <xsl:attribute name="readonly" select="concat('true')" />
        </xsl:if>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
        <!-- text input maxlength -->
        <xsl:if test="@max">
          <xsl:attribute name="maxlength" select="@max" />
        </xsl:if>

      </input>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'color' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="{ @dataType }">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
        <!-- TODO: this is the only line different from "text" template -->
        <xsl:attribute name="data-ng-init" select="concat($fullName, ' = ', $fullName, ' || ColorHelper.getRandomColor()')" />
        <!-- END TODO -->
      </input>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <!-- TODO: merge with others? -->
  <xsl:template match="variable[ @dataType = 'support'] ">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div style="display: none">
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="{ @dataType }">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </input>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'text' and @interface = 'textArea' ] ">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <textarea>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </textarea>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'time' ] ">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="time">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </input>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'date' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <input type="date">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
        <xsl:if test="@minDate">
          <xsl:attribute name="min" select="@minDate" />
        </xsl:if>
        <xsl:if test="@maxDate">
          <xsl:attribute name="max" select="@maxDate" />
        </xsl:if>
        <xsl:if test="@maxDate = 'today'">
          <xsl:attribute name="max" select="concat('{{ Date.now() | date:''yyyy-MM-dd'' }}')" />
        </xsl:if>
      </input>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'boolean' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />
    <!-- check -->
    <xsl:param name="containerName" />
    <xsl:param name="arrayOfExcludes" />
    <xsl:param name="myValue" />


    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <input type="checkbox">
        <xsl:if test="$myValue and $arrayOfExcludes and $containerName">
          <xsl:attribute name="data-ng-click" select="concat('vm.toggleCheckBoxesSelection(', $containerName, ', ',$apos, $myValue, $apos, ',[', $arrayOfExcludes, '])')" />
        </xsl:if>
        <!-- if it comes from a dataClass -->
        <xsl:if test="$myValue and $containerName">
          <xsl:attribute name="data-ng-change" select="concat('thisObject = ', $parent)" />
        </xsl:if>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </input>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <!-- TODO: rename to output? -->
  <xsl:template match="variable[ @dataType = 'label' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <p>
      <xsl:apply-templates select="@visible" />
      <xsl:apply-templates select="@computedFormula" />
      <xsl:call-template name="getTranslatedLabel">
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <xsl:call-template name="addID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </p>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'singlechoice' and ( not (@interface) or @interface = 'radio' ) ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="csslabelclass" select="'radioDescription'" />
        <xsl:with-param name="element" select="'span'" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <ul class="radiolist">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()" />
        </xsl:call-template>
        <!-- TODO test if variables need really to be declared -->
        <xsl:variable name="valuesSetName" select="@valuesSet" />
        <xsl:variable name="required" select="@required" />
        <xsl:variable name="escapedFullName">
          <xsl:call-template name="escapeFullName">
            <xsl:with-param name="fullName" select="$fullName" />
          </xsl:call-template>
        </xsl:variable>
        <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="radio">
          <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          <xsl:with-param name="groupName" select="$escapedFullName" />
          <xsl:with-param name="variableName" select="$fullName" />
          <xsl:with-param name="required" select="$required" />
        </xsl:apply-templates>
        <!-- TODO: refactor -->
        <xsl:apply-templates select="value" mode="radio">
          <xsl:with-param name="valuesSetName" select="@name" />
          <xsl:with-param name="groupName" select="$escapedFullName" />
          <xsl:with-param name="variableName" select="$fullName" />
          <xsl:with-param name="required" select="$required" />
        </xsl:apply-templates>
      </ul>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'singlechoice' and @interface = 'dropdown' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <select>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
        <option value=""></option>
        <xsl:variable name="valuesSetName" select="@valuesSet" />
        <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
          <xsl:with-param name="valuesSetName" select="$valuesSetName" />
        </xsl:apply-templates>
        <!-- TODO: refactor -->
        <xsl:apply-templates select="value" mode="select">
          <xsl:with-param name="valuesSetName" select="@name" />
        </xsl:apply-templates>
      </select>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'multiplechoice' and ( not (@interface) or @interface = 'checkbox' ) ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="csslabelclass" select="'checkBoxDescription'" />
        <xsl:with-param name="element" select="'span'" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
        <xsl:with-param name="defaultValueIfHidden" select="concat($apos, 'EMPTY_ARRAY', $apos)" />
      </xsl:call-template>
      <ul class="checkboxlist" data-ng-init="{ concat($fullName, ' = ', $fullName, ' || []') }">
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()" />
        </xsl:call-template>
        <xsl:variable name="required" select="@required" />
        <xsl:variable name="valuesSetName" select="@valuesSet" />
        <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="checkbox">
          <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          <xsl:with-param name="variableName" select="$fullName" />
          <xsl:with-param name="required" select="$required" />
        </xsl:apply-templates>
        <!-- TODO: refactor -->
        <xsl:apply-templates select="value" mode="checkbox">
          <xsl:with-param name="valuesSetName" select="@name" />
          <xsl:with-param name="variableName" select="$fullName" />
          <xsl:with-param name="required" select="$required" />
        </xsl:apply-templates>
      </ul>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <!-- TODO: check if it really works -->
  <xsl:template match="variable[ @dataType = 'multiplechoice' and @interface = 'autocomplete' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <xsl:variable name="name" select="@name" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <xsl:call-template name="addIndexToRootScope">
        <xsl:with-param name="indexName" select="@indexName" />
        <xsl:with-param name="indexClass" select="@indexClass" />
      </xsl:call-template>
      <xsl:call-template name="autocomplete-input">
        <xsl:with-param name="valuesSetName" select="@valuesSet"/>
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'set' ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" select="$labelFromRepeater" />
    
    <xsl:variable name="dataType" select="@itemsDataType"/>
    
    <xsl:choose>
      <xsl:when test="@saveAsSeparated">
        <xsl:variable name="fullName" select="concat('$parent.', $parent, '.', @name)" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()"/>
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:variable name="addElementClickAction" select="concat($fullName, ' = ', $fullName, ' || []; currentSet = ', $fullName, '; newGuid = vm.addElementToSet(currentSet); vm.setSelectedPage(', $apos, $fullName, '[', $apos, ' + newGuid + ', $apos, ']', $apos, ')')" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()"/>
        <xsl:variable name="addElementClickAction" select="concat($fullName, ' = ', $fullName, ' || []; currentSet = ', $fullName, '; vm.addElementToSet(currentSet)')" />
      </xsl:otherwise>  
    </xsl:choose>
    
    <xsl:choose>
      <xsl:when test="$meIsAPage">
        <xsl:variable name="container">section</xsl:variable>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="container">fieldset</xsl:variable>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:variable name="myIndexName">myIndex</xsl:variable>
    <xsl:variable name="elementName" select="concat($fullName, '[', $myIndexName ,']')" />
    
    <xsl:element name="{$container}">
      <xsl:attribute name="class">set</xsl:attribute>
      <xsl:attribute name="data-ng-controller">MyObjectController</xsl:attribute>
      <xsl:attribute name="data-ng-init" select="concat($fullName, ' = ', $fullName, ' || []; currentSet = ', $fullName)" />
      <xsl:if test="@saveAsSeparated">
        <xsl:attribute name="data-ng-controller">ModelSeparationController</xsl:attribute>
      </xsl:if>
      <xsl:attribute name="data-ng-init">
        <xsl:if test="@saveAsSeparated">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || saver.getGUID(); saver.setGUID(', $fullName, '); rootOf[saver.getGUID()] = crfs')"></xsl:value-of>
        </xsl:if>
        <xsl:if test="not(@saveAsSeparated)">
          <xsl:value-of select="concat($fullName,' = ', $fullName, ' || []; currentSet = ', $fullName)"></xsl:value-of>
        </xsl:if>
      </xsl:attribute>
      
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="csslabelclass" select="'setDescription'" />
        <xsl:with-param name="element" select="'span'" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
        <xsl:with-param name="defaultValueIfHidden" select="concat($apos, 'EMPTY_ARRAY', $apos)" />
      </xsl:call-template>

      <xsl:if test="$meIsAPage">
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:attribute name="data-ng-show" select="concat('vm.getSelectedPage() == ', $apos, $fullName, $apos)" />
      </xsl:if>

      <xsl:if test="$separateChildrenInPages">
        <section class="itemSelector">
          <button type="button"
            data-ng-click="{ $addElementClickAction }"
            data-ng-bind="'Add another { $dataType }' | translate">
          </button>
          <nav>
            <xsl:variable name="apos">'</xsl:variable>
            <xsl:variable name="itemKeyVariable" select="//dataClass[ @name = $dataType ]/variable[ @itemKey ]/@name" />
            <xsl:variable name="itemLabelVariable" select="//dataClass[ @name = $dataType ]/variable[ @itemLabel ]/@name" />
            <xsl:variable name="itemColorVariable" select="//dataClass[ @name = $dataType ]/variable[ @itemColor ]/@name" />
            <button type="button" data-ng-repeat="{ concat('childElement in ', $fullName, ' | orderBy:', $apos, $itemKeyVariable, $apos, ' track by childElement.guid') }">
              <xsl:attribute name="data-ng-click" select="concat('vm.setSelectedPage(', $apos, $fullName, '[', $apos, ' + childElement.guid + ', $apos, ']', $apos, ')')" />
              <xsl:attribute name="data-ng-style" select="concat('{ ', $apos, 'background-color', $apos, ' : childElement.', $itemColorVariable, ', ', $apos, 'color', $apos, ' : ColorHelper.adjustBrightness(childElement.', $itemColorVariable, ', -100) }')" />
              <xsl:value-of select="concat('{{', 'childElement.', $itemLabelVariable, ' }}')" />
            </button>
            <xsl:attribute name="data-ng-init" select="concat('vm.setSelectedPage(', $apos, $fullName, '[', $apos, ' + vm.maxInArrayByProperty(' , $fullName, ', ', $apos, $itemKeyVariable, $apos, ').guid + ', $apos, ']', $apos, ')' )" />
          </nav>
        </section>

        <span class="pageDescription"><xsl:value-of select="concat('Selected ', $dataType)" /></span>
      </xsl:if>

      <span
        data-ng-show="{ concat('!',$fullName, ' || ', $fullName, '.length == 0') }"
        data-ng-bind="'--- No { $dataType }s yet ---' | translate"
      ></span>

      <xsl:choose>
        <xsl:when test="@saveAsSeparated">
          <button type="button" data-ng-click="saver.loadJsonData()" data-ng-if="!status.dataLoaded &amp;&amp; !status.dataLoading">Load Data</button>
          <span data-ng-if="status.dataLoading">Loading data...</span>
          <xsl:variable name="nextParent" select="'rootOf[saver.getGUID()]'"></xsl:variable>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="nextParent" select="$fullName"></xsl:variable>
        </xsl:otherwise>
      </xsl:choose>

      <ul>
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()" />
        </xsl:call-template>

        <xsl:if test="@saveAsSeparated">
          <xsl:attribute name="data-ng-if">status.dataLoaded</xsl:attribute>
        </xsl:if>
        
        <li data-ng-controller="MyObjectController"
          data-ng-repeat="{ concat('childElement in ', $fullName, ' track by childElement.guid') }"
          data-ng-keypress="vm.onKeyPress($event, currentSet)"
        >
          <xsl:attribute name="data-ng-init" select="concat($myIndexName, ' = $index; ', $elementName, ' = ', $elementName, ' || {}; thisObject = ', $elementName)" />
          
          <xsl:if test="$separateChildrenInPages">
            <xsl:variable name="quot">'</xsl:variable>
            <xsl:attribute name="data-ng-show" select="concat('vm.getSelectedPage() == ', $quot, $fullName, '[', $quot, ' + childElement.guid + ', $quot, ']', $quot)" />
          </xsl:if>
          <!-- KEEP THE FOLLOWING SPAN AS IS, it is necessary to calculate index of element -->
          <span data-ng-show="false">
            <xsl:value-of select="concat('{{', $myIndexName, ' = $index }}')" />
          </span>

          <xsl:if test="count(//dataClass[ @name = $dataType ]/variable) &gt; 10 or $separateChildrenInPages">
            <button type="button"
              tabindex="-1"
              data-ng-click="vm.removeElementFromSet(currentSet, thisObject.guid)"
              data-ng-bind="'Remove this { $dataType }' | translate"
            ></button>
          </xsl:if>

          <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
            <xsl:with-param name="parent" select="$elementName"/>
          </xsl:apply-templates>

          <xsl:variable name="removeButtonText">
            <xsl:choose>
              <xsl:when test="count(//dataClass[ @name = $dataType ]/variable) &gt; 10 or $separateChildrenInPages">
                <xsl:value-of select="concat('Remove this ', $dataType)" />
              </xsl:when>
              <xsl:otherwise>X</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <button type="button"
            tabindex="-1"
            data-ng-click="vm.removeElementFromSet(currentSet, thisObject.guid)"
          data-ng-bind="'{ $removeButtonText }' | translate"
          ></button>
        </li>
      </ul>

      <xsl:if test="not($separateChildrenInPages)">
        <button type="button"
          data-ng-click="{ $addElementClickAction }"
          data-ng-bind="'Add another { $dataType }' | translate">
        </button>
      </xsl:if>

      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

    </xsl:element>
  </xsl:template>

  <xsl:template match="variable[ @valuesSetKey ]">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:variable name="valuesSetKey" select="@valuesSetKey"/>
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <fieldset>
      <xsl:attribute name="data-ng-init" select="concat($fullName, ' = ', $fullName, ' || {}')" />
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="element" select="'legend'" />
        <xsl:with-param name="csslabelclass" select="'groupDescription'" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
        <xsl:with-param name="defaultValueIfHidden" select="concat($apos, 'EMPTY_OBJECT', $apos)" />
      </xsl:call-template>
      <ul>
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:call-template name="inputAttributes">
          <xsl:with-param name="fullName" select="$fullName" />
          <xsl:with-param name="isInput" select="false()" />
        </xsl:call-template>
        <xsl:apply-templates select="//valuesSet[ @name=$valuesSetKey ]/value" mode="iterateForObject">
          <xsl:with-param name="dataType" select="$dataType"/>
          <xsl:with-param name="parent" select="$fullName"/>
          <xsl:with-param name="valuesSetName" select="$valuesSetKey"/>
        </xsl:apply-templates>
      </ul>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </fieldset>
  </xsl:template>

  <xsl:template match="value" mode="select">
    <xsl:param name="valuesSetName" />
    <option value="{ concat($valuesSetName, '.', @name) }">
      <xsl:call-template name="getTranslatedLabel" />
    </option>
  </xsl:template>

  <xsl:template match="value" mode="radio">
    <xsl:param name="valuesSetName" />
    <xsl:param name="groupName" />
    <xsl:param name="variableName" />
    <xsl:param name="required" />
    <xsl:variable name="myValue" select="concat($valuesSetName, '.', @name)" />
    <xsl:variable name="fullName" select="concat($variableName, '-', $myValue)" />
    <li>
      <xsl:call-template name="addWrapperID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

      <input type="radio"
        data-ng-model="{ $variableName }"
        value="{ $myValue }"
        name="{ $groupName }"
      >
        <xsl:call-template name="nestedRequired" >
          <xsl:with-param name="required" select="$required"/>
        </xsl:call-template>
        <xsl:call-template name="addID">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </input>

      <xsl:call-template name="addDescription">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="separatedTooltip" select="'false'" />
        <xsl:with-param name="cssclass" select="'valueDescription'" />
      </xsl:call-template>
    </li>
  </xsl:template>

  <xsl:template match="value" mode="checkbox">
    <xsl:param name="valuesSetName" />
    <xsl:param name="variableName" />
    <xsl:param name="required" />
    <xsl:variable name="myValue" select="concat($valuesSetName, '.', @name)" />
    <xsl:variable name="fullName" select="concat($variableName, '-', $myValue)" />

    <xsl:variable name="arrayOfExcludes">
      <xsl:apply-templates select="./excludes" />
    </xsl:variable>

    <li>
      <xsl:apply-templates select="@visible" />
      <xsl:call-template name="addWrapperID">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
      <xsl:variable name="apos">'</xsl:variable>
      <xsl:choose>
        <xsl:when test="@visible != ''">
          <xsl:variable name="visibility" select="@visible" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="visibility" select="'true'" />
        </xsl:otherwise>
      </xsl:choose>
      <input type="checkbox"
        value="{ $myValue }"
        data-ng-click="{ concat($variableName, ' = ', $variableName, ' || []; vm.toggleCheckBoxesSelection(', $variableName, ', ',$apos, $myValue, $apos, ',[', $arrayOfExcludes, '])') }"
        data-ng-checked="{ concat($variableName, '.indexOf(',$apos, $myValue, $apos, ') > -1 &amp;&amp; (', $visibility, ')') }"
      >
        <xsl:call-template name="nestedRequired" >
          <xsl:with-param name="required" select="$required"/>
        </xsl:call-template >
        <xsl:call-template name="addID">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </input>
      <xsl:call-template name="addDescription">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="separatedTooltip" select="'false'" />
        <xsl:with-param name="cssclass" select="'valueDescription'" />
      </xsl:call-template>
    </li>
  </xsl:template>

  <xsl:template match="value" mode ="iterateForObject">
    <xsl:param name="dataType"/>
    <xsl:param name="parent" />
    <xsl:param name="valuesSetName" />

    <!-- check -->
    <xsl:variable name="myValue" select="concat($valuesSetName, '__', @name)" />

    <xsl:choose>
      <xsl:when test="@label and @label != ''">
        <xsl:variable name="valueLabel" select="@label"></xsl:variable>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="valueLabel" select="@name"></xsl:variable>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:variable name="arrayOfExcludes">
      <xsl:apply-templates select="./excludes" />
    </xsl:variable>

    <xsl:variable name="fullName" select="concat($parent, '.',  $valuesSetName, '__',  @name, '')" />
    <!-- TODO: merge with instance and / or set -->
    <li data-ng-controller="MyObjectController">
      <xsl:attribute name="data-ng-init" select="concat($fullName, ' = ', $fullName, ' || {}; thisObject = ', $fullName)" />
      <xsl:apply-templates select="//dataClass[ @name = $dataType]">
        <xsl:with-param name="parent" select="$fullName"/>
        <xsl:with-param name="labelFromRepeater" select="$valueLabel" />
        <xsl:with-param name="myValue" select="$myValue"/>
        <xsl:with-param name="containerName" select="$parent"/>
        <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
      </xsl:apply-templates>
    </li>
  </xsl:template>

</xsl:stylesheet>
