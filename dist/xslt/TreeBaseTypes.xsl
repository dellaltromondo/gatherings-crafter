<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable [ @dataType= 'group' ] " >
    <xsl:param name="parent"/>
    <xsl:param name="meIsAPage"/>
    <!-- TODO: unify with dataclass instance in DataClassTypes.xsl -->
    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()"/>
      </xsl:otherwise>  
    </xsl:choose>
    
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:if test="$meIsAPage">
      <!--
        <xsl:attribute name="data-ng-click">
            <xsl:value-of select="concat($fullName, ' = ', $fullName, ' || {}; thisObject = ', $fullName)" />
        </xsl:attribute>    
        -->
        <li data-ng-click="vm.setSelectedPage('{$fullName}')">
          <!-- <xsl:call-template name="addVisibilityFormula" /> -->
          <xsl:if test="@label and @label != ''">
              <xsl:call-template name="translateString">
              <xsl:with-param name="string" select="@label" />
              </xsl:call-template>
          </xsl:if>
          <xsl:if test="not (@label)">
              <xsl:call-template name="translateString">
              <xsl:with-param name="string" select="@name" />
              </xsl:call-template>
          </xsl:if>
      </li>
    </xsl:if>

    <xsl:apply-templates select="./variable" >
      <xsl:with-param name="parent" select="$fullName" />
      <xsl:with-param name="meIsAPage" select="$separateChildrenInPages"/>
    </xsl:apply-templates>
  </xsl:template>

</xsl:stylesheet>
