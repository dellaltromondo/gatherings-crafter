<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable[ @dataType = 'regionPicker']">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <span id="{ @name }" region-picker="region-picker" data-ng-model="{ $fullName }"></span>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'provincePicker']">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <span id="{ @name }" province-picker="province-picker" data-ng-model="{ $fullName }">
        <xsl:attribute name="filter-source">
          <xsl:value-of select="concat($parent, '.', @filterSource)" />
        </xsl:attribute>
      </span>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'townPicker']">
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />
    <div>
      <xsl:call-template name="wrapperAttributes">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      </xsl:call-template>
      <span id="{ @name }" town-picker="town-picker" data-ng-model="{ $fullName }">
        <xsl:attribute name="filter-source">
          <xsl:value-of select="concat($parent, '.', @filterSource)" />
        </xsl:attribute>
      </span>
      <xsl:call-template name="afterVariable">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

</xsl:stylesheet>
