﻿<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable[ @dataType = 'group' ] ">
    <xsl:param name="parent" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

  </xsl:template>

  <xsl:template match="variable[ @dataType = 'text' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>

      <xsl:call-template name="basicFilterOperators">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>
      
      <span data-ng-show="{ concat($fullName, '.showFilter') }">
        <input data-ng-model="{ concat($fullName, '.filter') }" />
      </span>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'singlechoice' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>

      <xsl:call-template name="basicFilterOperators">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>
      
      <span data-ng-show="{ concat($fullName, '.showFilter') }">
        <!-- TODO: merge with CollectionbaseTypes -->
        <select data-ng-model="{ concat($fullName, '.filter') }">
          <option value=""></option>
          <xsl:variable name="valuesSetName" select="@valuesSet" />
          <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
          <xsl:apply-templates select="value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
        </select>
      </span>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'multiplechoice' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>

      <xsl:call-template name="setFilterOperators">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>
      
      <span data-ng-show="{ concat($fullName, '.showFilter') }">
        <!-- TODO: merge with CollectionbaseTypes -->
        <select data-ng-model="{ concat($fullName, '.filter') }">
          <option value=""></option>
          <xsl:variable name="valuesSetName" select="@valuesSet" />
          <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
          <xsl:apply-templates select="value" mode="select">
            <xsl:with-param name="valuesSetName" select="$valuesSetName" />
          </xsl:apply-templates>
        </select>
      </span>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'date' or @dataType = 'time' or @dataType = 'number' ] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>

      <xsl:call-template name="filterOperatorsForSortable">
        <xsl:with-param name="fullName" select="$fullName"/>
      </xsl:call-template>

      <div data-ng-show="{ concat($fullName, '.showFilter') }">
        <input type="{ @dataType }" data-ng-model="{ concat($fullName, '.filter.lowerBound') }">
          <xsl:apply-templates select="@precision" />
          <xsl:if test="@min">
            <xsl:attribute name="min" select="@min" />
          </xsl:if>
          <xsl:if test="@max">
            <xsl:attribute name="max" select="@max" />
          </xsl:if>
        </input>
      </div>
      <div data-ng-show="{ concat($fullName, '.showFilter &amp;&amp; ', $fullName, '.operator == ''between'' ') }">
        e <input type="{ @dataType }" data-ng-model="{ concat($fullName, '.filter.upperBound') }">
          <xsl:apply-templates select="@precision" />
          <xsl:if test="@min">
            <xsl:attribute name="min" select="@min" />
          </xsl:if>
          <xsl:if test="@max">
            <xsl:attribute name="max" select="@max" />
          </xsl:if>
        </input>
      </div>

      <div data-ng-show="{ concat($fullName, '.filter.upperBound &lt;= ', $fullName, '.filter.lowerBound') }">
        If lower bound is higher than upper bound, query may produce no result.
      </div>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'boolean'] ">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <div>
      <xsl:call-template name="commonToAll">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>

      <div data-ng-show="{ concat($fullName, '.showFilter') }">
        <label><input type="radio" data-ng-value="true" data-ng-model="{ concat($fullName, '.filter') }" /> si</label>
        <label><input type="radio" data-ng-value="false" data-ng-model="{ concat($fullName, '.filter') }" /> no</label>
      </div>

      <xsl:call-template name="addRemoveFilterButton">
        <xsl:with-param name="fullName" select="$fullName" />
      </xsl:call-template>
    </div>
  </xsl:template>
  
  <!-- FILTER UTILS -->

  <xsl:template name="commonToAll">
    <xsl:param name="fullName" />

    <xsl:attribute name="data-ng-init" select="concat('(', $fullName, '.export === undefined) ? ', $fullName, '.export = $root.visibilityConfig.', $parentDataClass, '.', @name, ' : ', $fullName, '.export = ', $fullName, '.export')" />
    
    <!-- TODO
    <xsl:attribute name="data-ng-show">
      <xsl:value-of select="concat($fullName, '.showFilter || ', $fullName, '.export || showMore')"/>
    </xsl:attribute>
    -->

    <xsl:call-template name="addExportButton">
      <xsl:with-param name="fullName" select="$fullName"/>
    </xsl:call-template>
    <xsl:call-template name="addDescription">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:call-template>
    <xsl:call-template name="addFilterButton">
      <xsl:with-param name="fullName" select="$fullName"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="addExportButton">
    <xsl:param name="fullName" />
    <button type="button" class="export-button" data-ng-click="{ concat($fullName, '.export = !', $fullName, '.export') }">
      <xsl:attribute name="data-ng-class" select="concat('{''icon-eye-plus'': !', $fullName, '.export, ''unselected'': !', $fullName, '.export, ''icon-eye-minus'': ', $fullName, '.export}')" />
    </button>
  </xsl:template>

  <xsl:template name="addFilterButton">
    <xsl:param name="fullName" />
    <button type="button" class="icon-filter" data-ng-click="{ concat('showMore = false; ', $fullName, '.showFilter = true') }">
      <xsl:attribute name="data-ng-class" select="concat('{''unselected'': !', $fullName, '.showFilter}')" />
    </button>
  </xsl:template>

  <xsl:template name="addRemoveFilterButton">
    <xsl:param name="fullName" />
    <button type="button" class="icon-cross"
      data-ng-show="{ concat($fullName, '.showFilter') }"
      data-ng-click="{ concat($fullName, '.showFilter = undefined; ', $fullName, '.filter = undefined') }"
    ></button>
  </xsl:template>

  <!-- TODO: merge with CollectionbaseTypes -->
  <xsl:template match="value" mode="select">
    <xsl:param name="valuesSetName" />
    <option value="{ concat($valuesSetName, '.', @name) }">
      <xsl:call-template name="getTranslatedLabel" />
    </option>
  </xsl:template>

  <!-- OPERATORS -->

  <xsl:template name="filterOperatorsForSortable">
    <xsl:param name="fullName" />
    <select
      data-ng-init="{ concat($fullName, '.operator = ''=''') }"
      data-ng-model="{ concat($fullName, '.operator' ) }"
      data-ng-if="{ concat($fullName, '.showFilter') }"
    >
      <option value="=" selected="selected">uguale a</option>
      <option value="&gt;">maggiore di</option>
      <option value="&lt;">minore di</option>
      <option value="between">compreso tra</option>
      <option value="!=">diverso da</option>
    </select>
  </xsl:template>

  <xsl:template name="basicFilterOperators">
    <xsl:param name="fullName" />
    <select
      data-ng-init="{ concat($fullName, '.operator = ''=''') }"
      data-ng-model="{ concat($fullName, '.operator' ) }"
      data-ng-if="{ concat($fullName, '.showFilter') }"
    >
      <option value="=" selected="selected">uguale a</option>
      <option value="!=">diverso da</option>
    </select>
  </xsl:template>

  <xsl:template name="setFilterOperators">
    <xsl:param name="fullName" />
    <select
      data-ng-init="{ concat($fullName, '.operator = ''contains''') }"
      data-ng-model="{ concat($fullName, '.operator' ) }"
      data-ng-if="{ concat($fullName, '.showFilter') }"
    >
      <option value="contains" selected="selected">contiene</option>
      <option value="notContains">non contiene</option>
    </select>
  </xsl:template>

</xsl:stylesheet>
