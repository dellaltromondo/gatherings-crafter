<xsl:stylesheet
      version="2.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns="http://www.w3.org/1999/xhtml">

  <!-- ######## DATACLASS ######## -->
  <!-- group of variables that can be inserted in different point of the data collection -->
  
  <xsl:template match="dataClass">
    <xsl:param name="parent" />
    <xsl:param name="valueLabel" />
    <xsl:param name="separateChildrenInPages" />

    <xsl:choose>
      <xsl:when test="not($parent)">
        <xsl:variable name="selectedParent" select="'crfs'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="selectedParent" select="$parent"/>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$selectedParent" />
      <xsl:with-param name="valueLabel" select="$valueLabel" />
      <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
    </xsl:apply-templates>
  </xsl:template>
  
  <!-- ######## INSTANCE OF DATACLASS ######## -->
  <!-- variable with datatype not in BaseTypes -->
  <!-- searches for a dataclass that has the same name of the datatype -->

  <xsl:template match="variable">
    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />

    <xsl:variable name="dataType" select="@dataType"/>
    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()"/>
      </xsl:otherwise>  
    </xsl:choose>

    <xsl:variable name="fullName" select="concat($parent, '.', @name)" />

    <xsl:if test="$meIsAPage">
    <!--
    <xsl:attribute name="data-ng-click">
        <xsl:value-of select="concat($fullName, ' = ', $fullName, ' || {}; thisObject = ', $fullName)" />
    </xsl:attribute>    
    -->
    <li data-ng-click="vm.setSelectedPage('{$fullName}')">
        <!-- <xsl:call-template name="addVisibilityFormula" /> -->
        <xsl:if test="@label and @label != ''">
            <xsl:call-template name="translateString">
            <xsl:with-param name="string" select="@label" />
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="not (@label)">
            <xsl:call-template name="translateString">
            <xsl:with-param name="string" select="@name" />
            </xsl:call-template>
        </xsl:if>
    </li>
    </xsl:if>

    <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
        <xsl:with-param name="parent" select="$fullName"/>
        <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages"/>
    </xsl:apply-templates>
  </xsl:template>
</xsl:stylesheet>
