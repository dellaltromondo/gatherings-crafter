# Gatherings Crafter

Unleash the dormant power of data collections

## What's useful for

Gatherings Crafter is a tool that creates data collections based on an XML definition.
The XML describes which variables are part of the collection, which datatype they have and their relationships.
It supports also formulas (written in javascript) to calculate values that depends on other variables.

See the following definition example:
```xml
<root>
    <dataClass name="EntrySchemas">
        <variable name="schemas" dataType="set" elementsDataType="EntrySchema" />
    </dataClass>

    <dataClass name="EntrySchema">
        <variable name="name" label="Name" dataType="text" />
        <variable name="icon" label="Icon" dataType="text" />
        <variable name="description" label="Description" dataType="text" />
        <variable name="type" label="In or Out?" dataType="singlechoice" valuesSet="inOrOut" />
    </dataClass>
    
    <valuesSets>
        <valuesSet name="inOrOut">
            <value name="IN" label="In"></value>
            <value name="OUT" label="Out"></value>
        </valuesSet>
    </valuesSets>
</root>
```

It can be also useful for creating masks to edit existing JSON data with a well defined structure.


## Status of the project

This project has recently been extracted from legacy code, thus it is not really a ready-to-use package.
All the functionalities are there, but setting up a new solution is not a straightforward configuration.

The immediate goal will be remove all the legacy code that already is in the codebase, by providing more abstraction.