/// <reference path="tsUnit.ts" />
/// <reference path="../controllers/StructuralDataController.ts" />
/// <reference path="../utils/GlobalConfig.ts" />

module StructuralDataControllerTests {

    import gvt = it.marionegri.givitiweb;

    export class StructuralDataControllerTests extends tsUnit.TestClass {

        private myController: gvt.StructuralDataController;
        private crfWithTwoElementsSet =
        {
            dataValidity: { startingDate: moment("2015-01-01").format(gvt.GlobalConfig.jsonDateFormat) },
            testSet: [{ name: "Primo", guid: "aaa" }, { name: "Secondo", guid: "bbb" }]
        };
        private crfWithThreeElementsSet = {
            dataValidity: { startingDate: moment("2015-01-15").format(gvt.GlobalConfig.jsonDateFormat) },
            testSet: [{ name: "Primo", guid: "aaa" }, { name: "Secondo", guid: "bbb" }, { name: "Terzo", guid: "ccc" }]
        };

        setUp() {
            window["periodMaxMonthValidity"] = "6";
            this.initializeControllerWithData({});
        }

        tearDown() {
            this.myController = null;
        }

        private initializeControllerWithData(data: any) {
            
            //LOL
            
            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } } });
            var mockScope: gvt.IStructuralDataModel = <gvt.IStructuralDataModel>{};
            this.myController = new gvt.StructuralDataController(null, null, mockScope, null, null, null, null, true, null, null, null);
    }

    testFillListOfData() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.isFalse(angular.equals(this.myController.$scope.crfs, {}));
    }

    testFilledCrfIsLast() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
    }

    testFilledCrfIsLastWhenReverseOrder() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
    }

    testFillListOfDataDoesNotReferenceOriginals() {
        var localCopy = angular.copy(this.crfWithTwoElementsSet);
        this.myController.fillCentreStructuralData({ centreStructuralData: [localCopy] });
        localCopy.testSet[0].name = "PrimoCambiato";
        this.areIdentical(this.myController.$scope.crfs.testSet[0].name, "Primo");
    }

    testClearDataDoesNotChangeOriginalData() {
        var firstCrf = angular.copy(this.crfWithTwoElementsSet);
        var secondCrf = angular.copy(this.crfWithThreeElementsSet);
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        var newCrfs = angular.copy(this.crfWithTwoElementsSet);
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.myController.clearData(this.myController.$scope.crfs);
        this.isTrue(angular.equals(firstCrf, this.crfWithTwoElementsSet));
        this.isTrue(angular.equals(secondCrf, this.crfWithThreeElementsSet));
    }

    testUpdateIndexDoesNotChangeOriginalData() {
        var firstCrf = angular.copy(this.crfWithTwoElementsSet);
        var secondCrf = angular.copy(this.crfWithThreeElementsSet);
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.isTrue(angular.equals(firstCrf, this.crfWithTwoElementsSet));
        this.isTrue(angular.equals(secondCrf, this.crfWithThreeElementsSet));
    }

    testUpdateIndex() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 2);
    }

    testUpdateIndexWhenReverseOrder() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
    }

    testUpdateIndexDoesNotChangeSetReference() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        var oldCrfsSet = this.myController.$scope.crfs.testSet;
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        var newCrfsSet = this.myController.$scope.crfs.testSet;
        this.areIdentical(oldCrfsSet, newCrfsSet);
    }

    testUpdateIndexDoesNotCollapseSetReference() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.areNotIdentical(this.myController.$scope.crfs.testSet, this.crfWithTwoElementsSet);
    }

    testSetLatestIndexSetsPeriodIndex() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.areIdentical(this.myController.$scope.status.periodIndex, 1);
    }

    testMultipleUpdateIndex() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithTwoElementsSet, this.crfWithThreeElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.myController.$scope.status.periodIndex = 1;
        this.myController.fillCRFS();
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 3);
    }

    testMultipleUpdateIndexWhenReverseOrder() {
        this.myController.fillCentreStructuralData({ centreStructuralData: [this.crfWithThreeElementsSet, this.crfWithTwoElementsSet] });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.myController.$scope.status.periodIndex = 1;
        this.myController.fillCRFS();
        this.areIdentical(this.myController.$scope.crfs.testSet.length, 2);
    }

    testMultipleUpdateIndexBugForRoomsRelationships() {
        var json = [
            {
                "dataValidity": { "startingDate": "2015-01-15" },
                "roomsData": {
                    "roomsSet": [],
                    "roomsRelationships": {
                        "b2057ce3-7dbe-91a4-ba13-c1799d9fa0ac": [
                            "e02e843e-b6f1-286d-5c23-845181dcec35"
                        ]
                    }
                }
            },
            {
                "dataValidity": { "startingDate": "2014-11-01" },
                "roomsData": {
                    "roomsSet": [],
                    "roomsRelationships": {}
                }
            }
        ];

        this.myController.fillCentreStructuralData({ centreStructuralData: json });
        this.myController.$scope.status.periodIndex = 0;
        this.myController.fillCRFS();
        this.myController.$scope.status.periodIndex = 1;
        this.myController.fillCRFS();

        this.isFalse(angular.equals(this.myController.$scope.crfs.roomsData.roomsRelationships["b2057ce3-7dbe-91a4-ba13-c1799d9fa0ac"], {}));
    }
}
}
