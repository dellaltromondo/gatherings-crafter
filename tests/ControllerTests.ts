/// <reference path="tsUnit.ts" />
/// <reference path="../controllers/Controller.ts" />


module ControllerTests {

    import gvt = it.marionegri.givitiweb;

    export class ClearDataTests extends tsUnit.TestClass {

        private myController: gvt.Controller;

        setUp() {
            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } }; });
            var mockScope: gvt.IGenericModel = <gvt.IGenericModel> {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null , null, null);
        }

        tearDown() {
            this.myController = null;
        }

        testClearedString() {
            var crfData = { "campo2": "ciao" };
            var crfDataWithEmptyString = { "campo2": "" };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo2, "");
        }

        testClearedStringInObject() {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "ciao" } };
            var crfDataWithEmptyString = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo3.campo4, "");
        }

        testClearedStringInArray() {
            var crfData = { 'campo1': ["ciao", "ciaone"], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.clearData(crfData);
            this.areIdentical(crfData.campo1[0], "");
        }

        /*testEmptyObjectPersistance() {
             var crfData = { 'campo1': ["ciaone"], "campo2": "", "campo3": {"campo4":{}} };
             var crfDataCopy = { 'campo1': [], "campo2": "", "campo3": {"campo4":""}};
             this.myController.clearData(crfData);
             this.areIdentical(angular.equals(crfData, crfDataCopy), true);
        }
        */
    }

    export class FillDataTests extends tsUnit.TestClass {

        private myController: gvt.Controller;

        setUp() {

            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } }; });
            var mockScope: gvt.IGenericModel = <gvt.IGenericModel> {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null , null, null);
        }

        tearDown() {
            this.myController = null;
        }

        testFilledString() {
            var crfData = { "campo1": "" };
            var crfDataWithString = { "campo1": "ciao" };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData,crfDataWithString), "Unable to fill string correctly");
        }

        testFilledStringInObject() {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            var crfDataWithString = { 'campo1': [], "campo2": "", "campo3": { "campo4": "ciao" } };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData,crfDataWithString), "Unable to fill Object correctly");
        }

        testFilledStringInArray() {
            var crfData = { 'campo1': [], "campo2": "", "campo3": { "campo4": "" } };
            var crfDataWithString = { 'campo1': ["ciao", "riciao"], "campo2": "", "campo3": { "campo4": "" } };
            this.myController.fillData(crfData, crfDataWithString);
            this.isTrue(angular.equals(crfData,crfDataWithString), "Unable to fill vector correctly");
        }

        testFillDataDoesNotReferenceOriginals() {
            var crfData = { 'campo1': [] };
            var newData = { 'campo1': [ "ciao", "riciao" ] };
            this.myController.fillData(crfData, newData);
            newData.campo1[0] = "ciaoCambiato";
            this.areIdentical(crfData.campo1[0], "ciao");
        }

        testFillDataDoesNotReferenceOriginalsInDepth() {
            var crfData = { 'campo1': { 'campo2' : [] } };
            var newData = { 'campo1': { 'campo2' : [ "ciao", "riciao" ] } };
            this.myController.fillData(crfData, newData);
            newData.campo1.campo2[0] = "ciaoCambiato";
            this.areIdentical(crfData.campo1.campo2[0], "ciao");
        }
    }

    export class RemoveElementFromSetTests extends tsUnit.TestClass {

        private myController: gvt.Controller;

        setUp() {

            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } }; });
            var mockScope: gvt.IGenericModel = <gvt.IGenericModel> {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null , null, null);
        }

        tearDown() {
            this.myController = null;
        }

        testElementIsRemoved() {
            var guid = gvt.GuidUtils.createGUID();
            var currentSet = [{ guid: guid }];

            //this.myController.removeElementFromSet(currentSet, guid);
            this.areIdentical(currentSet.length, 0);
        }

        testElementIsNotRemoved() {
            var guid1 = "a";
            var guid2 = "b";
            var guid3 = "c";
            var currentSet = [{ guid: guid1 }, { guid: guid3 }];
            try {
                //this.myController.removeElementFromSet(currentSet, guid2);
            }
            catch (e) {
            }
            this.areIdentical(currentSet.length, 2);
        }

        testFailedWhenElementNotFound() {
            var guid1 = "a";
            var guid2 = "b";
            var currentSet = [{ guid: guid1 }];

            this.throws(() => {
                //this.myController.removeElementFromSet(currentSet, guid2);
            });
        }
    }

    export class RemoveEmptyElementFromSetTests extends tsUnit.TestClass {

        private myController: gvt.Controller;

        setUp() {
            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } }; });
            var mockScope: gvt.IGenericModel = <gvt.IGenericModel> {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null , null, null);
        }

        testNotFailWhenNull () {
            try {
                this.myController.removeEmptyObjectsFromSets(null);
            } catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are passed");
            }
        }

        testNotFailWhenObjectContainsNull () {
            try {
                this.myController.removeEmptyObjectsFromSets({a: null});
            } catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are passed");
            }
        }

        testNotFailWhenNullInArray () {
            try {
                this.myController.removeEmptyObjectsFromSets( [ null ] );
            } catch (e) {
                this.fail("'removeEmptyObjectsFromSets' must not fail if null values are in the array");
            }
        }

        testRemoveNullsFromArray () {
            var array = [ null ];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        }

        testRemove () {
            var array = [ { a : "a" }, { b : "b" }, {} ];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, [ { a : "a" }, { b : "b" } ]));
        }

        testRemoveWhenLonelyElement () {
            var array = [ {} ];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        }

        testRemoveMultipleElements () {
            var array = [ {}, {} ];
            this.myController.removeEmptyObjectsFromSets(array);
            this.isTrue(angular.equals(array, []));
        }

        testRemoveWhenObjectChild () {
            var data = { array: [ { a : "a" }, { b : "b" }, {} ] };
            this.myController.removeEmptyObjectsFromSets(data);
            this.isTrue(angular.equals(data.array, [ { a : "a" }, { b : "b" } ]));
        }
    }

    export class AddElementToSetTests extends tsUnit.TestClass {

        private myController: gvt.Controller;

        setUp() {
            var mockJSONService = new tsUnit.Fake<gvt.IJSONService<any>>(new gvt.JSONService(null, null, null, null, null, null));
            mockJSONService.addFunction("$get", () => { return { then: () => { } }; });
            var mockScope: gvt.IGenericModel = <gvt.IGenericModel> {};
            this.myController = new gvt.Controller(null, null, mockScope, null, mockJSONService.create(), null , null, null);
        }

        tearDown() {
            this.myController = null;
        }

        testNewElementAddedWhenEmpty() {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 1);
        }

        testNewElementAddedWhenNotEmpty() {
            var mySet = [{ guid: "fake" }, { guid: "secondFake" }];
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 3);
        }

        testMultipleAdding() {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            this.areIdentical(mySet.length, 3);
        }

        testFailWhenUndefinedList() {
            var mySet;
            this.throws(() => {
                //this.myController.addElementToSet(mySet);
            });
        }

        testFailWhenString() {
            var mySet = "";
            this.throws(() => {
                //this.myController.addElementToSet(mySet);
            });
        }

        testGUIDIsNotEmpty() {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            this.areNotIdentical(mySet[0].guid, "");
            this.areNotIdentical(mySet[0].guid, gvt.GuidUtils.getEmptyGUID());
        }

        testGUIDIsDifferentWhenAddingTwo() {
            var mySet = [];
            //this.myController.addElementToSet(mySet);
            //this.myController.addElementToSet(mySet);
            this.areNotIdentical(mySet[0].guid, mySet[1].guid);
        }
    }
}
