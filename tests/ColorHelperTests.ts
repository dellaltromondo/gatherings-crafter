/// <reference path='../definitions/angular/angular.d.ts' />
/// <reference path="tsUnit.ts" />
/// <reference path="../utils/ColorHelper.ts" />


module ColorHelperTests {

    import gvt = it.marionegri.givitiweb;

    export class GetColorTests extends tsUnit.TestClass {

        private myColorHelper: gvt.ColorFromPercentage;
        private static defaultColorMap: gvt.ColorAndPercentage[] = [
            // ORDER is important!
            { percentage: 0.0, color: { r: 0x00, g: 0xff, b: 0 } },
            { percentage: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
            { percentage: 1.0, color: { r: 0xff, g: 0x00, b: 0 } }
        ];
        private colorMap: gvt.ColorAndPercentage[];

        setUp() {
            this.myColorHelper = new gvt.ColorFromPercentage();
        }

        tearDown() {
            this.myColorHelper = null;
        }

        testZeroPercentage() {
            var result = this.myColorHelper.getColor(0);
            var color = { r: 0x00, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        }

        testHalfPercentage() {
            var result = this.myColorHelper.getColor(0.5);
            var color = { r: 0xff, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        }

        testFullPercentage() {
            var result = this.myColorHelper.getColor(1);
            var color = { r: 0xff, g: 0x00, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        }

        testNegativePercentage() {
            var result = this.myColorHelper.getColor(-1);
            var color =  { r: 0x00, g: 0xff, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        }

        testMoreThanOnePercentage() {
            var result = this.myColorHelper.getColor(1.5);
            var color =  { r: 0xff, g: 0x00, b: 0 };
            var comparison = angular.equals(result, color);
            this.isTrue(comparison);
        }

        testBelowHalfPercentage() {
            var result = this.myColorHelper.getColor(0.3);
            var check : boolean = result.r <= 0xff && result.g == 0xff && result.b == 0;
            this.isTrue(check);
        }

        testOverHalfPercentage() {
            var result = this.myColorHelper.getColor(0.6);
            var check : boolean = result.r == 0xff && result.g < 0xff && result.b == 0;
            this.isTrue(check);
        }


    }
}